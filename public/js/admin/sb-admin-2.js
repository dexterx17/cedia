/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});

$('.delete-image').on('click', function(e) {
    e.preventDefault();
    var $el = $(this);
     swal({
              title: "Estás seguro?",
              text: "Esta acción NO se puede deshacer!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, eliminar!",
              cancelButtonText: "No, cancelar",
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm){
                if (isConfirm) {

                    $.get($el.attr('href'),function(response){
                        if(!response.error){
                            $el.parents('.col-md-4').fadeOut('slow');
                            new PNotify({
                                  title: 'Accion exitosa',
                                  text: response.msg,
                                  type: 'success',
                                  styling: 'bootstrap3'
                            });
                            $('#n_imagenes').text(parseInt($('#n_imagenes').text())-1);
                        }else{
                            new PNotify({
                                  title: 'Oh No!',
                                  text: response.msj    ,
                                  type: 'error',
                                  styling: 'bootstrap3'
                              });
                        }
                    },'json');
                }
        });
});

//Edit image
$(document).on('click','.edit-image',function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.get(url,function(data){
        $('#container-form-imagenes').html(data);
    });
});