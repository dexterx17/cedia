<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $user->nombre }} {{ $user->apellido }}</strong>,
    <br>
    <!--<p>
        Reciba un cordial saludo, a la vez nos permitimos informar que ha sido seleccionado como participante en la modalidad <b>Presencial</b> en el curso <strong>Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</strong>.
    </p>-->
    <p>
        El curso dará inicio el día Lunes 17 de Septiembre en las instalaciones de la Universidad de las Fuerzas Armadas ESPE, Extensión Latacunga, en el Bloque B, aula B310 en el horario de 08h00 a 14h00.
    </p>
    <p>
        No olvide que para el curso deberá traer:
        <ul>
            <li>Computadora portátil instalado Matlab a partir de la versión 2015</li>
            <li>Verificar que la instalación tenga los siguientes Toolboxes:
                <ul>
                    <li>Fuzzy</li>
                    <li>nnet</li>
                    <li>Stats</li>
                </ul>
            </li>
        </ul>
    </p>
    <hr>
    <p>
        Es importante indicar que el certificado por asistencia se rimitirá siempre y cuando cumplan el <b>80% de asistencia</b>; y a su vez el certificado de aprobación siempre y cuando alcancen el <b>70% de la nota</b>.
    </p>
    
	<hr>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	Teléfonos: +(593)958779578 <small>//</small> +(593)32811228 ext: 4332 <br>
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>