<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>


    Saludos,

    Compartimos la bibliografica compartida por el Dr. Gustavo Meschino

    <hr>
    Nos pedían para leer en la web...

    Yo recomendaría lo siguiente a modo introductorio:
    Las 4 partes del siguiente link:
    <a href="https://towardsdatascience.com/machine-learning-from-scratch-part-1-76603dececa6">https://towardsdatascience.com/machine-learning-from-scratch-part-1-76603dececa6</a>

    Para ver algo dinámico, divertido y en Python:

    Temas generales de Machine Learning:
    <a href="https://www.youtube.com/watch?v=vOppzHpvTiQ&t=13s">https://www.youtube.com/watch?v=vOppzHpvTiQ&t=13s</a>
    Basics of Neural Networks:
    <a href="https://www.youtube.com/watch?v=p69khggr1Jo&t=1s">https://www.youtube.com/watch?v=p69khggr1Jo&t=1s</a>

    Curso de Machine Learning de Google:
    <a href="https://developers.google.com/machine-learning/crash-course/">https://developers.google.com/machine-learning/crash-course/</a>


    -- 
    Gustavo Javier Meschino
    Laboratorio de Bioingeniería - Facultad de Ingeniería
    Instituto de Investigaciones Científicas y Tecnológicas en Electrónica (ICyTE)
    Universidad Nacional de Mar del Plata

    	<hr>
	<hr>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	Teléfonos: +(593)958779578 <small>//</small> +(593)32811228 ext: 4332 <br>
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>