<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $nombre }} {{ $apellido }}</strong>,
	<br>
	<p>Gracias por registrarse en el curso de <strong>Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</strong>, la confirmación de su cupo se realizará hasta el 11 de septiembre por este medio.</p>

    	<hr>
	<hr>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	Teléfonos: +(593)958779578 <small>//</small> +(593)32811228 ext: 4332 <br>
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>