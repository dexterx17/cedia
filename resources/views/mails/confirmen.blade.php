<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $user['nombre'] }} </strong>,
    <br>
    <p>
    Reciba un cordial saludo, por parte de los organizadores del curso de <b>Técnicas de Optimización para Algoritmos de Control Avanzado</b>.
    </p>
    <p>
    El motivo del presente es para confirmar su nombre para la impresión del Certificado de Asistencia, actualmente el nombre que se imprimirá en el certificado es el siguiente:<br>
    <strong> {{$user['nombre']}} </strong>
    </p>
    <p>
    En caso de requerir alguna modificación, como por ejemplo añadir el Titulo Universitario o alguna falta ortográfica, por favor reenviar el nombre que se desea para el certificado por este medio o por el grupo de Whatsapp del curso.
    </p>
    <br>
    <p>
        Atentamente,
    </p>
    
	<br>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	    
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>