<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $user->nombre }} {{ $user->apellido }}</strong>,
    <br>

    <p>Gracias por registrarse en el curso de <strong>Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</strong>, la confirmación de su cupo para la modalidad presencial o videoconferencia se realizará hasta el <b>11 de septiembre</b> por este medio.</p>
    
    <p>
        Además, les <b>invitamos</b> al lanzamiento del curso a través de video conferencia el <b>día 06 de septiembre a las 09h00</b> hora de Ecuador, dictada por el Dr. Gustavo Meschino y Dr. Marco Benalcazár.</p>
    <p>    
        Para lo cuál le compartimos el siguiente link <a href="https://cedia.zoom.us/j/7806728502">https://cedia.zoom.us/j/7806728502</a> con el ID de sala 7806728502 para la video conferencia a través de la plataforma <a href="https://zoom.us/join">Zoom</a>.
    </p>
    <p>
        Por favor si se pueden conectar unos 20 minutos antes para probar la conexión de audio y video.
    </p>

    
	<hr>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	Teléfonos: +(593)958779578 <small>//</small> +(593)32811228 ext: 4332 <br>
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>