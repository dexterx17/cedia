<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $user->nombre }} {{ $user->apellido }}</strong>,
    <br>
    <p>
    Reciba un cordial saludo, a la vez nos permitimos informar que su cupo ha sido reservado para el curso <b>Técnicas de Optimización para Algoritmos de Control Avanzado</b>.
    La confirmacón de su participación se realizará en el lanzamiento del curso a desarrollarse por video conferencia el <b>día Viernes 02 de agosto a las 11h30</b> hora de Ecuador, dictada por el instructor Ph.D. Willian de la Cruz.
    </p>
    <p>
    La videoconferencia se realizará a través del link  <a href="https://cedia.zoom.us/j/946086532?">https://cedia.zoom.us/j/946086532?</a> con el ID de sala 946086532 a través de la plataforma <a href="https://zoom.us/">Zoom</a>.
    </p>
    <p>
    Es importante indicar que <b>el cupo será confirmado únicamente si el participante se conecta a la videoconfencia</b>, para lo cual <b>en el chat de la plataforma zoom deberá dejar un mensaje con su nombre, email de registro y número de teléfono</b>, a fin de crear un grupo de WhatsApp del curso.
    </p>
    <p>
    Recuerde que el curso se realizará en las instalaciones de la Universidad de las Fuerzas Armadas ESPE, Sede Latacunga, del 26 de agosto al 13 de septiembre del presente año, en el horario de 07h30 a 13h00.
    </p>
    <br>
    <p>
        Atentamente,
    </p>
    
	<br>
	Grupo de investigación - <b>ARSI</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	    
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>