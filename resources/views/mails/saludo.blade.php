<!DOCTYPE html>
<html>
<head>
	<title>GIARSI</title>
</head>
<body>

	Saludos <strong>{{ $user->nombres }} {{ $user->apellidos }}</strong>,
	<br>
	<p>El presente mensaje tiene la finalidad de poner en su conocimiento y validar su información publicada en la web <a target="_blank" href="{{ route('user_info',$user->id) }}">{{ route('user_info',$user->id) }}</a>.</p>
	<p>En la siguiente tabla puede ver la información requerida y faltante para completar su perfil</p>
	<table border="1">
		<tbody>
			<tr>
				<th>Imagen:</th>
				<td><img src="{{ asset('img/pics/'.$user->imagen) }}" alt="{{ $user->nombres }} {{ $user->apellidos }}" width="50" height="70" /></td>
			</tr>
			<tr>
				<th>Nombres:</th>
				<td>{{ $user->nombres }}</td>
			</tr>
			<tr>
				<th>Apellidos:</th>
				<td>{{ $user->apellidos }}</td>
			</tr>
			<tr>
				<th>Titulo:</th>
				<td>{{ $user->titulo }}</td>
			</tr>
			<tr>
				<th>Universidad actual:</th>
				<td>
					@if($user->universidades->count()>0)
                    	{{ $user->universidad()->nombre }}
                    @endif
                </td>
			</tr>
			<tr>
				<th>ScholarID:</th>
				<td>{{ $user->scholar_id }}</td>
			</tr>
			<tr>
				<th>Telefono:</th>
				<td>{{ $user->telefono }}</td>
			</tr>
			<tr>
				<th>Skype:</th>
				<td>{{ $user->skype }}</td>
			</tr>
			<tr>
				<th>Email:</th>
				<td>{{ $user->email }}</td>
			</tr>
			<tr>
				<th>Facebook:</th>
				<td>{{ $user->facebook }}</td>
			</tr>
			<tr>
				<th>Twitter:</th>
				<td>{{ $user->twitter }}</td>
			</tr>
			<tr>
				<th>LinkedIn:</th>
				<td>{{ $user->linkedin }}</td>
			</tr>
			<tr>
				<th>About me:</th>
				<td>{{ $user->about_me }}</td>
			</tr>
			<tr>
				<th>{{ trans('front.areas_de_interes') }}:</th>
				<td>
					<ul>
						@foreach($user->intereses as $int )
                        <li>{{ $int->interes }}</li>
                        @endforeach
					</ul>
				</td>
			</tr>
			<tr>
				<th>Curriculo:</th>
				<td><a href="{{ asset('cv/'.$user->cv) }}" title="cv">Descargar cv</a></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<th>
					Publicaciones
				</th>
				<td>{{ $user->publicaciones->count() }} registros</td>
			</tr>
			<tr>
				<th>
					Investigaciones
				</th>
				<td>{{ $user->investigaciones->count() }} registros</td>
			</tr>
			<tr>
				<th>
					Experiencia laboral
				</th>
				<td>{{ $user->trabajos->count() }} registros</td>
			</tr>
			<tr>
				<th>
					Reconocimientos
				</th>
				<td>{{ $user->reconocimientos->count() }} registros</td>
			</tr>
		</tfoot>
	</table>
	<p>Para actualizar sus datos, acceder al siguiente enlace: <a href="{{ route('admin.usuarios.edit',$user->id) }}" title="Editar info">{{ route('admin.usuarios.edit',$user->id) }}</a> </p>
	
	<p>Para sugerencias, correcciones o comentarios por favor responder a este mail.</p>
	<hr>
	<hr>
	Grupo de investigación<br>
	<b>Automatización, Robótica y Sistemas Inteligentes</b>
	<br>
	Contactos: info@giarsi.com <small>//</small> vhandaluz1@espe.edu.ec
	<br>
	Teléfonos: +(593)984547194 <small>//</small> +(593)32811228 ext: 4332 <br>
	<img alt="GIARSI" src="{{ asset('img/logo_full.png') }}" width="400" height="150">
</body>
</html>