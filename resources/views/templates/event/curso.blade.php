<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- SEO -->
    <meta name="author" content="Jaime Santana - ARSI">
    <meta name="description" content="La capacitación consiste en un curso-taller orientadas a machine learning aplicado tecnologías inmersivas, realida virtual y realidad aumentada. El curso-taller tendrá una duración de 90 horas las mismas que serán dictadas en tres semanas por dos doctores con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el cual los instructores guiarán los diferentes trabajos a ser desarrollados por los participantes del curso.">
    <meta name="keywords" content="gather, responsive, event, meetup, template, conference, gather, rsvp, download">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Page Title -->
    <title>TÉCNICAS DE OPTIMIZACIÓN PARA  ALGORITMOS DE CONTROL AVANZADO </title>

    <meta property="og:url"                content="http://www.giarsi.com/curso" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="TÉCNICAS DE OPTIMIZACIÓN PARA  ALGORITMOS DE CONTROL AVANZADO" />
    <meta property="og:description"        content="La capacitación consiste en un curso-taller orientado a la descripción y desarrollo de algoritmos de optimización aplicados a diversas áreas de Ingeniería, por ejemplo, en obtención de modelos matemáticos y diseño de controladores más eficientes, en simulaciones por computadora, en robótica, entre otras. El curso-taller tendrá una duración de 90 horas, que serán dictadas en tres semanas por un doctor con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el instructor guiará los diferentes trabajos a ser desarrollados por los participantes del curso. " />
    <meta property="og:image"              content="{{ asset('img/curso.jpeg')}}" />

    <!-- Bootstrap -->
    <link href="{{ asset('css_event/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Plugins -->
    <link href="{{ asset('css_event/plugins/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/streamline-icons.css')}}" rel="stylesheet">

    <!-- Event Style -->
    <link href="{{ asset('css_event/event.css')}}" rel="stylesheet">

    <!-- Color Theme -->
    <!-- Options: green, purple, red, yellow, mint, blue, black  -->
    <link href="{{ asset('css_event/themes/mint.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="js/ie/respond.min.js"></script>
  <![endif]-->

    <!-- Modernizr -->
    <script src="{{ asset('js_event/modernizr.min.js')}}"></script>
    <!-- Subtle Loading bar -->
    <script src="{{ asset('js_event/plugins/pace.js')}}"></script>
</head>

<body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">
    <!--Preloader div-->
    <div class="preloader"></div>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top reveal-menu js-reveal-menu reveal-menu-hidden">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{ asset('img/logos/logo_negro.png') }}" alt="Gather"> </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#top">Inicio</a></li>
                    <li><a href="#speakers">Conferencistas</a></li>
                    <li><a href="#schedule">Temáticas</a></li>
                    <li><a href="#gallery">Galeria</a></li>
                    <li><a href="#sponsors" class="hidden-sm">Sponsors</a></li>
                    <li><a href="#venue" class="hidden-sm">Dirección</a></li>
                    <li><a href="#contact">Contacto</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!-- // End Fixed navbar -->

    <header id="top" class="header">
        <div class="container">

            <div class="header_top-bg">
                <div class="logo">
                    <a href="{{ route('home') }}"><img src="{{ asset('img/logos/logonegro.png') }}" alt="event-logo" width="300" height="80"></a>
                </div>
            </div>

            <h1 class="headline-support wow fadeInDown">TÉCNICAS DE OPTIMIZACIÓN <small>para </small></h1>
            <h3 class="headline wow fadeInDown" data-wow-delay="0.1s"> ALGORITMOS DE CONTROL AVANZADO  </h3>

            <div class="when_where wow fadeIn" data-wow-delay="0.4s">
                <p class="event_when">del 26 de agosto al 13 de septiembre de 2019</p>
                <p>desde las 7:30 am hasta la 1:00 pm</p>
            </div>

            <div class="header_bottom-bg">
                <a class="btn btn-primary btn-xl wow zoomIn" data-wow-delay="0.3s" href="#mas-info" >Mas información</a>
                <p class="cta_urgency wow fadeIn" data-wow-delay="0.5s"><small>{{ $cupos }} cupos disponibles... <br>Los cupos se confirmarán el 13 de septiembre</small></p>
            </div>

        </div>
        <!-- end .container -->
    </header>
    <!-- end .header -->

    <!-- 
     Highlight Section
     ====================================== -->

    <section class="highlight" id="mas-info">

        <div class="container">
            <p class="lead text-center">La capacitación consiste en un curso-taller orientado a la descripción y desarrollo de algoritmos de optimización aplicados a diversas áreas de Ingeniería, por ejemplo, en obtención de modelos matemáticos y diseño de controladores más eficientes, en simulaciones por computadora, en robótica, entre otras. El curso-taller tendrá una duración de 90 horas, que serán dictadas en tres semanas por un doctor con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el instructor guiará los diferentes trabajos a ser desarrollados por los participantes del curso. </p>

            <div class="countdown_wrap">

                <h6 class="countdown_title text-center">EL EVENTO EMPEZARÁ EN</h6>

                <!-- Countdown JS for the Event Date Starts here.
    TIP: You can change your event time below in the Same Format.  -->

                <ul id="countdown" data-event-date="26 august 2019 09:00:00">
                    <li class="wow zoomIn" data-wow-delay="0s"> <span class="days">17</span>
                        <p class="timeRefDays">days</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.2s"> <span class="hours">00</span>
                        <p class="timeRefHours">hours</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.4s"> <span class="minutes">00</span>
                        <p class="timeRefMinutes">minutes</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.6s"> <span class="seconds">00</span>
                        <p class="timeRefSeconds">seconds</p>
                    </li>
                </ul>
            </div>
            <!-- end .countdown_wrap -->

            <div class="text-center">

                <!-- Add to Calendar Plugin. 
                     For Customization, Visit https://addtocalendar.com/ -->

                <span class="addtocalendar atc-style-theme">
                <a class="atcb-link"><i class="fa fa-calendar"> </i> Agregar a mi Calendario</a>
                  <var class="atc_event">
                      <var class="atc_date_start">2018-09-17 09:00:00</var>
                      <var class="atc_date_end">2018-10-05 18:00:00</var>
                      <var class="atc_timezone">America/Guayaquil</var>
                      <var class="atc_title">TÉCNICAS DE OPTIMIZACIÓN PARA  ALGORITMOS DE CONTROL AVANZADO </var>
                      <var class="atc_description">La capacitación consiste en un curso-taller orientado a la descripción y desarrollo de algoritmos de optimización aplicados a diversas áreas de Ingeniería, por ejemplo, en obtención de modelos matemáticos y diseño de controladores más eficientes, en simulaciones por computadora, en robótica, entre otras. El curso-taller tendrá una duración de 90 horas, que serán dictadas en tres semanas por un doctor con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el instructor guiará los diferentes trabajos a ser desarrollados por los participantes del curso. </var>
                      <var class="atc_location">Latacunga, Cotopaxi</var>
                      <var class="atc_organizer">ESPE - arsi</var>
                      <var class="atc_organizer_email">vhandaluz1@espe.edu.ec</var>
                  </var>
               </span>

            </div>

        </div>
        <!-- end .container -->

    </section>
    <!-- end section.highlight -->

    <!-- 
     Our Speakers
     ====================================== -->

    <section class="speakers" id="speakers">
        <div class="container">

            <div class="section-title wow fadeInUp">
                <h4>NUESTROS EXPOSITOR</h4>
            </div>

            <div class="speaker-slider">

                <div class="speaker-info wow fadeIn" data-wow-delay="0s">
                    <img src="{{ asset('img/dr.cruz.jpg') }}" alt="avatar" class="img-responsive center-block"  width="120" height="150">
                    <p>Dr. William La Cruz </p>
                    <span>Ph.D. en Ciencias Mención Ciencias de la Computación <br> M.Sc. en Investigación de Operaciones </span>
                </div>
                <!-- end .speaker-info -->

            </div>

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.speakers -->

    <!-- 
     Our Schedule
     ====================================== -->

    <section class="schedule" id="schedule">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>CRONOGRAMA</h4>
            </div>

            <div class="nav-center">
                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist" id="schedule-tabs">
                    <li role="presentation" class="active"><a href="#day1" aria-controls="day1" role="tab" data-toggle="tab">Temáticas</a></li>
                    <!--<li role="presentation"><a href="#day2" aria-controls="day2" role="tab" data-toggle="tab">Day Two</a></li>
                     <li role="presentation"><a href="#day3" aria-controls="messages" role="tab" data-toggle="tab">Day Three</a></li> -->
                </ul>

            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="day1">

                    <!-- ########## Schedule Timeline DAY 01 Starts ########### -->

                    <section class="timeline">
                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Introducción al curso y preliminares Matemáticos</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Introducción a Python</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Introducción a la Optimización. Definición y descripción de los problemas de optimización a resolver. .</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Optimización sin Restricciones. Métodos de Búsqueda Unidimensional. </h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Métodos Multidimensionales. Métodos tipo gradiente</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Métodos de Direcciones Conjugadas.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Métodos Cuasi-Newton. </h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Resolución numérica de problemas de optimización sin restricciones</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Resolviendo Ecuaciones Lineales y Sistemas de Ecuaciones No Lineales.</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Algoritmos de Búsqueda Global</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Resolución numérica de sistemas de ecuaciones lineales y no lineales; y problemas de optimización sin restricciones usando algoritmos de búsqueda global.</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Optimización con restricciones. Problemas con restricciones de Igualdad. Problemas con restricciones de desigualdad</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Problemas de Optimización Convexa</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Algoritmos para Optimización con Restricciones </h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Resolución numérica problemas de optimización con restricciones.</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Resumen de los métodos vistos con casos de aplicación.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->
                    </section>
                    <!-- timeline -->

                    <!-- ########### End Schedule Timeline  DAY 01  ########### -->

                </div>

            </div>

        </div>
        <!-- end .container -->

    </section>
    <!-- end section.schedule -->

    <!-- 
     What to Expect : Benefits
     ====================================== -->

    <!-- TODO: Change Icons to Fonts for better theming. -->

    <section class="benefits">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>Resultados de la Capacitación</h4>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="benefit-item wow fadeInLeft">
                                <div class="benefit-icon"><i class="icon icon-bubble-love-streamline-talk"> </i></div>
                                <p>Conocerá los fundamentos de la Optimización Numérica y sus distintos métodos a fin de ser implementados en algunas aplicaciones en áreas específicas de Ingeniería.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="benefit-item wow fadeInRight">
                                <div class="benefit-icon"><i class="icon icon-map-pin-streamline"> </i></div>
                                <p>Logrará la habilidad de aprender nuevos algoritmos y enfoques de esta disciplina.</p>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="benefit-item wow fadeInLeft">
                                <div class="benefit-icon"> <i class="icon icon-cocktail-mojito-streamline"> </i></div>
                                <p>Podrá formular y plantear un problema de optimización para luego resolverlo con algunos de los métodos vistos en aplicaciones en solución de ecuaciones diferenciales y en diferentes estrategias de sistemas de control inteligente,</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="benefit-item wow fadeInRight">
                                <div class="benefit-icon"> <i class="icon icon-armchair-chair-streamline"> </i></div>
                                <p>Podrá analizar y evaluar el uso de los distintos métodos con el propósito de escoger el más adecuado para la resolución de un problema en particular.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.benefits -->

   <section class="gallery" id="inscribete">
        <div class="container">
            <div class="section-title">
                <a class="btn btn-primary btn-xl wow zoomIn" data-wow-delay="0.3s" href="#" data-toggle="modal" data-target="#register-now">RESERVAR MI CUPO</a>
            </div>
        </div>
    </section>
    <!-- 
     Gallery
     ====================================== -->

    <section class="gallery" id="gallery">
        <div class="container">
            <div class="section-title">
                <h4>GALERÍA</h4>
            </div>

            <div class="nav-center bottom-space-lg">
                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#photo-gallery" aria-controls="photo-gallery" role="tab" data-toggle="tab">Fotos</a></li>
                    <!--<li role="presentation"><a href="#video-gallery" aria-controls="video-gallery" role="tab" data-toggle="tab">Videos</a></li>
                     <li role="presentation"><a href="#day3" aria-controls="messages" role="tab" data-toggle="tab">Day Three</a></li> -->
                </ul>

            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="photo-gallery">

                    <div class="popup-gallery">

                        <div class="row">

                            <div class="col-md-6">
                                <a href="{{ asset('img/foto2.jpg') }}" title=""><img src="{{ asset('img/foto2.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn"></a>
                            </div>

                            <div class="col-md-6">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto1.jpg') }}" title=""><img src="{{ asset('img/foto1.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.2s"></a>
                                    </div>

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto5.jpg') }}" title=""><img src="{{ asset('img/foto5.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.2s"></a>
                                    </div>

                                </div>
                                <!-- end .row -->

                                <div class="row">

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto3.jpg') }}" title=""><img src="{{ asset('img/foto3.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.4s"></a>
                                    </div>

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto4.jpg') }}" title=""><img src="{{ asset('img/foto4.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.4s"></a>
                                    </div>

                                </div>
                                <!-- end .row -->

                            </div>

                        </div>
                        <!-- end .row -->

                    </div>
                    <!-- end .popup-gallery -->
                </div>
                <!-- end .tabpanel -->
        </div>
        <!-- end .tab-content -->

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.gallery -->

    <section class="faq">

        <div class="container">

            <div class="section-title">
                <h5>Preguntas Frecuentes</h5>
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInRight" data-wow-duration="2s">

                    <h6 class="faq-title">Cuáles son los conocimientos necesarios? (Pre-requisitos)</h6>
                    <p>
                    Dominio de un nivel medio de Álgebra Lineal y Cálculo. Específicamente, se espera que los alumnos tengan los siguientes conceptos:  
                        <ul>
                            <li>Álgebra Lineal: espacios vectoriales, subespacios vectoriales, matrices, normas vectoriales y matriciales, autovalores y autovectores, factorizaciones matriciales</li>
                            <li>Cálculo en una y varias variables: continuidad, diferenciación, vector gradiente, Jacobiano, Hessiana. </li>
                        </ul>
                    Bases de conceptos de programación y cierta experiencia programando en Pyhon o Matlab
                    </p>
                </div>
                <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s">
                    <h6 class="faq-title">Recursos o Medios para el Aprendizaje</h6>
                    <p>
                        <ul>
                            <li>Laboratorio de computo, cuyos equipos tengan instalado el software Python y las librerías Numpy, Scipy y Matplotlib, así como también Matlab </li>
                            <li>Proyector digital</li>
                            <li>Servicio de internet con buena velocidad de conectividad</li>
                        </ul>
                    <h6 class="faq-title">Cuáles son los Criterios de Evaluación?</h6>
                    <p>
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>Actividad</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tareas fuera de clase</td>
                                    <td>30 %</td>
                                </tr>
                                <tr>
                                    <td>Talleres o ejercicios</td>
                                    <td>60 %</td>
                                </tr>
                                <tr>
                                    <td>Exposiciones</td>
                                    <td>10 %</td>
                                </tr>
                                <!--<tr>
                                    <td>Pruebas</td>
                                    <td>0 %</td>
                                </tr>
                                <tr>
                                    <td>Examenes</td>
                                    <td>0 %</td>
                                </tr>-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th>100 %</th>
                                </tr>
                            </tfoot>
                        </table>
                        <small>Para aprobar el curso el participante deberá por lo menos alcanzar el 70% de la nota.</small>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end section.faq -->

   <section class="gallery" id="inscribete">
        <div class="container">
            <div class="section-title">
                <a class="btn btn-primary btn-xl wow zoomIn" data-wow-delay="0.3s" href="#" data-toggle="modal" data-target="#register-now">RESERVAR MI CUPO</a>
            </div>
        </div>
    </section>
    <!-- 
     Our Sponsors
     ====================================== -->

    <section class="sponsors" id="sponsors">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>NUESTROS SPONSORS
                    
                </h4>
                <img src="{{ asset('img/cecira.png') }}" alt="CECIRA" class="cecira">
                <!--<p>Event is supported by easily recognisable companies and products which we use everyday.</p>-->
            </div>
            <div class="sponsor-slider wow bounceIn">
                <div><img src="{{ asset('img/cedia.jpg') }}" class="img-responsive center-block" alt="Red Nacional de Investigación y Educación del Ecuador "> </div>
                <div><img src="{{ asset('img/u/espe.png') }}" class="img-responsive center-block" alt="Universidad de las Fuerzas Armadas"> </div>
                <div><img src="{{ asset('img/u/uti.png') }}" class="img-responsive center-block" alt="Universidad Tecnológica Indoamérica"> </div>
                <div><img src="{{ asset('img/u/espoch.png') }}" class="img-responsive center-block" alt="Escuela Superior Politécnica de Chimborazo"> </div>
                <div><img src="{{ asset('img/u/unach.png') }}" class="img-responsive center-block" alt="Universidad Nacional de Chimborazo"> </div>
            </div>
        </div>
        <!-- end .container -->
    </section>
    <!-- end section.sponsors -->

    <!-- 
     Location Map
     ====================================== -->

    <div class="g-maps" id="venue">

        <!-- Tip:  You can change location, zoom, color theme, height, image and Info text by changing data-* attribute below. -->
        <!-- Available Colors:    red, orange, yellow, green, mint, aqua, blue, purple, pink, white, grey, black, invert -->

        <div class="map" id="map_canvas" data-maplat="-0.935049" data-maplon="-78.611" data-mapzoom="15" data-color="green" data-height="400" data-img="images/marker.png" data-info="ESPE Latacunga"></div>

    </div>
    <!-- end div.g-maps -->
    <!-- 
     Contact us
     ====================================== -->

    <div class="highlight">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="contact-box">
                        <img src="images/location-icon.png" alt="location icon" class="wow zoomIn">
                        <h5>DIRECCIÓN</h5>
                        <p>Calle Quijano y Ordoñez y Hermanas Páez,
                            <br>
                            <br> Cotopaxi - Latacunga
                            <br>Ecuador
                            <br>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact-box">
                        <img src="images/email-icon.png" alt="email icon" class="wow zoomIn" data-wow-delay="0.3s">
                        <h5>CONTACTO</h5>
                        <p><a href="mailto:vhandaluz@espe.edu.ec">vhandaluz@espe.edu.ec</a>
                            <br> 
                            
                            <br> +593 3 2810206 / 2811240
                            <br> +593 3 2810208
                        </p>
                    </div>
                </div>
            </div>
            <!--  // end .row -->
        </div>
    </div>
    <!-- //  end .highlight -->

    <div class="container" id="contact">
        <div class="section-title">
            <h5>ENVIANOS UN MENSAJE</h5>
            <p>Si tienes alguna pregunta del evento, por favor contáctenos directamente.</p>
        </div>

        <div class="contact-form bottom-space-xl wow fadeInUp">

            <form action="php/contact.php" id="phpcontactform" method="POST">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3">


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="Nombre(s)" required>

                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Apellido(s)" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>

                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" placeholder="Teléfono" required>
                        </div>

                        <div class="form-group">
                            <label>Su Mensaje</label>
                            <textarea class="form-control" name="message" rows="6" placeholder="Ingrese su mensaje aquí" required> </textarea>
                        </div>

                        <div class="text-center top-space">
                            <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Enviar mensaje</button>
                            <div id="js-contact-result" data-success-msg="Formulario enviado correctamente." data-error-msg="Oops. Algo salio mal."></div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- // end .container -->
    <footer>

        <div class="social-icons">
            <a href="#" class="wow zoomIn"> <i class="fa fa-twitter"></i> </a>
            <a href="#" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fa fa-facebook"></i> </a>
            <a href="#" class="wow zoomIn" data-wow-delay="0.4s"> <i class="fa fa-linkedin"></i> </a>
        </div>
        <p> <small class="text-muted">Copyright © {{ date('y') }}. All rights reserved.</small></p>

    </footer>

    <a href="#top" class="back_to_top"><img src="images/back_to_top.png" alt="back to top"></a>

    <!-- 
     Registration Popup (PAYPAL)
     ====================================== -->

    <div class="modal fade" id="register-now" tabindex="-1" role="dialog" aria-labelledby="register-now-label">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="register-now-label">Registro del evento</h4>
                </div>
                <div class="modal-body">

                    <div class="registration-form">
                        <form action="{{ route('evento.registro') }}" method="POST" target="_top" id="paypal-regn">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nombre(s)</label>
                                        <input type="text" class="form-control" name="nombre" placeholder="Nombre(s)" required>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Apellido(s)</label>
                                        <input type="text" class="form-control" name="apellido" placeholder="Apellido(s)" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Cédula de identidad</label>
                                        <input type="text" class="form-control" name="ci" placeholder="Cédula de identidad" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Institución</label>
                                        <input type="text" class="form-control" name="institucion" placeholder="Institución a la que pertenece" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Cargo</label>
                                        <input type="text" class="form-control" name="cargo" placeholder="Cargo" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email ID</label>
                                <input type="email" class="form-control" name="email" placeholder="Correo electrónico" required>
                            </div>


                            <div class="text-center top-space">
                                <button type="submit" id="reserve-btn" class="btn btn-success btn-lg">Reservar mi Cupo</button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- 
     Javascripts : Nerd Stuff :)
     ====================================== -->

    <!-- jQuery Library -->
    <script src="{{ asset('js_event/jquery.min.js') }}"></script>

    <!-- Bootstrap JS -->
    <script src="{{ asset('js_event/bootstrap.min.js') }}"></script>

    <!-- 3rd party Plugins -->
    <script src="{{ asset('js_event/plugins/countdown.js') }}"></script>
    <script src="{{ asset('js_event/plugins/wow.js') }}"></script>
    <script src="{{ asset('js_event/plugins/slick.js') }}"></script>
    <script src="{{ asset('js_event/plugins/magnific-popup.js') }}"></script>
    <script src="{{ asset('js_event/plugins/validate.js') }}"></script>
    <script src="{{ asset('js_event/plugins/appear.js') }}"></script>
    <script src="{{ asset('js_event/plugins/count-to.js') }}"></script>
    <script src="{{ asset('js_event/plugins/nicescroll.js') }}"></script>

    <!-- Google Map -->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCOX9okNdsDlE4s1YpSyfXCaENTZH0DqY0&sensor=false"></script>
    <script src="{{ asset('js_event/plugins/infobox.js') }}"></script>
    <script src="{{ asset('js_event/plugins/google-map.js') }}"></script>
    <script src="{{ asset('js_event/plugins/directions.js') }}"></script>

    <!-- JS Includes -->
    <script src="{{ asset('js_event/includes/subscribe.js') }}"></script>
    <script src="{{ asset('js_event/includes/contact_form.js') }}"></script>

    <!-- Main Script -->
    <script src="{{ asset('js_event/main.js') }}"></script>

</body>

</html>
