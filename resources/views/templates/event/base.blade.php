<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- SEO -->
    <meta name="author" content="Jaime Santana - ARSI">
    <meta name="description" content="La capacitación consiste en un curso-taller orientadas a machine learning aplicado tecnologías inmersivas, realida virtual y realidad aumentada. El curso-taller tendrá una duración de 90 horas las mismas que serán dictadas en tres semanas por dos doctores con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el cual los instructores guiarán los diferentes trabajos a ser desarrollados por los participantes del curso.">
    <meta name="keywords" content="gather, responsive, event, meetup, template, conference, gather, rsvp, download">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Page Title -->
    <title>Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css_event/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Plugins -->
    <link href="{{ asset('css_event/plugins/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/streamline-icons.css')}}" rel="stylesheet">

    <meta property="og:url"                content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="TÉCNICAS DE OPTIMIZACIÓN PARA  ALGORITMOS DE CONTROL AVANZADO" />
    <meta property="og:description"        content="How much does culture influence creative thinking?" />
    <meta property="og:image"              content="{{ asset('img/curso.jpeg')}}" />

    <!-- Event Style -->
    <link href="{{ asset('css_event/event.css')}}" rel="stylesheet">

    <!-- Color Theme -->
    <!-- Options: green, purple, red, yellow, mint, blue, black  -->
    <link href="{{ asset('css_event/themes/mint.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="js/ie/respond.min.js"></script>
  <![endif]-->

    <!-- Modernizr -->
    <script src="{{ asset('js_event/modernizr.min.js')}}"></script>
    <!-- Subtle Loading bar -->
    <script src="{{ asset('js_event/plugins/pace.js')}}"></script>
</head>

<body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">
    <!--Preloader div-->
    <div class="preloader"></div>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top reveal-menu js-reveal-menu reveal-menu-hidden">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{ asset('img/logos/logo_negro.png') }}" alt="Gather"> </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#top">Inicio</a></li>
                    <li><a href="#speakers">Conferencistas</a></li>
                    <li><a href="#schedule">Temáticas</a></li>
                    <li><a href="#gallery">Galeria</a></li>
                    <li><a href="#sponsors" class="hidden-sm">Sponsors</a></li>
                    <li><a href="#venue" class="hidden-sm">Dirección</a></li>
                    <li><a href="#contact">Contacto</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <!-- // End Fixed navbar -->

    <header id="top" class="header">
        <div class="container">

            <div class="header_top-bg">
                <div class="logo">
                    <a href="{{ route('home') }}"><img src="{{ asset('img/logos/logonegro.png') }}" alt="event-logo" width="300" height="80"></a>
                </div>
            </div>

            <h1 class="headline-support wow fadeInDown">Machine Learning <small>aplicado a</small></h1>
            <h3 class="headline wow fadeInDown" data-wow-delay="0.1s"> Tecnologías Inmersivas para la Academia e Industria </h3>

            <div class="when_where wow fadeIn" data-wow-delay="0.4s">
                <p class="event_when">del 17 de septiembre al 05 de octubre del 2018</p>
                
            </div>

            <div class="header_bottom-bg">
                <a class="btn btn-primary btn-xl wow zoomIn" data-wow-delay="0.3s" href="#">CUPOS AGOTADOS</a>
                <p class="cta_urgency wow fadeIn" data-wow-delay="0.5s"><small>Gracias por tu interes... <br>Los cupos se confirmarán el 13 de septiembre</small></p>
            </div>

        </div>
        <!-- end .container -->
    </header>
    <!-- end .header -->

    <!-- 
     Highlight Section
     ====================================== -->

    <section class="highlight">

        <div class="container">
            <p class="lead text-center">La capacitación consiste en un curso-taller orientadas a machine learning aplicado tecnologías inmersivas, realidad virtual y realidad aumentada. El curso-taller tendrá una duración de 90 horas las mismas que serán dictadas en tres semanas por dos doctores con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el cual los instructores guiarán los diferentes trabajos a ser desarrollados por los participantes del curso.</p>

            <div class="countdown_wrap">

                <h6 class="countdown_title text-center">EL EVENTO EMPEZARÁ EN</h6>

                <!-- Countdown JS for the Event Date Starts here.
    TIP: You can change your event time below in the Same Format.  -->

                <ul id="countdown" data-event-date="17 september 2018 09:00:00">
                    <li class="wow zoomIn" data-wow-delay="0s"> <span class="days">17</span>
                        <p class="timeRefDays">days</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.2s"> <span class="hours">00</span>
                        <p class="timeRefHours">hours</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.4s"> <span class="minutes">00</span>
                        <p class="timeRefMinutes">minutes</p>
                    </li>
                    <li class="wow zoomIn" data-wow-delay="0.6s"> <span class="seconds">00</span>
                        <p class="timeRefSeconds">seconds</p>
                    </li>
                </ul>
            </div>
            <!-- end .countdown_wrap -->

            <div class="text-center">

                <!-- Add to Calendar Plugin. 
                     For Customization, Visit https://addtocalendar.com/ -->

                <span class="addtocalendar atc-style-theme">
                <a class="atcb-link"><i class="fa fa-calendar"> </i> Agregar a mi Calendario</a>
                  <var class="atc_event">
                      <var class="atc_date_start">2018-09-17 09:00:00</var>
                      <var class="atc_date_end">2018-10-05 18:00:00</var>
                      <var class="atc_timezone">America/Guayaquil</var>
                      <var class="atc_title">Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</var>
                      <var class="atc_description">La capacitación consiste en un curso-taller orientadas a machine learning aplicado tecnologías inmersivas, realida virtual y realidad aumentada. El curso-taller tendrá una duración de 90 horas las mismas que serán dictadas en tres semanas por dos doctores con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el cual los instructores guiarán los diferentes trabajos a ser desarrollados por los participantes del curso</var>
                      <var class="atc_location">Latacunga, Cotopaxi</var>
                      <var class="atc_organizer">ESPE - arsi</var>
                      <var class="atc_organizer_email">vhandaluz1@espe.edu.ec</var>
                  </var>
               </span>

            </div>

        </div>
        <!-- end .container -->

    </section>
    <!-- end section.highlight -->

    <!-- 
     Our Speakers
     ====================================== -->

    <section class="speakers" id="speakers">
        <div class="container">

            <div class="section-title wow fadeInUp">
                <h4>NUESTROS EXPOSITORES</h4>
            </div>

            <div class="speaker-slider">

                <div class="speaker-info wow fadeIn" data-wow-delay="0s">
                    <img src="{{ asset('img/Dr.Meschino.jpg') }}" alt="avatar" class="img-responsive center-block"  width="120" height="150">
                    <p>Dr. Gustavo Meschino</p>
                    <span>Ph.D. Electronics Engineering <br><small>Argentina</small> </span>
                </div>
                <!-- end .speaker-info -->

                <div class="speaker-info wow fadeIn" data-wow-delay="0.1s">
                    <img src="{{ asset('img/Dr.Benalcazar.png') }}" alt="avatar" class="img-responsive center-block" width="120" height="150">
                    <p>Dr. Marco Benalcázar</p>
                    <span>Director de Investigación EPN <br><small>Ecuador</small> </span>
                </div>
                <!-- end .speaker-info -->

            </div>

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.speakers -->

    <!-- 
     Our Schedule
     ====================================== -->

    <section class="schedule" id="schedule">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>CRONOGRAMA</h4>
            </div>

            <div class="nav-center">
                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist" id="schedule-tabs">
                    <li role="presentation" class="active"><a href="#day1" aria-controls="day1" role="tab" data-toggle="tab">Temáticas</a></li>
                    <!--<li role="presentation"><a href="#day2" aria-controls="day2" role="tab" data-toggle="tab">Day Two</a></li>
                     <li role="presentation"><a href="#day3" aria-controls="messages" role="tab" data-toggle="tab">Day Three</a></li> -->
                </ul>

            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="day1">

                    <!-- ########## Schedule Timeline DAY 01 Starts ########### -->

                    <section class="timeline">
                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Introducción a Tecnologías inmersivas (realidad virtual y realidad aumentada)</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Introducción a la Inteligencia Computacional y Machine Learning. Tipos de modelos propuestos por el Machine Learning.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Clustering, conceptos y algoritmos de ejemplo.</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Introducción a los Sistemas Difusos. Generalización de la lógica proposicional.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Evaluación de proposiciones lógicas difusas con datos.</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Creación de un sistema de inferencia basado en reglas.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Sistema de inferencia difusa de Sugeno. Validación de modelos.</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Métodos de optimización: Algoritmos genéticos.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Introducción a las redes neuronales. El Perceptrón simple.</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Redes neuronales multicapa. Entrenamiento.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s"> Entrenamiento de una red neuronal para clasificación.</h2>
                                
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Arquitecturas. Métodos de validación y estimación del error.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Redes de base radial: de regresión generalizada y probabilísticas.</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Redes no supervisadas. Mapas autoorganizados (SOM).</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->

                        <div class="timeline-block">
                            <div class="timeline-bullet wow zoomIn" data-wow-delay="0s">

                            </div>
                            <!-- timeline-bullet -->

                            <div class="timeline-content">
                                <h2 class="wow flipInX" data-wow-delay="0.3s">Análisis de un conjunto de datos por medio de SOM.</h2>
                                <h2 class="date wow flipInX" data-wow-delay="0.3s">Resumen de los métodos vistos con casos de aplicación.</h2>
                            </div>
                            <!-- timeline-content -->
                        </div>
                        <!-- timeline-block -->
                    </section>
                    <!-- timeline -->

                    <!-- ########### End Schedule Timeline  DAY 01  ########### -->

                </div>

            </div>

        </div>
        <!-- end .container -->

    </section>
    <!-- end section.schedule -->

    <!-- 
     What to Expect : Benefits
     ====================================== -->

    <!-- TODO: Change Icons to Fonts for better theming. -->

    <section class="benefits">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>Resultados de la Capacitación</h4>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="benefit-item wow fadeInLeft">
                                <div class="benefit-icon"><i class="icon icon-bubble-love-streamline-talk"> </i></div>
                                <p>Conocerá los fundamentos del Machine Learning y algunos de sus métodos a fin de ser implementados en aplicaciones que consideren tecnologías inmersivas.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="benefit-item wow fadeInRight">
                                <div class="benefit-icon"><i class="icon icon-map-pin-streamline"> </i></div>
                                <p>Logrará la habilidad de aprender nuevos algoritmos y enfoques de este paradigma.</p>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="benefit-item wow fadeInLeft">
                                <div class="benefit-icon"> <i class="icon icon-cocktail-mojito-streamline"> </i></div>
                                <p>Podrá plantear un problema para optimizar con Algoritmos Genéticos.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="benefit-item wow fadeInRight">
                                <div class="benefit-icon"> <i class="icon icon-armchair-chair-streamline"> </i></div>
                                <p>Tendrá la habilidad de utilizar Redes Neuronales Artificiales para clasificación y regresión.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="benefit-item wow fadeInRight">
                                <div class="benefit-icon"> <i class="icon icon-armchair-chair-streamline"> </i></div>
                                <p>Podrá analizar y evaluar el uso de los distintos enfoques de algoritmos de aprendizaje a partir de datos.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.benefits -->

    <!-- 
     Gallery
     ====================================== -->

    <section class="gallery" id="gallery">
        <div class="container">
            <div class="section-title">
                <h4>GALERÍA</h4>
            </div>

            <div class="nav-center bottom-space-lg">
                <!-- Nav tabs -->
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a href="#photo-gallery" aria-controls="photo-gallery" role="tab" data-toggle="tab">Fotos</a></li>
                    <!--<li role="presentation"><a href="#video-gallery" aria-controls="video-gallery" role="tab" data-toggle="tab">Videos</a></li>
                     <li role="presentation"><a href="#day3" aria-controls="messages" role="tab" data-toggle="tab">Day Three</a></li> -->
                </ul>

            </div>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="photo-gallery">

                    <div class="popup-gallery">

                        <div class="row">

                            <div class="col-md-6">
                                <a href="{{ asset('img/foto2.jpg') }}" title=""><img src="{{ asset('img/foto2.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn"></a>
                            </div>

                            <div class="col-md-6">
                                <div class="row">

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto1.jpg') }}" title=""><img src="{{ asset('img/foto1.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.2s"></a>
                                    </div>

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto5.jpg') }}" title=""><img src="{{ asset('img/foto5.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.2s"></a>
                                    </div>

                                </div>
                                <!-- end .row -->

                                <div class="row">

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto3.jpg') }}" title=""><img src="{{ asset('img/foto3.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.4s"></a>
                                    </div>

                                    <div class="col-sm-6">
                                        <a href="{{ asset('img/foto4.jpg') }}" title=""><img src="{{ asset('img/foto4.jpg') }}" alt="gallery image" class="img-responsive wow fadeIn" data-wow-delay="0.4s"></a>
                                    </div>

                                </div>
                                <!-- end .row -->

                            </div>

                        </div>
                        <!-- end .row -->

                    </div>
                    <!-- end .popup-gallery -->
                </div>
                <!-- end .tabpanel -->
        </div>
        <!-- end .tab-content -->

        </div>
        <!-- end .container -->
    </section>
    <!-- end section.gallery -->

    <section class="faq">

        <div class="container">

            <div class="section-title">
                <h5>Preguntas Frecuentes</h5>
            </div>
            <div class="row">
                <div class="col-md-6 wow fadeInRight" data-wow-duration="2s">

                    <h6 class="faq-title">Cuáles son los conocimientos necesarios? (Pre-requisitos)</h6>
                    <p>
                        Dominio de un nivel introductorio de álgebra, estadística y cálculo. Más específicamente, se espera que los alumnos tengan los siguientes conceptos:
                        <ul>
                            <li>Álgebra: Variables, coeficientes y funciones, Ecuaciones lineales, Logaritmos y ecuaciones logarítmicas. Función sigmoide. Multiplicación de matrices, Trigonometría. Gráficos de funciones en 2 y 3 dimensiones. Histogramas</li>
                            <li>Estadística: Media, mediana, valores atípicos y desviación estándar, capacidad para leer un histograma.</li>
                            <li>Cálculo: concepto de derivada, gradiente o pendiente, derivadas parciales, regla de la cadena.</li>
                            <li>Bases de conceptos de programación y cierta experiencia programando en MatLab y Unity 3D.</li>
                        </ul>
                    </p>
                </div>
                <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s">
                    <h6 class="faq-title">Recursos o Medios para el Aprendizaje</h6>
                    <p>
                        <ul>
                            <li>Laboratorio de cómputo instalados los softwares de MatLab y Unity3D</li>
                            <li>Proyector digital</li>
                            <li>Laboratorio de investigación que dispongan de dispositivos de realidad virtual y realidad aumentada</li>
                            <li>Servicio de internet con buena velocidad de conectividad</li>
                        </ul>
                    <h6 class="faq-title">Cuáles son los Criterios de Evaluación?</h6>
                    <p>
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>Actividad</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tareas fuera de clase</td>
                                    <td>30 %</td>
                                </tr>
                                <tr>
                                    <td>Talleres o ejercicios</td>
                                    <td>60 %</td>
                                </tr>
                                <tr>
                                    <td>Exposiciones</td>
                                    <td>10 %</td>
                                </tr>
                                <!--<tr>
                                    <td>Pruebas</td>
                                    <td>0 %</td>
                                </tr>
                                <tr>
                                    <td>Examenes</td>
                                    <td>0 %</td>
                                </tr>-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Total</th>
                                    <th>100 %</th>
                                </tr>
                            </tfoot>
                        </table>
                        <small>Para aprobar el curso el participante deberá por lo menos alcanzar el 70% de la nota.</small>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- end section.faq -->

    <!-- 
     Our Sponsors
     ====================================== -->

    <section class="sponsors" id="sponsors">

        <div class="container">
            <div class="section-title wow fadeInUp">
                <h4>NUESTROS SPONSORS
                    
                </h4>
                <img src="{{ asset('img/cecira.png') }}" alt="CECIRA" class="cecira">
                <!--<p>Event is supported by easily recognisable companies and products which we use everyday.</p>-->
            </div>
            <div class="sponsor-slider wow bounceIn">
                <div><img src="{{ asset('img/cedia.jpg') }}" class="img-responsive center-block" alt="Red Nacional de Investigación y Educación del Ecuador "> </div>
                <div><img src="{{ asset('img/u/espe.png') }}" class="img-responsive center-block" alt="Universidad de las Fuerzas Armadas"> </div>
                <div><img src="{{ asset('img/u/epn.png') }}" class="img-responsive center-block" alt="Escuela Politécnica Nacional"> </div>
                <div><img src="{{ asset('img/u/espoch.png') }}" class="img-responsive center-block" alt="Escuela Superior Politécnica de Chimborazo"> </div>
                <div><img src="{{ asset('img/u/unach.png') }}" class="img-responsive center-block" alt="Universidad Nacional de Chimborazo"> </div>
            </div>
        </div>
        <!-- end .container -->
    </section>
    <!-- end section.sponsors -->

    <!-- 
     Location Map
     ====================================== -->

    <div class="g-maps" id="venue">

        <!-- Tip:  You can change location, zoom, color theme, height, image and Info text by changing data-* attribute below. -->
        <!-- Available Colors:    red, orange, yellow, green, mint, aqua, blue, purple, pink, white, grey, black, invert -->

        <div class="map" id="map_canvas" data-maplat="-0.935049" data-maplon="-78.611" data-mapzoom="15" data-color="green" data-height="400" data-img="images/marker.png" data-info="ESPE Latacunga"></div>

    </div>
    <!-- end div.g-maps -->
    <!-- 
     Contact us
     ====================================== -->

    <div class="highlight">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="contact-box">
                        <img src="images/location-icon.png" alt="location icon" class="wow zoomIn">
                        <h5>DIRECCIÓN</h5>
                        <p>Calle Quijano y Ordoñez y Hermanas Páez,
                            <br>
                            <br> Cotopaxi - Latacunga
                            <br>Ecuador
                            <br>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact-box">
                        <img src="images/email-icon.png" alt="email icon" class="wow zoomIn" data-wow-delay="0.3s">
                        <h5>CONTACTO</h5>
                        <p><a href="mailto:vhandaluz@espe.edu.ec">vhandaluz@espe.edu.ec</a>
                            <br> 
                            
                            <br> +593 3 2810206 / 2811240
                            <br> +593 3 2810208
                        </p>
                    </div>
                </div>
            </div>
            <!--  // end .row -->
        </div>
    </div>
    <!-- //  end .highlight -->

    <div class="container" id="contact">
        <div class="section-title">
            <h5>ENVIANOS UN MENSAJE</h5>
            <p>Si tienes alguna pregunta del evento, por favor contáctenos directamente.</p>
        </div>

        <div class="contact-form bottom-space-xl wow fadeInUp">

            <form action="php/contact.php" id="phpcontactform" method="POST">
                <div class="row">

                    <div class="col-md-6 col-md-offset-3">


                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" class="form-control" name="first_name" placeholder="Nombre(s)" required>

                                </div>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Apellido</label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Apellido(s)" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Email" required>
                        </div>

                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone" placeholder="Teléfono" required>
                        </div>

                        <div class="form-group">
                            <label>Su Mensaje</label>
                            <textarea class="form-control" name="message" rows="6" placeholder="Ingrese su mensaje aquí" required> </textarea>
                        </div>

                        <div class="text-center top-space">
                            <button type="submit" class="btn btn-success btn-block btn-lg" id="js-contact-btn">Enviar mensaje</button>
                            <div id="js-contact-result" data-success-msg="Formulario enviado correctamente." data-error-msg="Oops. Algo salio mal."></div>
                        </div>

                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- // end .container -->
    <footer>

        <div class="social-icons">
            <a href="#" class="wow zoomIn"> <i class="fa fa-twitter"></i> </a>
            <a href="#" class="wow zoomIn" data-wow-delay="0.2s"> <i class="fa fa-facebook"></i> </a>
            <a href="#" class="wow zoomIn" data-wow-delay="0.4s"> <i class="fa fa-linkedin"></i> </a>
        </div>
        <p> <small class="text-muted">Copyright © {{ date('y') }}. All rights reserved.</small></p>

    </footer>

    <a href="#top" class="back_to_top"><img src="images/back_to_top.png" alt="back to top"></a>

    <!-- 
     Registration Popup (PAYPAL)
     ====================================== -->

    <div class="modal fade" id="register-now" tabindex="-1" role="dialog" aria-labelledby="register-now-label">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="register-now-label">Registro del evento
</h4>
                </div>
                <div class="modal-body">

                    <div class="registration-form">

                        <!-- Testing Paypal? Change Paypal URL to https://www.sandbox.paypal.com/cgi-bin/webscr If you are testing -->

                        <form action="{{ route('evento.registro') }}" method="POST" target="_top" id="paypal-regn">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nombre(s)</label>
                                        <input type="text" class="form-control" name="nombre" placeholder="Nombre(s)" required>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Apellido(s)</label>
                                        <input type="text" class="form-control" name="apellido" placeholder="Apellido(s)" required>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email ID</label>
                                <input type="email" class="form-control" name="email" placeholder="Correo electrónico" required>
                            </div>

                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <label>CI</label>
                                        <input type="text" class="form-control" name="ci" placeholder="Cédula de identidad" required>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label>Tipo</label>
                                        <select class="form-control" name="tipo" required>
                                            <option value="presencial">Presencial</option>
                                            <option value="videoconferencia">Videoconferencia</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="text-center top-space">
                                <button type="submit" id="reserve-btn" class="btn btn-success btn-lg">Reservar mi Cupo</button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- 
     Javascripts : Nerd Stuff :)
     ====================================== -->

    <!-- jQuery Library -->
    <script src="{{ asset('js_event/jquery.min.js') }}"></script>

    <!-- Bootstrap JS -->
    <script src="{{ asset('js_event/bootstrap.min.js') }}"></script>

    <!-- 3rd party Plugins -->
    <script src="{{ asset('js_event/plugins/countdown.js') }}"></script>
    <script src="{{ asset('js_event/plugins/wow.js') }}"></script>
    <script src="{{ asset('js_event/plugins/slick.js') }}"></script>
    <script src="{{ asset('js_event/plugins/magnific-popup.js') }}"></script>
    <script src="{{ asset('js_event/plugins/validate.js') }}"></script>
    <script src="{{ asset('js_event/plugins/appear.js') }}"></script>
    <script src="{{ asset('js_event/plugins/count-to.js') }}"></script>
    <script src="{{ asset('js_event/plugins/nicescroll.js') }}"></script>

    <!-- Google Map -->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCOX9okNdsDlE4s1YpSyfXCaENTZH0DqY0&sensor=false"></script>
    <script src="{{ asset('js_event/plugins/infobox.js') }}"></script>
    <script src="{{ asset('js_event/plugins/google-map.js') }}"></script>
    <script src="{{ asset('js_event/plugins/directions.js') }}"></script>

    <!-- JS Includes -->
    <script src="{{ asset('js_event/includes/subscribe.js') }}"></script>
    <script src="{{ asset('js_event/includes/contact_form.js') }}"></script>

    <!-- Main Script -->
    <script src="{{ asset('js_event/main.js') }}"></script>

</body>

</html>
