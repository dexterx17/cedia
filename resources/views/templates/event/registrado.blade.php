<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- SEO -->
    <meta name="author" content="Jaime Santana - ARSI">
    <meta name="description" content="La capacitación consiste en un curso-taller orientadas a machine learning aplicado tecnologías inmersivas, realida virtual y realidad aumentada. El curso-taller tendrá una duración de 90 horas las mismas que serán dictadas en tres semanas por dos doctores con amplia experiencia en la temática. El curso considera un 33,3% de trabajo práctico del participante, tiempo en el cual los instructores guiarán los diferentes trabajos a ser desarrollados por los participantes del curso.">
    <meta name="keywords" content="gather, responsive, event, meetup, template, conference, gather, rsvp, download">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Page Title -->
    <title>TÉCNICAS DE OPTIMIZACIÓN PARA  ALGORITMOS DE CONTROL AVANZADO</title>

    <!-- Bootstrap -->
    <link href="{{ asset('css_event/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Google Font : Open Sans & Montserrat -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Plugins -->
    <link href="{{ asset('css_event/plugins/animate.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/font-awesome.css')}}" rel="stylesheet">
    <link href="{{ asset('css_event/plugins/streamline-icons.css')}}" rel="stylesheet">

    <!-- Event Style -->
    <link href="{{ asset('css_event/event.css')}}" rel="stylesheet">

    <!-- Color Theme -->
    <!-- Options: green, purple, red, yellow, mint, blue, black  -->
    <link href="{{ asset('css_event/themes/mint.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="js/ie/respond.min.js"></script>
  <![endif]-->

    <!-- Modernizr -->
    <script src="{{ asset('js_event/modernizr.min.js')}}"></script>
    <!-- Subtle Loading bar -->
    <script src="{{ asset('js_event/plugins/pace.js')}}"></script>
</head>

<body class="animate-page" data-spy="scroll" data-target="#navbar" data-offset="100">
    <!--Preloader div-->
    <div class="preloader"></div>

    <header id="top" class="header">
        <div class="container">

            <div class="header_top-bg">
                <div class="logo">
                    <a href="{{ route('home') }}"><img src="{{ asset('img/EnNegroTransparente.png') }}" alt="event-logo" width="300" height="80"></a>
                </div>
            </div>

            <h3 class="headline-support wow fadeInDown">Registro exitoso</h3>
            <h1 class="headline wow fadeInDown" data-wow-delay="0.1s"> Gracias {{ $registro->nombre }} {{ $registro->apellido }}, lo esperamos </h1>

            <div class="when_where wow fadeIn" data-wow-delay="0.4s">
                <p class="event_when">del 26 de agosto al 13 de septiembre de 2019</p>
                <p>desde las 7:30 am hasta la 1:00 pm</p>
                <p class="event_where">ESPE, Extensión Latacunga</p>
            </div>

            <div class="header_bottom-bg">
                <a class="btn btn-default" href="{{ route('curso') }}">Regresar a página principal</a>
            </div>

        </div>
        <!-- end .container -->
    </header>
</body>
</html>
