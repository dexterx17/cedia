<!DOCTYPE html>
<html style="" class=" js no-touch csstransitions" lang="en"><head>
        <title>@yield('title','Investigacion')</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="owwwlab.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <meta name="description" content="A theme for faculty profile page">
        <meta name="keywords" content="cedia, cepra, espe, uta, espoch">

        <link rel="shortcut icon" href="{{ asset('img/fav/favicon.ico') }}">

        <!--CSS styles-->
        <link rel="stylesheet" href="{{ asset('css/one/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}  ">
        <link rel="stylesheet" href="{{ asset('css/one/perfect-scrollbar-0.css') }}">
        <link rel="stylesheet" href="{{ asset('css/one/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/one/style.css') }}">
        <link id="theme-style" rel="stylesheet" href="{{ asset('css/one/default.css') }}">

        
        <!--/CSS styles-->
        <!--Javascript files-->
        <script type="text/javascript" src="{{ asset('js/one/jquery-1.11.3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/one/TweenMax.min.') }}js"></script>
        <script type="text/javascript" src="{{ asset('js/one/jquery.touchSwipe.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/one/jquery.carouFredSel-6.2.1-packed.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('js/one/modernizr.custom.63321.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/one/jquery.dropdownit.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/one/ScrollToPlugin.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/one/bootstrap.') }}min.js"></script>

        <script type="text/javascript" src="{{ asset('js/one/jquery.mixitup.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/one/masonry.min.') }}js"></script>

        <script type="text/javascript" src="{{ asset('js/one/perfect-scrollbar-0.4.5.with-mousewheel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/one/jquery.nicescroll.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/one/magnific-po') }}pup.js"></script>
        <script type="text/javascript" src="{{ asset('js/one/custom.js') }}"></script>

        <!--/Javascript files-->

    </head>
    <body>
        <div id="wrapper">
            <a class="mobilemenu" href="#sidebar">
                <i class="fa fa-reorder">
                </i>
            </a>
            @include('templates.one.partials.side_menu')
            @yield('content')
    </body>
</html>