<div id="sidebar">
    <div id="sidebar-wrapper">
        <div id="sidebar-inner">
            <!-- Profile/logo section-->
            <div class="clearfix" id="profile">
                <a href="{{ route('home') }}" title="Volver a inicio">Volver a inicio</a>
                <div class="portrate hidden-xs" style="background-image: url('{{ asset('img/pics/'.$user_data->imagen) }}') !important;">
                </div>
                <div class="title">
                    <h2>
                        {{ $user_data->nombres }} {{ $user_data->apellidos }} 
                    </h2>
                    <h3>
                        {{ $user_data->titulo }}
                    </h3>
                </div>
            </div>
            <!-- /Profile/logo section-->
            <!-- Main navigation-->
            <div id="main-nav">
                <ul id="navigation">
                    <li @if($page_active=="info") class="currentmenu" @endif>
                        <a href="{{ route('user_info',$user_data->id) }}">
                            <i class="fa fa-user">
                            </i>
                            <div class="text">
                                {{ trans('front.acerca_de_mi') }}
                            </div>
                        </a>
                    </li>
                    @if( $user_data->trabajos()->count()>0 )
                    <li @if($page_active=="experiencia") class="currentmenu" @endif>
                        <a href="{{ route('user_experiencia',$user_data->id) }}">
                            <i class="fa fa-clock-o">
                            </i>
                            <div class="text">
                                {{ trans('front.experiencia_laboral') }}
                            </div>
                        </a>
                    </li>
                    @endif
                    @if( $user_data->investigaciones()->count()>0 )
                    <li @if($page_active=="investigaciones") class="currentmenu" @endif>
                        <a href="{{ route('user_investigaciones',$user_data->id) }}">
                            <i class="fa fa-book">
                            </i>
                            <div class="text">
                                {{ ucfirst(trans('front.investigacion')) }}
                            </div>
                        </a>
                    </li>
                    @endif
                    @if( $user_data->publicaciones()->count()>0 )
                    <li @if($page_active=="publicaciones") class="currentmenu" @endif>
                        <a href="{{ route('user_publicaciones',$user_data->id) }}">
                            <i class="fa fa-edit">
                            </i>
                            <div class="text">
                                {{ trans_choice('front.publicaciones',10) }}
                            </div>
                        </a>
                    </li>
                    @endif
                    @if( $user_data->reconocimientos()->count()>0 )
                    <li @if($page_active=="reconocimientos") class="currentmenu" @endif>
                        <a href="{{ route('user_reconocimientos',$user_data->id) }}">
                            <i class="fa fa-calendar">
                            </i>
                            <div class="text">
                                {{ ucfirst(trans('front.reconocimientos')) }}
                            </div>
                        </a>
                    </li>
                    @endif
                    <!--<li>
                        <a href="#">
                            <i class="fa fa-picture-o">
                            </i>
                            <div class="text">
                                Gallery
                            </div>
                        </a>
                    </li>-->
                    @if( $user_data->cv )
                    <li class="external">
                        <a href="{{ asset('cv') }}/{{ $user_data->cv }}" target="_blank">
                            <i class="fa fa-download">
                            </i>
                            <div class="text">
                                Download CV
                            </div>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
            <!-- /Main navigation-->
            <!-- Sidebar footer -->
            <div id="sidebar-footer">
                <div class="social-icons">
                    <ul>
                        @if($user_data->facebook)
                        <li>
                            <a href="{{ $user_data->facebook }}" target="_blank">
                                <i class="fa fa-facebook">
                                </i>
                            </a>
                        </li>
                        @endif
                        @if($user_data->twitter)
                        <li>
                            <a href="#">
                                <i class="fa fa-twitter">
                                </i>
                            </a>
                        </li>
                        @endif
                        @if($user_data->linkedin)
                        <li>
                            <a href="#">
                                <i class="fa fa-linkedin">
                                </i>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div id="copyright">
                    copyright  GIARSI  {{ date('Y')}}
                </div>
            </div>
            <!-- /Sidebar footer -->
        </div>
    </div>

</div>