<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="ESPE, investigacion, desarrollo de proyectos">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Jaime Santana">
    <title>@yield('title',trans('front.grupo_nombre')) | ESPE</title>

    <link rel="shortcut icon" href="{{ asset('img/fav/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/fav/apple-touch-icon-144x144.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/fav/apple-touch-icon-114x114.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/fav/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/fav/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/fav/apple-touch-icon-60x60.png') }}" />

    <meta name="description" content="{{ trans('front.grupo_investigacion') }} {{ trans('front.grupo_nombre') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/front.css') }}">
    @yield('css')
  </head>
  <body>
    @yield('content')
    
    @include('templates.basic.partials.footer')

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-arrow-up"></i>
    </a>

    <div id="loader">
      <div class="cssload-thecube">
        <div class="cssload-cube cssload-c1"></div>
        <div class="cssload-cube cssload-c2"></div>
        <div class="cssload-cube cssload-c4"></div>
        <div class="cssload-cube cssload-c3"></div>
      </div>
    </div>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{ asset('js/front.js') }}"></script>
    <script src="{{ asset('js/basic/jquery.collapse.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/one/jquery.mixitup.min.js') }}"></script>
    @yield('js')
    <script type="text/javascript" charset="utf-8">
        $('div#pub-grid').mixitup({
        layoutMode: 'list',
        easing : 'snap',
        transitionSpeed :600,
        onMixEnd: function(){
            $(".tooltips").tooltip();
        }
    }).on('click','div.pubmain .pubcollapse',function(e){
        var $this = $(this), 
            $item = $this.closest(".item");
        
        $item.find('div.pubdetails').slideToggle(function(){
            $this.children("i").toggleClass('icon-collapse-alt icon-expand-alt');
        },function(){
            $this.children("i").toggleClass('icon-expand-alt icon-collapse-alt');
        });
        console.log(e);
        console.log($item.attr('target'));
        e.preventDefault();
    });
   // $('#register-now').modal('show');
</script>
  </body>
</html>