<nav class="navbar navbar-toggleable-sm fixed-top navbar-light bg-faded">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img alt="{{ trans('front.grupo_nombre') }}" id="img-logo" src="{{ asset('img/logos/logo_blanco.png') }}"/>
        </a>
        <button aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarCollapse" data-toggle="collapse" type="button">
            <i class="icon-menu">
            </i>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#hero-area">
                        {{ trans('front.home') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about">
                        {{ trans('front.acerca_de') }}
                    </a>
                </li>
                <!--<li class="nav-item">
                <a class="nav-link" href="#services">{{ trans('front.areas_investigacion') }}</a>
              </li>-->
                <li class="nav-item">
                    <a class="nav-link" href="#portfolio">
                        {{ trans_choice('front.proyectos',10) }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#why">
                        {{ trans_choice('front.publicaciones',10) }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#team">
                        {{ trans_choice('front.investigadores',10) }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">
                        {{ trans('front.contacto') }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>