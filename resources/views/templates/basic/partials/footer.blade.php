<!-- Footer Section Start -->
<footer>
    <!-- Footer Area Start -->
    <section class="footer-Content">
        <div class="container wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
            
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="widget">
                        <div class="textwidget">
                            <br>
                            <img src="{{ asset('img/EnBlancoTransparente.png') }}" alt="GIARSI" width="500" height="80">
                            <br>
                            <br>
                            <h6 class="text-center">
                                {{ trans('front.grupo_nombre') }}
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-6 col-xs-12">
                    <div class="widget">
                        <h3 class="block-title">
                            Links
                        </h3>
                        <ul class="menu">
                            <li>
                                <a href="{{ route('home') }}#about">
                                    {{ trans('front.acerca_de') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('home') }}#portfolio">
                                    {{ trans_choice('front.proyectos',10) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('home') }}#why">
                                    {{ trans_choice('front.publicaciones',10) }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('home') }}#team">
                                    {{ trans_choice('front.investigadores',10) }}
                                </a>
                            </li>
                            <li>
                                <a href="http://www.espe.edu.ec/" target="_blank">
                                    ESPE
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="widget">
                        <h3 class="block-title">
                            {{ trans('front.galeria') }}
                        </h3>
                        <ul class="featured-list">
                            @foreach($imagenes as $img)
                            <li>
                                <a href="#">
                                    <img alt="{{ $img->nombre }}" src="{{ asset('img/uploads/'.$img->ruta) }}" width="90" height="65" />
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer area End -->
    <!-- Copyright Start  -->
    <div id="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="site-info pull-left wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                        <p>
                            Template by
                            <a href="http://graygrids.com" rel="nofollow">
                                GrayGrids
                            </a>
                        </p>
                    </div>
                    <div class="bottom-social-icons social-icon pull-right wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1000ms">
                        <a class="twitter" href="">
                            <i class="fa fa-twitter">
                            </i>
                        </a>
                        <a class="facebook" href="#">
                            <i class="fa fa-facebook">
                            </i>
                        </a>
                        <a class="google-plus" href="#">
                            <i class="fa fa-google-plus">
                            </i>
                        </a>
                        <a class="linkedin" href="#">
                            <i class="fa fa-linkedin">
                            </i>
                        </a>
                        <a class="dribble" href="#">
                            <i class="fa fa-dribbble">
                            </i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright End -->
</footer>
<!-- Footer Section End -->
