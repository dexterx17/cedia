@extends('back.template.base')

@section('title','Dashboard admin')
@section('css')
   <!-- FullCalendar -->
   
@endsection

@section('content')	
	@include('back.template.side_menu')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $usuarios->count() }}</div>
                                <div>{{ trans('admin.investigadores') }}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('admin.usuarios.index') }}">
                        <div class="panel-footer">
                            <span class="pull-left">{{ trans('admin.ver_detalles') }}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $publicaciones->count() }}</div>
                                <div>{{ trans('admin.publicaciones') }}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('admin.publicaciones.index') }}">
                        <div class="panel-footer">
                            <span class="pull-left">{{ trans('admin.ver_detalles') }}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $proyectos->count() }}</div>
                                <div>{{ trans_choice('front.proyectos',$proyectos->count()) }}</div>
                            </div>
                        </div>
                    </div>
                    <a href="{{ route('admin.proyectos.index') }}">
                        <div class="panel-footer">
                            <span class="pull-left">{{ trans('admin.ver_detalles') }}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-map-marker fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{{ $ponencias->count() }}</div>
                                <div>{{ trans_choice('front.ponencias',$ponencias->count()) }}</div>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <span class="pull-left">{{ trans('admin.ver_detalles') }}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div id="map" style="width: 700px"></div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('js/mapdata.js') }}"></script>
    <script src="{{ asset('js/worldmap.js') }}"></script>
    <script src="{{ asset('js/search.js') }}"></script>   

    <script type="text/javascript">
        console.log(simplemaps_worldmap_mapdata);

        var locations ={ };

            @foreach($ponencias as $index=>$ponencia)
                locations[{{$index}}] = {
                    name: "{{ $ponencia->evento }}",
                    lat: {{ $ponencia->lat }}, 
                    lng: {{ $ponencia->lng }},
                    description: "{{ $ponencia->titulo}}",
                    color: 'default',
                    url: '#',
                    size: 'default'
                };
            
            @endforeach
            simplemaps_worldmap_mapdata.locations = locations;
            
    </script>
@endsection