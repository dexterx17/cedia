<!DOCTYPE html>
<html lang="es">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title> @yield('title') | ESPE</title>
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="{{ asset('imagenes/icon.png')}}" rel="shortcut icon">

	    <!-- Bootstrap Core CSS -->
	    <link href="{{ asset('css/admin/bootstrap.css') }}" rel="stylesheet">

	    <!-- MetisMenu CSS -->
	    <link href="{{ asset('css/admin/metisMenu.min.css') }}" rel="stylesheet">

	    <link href="{{ asset('css/admin/sweetalert.css') }}" rel="stylesheet">

	    <!-- Custom CSS -->
	    <link href="{{ asset('css/admin/sb-admin-2.css') }}" rel="stylesheet">

	    <!-- Custom Fonts -->
	    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css">

	    <link rel="stylesheet" href="{{ asset('css/libs/basic.min.css') }}">
    	<link rel="stylesheet" href="{{ asset('css/libs/dropzone.min.css') }}">
    	

    	<link rel="stylesheet" href="{{ asset('css/libs/pnotify.css') }}">
    	<link rel="stylesheet" href="{{ asset('css/libs/pnotify.buttons.css') }}">

	    <link rel="shortcut icon" href="{{ asset('img/fav/favicon.ico') }}">
	    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('img/fav/apple-touch-icon-144x144.png') }}" />
	    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/fav/apple-touch-icon-114x114.png') }}" />
	    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/fav/apple-touch-icon-72x72.png') }}" />
	    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('img/fav/apple-touch-icon-57x57.png') }}" />
	    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('img/fav/apple-touch-icon-60x60.png') }}" />


	    @yield('css')
    </head>
    
	<body>
		<div id="wrapper">
			@yield('content')

		</div>
    	<!-- /#wrapper -->

		<!-- jQuery -->
		<script src="{{ asset('js/admin/jquery.min.js') }}"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="{{ asset('js/admin/bootstrap.js') }}"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="{{ asset('js/admin/metisMenu.min.js')}}"></script>

		<script src="{{ asset('js/libs/dropzone.min.js') }}"></script>
		
		<script src="{{ asset('js/libs/pnotify.js') }}"></script>
		<script src="{{ asset('js/libs/pnotify.buttons.js') }}"></script>

		<!-- Custom Theme JavaScript -->
		<script src="{{ asset('js/admin/sb-admin-2.js') }}"></script>
		
		<script src="{{ asset('js/admin/sweetalert.min.js') }}"></script>

	    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
	    @yield('js')
	</body>
</html>
