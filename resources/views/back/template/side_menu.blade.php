<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
            <span class="sr-only">
                Toggle navigation
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
        </button>
        <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
            Admin 
        </a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-tasks fa-fw">
                </i>
                <i class="fa fa-caret-down">
                </i>
            </a>
            <ul class="dropdown-menu dropdown-tasks">
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>
                                    Task 1
                                </strong>
                                <span class="pull-right text-muted">
                                    40% Complete
                                </span>
                            </p>
                            <div class="progress progress-striped active">
                                <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" class="progress-bar progress-bar-success" role="progressbar" style="width: 40%">
                                    <span class="sr-only">
                                        40% Complete (success)
                                    </span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="divider">
                </li>
                <li>
                    <a class="text-center" href="#">
                        <strong>
                            See All Tasks
                        </strong>
                        <i class="fa fa-angle-right">
                        </i>
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-tasks -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw">
                </i>
                <i class="fa fa-caret-down">
                </i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="#">
                        <i class="fa fa-user fa-fw">
                        </i>
                        User Profile
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-gear fa-fw">
                        </i>
                        Settings
                    </a>
                </li>
                <li class="divider">
                </li>
                <li>
                    <a href="{{ url('/logout') }}">
                        <i class="fa fa-sign-out fa-fw">
                        </i>
                        Logout
                    </a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li class="sidebar-search">
                    <div class="input-group custom-search-form">
                        <input class="form-control" placeholder="Search..." type="text">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search">
                                    </i>
                                </button>
                            </span>
                        </input>
                    </div>
                    <!-- /input-group -->
                </li>
                <li>
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="fa fa-dashboard fa-fw">
                        </i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.usuarios.index') }}">
                        <i class="fa fa-users fa-fw">
                        </i>
                        {{ trans('admin.investigadores') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.publicaciones.index') }}">
                        <i class="fa fa-tasks fa-fw">
                        </i>
                        {{ trans('admin.publicaciones') }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.proyectos.index') }}">
                        <i class="fa fa-edit fa-fw">
                        </i>
                        {{ trans_choice('front.proyectos',10) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.documentos.index') }}">
                        <i class="fa fa-edit fa-fw">
                        </i>
                        {{ trans_choice('front.documentos',10) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.universidades.index') }}">
                    <i class="fa fa-users fa-fw">
                        </i>
                        {{ trans_choice('front.universidades',10) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.ponencias.index') }}">
                    <i class="fa fa-users fa-fw">
                        </i>
                        {{ trans_choice('front.ponencias',10) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.asistentes.index') }}">
                    <i class="fa fa-users fa-fw">
                        </i>
                        {{ trans_choice('admin.asistentes_investigadores',10) }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.titulaciones.index') }}">
                    <i class="fa fa-users fa-fw">
                        </i>
                        {{ trans_choice('admin.trabajos_titulacion',10) }}
                    </a>
                </li>

                <li></li>
                <li>
                    <a href="#">
                        <i class="fa fa-sitemap fa-fw">
                         </i>
                        Generales
                        <span class="fa arrow">
                        </span>
                     </a>
                    <ul class="nav nav-second-level">
                     <li>
                         <a href="{{ route('admin.tipos_documentos.index') }}">
                             Tipos de documentos
                         </a>
                     </li>
                     <li>
                         <a href="{{ route('admin.intereses.index') }}">
                             Intereses
                         </a>
                     </li>
                     <li>
                    <a href="{{ route('admin.participantes.index') }}">
                            {{ trans('admin.participantes') }} curso ML
                        </a>
                    </li>
                     <!--<li>
                         <a href="#">
                             Third Level
                             <span class="fa arrow">
                             </span>
                         </a>
                         <ul class="nav nav-third-level">
                             <li>
                                 <a href="#">
                                     Third Level Item
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     Third Level Item
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     Third Level Item
                                 </a>
                             </li>
                             <li>
                                 <a href="#">
                                     Third Level Item
                                 </a>
                             </li>
                         </ul>
                         <!-- /.nav-third-level 
                     </li>-->
                    </ul>
                    <!-- /.nav-second-level -->
                </li>
                <li></li>
                <li>
                    <a href="{{ route('home') }}" target="_blank">Ir a Front End</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>