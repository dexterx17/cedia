<div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
    <div class="thumbnail">
        <div class="image view view-first">
            <img alt="image" src='{{ asset("img/uploads/$imagen->ruta") }}' style="width: 100%; display: block;"/>
            <div class="mask">
                <p>
                    {{ $imagen->ruta }}
                </p>
                <div class="tools tools-bottom">
                    <a href='{{ route("admin.imagenes.edit",$imagen->id) }}' class="edit-image">
                        <i class="fa fa-pencil">
                        </i>
                    </a>
                    <a href='{{ route("admin.imagenes.destroy",$imagen->id) }}' class="delete-image">
                        <i class="fa fa-times">
                        </i>
                    </a>
                </div>
            </div>
        </div>
        @if($imagen->nombre)
        <div class="caption">
            <p>
                {{ $imagen->nombre }}
            </p>
        </div>
        @endif
    </div>
</div>