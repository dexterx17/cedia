@extends('back.template.base')

@section('title',trans('admin.editar').' '.$usuario->nombres.' '.$usuario->apellidos)


@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.usuarios.index') }}" title="{{ trans('admin.investigadores') }}">{{ trans('admin.investigadores') }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('admin.investigador') }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="" id="tabs" data-example-id="togglable-tabs" role="tabpanel">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#infor">
                                   {{ trans('admin.informacion') }}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#contacto">
                                   {{ trans('admin.contactos') }}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#experiencia">
                                    {{ trans('admin.experiencia') }}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#investigacion">
                                    {{ trans('admin.investigacion') }}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#reconocimientos">
                                    {{ trans_choice('admin.reconocimientos',10) }}
                                </a>
                            </li>
                        </ul>
                    
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="infor">
                                <!-- content starts here -->
                                <form class="form-horizontal form-label-left" action="{{route('admin.usuarios.update',$usuario->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">
                                            {{ trans('admin.nombres') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->nombres }}" id="nombres" name="nombres" placeholder="{{ trans('admin.nombres') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellidos">
                                            {{ trans('admin.apellidos') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->apellidos }}" id="apellidos" name="apellidos" placeholder="{{ trans('admin.apellidos') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidades">
                                            {{ trans('admin.universidad') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::select('universidades[]',$universidades,$usuario->universidades->lists('id')->all(),['id'=>'universidades-list', 'required'=>'required','class'=>'form-control col-md-7 col-xs-12']) !!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">
                                            {{ trans('admin.titulo') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->titulo }}" id="titulo" name="titulo" placeholder="{{ trans('admin.titulo') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="about_me">
                                            {{ trans('admin.about_me') }}
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="about_me" name="about_me" placeholder="{{ trans('admin.about_me') }}" class="form-control col-md-7 col-xs-12" >{{ $usuario->about_me }}</textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intereses">
                                            {{ trans_choice('front.areas_de_interes',10) }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            {!! Form::select('intereses[]',$intereses,$usuario->intereses->lists('id')->all(),['id'=>'intereses-list','multiple'=>'multiple', 'class'=>'']) !!}
                                            <!--<input type="text" name="intereses" id='intereses-list' data-role="tagsinput"   class='form-control col-md-9 col-xs-9'>-->
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="scholar_id">
                                            {{ trans('admin.scholar_id') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->scholar_id }}" id="scholar_id" name="scholar_id" placeholder="{{ trans('admin.scholar_id') }}" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagen">
                                            {{ trans('admin.imagen') }}
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            {!! Form::file('imagen',['class'=>'form-control','placeholder'=>'Imagen']) !!}
                                        </div>
                                        @if($usuario->imagen)
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <img src="{{ asset('img/pics/'.$usuario->imagen) }}" alt="{{ $usuario->nombres }}" width="150" height="150">
                                        </div>
                                        @endif
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cv">
                                            CV
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            {!! Form::file('cv',['class'=>'form-control','placeholder'=>'Curriculo Vitae']) !!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">
                                            {{ trans('admin.password') }}
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12"  id="password" name="password" type="password"></input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol">
                                            {{ trans('admin.tipo') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            {!! Form::select('rol', [
                                                'docente' => trans('admin.docente'),
                                                'investigador' => trans('admin.investigador'),
                                                'tesista' => trans('admin.tesista'),
                                                'pasante' => trans('admin.pasante')
                                                ],$usuario->rol,[
                                                    'required'=>'required',
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                ]) !!}

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                            {{ trans('admin.rol') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            {!! Form::select('type', [
                                                'admin' => trans('admin.admin'),
                                                'autor' => trans('admin.autor'),
                                                ],$usuario->type,[
                                                    'required'=>'required',
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                ]) !!}

                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="scholar_id">
                                            {{ trans('admin.scholar_id') }}
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->scholar_id }}" id="scholar_id" name="scholar_id" placeholder="{{ trans('admin.scholar_id') }}" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="orden">
                                            {{ trans('admin.orden') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->orden }}" id="orden" name="orden" placeholder="{{ trans('admin.orden') }}" required="required" type="number">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="ln_solid">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <a  href="{{ route('admin.usuarios.index') }}"class="btn btn-primary" >
                                                {{ trans('admin.cancelar') }}
                                            </a>
                                            <button class="btn btn-success" id="send" type="submit">
                                                {{ trans('admin.guardar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane" id="contacto">
                                <!-- content starts here -->
                                <form class="form-horizontal form-label-left" action="{{route('admin.usuarios.update',$usuario->id)}}" method="POST">
                                    {{ csrf_field() }}
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                                            {{ trans('admin.email') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->email }}" id="email" name="email" required="required" type="email">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">
                                            {{ trans('admin.telefono') }}
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" id="telefono" value="{{ $usuario->telefono }}" name="telefono" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook">
                                            Facebook
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->facebook }}" id="facebook" name="facebook" placeholder="URL de facebook" type="url">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="skype">
                                            Skype
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->skype }}" id="skype" name="skype" placeholder="Usuario skype" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="twitter">
                                            Twitter
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->twitter }}" id="twitter" name="twitter" placeholder="URL de twitter" type="url">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linkedin">
                                            LinkedIn
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->linkedin }}" id="linkedin" name="linkedin" placeholder="URL de linkedin" type="url">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="ln_solid">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <a  href="{{ route('admin.usuarios.index') }}"class="btn btn-primary" >
                                                {{ trans('admin.cancelar') }}
                                            </a>
                                            <button class="btn btn-success" id="send" type="submit">
                                                {{ trans('admin.guardar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane" id="experiencia">
                                <!-- content starts here -->
                                @include('back.experiencias.manager', ['experiencias' => $usuario->trabajos])
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane" id="investigacion">
                                <!-- content starts here -->
                                @include('back.investigaciones.manager', ['investigaciones' => $usuario->investigaciones])
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane" id="reconocimientos">
                                <!-- content starts here -->
                                @include('back.reconocimientos.manager', ['reconocimientos' => $usuario->reconocimientos])
                                <!-- content ends here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection

@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function(){

            $('#usuarios-list').chosen({
                placeholder_text_single:"Selecciona uno o varios autores",
                width:'100%'
            });
            $('#universidades-list').chosen({
                placeholder_text_single:"Selecciona una o varias universidades",
                width:'100%'
            });
            $('#intereses-list').chosen({
                placeholder_text_single:"Selecciona una o varias universidades",
                width:'100%'
            });
        });
    </script>
    
    <script  type="text/javascript" charset="utf-8" >
        $(document).on('click','.add-item-form',function(item){

            var url = $(this).attr('url');

            var user_id = $(this).attr('parent');

            var container = $(this).attr('container');

            $.get(url,{'user_id':user_id},function(response){
                $('#'+container).html(response);
                $('#desde').datepicker({
                      format: 'yyyy-mm-dd',
                });
                $('#hasta').datepicker({
                      format: 'yyyy-mm-dd',
                });
            });
        });
        $(document).on('click','.cancel-item-form',function(item){
            var container = $(this).attr('container');
            $('#'+container).fadeIn('slow').html('');
        });
        $(document).on('click','.edit-item-form',function(e){
            e.preventDefault();
            var container = $(this).attr('container');
            var url = $(this).attr('href');

            $.get(url,{},function(response){
                $('#'+container).html(response);
                $('#'+container).find('form input[type="text"]:first').focus();
                $('#desde').datepicker({
                      format: 'yyyy-mm-dd',
                });
                $('#hasta').datepicker({
                      format: 'yyyy-mm-dd',
                });
            });
        });

        $(document).on('click','.delete-item-form',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var $parent = $(this);
            swal({
              title: "{!! trans('admin.estas_seguro') !!}?",
              text: "{!! trans('admin.esta_accion_no') !!}!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
              cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
              closeOnConfirm: false,
              closeOnCancel: true
            },
            function(isConfirm){
              if (isConfirm) {
                $.get(url,{},function(data){
                    if(data.success){
                        $parent.parents('[item="item'+data.id+'"]').fadeOut('slow');
                        swal("Eliminado!", "Eliminado correctamente", "success");
                    }else{
                        swal("Error", "Error al eliminar", "error");
                    }
                },'json');
              }
            });
        });
    </script>
@endsection