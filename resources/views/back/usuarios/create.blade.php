@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans('admin.usuario'))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.usuarios.index') }}" title="{{ trans('admin.investigadores') }}">{{ trans('admin.investigadores') }}</a>
        </li>
        <li>{{ trans('admin.crear') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('admin.usuario') }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Ups! </strong> Ha ocurrido un error con tus datos.<br>
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.usuarios.store')}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">
                                {{ trans('admin.nombres') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('nombres') }}" id="nombres" name="nombres" placeholder="{{ trans('admin.nombres') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellidos">
                                {{ trans('admin.apellidos') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('apellidos') }}" id="apellidos" name="apellidos" placeholder="{{ trans('admin.apellidos') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidades">
                                {{ trans('admin.universidad') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('universidades[]',$universidades,old('universidades[]'),['required'=>'required','class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">
                                {{ trans('admin.titulo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('titulo') }}" id="titulo" name="titulo" placeholder="{{ trans('admin.titulo') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="about_me">
                                {{ trans('admin.about_me') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea id="about_me" name="about_me" placeholder="{{ trans('admin.about_me') }}" class="form-control col-md-7 col-xs-12" >{{ old('about_me') }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="intereses">
                                {{ trans_choice('front.areas_de_interes',10) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('intereses[]',$intereses,old('intereses[]'),['id'=>'intereses-list','multiple'=>'multiple', 'class'=>'form-control col-md-9 col-xs-9']) !!}
                                
                                <!--<input type="text" name="intereses" id='intereses-list' data-role="tagsinput"   class='form-control col-md-9 col-xs-9'>-->
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="scholar_id">
                                {{ trans('admin.scholar_id') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('scholar_id') }}" id="scholar_id" name="scholar_id" placeholder="{{ trans('admin.scholar_id') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                                {{ trans('admin.email') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('email') }}" id="email" name="email" required="required" type="email">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">
                                {{ trans('admin.telefono') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('telefono') }}" id="telefono" name="telefono" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="skype">
                                {{ trans('admin.skype') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ old('skype') }}" id="skype" name="skype" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagen">
                                {{ trans('admin.imagen') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::file('imagen',['class'=>'form-control','placeholder'=>'Imagen']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cv">
                                CV
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::file('cv',['class'=>'form-control','placeholder'=>'Curriculo Vitae']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">
                                {{ trans('admin.password') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12"  id="password" name="password" type="password" required="required"></input>
                            </div>
                        </div>
                         <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol">
                                {{ trans('admin.tipo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                {!! Form::select('rol', [
                                    'docente' => trans('admin.docente'),
                                    'investigador' => trans('admin.investigador'),
                                    'tesista' => trans('admin.tesista'),
                                    'pasante' => trans('admin.pasante')
                                    ],old('rol'),[
                                        'required'=>'required',
                                        'class'=>'form-control col-md-7 col-xs-12'
                                    ]) !!}

                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                {{ trans('admin.rol') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                {!! Form::select('type', [
                                    'autor' => trans('admin.autor'),
                                    'admin' => trans('admin.admin')
                                    ],old('type'),[
                                        'required'=>'required',
                                        'class'=>'form-control col-md-7 col-xs-12'
                                    ]) !!}

                            </div>
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a  href="{{ route('admin.usuarios.index') }}"class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    
<script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
    $('#intereses-list').chosen({
            placeholder_text_single:"Selecciona una o varias universidades",
            width:'100%'
        });
    

</script>
@endsection