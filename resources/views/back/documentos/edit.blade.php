@extends('back.template.base')

@section('title',trans('admin.editar').' '.trans_choice('front.documentos',1))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection
@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.documentos.index') }}" title="{{ trans_choice('front.documentos',10) }}">{{ trans_choice('front.documentos',10) }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('front.documentos',1) }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.documentos.update',$documento->id)}}" method="POST"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                                {{ trans('admin.nombre') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="nombre" name="nombre" value="{{ $documento->nombre }}" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">
                                {{ trans('admin.tipo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control col-md-7 col-xs-12" id="tipo_id" name="tipo_id"required="required" type="text">
                                    @foreach($tipos as $key=> $value)
                                        <option value="{{ $key }}" @if($documento->tipo_id==$key) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumen">
                                {{ trans('admin.resumen') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="resumen" name="resumen" placeholder="{{ trans('admin.resumen') }}">{{ $documento->resumen }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="palabras_clave">
                                {{ trans('admin.palabras_clave') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" class="form-control col-md-7 col-xs-12" value="{{ $documento->palabras_clave }}" id="palabras_clave" name="palabras_clave" ></input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="documento">
                                {{ trans('admin.documento') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="file" class="form-control col-md-7 col-xs-12" id="documento"  name="documento"></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referencias">
                                {{ trans_choice('admin.referencias',10) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <select class="form-control col-md-7 col-xs-12" id="referencias" name="referencias[]" required="required" multiple> 
                                    @foreach($proyectos as $key=> $value)
                                        <option value="{{ $key }}" @if(in_array($key,$referencias)) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_inicio">
                                {{ trans('admin.fecha') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon">Fecha</span>
                                    <input type="text" class="input-sm form-control" name="fecha" value="{{ $documento->fecha }}" />
                            </div>
                        </div>
                        <input type="hidden" name="upload_by" value="{{ Auth::user()->id }}">
                       
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('admin.documentos.index') }}" class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('.input-daterange').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#referencias').chosen({
            placeholder_text_single:"Selecciona una o varias referencias",
            width:'100%'
        });
    </script>
@endsection