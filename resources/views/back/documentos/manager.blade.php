@extends('back.template.base')

@section('title',trans_choice('front.documentos',10).' admin')

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans_choice('front.documentos',10) }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans_choice('front.documentos',10) }}   <small> {{ trans('admin.listado') }} </small>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <a href="{{ route('admin.documentos.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans_choice('front.documentos',1) }}</a>
                    <!-- start documentos list -->
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th style="width: 40%">
                                    {{ trans('admin.documento') }}
                                </th>
                                <th>
                                    {{ trans('admin.referencias') }}
                                </th>
                                <th>
                                    {{ trans('admin.fecha') }}
                                </th>
                                <th>
                                    {{ trans('admin.categoria') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($documentos_data as $index=>$pub)
                            <tr id="user{{$pub->id}}">
                                <td>
                                    {{ $index+1 }}
                                </td>
                                <td>
                                    <a href="{{ asset('uploads/'.$pub->ruta) }}">
                                        {{ $pub->nombre }}
                                    </a>
                                    <br>
                                    <small>{{ $pub->resumen }}</small>
                                </td>
                                <td>
                                    <ul>
                                        @foreach($pub->referencias() as $ref)
                                        <li>{{ $ref->nombre }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    {{ $pub->fecha }}
                                </td>
                                <td>
                                    <strong> {{ $pub->tipo->tipo }}</strong>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="{{ route('proyecto',$pub->id) }}">
                                        <i class="fa fa-eye">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.documentos.edit',$pub->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.documentos.destroy',$pub->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $documentos_data->render() !!}
                    <!-- end documentos list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready(function(){

    });
</script>
@endsection