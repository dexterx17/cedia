<div id="investigacion-container">
	
</div>
<table class="table table-striped projects">
	<caption>Listado de proyectos de investigacion <button type="button" class="btn btn-success add-item-form" parent="{{ $usuario->id }}" container="investigacion-container" url="{{ route('admin.investigaciones.create') }}">Nuevo proyecto</button></caption>
	<thead>
		<tr>
			<th width="15%">Cargo</th>
			<th>Proyecto</th>
			<th>Financiamiento</th>
			<th>Fecha</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($investigaciones as $investigacion)
		<tr item="item{{ $investigacion->id }}">
			<td>{{ $investigacion->cargo }}</td>
			<td>{{ $investigacion->proyecto }}</td>
			<td> <i class="fa fa-usd"></i> {{ $investigacion->financiamiento }}</td>
			<td>{{ $investigacion->desde }} <br> {{ $investigacion->hasta }}</td>
			<td class="text-center">
                <a class="btn btn-info btn-xs edit-item-form" href="{{ route('admin.investigaciones.edit',$investigacion->id)}}" container="investigacion-container">
                    <i class="fa fa-pencil">
                    </i>
                    {{ trans('admin.editar') }}
                </a>
                <a href="{{route('admin.investigaciones.destroy',$investigacion->id)}}" class="btn btn-danger btn-xs delete-item-form">
                    <i class="fa fa-trash-o"></i> 
                    {{ trans('admin.eliminar') }}
                </a>
            </td>
		</tr>
		@endforeach
	</tbody>
</table>