
<div class="x_panel">
    <div class="x_title">
        <h2>Proyecto de investigacion <small> {{ trans('admin.informacion') }}</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <!-- content starts here -->
        <form class="form-horizontal form-label-left" action="{{route('admin.investigaciones.update',$investigacion->id)}}" method="POST">
        {{ csrf_field() }}
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo">
                    {{ trans('admin.cargo') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="cargo" name="cargo" value="{{ $investigacion->cargo }}" placeholder="{{ trans('admin.cargo') }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="proyecto">
                    {{ trans_choice('front.proyectos',1) }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="proyecto" name="proyecto" value="{{ $investigacion->proyecto }}" placeholder="{{ trans_choice('front.proyectos',1) }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="financiamiento">
                    Financiamiento
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="financiamiento" name="financiamiento" value="{{ $investigacion->financiamiento }}" placeholder="Financiamiento" required="required" type="number">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">
                    {{ trans('admin.descripcion') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12" id="descripcion" name="descripcion" placeholder="{{ trans('admin.descripcion') }}">{{ $investigacion->descripcion }}</textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="objetivos">
                    {{ trans('admin.objetivos') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12" id="objetivos" name="objetivos" placeholder="{{ trans('admin.objetivos') }}">{{ $investigacion->objetivos }}</textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="participantes">
                    {{ trans('admin.participantes') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12" id="participantes" name="participantes" placeholder="{{ trans('admin.participantes') }}" required="required">{{ $investigacion->participantes }}</textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="desde">
                    {{ trans('admin.fecha_inicio') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group date">
                        <input type="text" class="form-control" value="{{ $investigacion->desde }}" name="desde" id="desde">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hasta">
                    {{ trans('admin.fecha_final') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group date">
                        <input type="text" class="form-control" value="{{ $investigacion->hasta }}" name="hasta" id="hasta">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="ln_solid">
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="user_id" value="{{ $investigacion->user_id }}">
                    <a href="#" class="btn btn-primary cancel-item-form" container="investigacion-container">
                        {{ trans('admin.cancelar') }}
                    </a>
                    <button class="btn btn-success" id="send" type="submit">
                        {{ trans('admin.guardar') }}
                    </button>
                </div>
            </div>
        </form>
        <!-- content ends here -->
    </div>
</div>
        
