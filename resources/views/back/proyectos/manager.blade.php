@extends('back.template.base')

@section('title',trans_choice('front.proyectos',10).' admin')

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans_choice('front.proyectos',10) }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans_choice('front.proyectos',10) }}   <small> {{ trans('admin.listado') }} </small>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <a href="{{ route('admin.proyectos.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans_choice('front.proyectos',1) }}</a>
                    <!-- start proyectos list -->
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th style="width: 40%">
                                    {{ trans('admin.nombre') }}
                                </th>
                                <th>
                                    {{ trans_choice('admin.universidades',10) }}
                                </th>
                                <th>
                                    {{ trans('admin.periodo') }}
                                </th>
                                <th>
                                    {{ trans('admin.financiamiento') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($proyectos_data as $index=>$pub)
                            <tr id="user{{$pub->id}}">
                                <td>
                                    {{ $index+1 }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.proyectos.show',$pub->id) }}">
                                        {{ $pub->nombre }}
                                        <br/>
                                        <small>
                                            {{ $pub->email}}
                                        </small>
                                    </a>
                                </td>
                                <td>
                                    @foreach($pub->universidades as $u)
                                        <a href="#" title="{{ $u->abreviacion }}">{{ $u->abreviacion }}</a>,
                                    @endforeach
                                </td>
                                <td>
                                    {{ $pub->fecha_inicio }} - {{ $pub->fecha_final }}
                                </td>
                                <td>
                                    <strong>$ {{ $pub->presupuesto }}</strong> <small class="label label-info">{{ $pub->financiamiento }}</small>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="{{ route('proyecto',$pub->id) }}">
                                        <i class="fa fa-eye">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.proyectos.edit',$pub->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.proyectos.destroy',$pub->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $proyectos_data->render() !!}
                    <!-- end proyectos list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready(function(){

    });
</script>
@endsection