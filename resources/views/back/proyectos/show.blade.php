@extends('back.template.base')

@section('title',trans_choice('front.proyectos',10).' admin')

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.proyectos.index') }}" title="Proyectos">Proyecto</a>
        </li>
        <li>{{ trans_choice('front.detalles',10) }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ $proyecto->nombre }}
                        <a href="{{ route('admin.proyectos.edit',$proyecto->id) }}" class="btn btn-default" title="Editar proyecto"><i class="fa fa-edit"></i></a>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                        <div class="row">
                            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                                <h4>Investigadores</h4>
                                <table class="table table-responsive table-hover table-bordered">
                                    @foreach($proyecto->usuarios as $user)
                                    <tr>
                                        <td>
                                            {{ $user->nombres }} {{ $user->apellidos }} <br>
                                            <small>{{ $user->email }}</small>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                                <h4>Instituciones</h4>
                                <table class="table table-responsive table-hover table-bordered">
                                    @foreach($proyecto->universidades as $universidad)
                                    <tr>
                                        <td title="{{ $universidad->nombre }}">
                                            <img src="{{ asset('img/u/'.$universidad->logo) }}" width="60" height="60" alt="{{ $universidad->nombre }}">
                                            <strong>{{ $universidad->abreviacion }}</strong>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
                                <div class="" id="tabs" data-example-id="togglable-tabs" role="tabpanel">
                                    <!-- required for floating -->
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#infor">
                                               {{ trans('admin.informacion') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#informes">
                                               {{ trans('admin.informes') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#laboratorios">
                                               {{ trans('admin.laboratorios') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#equipos">
                                               {{ trans('admin.equipos') }}
                                            </a>
                                        </li>
                                    </ul>
                                
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="infor">
                                            <dl>
                                                <dt>Presupuesto</dt>
                                                <dd>$ {{ $proyecto->presupuesto }} <small>{{ $proyecto->financiamiento }}</small></dd>
                                                <dt>Periodo</dt>
                                                <dd>Desde: {{ $proyecto->fecha_inicio }} hasta {{ $proyecto->fecha_final }}</dd>
                                                <dt>Objetivo General</dt>
                                                <dd class="text-justify">{{ $proyecto->objetivo_general }}</dd>
                                                <dt>Objetivos Específicos</dt>
                                                <dd class="text-justify">{!! $proyecto->objetivos_especificos !!}</dd>
                                                <dt>Resumen</dt>
                                                <dd class="text-justify">{{ $proyecto->resumen }}</dd>
                                                <dt>Descripción</dt>
                                                <dd class="text-justify">{{ $proyecto->descripcion }}</dd>
                                            </dl>
                                        </div>
                                        <div class="tab-pane active" id="informes">
                                            @foreach($tipos as $tipo)
                                            <div class="panel-group">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" href="#collapse1">{{ $tipo->tipo }}</a>
                                                            <a href="{{ route('admin.documentos.create',['ref'=>$proyecto->id, 'tipo' => $tipo->id]) }}" title="Nuevo documento" class="btn btn-default btn-sm">Nuevo documento</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse1" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                @foreach($tipo->documentosByProyecto($proyecto->id)->get() as $doc)
                                                                <div class="col-lg-3 col-md-3">
                                                                    <a href="{{ asset('files/'.$doc->ruta) }}" target="_blank">
                                                                        {{ $doc->nombre }}
                                                                    </a>
                                                                    <br>
                                                                    {{ $doc->uploadBy->nombres }} {{ $doc->uploadBy->apellidos }} 
                                                                    <br>
                                                                    {{ $doc->created_at }}
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <div class="panel-footer">
                                                            <small>
                                                                {{ $tipo->descripcion }}
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach



                                        </div>
                                        <div class="tab-pane active" id="laboratorios">
                                            
                                        </div>
                                        <div class="tab-pane active" id="equipos">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script>

    $(document).ready(function(){

    });
</script>
@endsection