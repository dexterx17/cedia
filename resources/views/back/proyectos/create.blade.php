@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans_choice('front.proyectos',1))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection
@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.proyectos.index') }}" title="{{ trans_choice('front.proyectos',10) }}">{{ trans_choice('front.proyectos',10) }}</a>
        </li>
        <li>{{ trans('admin.crear') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('front.proyectos',1) }}<small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.proyectos.store')}}" method="POST">
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                                {{ trans('admin.nombre') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="nombre" name="nombre" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">
                                {{ trans('admin.descripcion') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="descripcion" name="descripcion" placeholder="{{ trans('admin.descripcion') }}"></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumen">
                                {{ trans('admin.resumen') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="resumen" name="resumen" placeholder="{{ trans('admin.resumen') }}"></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="objetivo_general">
                                {{ trans('admin.objetivo_general') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="objetivo_general" name="objetivo_general" placeholder="{{ trans('admin.objetivo_general') }}"></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="objetivos_especificos">
                                {{ trans('admin.objetivos_especificos') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="objetivos_especificos"  rows="5" name="objetivos_especificos" placeholder="{{ trans('admin.objetivos_especificos') }}"></textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="presupuesto">
                                {{ trans('admin.presupuesto') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="number" class="form-control col-md-7 col-xs-12" id="presupuesto"  rows="5" name="presupuesto" placeholder="{{ trans('admin.presupuesto') }}" />
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="financiamiento">
                                {{ trans('admin.financiamiento') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              {!! Form::select('financiamiento', [
                                'interno' => trans('admin.interno'),
                                'externo' => trans('admin.externo')
                                ],old('financiamiento'),[
                                    'required'=>'required',
                                    'class'=>'form-control col-md-7 col-xs-12'
                                ]) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidades">
                                {{ trans_choice('admin.universidades',10) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('universidades[]',$universidades,'',['id'=>'universidades-list','required'=>'required','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuarios">
                                {{ trans('admin.participantes') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('usuarios[]',$usuarios,'',['id'=>'usuarios-list','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_inicio">
                                {{ trans('admin.periodo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon">Desde</span>
                                    <input type="text" class="input-sm form-control" name="fecha_inicio" />
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" name="fecha_final" />
                                </div>
                            </div>
                        </div>
                       
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('admin.proyectos.index') }}" class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('.input-daterange').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#usuarios-list').chosen({
            placeholder_text_single:"Selecciona uno o varios autores",
            width:'100%'
        });
        $('#universidades-list').chosen({
            placeholder_text_single:"Selecciona una o varias universidades",
            width:'100%'
        });
    </script>
@endsection