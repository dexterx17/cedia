@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans_choice('front.proyectos',1))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection
@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.proyectos.index') }}" title="{{ trans_choice('front.proyectos',10) }}">{{ trans_choice('front.proyectos',10) }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('front.proyectos',1) }}<small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="" id="tabs" data-example-id="togglable-tabs" role="tabpanel">
                        <!-- required for floating -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#infor">
                                   {{ trans('admin.informacion') }}
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#imagenes">
                                   {{ trans('admin.imagenes') }}
                                   <span class="badge" id="n_imagenes">{{ $proyecto->imagenes()->count() }}</span>
                                </a>
                            </li>
                        </ul>
                    
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="infor">
                                <!-- content starts here -->
                                <form class="form-horizontal form-label-left" action="{{route('admin.proyectos.update',$proyecto->id)}}" method="POST">
                                {{ csrf_field() }}
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                                            {{ trans('admin.nombre') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{  $proyecto->nombre }}" id="nombre" name="nombre" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">
                                            {{ trans('admin.descripcion') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12" id="descripcion" name="descripcion" placeholder="{{ trans('admin.descripcion') }}">{{  $proyecto->descripcion }}</textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumen">
                                            {{ trans('admin.resumen') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12" id="resumen" name="resumen" placeholder="{{ trans('admin.resumen') }}">{{  $proyecto->resumen }}</textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="objetivo_general">
                                            {{ trans('admin.objetivo_general') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12" id="objetivo_general" name="objetivo_general" placeholder="{{ trans('admin.objetivo_general') }}">{{  $proyecto->objetivo_general }}</textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="objetivos_especificos">
                                            {{ trans('admin.objetivos_especificos') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <textarea class="form-control col-md-7 col-xs-12" id="objetivos_especificos"  rows="5" name="objetivos_especificos" placeholder="{{ trans('admin.objetivos_especificos') }}">{{ $proyecto->objetivos_especificos }}</textarea>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="presupuesto">
                                            {{ trans('admin.presupuesto') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" id="presupuesto"  rows="5" name="presupuesto" placeholder="{{ trans('admin.presupuesto') }}" value="{{ $proyecto->presupuesto }}" />
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="financiamiento">
                                            {{ trans('admin.financiamiento') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                        {!! Form::select('financiamiento', [
                                            'interno' => trans('admin.interno'),
                                            'externo' => trans('admin.externo')
                                            ],$proyecto->financiamiento,[
                                                'required'=>'required',
                                                'class'=>'form-control col-md-7 col-xs-12'
                                            ]) !!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidades">
                                            {{ trans_choice('admin.universidades',10) }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            {!! Form::select('universidades[]',$universidades,$proyecto->universidades->lists('id')->all(),['id'=>'universidades-list','required'=>'required','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuarios">
                                            {{ trans('admin.participantes') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            {!! Form::select('usuarios[]',$usuarios,$proyecto->usuarios->lists('id')->all(),['id'=>'usuarios-list','required'=>'required','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_inicio">
                                            {{ trans('admin.periodo') }}
                                        </label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="input-daterange input-group" id="datepicker">
                                                <span class="input-group-addon">Desde</span>
                                                <input type="text" class="input-sm form-control" name="fecha_inicio" value="{{ $proyecto->fecha_inicio }}"/>
                                                <span class="input-group-addon">hasta</span>
                                                <input type="text" class="input-sm form-control" name="fecha_final" value="{{ $proyecto->fecha_final }}" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ln_solid">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <a href="{{ route('admin.proyectos.index') }}" class="btn btn-primary" >
                                                {{ trans('admin.cancelar') }}
                                            </a>
                                            <button class="btn btn-success" id="send" type="submit">
                                                {{ trans('admin.guardar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane" id="imagenes">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>
                                                </h2>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" id="container-form-imagenes">
                                                        
                                                    </div>
                                                </div>
                                                <p>
                                                    Imágenes de <b>{{ $proyecto->nombre }}</b> <small>({{ trans('admin.maximo_imagenes',['num'=>3]) }})</small>
                                                </p>
                                                <div class="row">
                                                    @foreach($proyecto->imagenes()->get() as $imagen )
                                                        @include('back.template.image_block', ['imagen'=>$imagen])
                                                    @endforeach
                                                    <div class="clearfix">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="x_content">
                                                <form action="{{ route('admin.imagenes.store',$proyecto->id) }}" class="dropzone">
                                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                                <input type="hidden" name="referencia" value="proyectos">
                                                </form>
                                                <br/>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--row-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">


        $(document).ready(function(){
            $('.dropzone').dropzone({
                'maxFilesize':1,
                'maxFiles':3,
                'paramName':'imagen',
                'acceptedFiles':'image/*',
                sending:function(file, xhr, formData){
                    alert('{{ Session::token() }}');
                    formData.append("_token", '{{ Session::token() }}');
                    formData.append("referencia", 'proyectos');
                },
                complete:function(a,b){
                    if(a.status=="success"){
                        new PNotify({
                              title: '{{ trans("admin.accion_exitosa") }}',
                              text: '{{ trans("admin.imagen_cargada_ok") }}!',
                              type: 'success',
                              styling: 'bootstrap3'
                        });
                        $('#n_imagenes').text(parseInt($('#n_imagenes').text())+1);
                    }
                },
                error:function(a,b){
                    if(a.status=="error"){
                         new PNotify({
                              title: 'Error!',
                              text: b,
                              type: 'error',
                              styling: 'bootstrap3'
                          });
                    }
                }
            });


        });
        $(document).on('click','.btn-delete',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            swal({
              title: "{!! trans('admin.estas_seguro') !!}?",
              text: "{!! trans('admin.esta_accion_no') !!}!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
              cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
              closeOnConfirm: false,
              closeOnCancel: true
            },
            function(isConfirm){
              if (isConfirm) {
                $.get(url,{},function(data){
                    if(data.success){
                        $('#video'+data.id).fadeOut('slow');
                        swal("Eliminado!", "Eliminado correctamente", "success");
                    }else{
                        swal("Error", "Error al eliminar", "error");
                    }
                },'json');
              }
            });
        });
        $('.input-daterange').datepicker({
            format: 'yyyy-mm-dd'
        });
        $('#usuarios-list').chosen({
            placeholder_text_single:"Selecciona uno o varios autores",
            width:'100%'
        });
        $('#universidades-list').chosen({
            placeholder_text_single:"Selecciona una o varias universidades",
            width:'100%'
        });
    </script>
@endsection