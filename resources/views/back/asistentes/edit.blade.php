@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans('admin.usuario'))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.asistentes.index') }}" title="{{ trans('admin.investigadores') }}">{{ trans('admin.investigadores') }}</a>
        </li>
        <li>{{ trans('admin.crear') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('admin.asistente') }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Ups! </strong> Ha ocurrido un error con tus datos.<br>
                        <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.asistentes.update',$usuario->id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombres">
                                {{ trans('admin.nombres') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->nombres }}" id="nombres" name="nombres" placeholder="{{ trans('admin.nombres') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellidos">
                                {{ trans('admin.apellidos') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->apellidos }}" id="apellidos" name="apellidos" placeholder="{{ trans('admin.apellidos') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidad_id">
                                {{ trans('admin.universidad') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('universidad_id',$universidades,$usuario->universidad_id,['required'=>'required','class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="carrera">
                                {{ trans('admin.carrera') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->carrera }}" id="carrera" name="carrera" placeholder="{{ trans('admin.carrera') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="area_investigacion">
                                {{ trans_choice('admin.areas_de_investigacion',10) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12"> 
                            <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->area_investigacion }}" id="area_investigacion" name="area_investigacion" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pasante">
                                {{ trans('admin.pasante') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" @if($usuario->pasante) checked @endif id="pasante" name="pasante" type="checkbox">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pasante_desde">
                                {{ trans('admin.periodo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon">Desde</span>
                                    <input type="text" class="input-sm form-control" name="pasante_desde" value="{{ $usuario->pasante_desde }}" />
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" name="pasante_hasta" value="{{ $usuario->pasante_hasta }}" />
                                </div>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tesista">
                                {{ trans('admin.tesista') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" @if($usuario->tesista) checked @endif id="tesista" name="tesista" type="checkbox">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tesista_desde">
                                {{ trans('admin.periodo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon">Desde</span>
                                    <input type="text" class="input-sm form-control" name="tesista_desde" value="{{ $usuario->tesista_desde }}"/>
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" name="tesista_hasta" value="{{ $usuario->tesista_hasta }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="about_me">
                                {{ trans('admin.about_me') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea id="about_me" name="about_me" placeholder="{{ trans('admin.about_me') }}" class="form-control col-md-7 col-xs-12" >{{ $usuario->about_me }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                                {{ trans('admin.email') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->email }}" id="email" name="email" type="email">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">
                                {{ trans('admin.telefono') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->telefono }}" id="telefono" name="telefono" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="skype">
                                {{ trans('admin.skype') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $usuario->skype }}" id="skype" name="skype" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="imagen">
                                {{ trans('admin.imagen') }}
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                {!! Form::file('imagen',['class'=>'form-control','placeholder'=>'Imagen']) !!}
                            </div>
                            @if($usuario->imagen)
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <img src="{{ asset('img/pics/'.$usuario->imagen) }}" alt="Imagen">
                            </div>
                            @endif
                        </div>                        
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a  href="{{ route('admin.asistentes.index') }}"class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('.input-daterange').datepicker({
            format: 'yyyy-mm-dd',
        });
        $('#universidades-list').chosen({
            placeholder_text_single:"Selecciona una o varias universidades",
            width:'100%'
        });
    </script>
@endsection