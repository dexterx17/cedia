@extends('back.template.base')

@section('title',trans('admin.asistentes_investigadores').' admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/libs/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans('admin.asistentes_investigadores') }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans('admin.asistentes_investigadores') }}   <small> {{ trans('admin.listado') }} </small>

                        <a href="{{ route('admin.asistentes.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans('admin.asistente') }}</a>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    
                    <!-- start asistentes list -->
                    <table class="table table-striped projects sorted_table"  id="myTable">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    {{ trans('admin.nombre') }}
                                </th>
                                <th>
                                    {{ trans('admin.rol') }}
                                </th>
                                <th>
                                    {{ trans('admin.universidad') }} <br>
                                    <small>{{ trans('admin.carrera') }}</small>
                                </th>
                                <th>
                                    {{ trans('admin.areas_de_investigacion') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($asistentes_data as $index=>$usuar)
                            <tr id="user{{$usuar->id}}" data-user="{{$usuar->id}}">
                                <td>
                                    {{ $index+1 }} 
                                     <i class="fa fa-arrows movible"></i>
                                </td>
                                <td>
                                    <a href="{{ route('user_info',$usuar->id) }}">
                                        {{ $usuar->nombres }} {{ $usuar->apellidos }}
                                        <br/>
                                        <small>
                                            {{ $usuar->email}}
                                        </small>
                                    </a>
                                </td>
                                <td>
                                    @if($usuar->pasante)
                                        Pasante: {{ $usuar->pasante_desde }} / {{ $usuar->pasante_hasta }}</br>
                                    @endif
                                    @if($usuar->tesista)
                                        Tesista: {{ $usuar->tesista_desde }} / {{ $usuar->tesista_hasta }}
                                    @endif
                                </td>
                                <td>
                                    {{ $usuar->universidad->nombre }} <br>
                                    <small>{{ $usuar->carrera }}</small>
                                </td>
                                <td>
                                    {{ $usuar->area_investigacion }}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" target="_blank" href="{{ route('user_info',$usuar->id) }}">
                                        <i class="fa fa-folder">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.asistentes.edit',$usuar->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.asistentes.destroy',$usuar->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    <!-- end asistentes list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script src="{{ asset('js/libs/jquery-sortable-min.js') }}"></script>
<script src="{{ asset('js/libs/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready(function(){
        var orden = [];
        // Sortable rows
        var group = $('.sorted_table').sortable({
                group: 'serialization',
                containerSelector: 'table',
                itemPath: '> tbody',
                itemSelector: 'tr',
                handle: 'i.fa-arrows',
                placeholder: '<tr class="placeholder"/>',
                serialize: function ($parent, $children, parentIsContainer) {
                    var result = $.extend({}, $parent.data());
                   // console.log($parent.html());
                    if($parent.data('user'))
                        orden.push({'orden':($parent.index()+1),'user':$parent.data('user')});
                },
                onDrop: function ($item, container, _super) {
                    orden = [];
                    var data = group.sortable("serialize").get();
                    $.post('{{ route("admin.asistentes.reordenar") }}',{'info':orden,_token: '{{ Session::token() }}'},function(response){
                        console.log(response);
                        console.log('response');
                        if(!response.error){
                            swal({
                                title:"OK",
                                text: "Actualizado correctamente!",
                                icon:"success",
                                buttons: false,
                                timer: 500
                            });
                        }else{
                            swal("Error", "Error al actualizar", "error");
                        }
                    });
                    _super($item, container);
                  }
            });
        });
        $(document).ready( function () {
            $('#myTable').DataTable();
        } );
</script>
@endsection