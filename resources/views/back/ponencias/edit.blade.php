@extends('back.template.base')

@section('title',trans('admin.editar').' '.trans_choice('admin.ponencias',1))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.ponencias.index') }}" title="{{ trans_choice('admin.ponencias',10) }}">{{ trans_choice('admin.ponencias',10) }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('admin.ponencias',1) }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.ponencias.update', $ponencia->id)}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tema">
                                {{ trans('admin.tema') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="tema" value="{{ $ponencia->tema }}" name="tema" placeholder="{{ trans('admin.tema') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="evento">
                                {{ trans('admin.evento') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="evento" value="{{ $ponencia->evento }}" name="evento" placeholder="{{ trans('admin.evento') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lugar">
                                {{ trans('admin.lugar') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="lugar" value="{{ $ponencia->lugar }}" name="lugar" placeholder="{{ trans('admin.lugar') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="participantes">
                                {{ trans('admin.participantes') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input id="participantes" name="participantes" value="{{ $ponencia->participantes }}" placeholder="{{ trans('admin.participantes') }}" class="form-control col-md-7 col-xs-12" type="text"></input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo">
                                {{ trans('admin.cargo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="cargo" value="{{ $ponencia->cargo }}" name="cargo" placeholder="{{ trans('admin.cargo') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumen">
                                {{ trans('admin.resumen') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="resumen" name="resumen" required="required">{{ $ponencia->resumen }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha">
                                {{ trans('admin.fecha') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-group date">
                                    <input type="text" class="form-control" name="fecha" id="fecha" value="{{ $ponencia->fecha }}">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="financiamiento">
                                {{ trans('admin.financiamiento') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" value="{{ $ponencia->financiamiento }}" id="financiamiento" name="financiamiento" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a  href="{{ route('admin.ponencias.index') }}"class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    
<script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('#fecha').datepicker({
              format: 'yyyy-mm-dd',
        });
    

</script>
@endsection