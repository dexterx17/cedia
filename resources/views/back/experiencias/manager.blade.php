<div id="experiencia-container">
	
</div>
<table class="table table-striped projects">
	<caption>Listado de experiencia laboral <button type="button" class="btn btn-success add-item-form" parent="{{ $usuario->id }}" container="experiencia-container" url="{{ route('admin.experiencias.create') }}">Nueva experiencia laboral</button></caption>
	<thead>
		<tr>
			<th width="15%">Cargo</th>
			<th>Institución</th>
			<th>Tiempo</th>
			<th>Fecha</th>
			<th>Administrativo</th>
			<th>Ubicación</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($experiencias as $experiencia)
		<tr item="item{{ $experiencia->id }}">
			<td>{{ $experiencia->cargo }}</td>
			<td>
				{{ $experiencia->institucion }}
				<br>
				<small>{{ $experiencia->departamento }}</small>
			</td>
			<td>{{ $experiencia->tiempo }}</td>
			<td>{{ $experiencia->desde }} <br> {{ $experiencia->hasta }} <br> @if($experiencia->administrativo==1) Actualidad @endif</td>
			<td>@if($experiencia->administrativo==0) NO @else SI @endif</td>
			<td>{{ $experiencia->ubicacion }}</td>
			<td class="text-center">
                <a class="btn btn-info btn-xs edit-item-form" href="{{ route('admin.experiencias.edit',$experiencia->id)}}" container="experiencia-container">
                    <i class="fa fa-pencil">
                    </i>
                    {{ trans('admin.editar') }}
                </a>
                <a href="{{route('admin.experiencias.destroy',$experiencia->id)}}" class="btn btn-danger btn-xs delete-item-form">
                    <i class="fa fa-trash-o"></i> 
                    {{ trans('admin.eliminar') }}
                </a>
            </td>
		</tr>
		@endforeach
	</tbody>
</table>