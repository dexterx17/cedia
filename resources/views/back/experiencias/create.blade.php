
<div class="x_panel">
    <div class="x_title">
        <h2>Experiencia laboral <small> {{ trans('admin.informacion') }}</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <!-- content starts here -->
        <form class="form-horizontal form-label-left" action="{{route('admin.experiencias.store')}}" method="POST">
        {{ csrf_field() }}
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cargo">
                    {{ trans('admin.cargo') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="cargo" name="cargo" placeholder="{{ trans('admin.cargo') }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="institucion">
                    {{ trans_choice('front.institucion',1) }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="institucion" name="institucion" placeholder="{{ trans_choice('front.institucion',1) }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="departamento">
                    Departamento
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="departamento" name="departamento" placeholder="Departamento" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ubicacion">
                    Ubicación
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="ubicacion" name="ubicacion" placeholder="Ubicación (Santa Prisca, Quito)" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tiempo">
                    Tiempo
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                      {!! Form::select('tiempo', [
                        'tiempo completo' => trans('admin.tiempo_completo'),
                        'medio tiempo' => trans('admin.medio_tiempo'),
                        ],'',[
                            'required'=>'required',
                            'class'=>'form-control col-md-7 col-xs-12'
                        ]) !!}
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="administrativo">
                    {{ trans('admin.administrativo') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('administrativo', [
                        '0' => trans('admin.no'),
                        '1' => trans('admin.si'),
                        ],'',[
                            'required'=>'required',
                            'class'=>'form-control col-md-7 col-xs-12'
                        ]) !!}
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="funciones">
                    {{ trans('admin.funciones') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12" id="funciones" name="funciones" placeholder="{{ trans('admin.funciones') }}"></textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="desde">
                    {{ trans('admin.fecha_inicio') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group date">
                        <input type="text" class="form-control" name="desde" id="desde">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hasta">
                    {{ trans('admin.fecha_final') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group date">
                        <input type="text" class="form-control" name="hasta" id="hasta">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="actual">
                    Trabajo actual
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    {!! Form::select('actual', [
                        '0' => trans('admin.no'),
                        '1' => trans('admin.si'),
                        ],'',[
                            'required'=>'required',
                            'class'=>'form-control col-md-7 col-xs-12'
                        ]) !!}
                </div>
            </div>
           
            <div class="ln_solid">
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="user_id" value="{{ $user_id }}">
                    <a href="#" class="btn btn-primary cancel-item-form" container="investigacion-container">
                        {{ trans('admin.cancelar') }}
                    </a>
                    <button class="btn btn-success" id="send" type="submit">
                        {{ trans('admin.guardar') }}
                    </button>
                </div>
            </div>
        </form>
        <!-- content ends here -->
    </div>
</div>
        
