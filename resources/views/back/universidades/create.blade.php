@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans('admin.universidad'))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection
@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.universidades.index') }}" title="{{ trans('admin.universidades') }}">{{ trans_choice('admin.universidades',10) }}</a>
        </li>
        <li>{{ trans('admin.crear') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('admin.universidades',1) }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.universidades.store')}}" method="POST" enctype="multipart/form-data" >
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                                {{ trans('admin.nombre') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="nombre" name="nombre" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="abreviacion">
                                {{ trans('admin.abreviacion') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="abreviacion" name="abreviacion" placeholder="{{ trans('admin.abreviacion') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">
                                {{ trans('admin.url') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="url" name="url" placeholder="{{ trans('admin.url') }}" required="required" type="url">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">
                                {{ trans('admin.direccion') }}
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="direccion" name="direccion" placeholder="{{ trans('admin.direccion') }}" ></textarea>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">
                                {{ trans('admin.telefono') }}
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="telefono" name="telefono" placeholder="{{ trans('admin.telefono') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="visible">
                                {{ trans('admin.visible') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" @if(old('visible')) checked @endif id="visible" name="visible" type="checkbox">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="logo">
                                {{ trans('admin.logo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::file('logo',['class'=>'form-control','placeholder'=>'logo']) !!}
                            </div>
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('admin.universidades.index') }}" class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('#fecha_universidad').datepicker({
              format: 'yyyy-mm-dd',
        });
         $('#usuarios-list').chosen({
            placeholder_text_single:"Selecciona uno o varios autores",
            width:'100%'
        });
    </script>
@endsection