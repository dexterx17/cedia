@extends('back.template.base')

@section('title',trans('front.universidades').' admin')

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans_choice('admin.universidades',10) }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans_choice('admin.universidades',10) }}   <small> {{ trans('admin.listado') }} </small>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <a href="{{ route('admin.universidades.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans_choice('admin.universidades',1) }}</a>
                    <!-- start universidades list -->
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th>
                                    {{ trans('admin.logo') }}
                                </th>
                                <th style="width: 20%">
                                    {{ trans('admin.nombre') }} <small>{{ trans('admin.abreviacion') }}</small>
                                </th>
                                <th>
                                    {{ trans('admin.direccion') }}
                                </th>
                                <th>
                                    {{ trans('admin.telefono') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($universidades_data as $index=>$pub)
                            <tr id="user{{$pub->id}}">
                                <td>
                                    {{ $index+1 }}
                                </td>
                                <td>
                                    <img src="{{ asset('img/u/'.$pub->logo) }}" alt="{{ $pub->nombre }}" width="120" height="100">
                                </td>
                                <td>
                                    <a href="{{ $pub->url }}" target="_blank">
                                        {{ $pub->nombre }}
                                        <br/>
                                        <small>
                                            {{ $pub->abreviacion}}
                                        </small>
                                    </a>
                                </td>
                                <td>
                                    {{ $pub->direccion }}
                                </td>
                                <td>
                                    {{ $pub->telefono }}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.universidades.show',$pub->id) }}">
                                        <i class="fa fa-folder">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.universidades.edit',$pub->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.universidades.destroy',$pub->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $universidades_data->render() !!}
                    <!-- end universidades list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready(function(){

    });
</script>
@endsection