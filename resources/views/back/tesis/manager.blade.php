@extends('back.template.base')

@section('title',trans_choice('admin.trabajos_titulacion',10).' admin')


@section('css')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans_choice('admin.trabajos_titulacion',10) }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans_choice('admin.trabajos_titulacion',10) }}   <small> {{ trans('admin.listado') }} </small>
                        <a href="{{ route('admin.titulaciones.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans_choice('admin.trabajos_titulacion',1) }}</a>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- start titulaciones list -->
                    <table class="table table-striped projects" id="myTable">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th>
                                    {{ trans('admin.titulo') }}
                                </th>
                                <th style="width: 20%">
                                    {{ trans('admin.tema') }}
                                </th>
                                <th>
                                    {{ trans_choice('admin.estudiantes',10) }}
                                </th>
                                <th>
                                    {{ trans_choice('admin.director',1) }} <small>{{ trans_choice('admin.codirector',1) }}</small>
                                </th>
                                <th>
                                    {{ trans('admin.estado') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($tesis_data as $index=>$pub)
                            <tr id="user{{$pub->id}}">
                                <td>
                                    {{ $index+1 }}
                                </td>
                                <td>
                                    {{ $pub->titulo }}
                                    <small class="label label-info">
                                        {{ $pub->tipo }}
                                    </small>
                                </td>
                                <td>
                                    {{ $pub->tema }}
                                </td>
                                <td>
                                    <ul class="list">
                                    @foreach($pub->tesistas() as $tesista)
                                        <li>
                                            {{ $tesista->nombres }} {{ $tesista->apellidos }} <br>
                                        </li>
                                    @endforeach
                                    </ul>
                                </td>
                                <td>
                                    @if($pub->director)
                                        {{ $pub->director->nombres }} {{ $pub->director->apellidos }}
                                    @endif
                                    <br/>
                                    @if($pub->codirector)
                                    <small>
                                        {{ $pub->codirector->nombres }} {{ $pub->codirector->apellidos }}
                                    </small>
                                    @endif
                                </td>
                                <td>
                                    {{ $pub->estado }}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.titulaciones.show',$pub->id) }}">
                                        <i class="fa fa-folder">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.titulaciones.edit',$pub->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.titulaciones.destroy',$pub->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $tesis_data->render() !!}
                    <!-- end titulaciones list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready( function () {
        $('#myTable').DataTable();
    });
</script>
@endsection