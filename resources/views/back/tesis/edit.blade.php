@extends('back.template.base')

@section('title',trans('admin.crear').' '.trans_choice('admin.trabajos_titulacion',1))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection
@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.titulaciones.index') }}" title="{{ trans_choice('front.trabajos_titulacion',10) }}">{{ trans_choice('admin.trabajos_titulacion',10) }}</a>
        </li>
        <li>{{ trans('admin.crear') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans_choice('admin.trabajos_titulacion',1) }}<small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.titulaciones.update',$tesis->id)}}" method="POST">
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">
                                {{ trans('admin.tipo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              {!! Form::select('tipo', [
                                'doctorado' => trans('admin.doctorado'),
                                'maestria' => trans('admin.maestria'),
                                'pregrado' => trans('admin.pregrado')
                                ],$tesis->tipo,[
                                    'required'=>'required',
                                    'class'=>'form-control col-md-7 col-xs-12'
                                ]) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">
                                {{ trans('admin.titulo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="titulo" value="{{ $tesis->titulo }}" name="titulo" placeholder="{{ trans('admin.titulo') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tema">
                                {{ trans('admin.tema') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="tema" value="{{ $tesis->tema }}" name="tema" placeholder="{{ trans('admin.tema') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="resumen">
                                {{ trans('admin.resumen') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" id="resumen" name="resumen" placeholder="{{ trans('admin.resumen') }}">{{ $tesis->resumen }}</textarea>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="universidades">
                                {{ trans_choice('admin.universidades',1) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('universidad_id',$universidades,$tesis->universidad_id,['id'=>'universidades-list','required'=>'required', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuarios">
                                {{ trans_choice('admin.estudiantes',5) }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('estudiantes[]',$estudiantes,'',['id'=>'usuarios-list','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="director_id">
                                {{ trans_choice('admin.director',1) }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('director_id',$investigadores,$tesis->director_id,['id'=>'director_id','class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codirector_id">
                                {{ trans_choice('admin.codirector',1) }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('codirector_id',$investigadores,$tesis->codirector_id,['id'=>'codirector_id','class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="desde">
                                {{ trans('admin.periodo') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-daterange input-group" id="datepicker">
                                    <span class="input-group-addon">Desde</span>
                                    <input type="text" class="input-sm form-control" value="{{ $tesis->desde }}" name="desde" />
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" value="{{ $tesis->hasta }}" name="hasta" />
                                </div>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="estado">
                                {{ trans('admin.estado') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              {!! Form::select('estado', [
                                'desarrollo' => trans('admin.en_desarrollo'),
                                'defendida' => trans('admin.defendida'),
                                'publicada' => trans('admin.publicada')
                                ],$tesis->estado,[
                                    'required'=>'required',
                                    'class'=>'form-control col-md-7 col-xs-12'
                                ]) !!}
                            </div>
                        </div>
                       
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('admin.titulaciones.index') }}" class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $('.input-daterange').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#usuarios-list').chosen({
            placeholder_text_single:"Selecciona uno o varios autores",
            width:'100%'
        });

        $('#director_id, #codirector_id').chosen({
            placeholder_text_single:"Selecciona un docente",
            width:'100%'
        });

        $('#universidades-list').chosen({
            placeholder_text_single:"Selecciona una o varias universidades",
            width:'100%'
        });
    </script>
@endsection