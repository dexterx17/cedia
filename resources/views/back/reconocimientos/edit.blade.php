
<div class="x_panel">
    <div class="x_title">
        <h2>Reconocimiento <small> {{ trans('admin.informacion') }}</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <!-- content starts here -->
        <form class="form-horizontal form-label-left" action="{{route('admin.reconocimientos.update',$reconocimiento->id)}}" method="POST">
        {{ csrf_field() }}
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="reconocimiento">
                    {{ trans_choice('admin.reconocimientos',1) }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="reconocimiento" value="{{ $reconocimiento->reconocimiento }}" name="reconocimiento" placeholder="{{ trans_choice('admin.reconocimientos',1) }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ubicacion">
                    {{ trans_choice('admin.ubicacion',1) }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="ubicacion" value="{{ $reconocimiento->ubicacion }}" name="ubicacion" placeholder="{{ trans_choice('admin.ubicacion',1) }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">
                    {{ trans('admin.descripcion') }}
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <textarea class="form-control col-md-7 col-xs-12" id="descripcion" name="descripcion" placeholder="{{ trans('admin.descripcion') }}" >{{ $reconocimiento->descripcion }}</textarea>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha">
                    {{ trans('admin.fecha') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="input-group date">
                        <input type="text" class="form-control" value="{{ $reconocimiento->fecha }}" name="fecha" id="desde">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="ln_solid">
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="hidden" name="user_id" value="{{ $reconocimiento->user_id }}">
                    <a href="#" class="btn btn-primary cancel-item-form" container="reconocimiento-container">
                        {{ trans('admin.cancelar') }}
                    </a>
                    <button class="btn btn-success" id="send" type="submit">
                        {{ trans('admin.guardar') }}
                    </button>
                </div>
            </div>
        </form>
        <!-- content ends here -->
    </div>
</div>