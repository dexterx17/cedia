<div id="reconocimiento-container">
	
</div>
<table class="table table-striped projects">
	<caption>Listado de reconocimientos <button type="button" class="btn btn-success add-item-form" parent="{{ $usuario->id }}" container="reconocimiento-container" url="{{ route('admin.reconocimientos.create') }}">Nuevo reconocimiento</button></caption>
	<thead>
		<tr>
			<th width="15%">Fecha</th>
			<th>Reconocimiento</th>
			<th>Ubicacion</th>
			<th>Descripción</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($reconocimientos as $reconocimiento)
		<tr item="item{{ $reconocimiento->id }}">
			<td>{{ $reconocimiento->fecha }}</td>
			<td>{{ $reconocimiento->reconocimiento }}</td>
			<td>{{ $reconocimiento->ubicacion }}</td>
			<td>{{ $reconocimiento->descripcion }}</td>
			<td class="text-center">
                <a class="btn btn-info btn-xs edit-item-form" href="{{ route('admin.reconocimientos.edit',$reconocimiento->id)}}" container="reconocimiento-container">
                    <i class="fa fa-pencil">
                    </i>
                    {{ trans('admin.editar') }}
                </a>
                <a href="{{route('admin.reconocimientos.destroy',$reconocimiento->id)}}" class="btn btn-danger btn-xs delete-item-form">
                    <i class="fa fa-trash-o"></i> 
                    {{ trans('admin.eliminar') }}
                </a>
            </td>
		</tr>
		@endforeach
	</tbody>
</table>