@extends('back.template.base')

@section('title',trans('admin.editar').' '.trans('admin.publicacion'))

@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.publicaciones.index') }}" title="{{ trans('admin.publicaciones') }}">{{ trans('admin.publicaciones') }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('admin.publicacion') }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <form class="form-horizontal form-label-left" action="{{route('admin.publicaciones.update',$publicacion->id)}}" method="POST">
                    {{ csrf_field() }}
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="titulo">
                                {{ trans('admin.titulo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="titulo" value="{{ $publicacion->titulo }}" name="titulo" placeholder="{{ trans('admin.titulo') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="autores">
                                {{ trans('admin.autores') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="autores" value="{{ $publicacion->autores }}" name="autores" placeholder="{{ trans('admin.autores') }}" required="required" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="usuarios">
                                {{ trans('admin.usuarios') }}
                                <span class="required">
                                    * 
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('usuarios[]',$usuarios,$publicacion->users()->lists('users.id')->all(),['id' => 'usuarios-list','required'=>'required','multiple'=>'multiple', 'class'=>'form-control col-md-7 col-xs-12']) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rol">
                                {{ trans('admin.tipo') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                {!! Form::select('rol', [
                                    'bookchapter' => trans('admin.bookchapter'),
                                    'cpaper' => trans('admin.cpaper'),
                                    'jpaper' => trans('admin.jpaper'),
                                    'book' => trans('admin.book'),
                                    'magazine' => trans('admin.magazine'),
                                    ],$publicacion->tipo,[
                                        'required'=>'required',
                                        'class'=>'form-control col-md-7 col-xs-12'
                                    ]) !!}
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_publicacion">
                                {{ trans('admin.fecha') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="input-group date">
                                    <input type="text" name="fecha_publicacion" id="fecha_publicacion" class="form-control" value="{{ $publicacion->fecha_publicacion }}" >
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">
                                {{ trans('admin.url') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="url"  value="{{ $publicacion->url }}" name="url" placeholder="{{ trans('admin.url') }}" required="required" type="url">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="abstract">
                                {{ trans('admin.abstract') }}
                                <span class="required">
                                    *
                                </span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea class="form-control col-md-7 col-xs-12" rows="5" id="abstract" name="abstract" placeholder="{{ trans('admin.abstract') }}" required="required">{{ $publicacion->descripcion }}</textarea>
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="conferencia">
                                {{ trans('admin.conferencia') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="conferencia" value="{{ $publicacion->conferencia }}" name="conferencia" placeholder="{{ trans('admin.conferencia') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="revista">
                                {{ trans('admin.revista') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="revista" value="{{ $publicacion->revista }}" name="revista" placeholder="{{ trans('admin.revista') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="paginas">
                                {{ trans('admin.paginas') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="paginas" value="{{ $publicacion->paginas }}"  name="paginas" placeholder="{{ trans('admin.paginas') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="editorial">
                                {{ trans('admin.editorial') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="editorial" value="{{ $publicacion->editorial }}" name="editorial" placeholder="{{ trans('admin.editorial') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="citas">
                                {{ trans('admin.citas') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="citas" value="{{ $publicacion->citas }}" name="citas" placeholder="{{ trans('admin.citas') }}" type="number">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="citas_url">
                                {{ trans('admin.citas_url') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="citas_url" value="{{ $publicacion->citas_url }}" name="citas_url" placeholder="{{ trans('admin.citas_url') }}" type="url">
                                </input>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="scholar_id">
                                {{ trans('admin.scholar_id') }}
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12" id="scholar_id" value="{{ $publicacion->scholar_id }}" name="scholar_id" placeholder="{{ trans('admin.scholar_id') }}" type="text">
                                </input>
                            </div>
                        </div>
                        <div class="ln_solid">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <a  href="{{ route('admin.publicaciones.index') }}"class="btn btn-primary" >
                                    {{ trans('admin.cancelar') }}
                                </a>
                                <button class="btn btn-success" id="send" type="submit">
                                    {{ trans('admin.guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection
@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>
    <script type="text/javascript" charset="utf-8">
        $('#fecha_publicacion').datepicker({
              format: 'yyyy-mm-dd',
        });
        $('#usuarios-list').chosen({
            placeholder_text_single:"Selecciona uno o varios autores",
            width:'100%'
        });
    </script>
@endsection