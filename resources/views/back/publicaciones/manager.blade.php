@extends('back.template.base')

@section('title',trans('admin.publicaciones').' admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans('admin.publicaciones') }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans('admin.publicaciones') }}   <small> {{ trans('admin.listado') }} </small>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- content starts here -->
                    <a href="{{ route('admin.publicaciones.create') }}" class="btn btn-default btn-primary">{{ trans('admin.crear') }} {{ trans('admin.publicacion') }}</a>
                    <!-- start publicaciones list -->
                    <table class="table table-striped projects" id="myTable">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    {{ trans('admin.nombre') }}
                                </th>
                                <th>
                                    {{ trans('admin.fecha') }}
                                </th>
                                <th>
                                    {{ trans('admin.autores') }}
                                </th>
                                <th>
                                    {{ trans('admin.citaciones') }}
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($publicaciones_data as $index=>$pub)
                            <tr id="user{{$pub->id}}">
                                <td>
                                    {{ $index+1 }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.publicaciones.show',$pub->id) }}">
                                        {{ $pub->titulo }}
                                        <br/>
                                        <small>
                                            {{ $pub->email}}
                                        </small>
                                    </a>
                                </td>
                                <td>
                                    {{ $pub->fecha_publicacion }}
                                </td>
                                <td>
                                    @foreach($pub->users as $us)
                                        {{ $us->nombres }} {{ $us->apellidos }},
                                    @endforeach
                                </td>
                                <td>
                                    {{ $pub->citas }}
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="{{ route('admin.publicaciones.show',$pub->id) }}">
                                        <i class="fa fa-folder">
                                        </i>
                                        {{ trans('admin.ver') }}
                                    </a>
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.publicaciones.edit',$pub->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.publicaciones.destroy',$pub->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $publicaciones_data->render() !!}
                    <!-- end publicaciones list -->
                    <!-- content ends here -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>
@endsection