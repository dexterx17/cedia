<div class="x_panel">
    <div class="x_title">
        <h2>{{ trans('admin.imagen') }} <small>{{ trans('admin.informacion') }}</small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
         <!-- content starts here -->
        <form class="form-horizontal form-label-left" action="{{route('admin.imagenes.update',$imagen->id)}}" method="POST">
        {{ csrf_field() }}
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                    {{ trans('admin.nombre') }}
                    <span class="required">
                        *
                    </span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="nombre" name="nombre" value="{{ $imagen->nombre }}" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="autor">
                    {{ trans('admin.autor') }}
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="autor" value="{{ $imagen->autor }}" name="autor" type="text">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="creditos">
                    {{ trans('admin.creditos') }}
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" id="creditos" name="creditos" value="{{ $imagen->creditos }}" placeholder="{{ trans('admin.creditos_imagen') }}">
                    </input>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="destacado">
                    {{ trans('admin.destacado') }}
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::checkbox('destacado',1,$imagen->destacada,['class'=>'form-control col-md-7 col-xs-12','id'=>'destacado']) !!}   
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tresd">
                    {{ trans('admin.tresd') }}
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    {!! Form::checkbox('tresd',1,$imagen->tresd,['class'=>'form-control col-md-7 col-xs-12','id'=>'tresd']) !!}
                    <input type="hidden" name="tabla_referencia" value="{{ $imagen->tabla_referencia }}" />   
                    <input type="hidden" name="id_referencia" value="{{ $imagen->id_referencia }}" />   
                </div>
            </div>
            <div class="ln_solid">
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <a  href="{{ URL::previous() }}"class="btn btn-primary" >
                        {{ trans('admin.cancelar') }}
                    </a>
                    <button class="btn btn-success" id="send" type="submit">
                        {{ trans('admin.guardar') }}
                    </button>
                </div>
            </div>
        </form>
        <!-- content ends here -->
    </div>
</div>