@extends('back.template.base')

@section('title',trans('admin.usuarios').' admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/libs/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">
    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>{{ trans('admin.participantes') }}</li>
    </ul>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans('admin.participantes') }}   <small> {{ trans('admin.listado') }} </small>

                        <small class="pull-right">
                            <ul class="list">
                                <li>Presencial <i class="badge">{{ $presencial }}</i></li>
                                <li>Videoconferencia <i class="badge">{{ $videoconferencia }}</i></li>
                            </ul>
                        </small>
                    </h2>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="x_content">
                    <!-- start usuarios list -->
                    <table class="table table-striped projects" id="myTable">
                        <thead>
                            <tr>
                                <th style="width: 5%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    {{ trans('admin.nombre') }}
                                </th>
                                <th>
                                    {{ trans('admin.email') }}
                                </th>
                                <th>
                                    {{ trans('admin.ci') }}
                                </th>
                                <th>
                                    {{ trans('admin.modalidad') }}
                                </th>
                                <th>
                                    Fecha de registro
                                </th>
                                <th>
                                    Institución
                                </th>
                                <th>
                                    Elegíd@
                                </th>
                                <th style="width: 25%" class="text-center">
                                    {{ trans('admin.acciones')}}
                                </th>
                            </tr>
                        </thead>
                        <tbody >
                        	@foreach($usuarios_data as $index=>$usuar)
                            <tr id="user{{$usuar->id}}" data-user="{{$usuar->id}}">
                                <td>
                                    {{ $usuar->id }} 
                                </td>
                                <td>
                                    {{ $usuar->nombre }} {{ $usuar->apellido }}
                                </td>
                                <td>
                                    {{ ucfirst($usuar->email) }}
                                </td>
                                <td>
                                    {{ $usuar->ci }}
                                </td>
                                <td>
                                    {{ $usuar->tipo }}
                                </td>
                                <td>
                                    {{ $usuar->created_at }}
                                </td>
                                <td>
                                    {{ $usuar->institucion}} <br>
                                    <small>{{ $usuar->cargo }}</small>
                                </td>
                                <td>
                                    <input type="checkbox" value="si" participante="{{$usuar->id}}" class="elegir" @if($usuar->elegido) checked @endif>
                                    <br>
                                    <span style="display:none;">
                                        @if($usuar->elegido) elegido @endif
                                    </span>
                                </td>
                                <td class="text-center">
                                    <a class="btn btn-info btn-xs" href="{{ route('admin.participantes.edit',$usuar->id)}}">
                                        <i class="fa fa-pencil">
                                        </i>
                                        {{ trans('admin.editar') }}
                                    </a>
                                    <a href="{{route('admin.participantes.destroy',$usuar->id)}}" class="btn btn-danger btn-xs btn-delete">
                                        <i class="fa fa-trash-o"></i> 
                                        {{ trans('admin.eliminar') }}
                                    </a>
                                </td>
                            </tr>
                        	@endforeach
                        </tbody>
                    </table>
                    {!! $usuarios_data->render() !!}
                    <!-- end usuarios list -->
                    <!-- content ends here -->
                </div>
                <br><br>
            </div>
        </div>
    </div>
    <!-- /.row -->
           
</div>
@endsection

@section('js')
<script src="{{ asset('js/libs/jquery-sortable-min.js') }}"></script>
<script src="{{ asset('js/libs/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    $(document).on('change','.elegir',function(e){

        var valor = 0;
        if ($(this).is(':checked')){
            valor = 1;
        }
        var participantes = $(this).attr('participante');
        console.log(valor);
        $.get('{{ route("admin.participantes.elegir","") }}/'+participantes,{'elegido':valor},function(data){
            //alert(data);
        });
        
        
    });
    $(document).on('click','.btn-delete',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        swal({
          title: "{!! trans('admin.estas_seguro') !!}?",
          text: "{!! trans('admin.esta_accion_no') !!}!",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
          cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
          closeOnConfirm: false,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            $.get(url,{},function(data){
                if(data.success){
                    $('#user'+data.id).fadeOut('slow');
                    swal("Eliminado!", "Eliminado correctamente", "success");
                }else{
                    swal("Error", "Error al eliminar", "error");
                }
            },'json');
          }
        });
    });
    $(document).ready( function () {
        $('#myTable').DataTable();
    } );
</script>
@endsection