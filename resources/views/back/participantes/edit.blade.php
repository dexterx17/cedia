@extends('back.template.base')

@section('title',trans('admin.editar').' '.$participante->nombre.' '.$participante->apellido)


@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/chosen.min.css') }}">
@endsection

@section('content')
    @include('back.template.side_menu')

<div id="page-wrapper">

    <ul class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}" title="Dashboard">Dashboard</a>
        </li>
        <li>
            <a href="{{ route('admin.participantes.index') }}" title="{{ trans('admin.investigadores') }}">{{ trans('admin.investigadores') }}</a>
        </li>
        <li>{{ trans('admin.editar') }}</li>
    </ul>
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>{{ trans('admin.investigador') }} <small>{{ trans('admin.informacion') }}</small></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                                <form class="form-horizontal form-label-left" action="{{route('admin.participantes.update',$participante->id)}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ci">
                                           Cédula
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $participante->ci }}" id="ci" name="ci" placeholder="{{ trans('admin.ci') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">
                                            Nombres
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $participante->nombre }}" id="nombre" name="nombre" placeholder="{{ trans('admin.nombre') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="apellido">
                                            Apellidos
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $participante->apellido }}" id="apellido" name="apellido" placeholder="{{ trans('admin.apellido') }}" required="required" type="text">
                                            </input>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">
                                            {{ trans('admin.email') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input class="form-control col-md-7 col-xs-12" value="{{ $participante->email }}" id="email" name="email" placeholder="{{ trans('admin.email') }}" required="required" type="email">
                                            </input>
                                        </div>
                                    </div>
                                   
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">
                                            {{ trans('admin.tipo') }}
                                            <span class="required">
                                                *
                                            </span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">

                                            {!! Form::select('tipo', [
                                                'presencial' => 'Presencial',
                                                'videoconferencia' => 'Videoconferencia',
                                                ],$participante->tipo,[
                                                    'required'=>'required',
                                                    'class'=>'form-control col-md-7 col-xs-12'
                                                ]) !!}

                                        </div>
                                    </div>
                                  
                                    <div class="ln_solid">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <a  href="{{ route('admin.participantes.index') }}"class="btn btn-primary" >
                                                {{ trans('admin.cancelar') }}
                                            </a>
                                            <button class="btn btn-success" id="send" type="submit">
                                                {{ trans('admin.guardar') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                <!-- content ends here -->
                           
                      
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
@endsection

@section('js')
    <script src="{{ asset('js/admin/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('js/libs/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function(){

            $('#participantes-list').chosen({
                placeholder_text_single:"Selecciona uno o varios autores",
                width:'100%'
            });
            $('#universidades-list').chosen({
                placeholder_text_single:"Selecciona una o varias universidades",
                width:'100%'
            });
            $('#intereses-list').chosen({
                placeholder_text_single:"Selecciona una o varias universidades",
                width:'100%'
            });
        });
    </script>
    
    <script  type="text/javascript" charset="utf-8" >
        $(document).on('click','.add-item-form',function(item){

            var url = $(this).attr('url');

            var user_id = $(this).attr('parent');

            var container = $(this).attr('container');

            $.get(url,{'user_id':user_id},function(response){
                $('#'+container).html(response);
                $('#desde').datepicker({
                      format: 'yyyy-mm-dd',
                });
                $('#hasta').datepicker({
                      format: 'yyyy-mm-dd',
                });
            });
        });
        $(document).on('click','.cancel-item-form',function(item){
            var container = $(this).attr('container');
            $('#'+container).fadeIn('slow').html('');
        });
        $(document).on('click','.edit-item-form',function(e){
            e.preventDefault();
            var container = $(this).attr('container');
            var url = $(this).attr('href');

            $.get(url,{},function(response){
                $('#'+container).html(response);
                $('#'+container).find('form input[type="text"]:first').focus();
                $('#desde').datepicker({
                      format: 'yyyy-mm-dd',
                });
                $('#hasta').datepicker({
                      format: 'yyyy-mm-dd',
                });
            });
        });

        $(document).on('click','.delete-item-form',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            var $parent = $(this);
            swal({
              title: "{!! trans('admin.estas_seguro') !!}?",
              text: "{!! trans('admin.esta_accion_no') !!}!",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "{{ trans('admin.si_eliminar') }}!",
              cancelButtonText: "{{ trans('admin.no_cancelar') }}!",
              closeOnConfirm: false,
              closeOnCancel: true
            },
            function(isConfirm){
              if (isConfirm) {
                $.get(url,{},function(data){
                    if(data.success){
                        $parent.parents('[item="item'+data.id+'"]').fadeOut('slow');
                        swal("Eliminado!", "Eliminado correctamente", "success");
                    }else{
                        swal("Error", "Error al eliminar", "error");
                    }
                },'json');
              }
            });
        });
    </script>
@endsection