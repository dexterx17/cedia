<div class="item mix cpaper mix_all" data-year="{{  date('Y', strtotime($pub->fecha_publicacion))}}" style=" display: block; opacity: 1;">
    <div class="pubmain">
        <div class="pubassets">
            <a class="pubcollapse" href="#">
                <i class="fa fa-expand">
                </i>
            </a>
            <a class="tooltips" data-original-title="External link" href="{{ $pub->url }}" target="_blank" title="">
                <i class="fa fa-external-link">
                </i>
            </a>
            <a class="tooltips" data-original-title="Download" href="{{ $pub->url }}" target="_blank" title="">
                <i class="fa fa-cloud-download">
                </i>
            </a>
        </div>
        <h4 class="pubtitle">
            {{ $pub->titulo }}
        </h4>
        <div class="pubauthor">
            <b class="badge badge-primary">
                {{ $pub->fecha_publicacion }}
            </b>
            {{ $pub->autores }}
        </div>
        <div class="pubcite">
            <span class="label label-warning">
                Conference Papers
            </span>
            {{ $pub->conferencia }} |
            <strong>
                {{ $pub->editorial }}
            </strong>
        </div>
    </div>
    <div class="pubdetails">
        <h4>
            Abstract
        </h4>
        <p>
            {{ $pub->descripcion }}
        </p>
    </div>
</div>