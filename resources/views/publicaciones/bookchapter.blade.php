<div class="item mix {{ $pub->tipo }} mix_all" data-year="2011" style=" display: block; opacity: 1;">
    <div class="pubmain">
        <div class="pubassets">
            <a class="pubcollapse" href="#">
                <i class="fa fa-expand">
                </i>
            </a>
            <a class="tooltips" data-original-title="External link" href={{ $pub->url }}" target="_blank" title="">
                <i class="fa fa-external-link">
                </i>
            </a>
        </div>
        <h4 class="pubtitle">
            {{ $pub->titulo }}
        </h4>
        <div class="pubauthor">
            {{ $pub->autores}}
        </div>
        <div class="pubcite">
            <span class="label label-info">
                Book Chapter
            </span>
            Springer; 1st Edition, 29 Sep 2011.  |
            <strong>
                ISBN-10:
            </strong>
            1441964568  |
            <strong>
                ISBN-13:
            </strong>
            978-1441964564
        </div>
    </div>
    <div class="pubdetails">
        <img align="left" alt="image" src="Mauricio%20Iturralde_publicai_files/150x200.png" style="padding:0 30px 30px 0;">
            <h4>
                {{ $pub->titulo }}
            </h4>
            <p>
                Abstract
            </p>
            <p>
                {{ $pub->descripcion}}
            </p>
        </img>
    </div>
</div>