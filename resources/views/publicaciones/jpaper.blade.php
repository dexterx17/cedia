<div class="item mix jpaper mix_all" data-year="2013" style=" display: block; opacity: 1;">
    <div class="pubmain">
        <div class="pubassets">
            <a class="pubcollapse" href="#">
                <i class="fa fa-expand">
                </i>
            </a>
            <a class="tooltips" data-original-title="External link" href="http://link.springer.com/article/10.1007%2Fs11277-013-1086-z" target="_blank" title="">
                <i class="fa fa-external-link">
                </i>
            </a>
            <a class="tooltips" data-original-title="Download" href="http://www.miturralde.com/pdfs/journal-springer-1.pdf" target="_blank" title="">
                <i class="fa fa-cloud-download">
                </i>
            </a>
        </div>
        <h4 class="pubtitle">
            "Resource  Allocation for Real Time Services in LTE Networks: Resource allocation using cooperative game theory and virtual token mechanism"
        </h4>
        <div class="pubauthor">
            <strong>
                Mauricio Iturralde
            </strong>
            , Tara Yahiya, Anne Wei, Andre-Luc Beylot
        </div>
        <div class="pubcite">
            <span class="label label-success">
                Journal Paper
            </span>
            Journal Springer Wireless Personal Communications. 2013
        </div>
    </div>
    <div class="pubdetails">
        <h4>
            Abstract
        </h4>
        <p>
           abstra
        </p>
    </div>
</div>