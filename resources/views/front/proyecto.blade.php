@extends('templates.basic.base')

@section('css')
<!-- Required Core stylesheet -->
<link rel="stylesheet" href="{{ asset('css/glide.core.min.css') }}">

<!-- Optional Theme stylesheet -->
<link rel="stylesheet" href="{{ asset('css/glide.theme.min.css') }}">
<style type="text/css">
    .img-slider{
        min-height: 300px;
        height: 300px;
        width: 100%;
    }
</style>
@endsection

@section('title',$proyecto->nombre)
@section('content')
<!-- Header Section Start -->
<header id="hero-area" style='background: rgba(0, 0, 0, 0) url("@if($proyecto->imagenes()->count()>0) {{ asset('img/uploads/'.$proyecto->imagenes()->inRandomOrder()->first()->ruta )}} @else {{ asset('img/hero-area.jpg') }} @endif') no-repeat scroll 0 0 / cover'>    
    <div class="overlay"></div>
    @include('templates.basic.partials.menu')
    <div class="container">
      <div class="row justify-content-md-center">
          <div class="col-md-10">
              <div class="item active">
                  <div class="contents text-center">
                      <p class="lead wow fadeIn" data-wow-delay="400ms" data-wow-duration="1000ms">
                          Proyecto
                      </p>
                      <h1 class="wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                          {{ $proyecto->nombre }}
                      </h1>
                      <a class="btn btn-common wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms" href="#info" rel="nofollow">
                          {{ trans('front.resumen') }}
                      </a>
                      <div class="banner_bottom_btn wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1000ms">
                          <a class="js-target-scroll" href="#info">
                              <i class="icon-mouse">
                              </i>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</header>
<!-- Header Section End -->
<!-- About Section Start -->
<section class="section" id="info">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="100ms" data-wow-duration="1000ms">
                Resumen
            </h3>
        </div>
        <div class="row">
            <div class="col-md-6 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1000ms">
                @if($proyecto->objetivo_general)
                <h2 class="section-title wow fadeInUp" data-wow-delay="350ms" data-wow-duration="1000ms">
                    Objetivo General
                </h2>
                <blockquote>
                    <p class="text-justify">
                    
                    {!! $proyecto->objetivo_general !!}</p>
                </blockquote>
                @endif
                <blockquote>
                    <p class="text-justify">
                        <strong>Presupuesto: </strong>
                        $ {{ $proyecto->presupuesto }}
                         <small>(Financimiento {{ $proyecto->financiamiento }})</small>
                    </p>
                </blockquote>
            </div>
            <div class="col-md-6 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h4 class="section-subtitle text-center wow fadeInDown" data-wow-delay="100ms" data-wow-duration="1000ms">
                    Galería
                </h4>

                <div class="glide">
                  <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        @foreach($proyecto->imagenes()->get() as $img )
                        <li class="glide__slide">
                            <img src="{{ asset('img/uploads/'.$img->ruta) }}" alt="{{ $img->nombre }}" height="300" class="img-slider" />
                        </li>
                        @endforeach
                    </ul>

                  </div>
                   <div class="glide__arrows" data-glide-el="controls">
                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<">prev</button>
                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">">next</button>
                  </div>    
                </div>
            </div>
        </div>
      
    </div>
</section>
<!-- About Section End -->

<!-- Services Section Start -->
<section class="services section" id="services">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.participantes') }}
            </h3>
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.instituciones') }}
            </h2>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
               <div class="owl-carousel owl-theme touch-slider">
                        
                @foreach($proyecto->universidades as $index=>$u)
                     <div class="item @if($index==0) active @endif">
                        <div class="service-box wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">

                                <img src="{{ asset('img/u/'.$u->logo) }}" alt="{{ $u->nombre }}" width="120" heigth="120">
                            
                            <h3>
                               <a href="#" title="{{ $u->nombre }}">
                                    {{ $u->abreviacion }}
                                </a>
                            </h3>

                        </div>
                    </div>
                @endforeach
                        
                </div>
            </div>
        </div> 

        <div class="section-header">
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans_choice('front.investigadores',$proyecto->usuarios->count()) }}
            </h2>
        </div>
        <div class="row">
            @foreach($proyecto->usuarios as $u)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-team wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1000ms">
                    <img alt="{{ $u->nombres }} {{ $u->apellidos }}" src="@if($u->imagen=="") img/team/1.png @else {{ asset('img/pics/'.$u->imagen) }} @endif">
                        <div class="team-content">
                            <h4 class="tem-member">
                                <a href="{{ route('user_info',$u->id) }}" title="{{ $u->nombres }} {{ $u->apellidos }}">
                                    {{ $u->nombres }}
                                    <br>
                                        {{ $u->apellidos }}
                                    </br>
                                </a>
                            </h4>
                            <h6>
                                {{ $u->titulo }}
                            </h6>
                            @if($u->universidades->count()>0)
                            <p>
                                {{ $u->universidad()->nombre }}
                            </p>
                            @endif
                            <!--<ul class="team-social">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-linkedin">
                                        </i>
                                    </a>
                                </li>
                            </ul>-->
                        </div>
                    </img>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- Team Section End  -->


<!-- About Section Start -->
<section class="section" id="info">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="100ms" data-wow-duration="1000ms">
                Resumen
            </h3>
        </div>
        <div class="row">
            <div class="col-md-6 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1000ms">
                <p class="text-justify">
                {!! $proyecto->resumen !!}
                </p>
                <p class="text-justify">
                {!! $proyecto->descripcion !!}
                </p>
                <hr>
            </div>
            <div class="col-md-6 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                @if($proyecto->objetivos_especificos)
                <h2 class="section-title wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms">
                    Objetivos Específicos
                </h2>
                <p class="text-justify"> {!! $proyecto->objetivos_especificos !!}</p>
                @endif
            </div>
        </div>
        <div class="row">
            @foreach($proyecto->imagenes()->get() as $img )
                <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                    <figure class="snip1539">
                        <img src="{{ asset('img/uploads/'.$img->ruta) }}" alt="{{ $img->nombre }}"/>
                    </figure>
                </div>
            @endforeach
        </div>
      
    </div>
</section>
<!-- About Section End -->

<!-- Services Section Start -->
<section class="services section" id="services">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.otros_proyectos') }}
            </h3>
        </div>
        <div class="row">
            <div class="row">
            @foreach($proyectos as $index=>$proyecto)
                <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                        <figure class="snip1493">
                          <div class="image">
                            <img alt="{{ $proyecto->nombre }}" src="@if($proyecto->imagenes()->count()>0) {{ asset('img/uploads/'.$proyecto->imagenes()->inRandomOrder()->first()->ruta )}} @else {{ asset('img/logos/logo_negro.png') }} @endif"/>
                          </div>
                          <figcaption>
                            <div class="date"><span class="day">{{  date('Y', strtotime($proyecto->fecha_final))}}</span><span class="month">{{  date('Y', strtotime($proyecto->fecha_inicio))}}</span></div>
                            <p class="text-justify">
                              {{ $proyecto->nombre }}
                            </p>
                            <footer>
                             @foreach($proyecto->universidades as $u)
                              <div> {{ $u->abreviacion }}</div>
                             @endforeach
                            </footer>
                          </figcaption>
                          <a href="{{ route('proyecto',$proyecto->id)}}"></a>
                        </figure>
                </div>
            @endforeach
        </div>
        </div>
    </div>
</section>
<!-- Services Section End -->
@endsection

@section('js')

<script src="{{ asset('js/glide.min.js') }}"></script>

<script>
  new Glide('.glide').mount();
</script>

@endsection