<section class="services section" id="services">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.que_hacemos') }}
            </h3>
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.areas_investigacion') }}
            </h2>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1456">
                  <img src="{{ asset('img/automatizacion.jpeg') }}" alt="{{ trans('front.automatizacion') }}" />
                  <div class="title">
                    <div>
                      <h4>{{ trans('front.automatizacion') }}</h4>
                    </div>
                  </div>
                  <figcaption>
                    <p><b>GIARSI</b> resuelve problemas de modernización en la industria, que incluyen multiples procesos y permitiendo la introducción de la tecnología en la industria moderna</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1456">
                  <img src="{{ asset('img/robotica.jpeg') }}" alt="{{ trans('front.robotica') }}" />
                  <div class="title">
                    <div>
                      <h4>{{ trans('front.robotica') }}</h4>
                    </div>
                  </div>
                  <figcaption>
                    <p><b>GIARSI</b> es lider en desarrollo de proyectos robóticos, sus áreas de experiencia incluyen vehículos terrestres autónomos, brazos manipuladores y vehículos aereos</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1456">
                  <img src="{{ asset('img/sistemas_inteligentes.jpeg') }}" alt="{{ trans('front.sistemas_inteligentes') }}" />
                  <div class="title">
                    <div>
                      <h4>{{ trans('front.sistemas_inteligentes') }}</h4>
                    </div>
                  </div>
                  <figcaption>
                    <p><b>GIARSI</b> trabaja en el desarrollo de sistemas inteligentes que involucran aplicaciones con machine learning,  algoritmos geneticos, sistemas expertos y más.</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1456">
                  <img src="{{ asset('img/realidad_virtual.jpg') }}" alt="{{ trans('front.realidad_virtual') }}" />
                  <div class="title">
                    <div>
                      <h4>{{ trans('front.realidad_virtual') }}</h4>
                    </div>
                  </div>
                  <figcaption>
                    <p><b>GIARSI</b> desarrolla entornos de realidad virtual y realidad aumentada para las areas  técnica y sociales, cubriendo aplicaciones de entrenamiento, rehabilitación y entretenimiento.</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
        </div>
    </div>
</section>
<section class="services section" id="services">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.que_hacemos') }}
            </h3>
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.areas_investigacion') }}
            </h2>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1361">
                  <img src="{{ asset('img/automatizacion.jpeg') }}" alt="{{ trans('front.automatizacion') }}" />
                  <figcaption>
                      <h3>{{ trans('front.automatizacion') }}</h3>
                        <p><b>GIARSI</b> resuelve problemas de la industria que incluyen la automatización de difirentes sistemas, permitiendo resolver problema de la industria moderna.</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1361">
                  <img src="{{ asset('img/robotica.jpeg') }}" alt="{{ trans('front.robotica') }}" />
                  <figcaption>
                    <h3>{{ trans('front.robotica') }}</h3>
                    <p><b>GIARSI</b> es lider en desarrollo de proyectos robóticos, sus áreas de experiencia incluyen vehículos terrestres autónomos, brazos manipuladores y vehículos aereos.</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1361">
                  <img src="{{ asset('img/sistemas_inteligentes.jpeg') }}" alt="{{ trans('front.sistemas_inteligentes') }}" />
                  <figcaption>
                    <h3>{{ trans('front.sistemas_inteligentes') }}</h3>
                    <p><b>GIARSI</b> trabaja en el desarrollo de sistemas inteligentes que involucran aplicaciones con machine learning,  algoritmos geneticos, sistemas expertos y más.</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                <figure class="snip1361">
                  <img src="{{ asset('img/realidad_virtual.jpg') }}" alt="{{ trans('front.realidad_virtual') }}" />
                  <figcaption>
                    <h3>{{ trans('front.realidad_virtual') }}</h3>
                    <p><b>GIARSI</b> desarrolla entornos de realidad virtual y realidad aumentada para las areas  técnica y sociales, cubriendo aplicaciones de entrenamiento, rehabilitación y entretenimiento</p>
                  </figcaption>
                  <a href="#"></a>
                </figure>
            </div>
        </div>
    </div>
</section>