<div class="why" id="why">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.publicaciones_de_nuestro_equipo') }}
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div data-collapse="" id="pubs">
                    @foreach($publicaciones_group as $anio=>$ps)
                        @if($anio>=2016)
                            <h3 class="section-title wow fadeInUp open" data-wow-delay="{{$anio-1700}}ms" data-wow-duration="1000ms">
                                {{ trans_choice('front.publicaciones',10) }} {{ $anio }}
                            </h3>
                            <div>
                                 <div class="section color-2" id="pub-grid">
                                    <div class="section-container">
                                        <div class=" pubs-slider">
                                            <!-- Paper 0 -->
                                            @foreach($ps as $pub)
                                                    <figure class="snip1291 item">
                                                      <blockquote>{{ $pub->titulo }}</blockquote>
                                                      <div class="author"><img src="http://www.gravatar.com/avatar/" alt="sq-sample7"/>
                                                        <h5>{{ $pub->editorial }}</h5><span>{{ $pub->fecha_publicacion }}</span>
                                                      </div>
                                                    </figure>
                                                
                                            @endforeach
                                            <!-- End papers-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div><!--/row-->
    </div>
</div>
<div class="why" id="why">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.publicaciones_de_nuestro_equipo') }}
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div data-collapse="" id="pubs">
                    @foreach($publicaciones_group as $anio=>$ps)
            @if($anio>=2016)
                    <h3 class="section-title wow fadeInUp" data-wow-delay="{{$anio-1700}}ms" data-wow-duration="1000ms">
                        {{ trans_choice('front.publicaciones',10) }} {{ $anio }}
                    </h3>
                    <div>
                         <div class="section color-2" id="pub-grid">
                                <div class="section-container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pitems" style="   ">
                                                <!-- Paper 0 -->
                                                @foreach($ps as $pub)
                                                    @includeIf('publicaciones.'.$pub->tipo,['pub'=>$pub])
                                                @endforeach

                                                <!-- End papers-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    @endif
          @endforeach
                </div>
            </div>
        </div><!--/row-->
    </div>
</div>