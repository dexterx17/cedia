<!-- Why Section Start -->
<div class="why" id="portfolio">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans_choice('front.proyectos',10) }}
            </h3>
        </div>
        <div class="row">
            @foreach($proyectos as $index=>$proyecto)
            <div class="col-md-4 col-sm-6 col-xs-12 col-lg-4">
                <figure class="snip1493">
                    <div class="image">
                        <img alt="{{ $proyecto->nombre }}" src="@if($proyecto->imagenes()->count()>0) {{ asset('img/uploads/'.$proyecto->imagenes()->inRandomOrder()->first()->ruta )}} @else {{ asset('img/logos/logo_negro.png') }} @endif"/>
                        <!--<img alt="ls-sample1" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/ls-sample1.jpg"/>-->
                    </div>
                    <figcaption>
                        <div class="date">
                            <span class="day">
                                {{  date('Y', strtotime($proyecto->fecha_final))}}
                            </span>
                            <span class="month">
                                {{  date('Y', strtotime($proyecto->fecha_inicio))}}
                            </span>
                        </div>
                        <p class="text-justify">
                            {{ $proyecto->nombre }}
                        </p>
                        <footer>
                            @foreach($proyecto->universidades as $u)
                            <div>
                                {{ $u->abreviacion }}
                            </div>
                            @endforeach
                        </footer>
                    </figcaption>
                    <a href="{{ route('proyecto',$proyecto->id)}}">
                    </a>
                </figure>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Why Section End -->
