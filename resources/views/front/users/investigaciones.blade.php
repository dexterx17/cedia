@extends('templates.one.publications')

@section('title',$user_data->nombres.' '.$user_data->apellidos)

@section('content')

<div id="main">            
    <div id="research" class="page">
        <div class="page-container">
            <div class="pageheader">

                <div class="headercontent">

                    <div class="section-container">
                        <h2 class="title">{{ trans('front.resumen_investigacion') }}</h2>
                    </div>
                </div>
            </div>

            <div class="pagecontents">
                <!--<div class="section color-1">
                    <div class="section-container">
                        <div class="title text-center">
                            <h3>Laboratory Personel</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                
                                <div id="labp-heads-wrap">
                                    
                                    <div id="lab-carousel">
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                        <div><img alt="image" src="img/lab/120x120.png" width="120" height="120" class="img-circle lab-img" /></div>
                                    </div>
                                    <div>
                                        <a href="#" id="prev"><i class="fa fa-chevron-circle-left"></i></a>
                                        <a href="#" id="next"><i class="fa fa-chevron-circle-right"></i></a>
                                    </div>
                                </div>

                                <div id="lab-details">
                                    <div>
                                        <h3>David A. Doe</h3>
                                        <h4>Postdoctoral fellow</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                    <div>
                                        <h3>James Doe</h3>
                                        <h4>Postdoctoral fellow</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                    <div>
                                        <h3>Nadja Sriram</h3>
                                        <h4>Postdoctoral fellow</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                    <div>
                                        <h3>Davide Doe</h3>
                                        <h4>Research Assistant</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                    <div>
                                        <h3>Pauline Doe</h3>
                                        <h4>Summer Intern</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                    <div>
                                        <h3>James Doe</h3>
                                        <h4>Postdoctoral fellow</h4>
                                        <a href="#" class="btn btn-info">+ Follow</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3>Great lab Personel!</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>
                            </div>
                        </div>
                    </div>
                </div>-->

                
                <div class="section color-2" style="padding-top:10px !important;">
                    <div class="section-container">
                        
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="title text-center">
                                    <h3>{{ trans('front.proyectos_investigacion') }}</h3>
                                </div>
                                <ul class="ul-withdetails">
                                	@foreach($investigaciones as $inv)
                                    <li>
                                        <div class="row">
                                            <div class="col-sm-4 col-md-3">
                                                <div class="image">
                                                    <img alt="image" src="img/lab/400x400-lte.png"  class="img-responsive">
                                                    <div class="imageoverlay">
                                                        <i class="fa fa-search"></i>
                                                    </div>

                                                </div>
                                            </div>

                
                                            <div class="col-sm-8 col-md-9">
                                                <div class="meta">
                                                    <h3 class="text-justify">{{ $inv->proyecto }}</h3>
                                                    <p>
                                                        {{ $inv->cargo }}
                                                        <span class="badge badge-success pull-right"> {{ date('M-Y',strtotime($inv->desde)) }} // {{ date('M-Y',strtotime($inv->hasta)) }}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="details">
                                            <p>{!! $inv->descripcion !!}</p>
                                        	<ul class="list-unstyled">
                                                <li>
                                                    <span><b>Financiamiento:</b> <i class="fa fa-usd"></i> {{ $inv->financiamiento }}</span>
                                                </li>
                                               
                                                <li>
                                                    <span><b>Participantes:</b> <br> {!! $inv->participantes !!}</span>
                                                </li>
                                                @if(isset($inv->objetivo))
                                                <li>
                                                    <span><b>Objetivo:</b> <br> {!! $inv->objetivo !!} </span>
                                                </li>
                                                @endif
                                            </ul>   
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div><!--/research-->
</div><!--/main-->
@endsection