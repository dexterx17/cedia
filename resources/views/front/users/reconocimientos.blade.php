@extends('templates.one.publications')

@section('title',$user_data->nombres.' '.$user_data->apellidos)

@section('content')

<div id="main">

    <div id="biography" class="page home" data-pos="home">
        <div class="pageheader">
            <div class="headercontent">
                <div class="section-container">
                    <h2 class="title">{{ trans('front.reconocimientos') }}</h2>
                </div>        
            </div>
        </div>

        <div class="pagecontents">
            <div class="section color-2" style="padding-top:10px !important;">
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="title text-center">
                                <h3>Honors, Awards and Grants</h3>
                            </div>
                            <ul class="timeline">
                                @foreach($reconocimientos as $index=>$r)
                                <li class="@if($index==0) open @endif">
                                    <div class="date"> {{ date('M-Y',strtotime($r->fecha)) }}</div>
                                    <div class="circle"></div>
                                    <div class="data">
                                        <div class="subject">{{ $r->reconocimiento }}</div>
                                        <div class="text row">
                                            <div class="col-md-2">
                                                <img alt="image" class="thumbnail img-responsive" src="img/personal/awards100x100.png" >
                                            </div>
                                            <div class="col-md-10">
                                                <h3>{{ $r->descripcion }}</h3>
                                                <p>{{ $r->ubicacion }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>   

        </div>
    </div><!--/biography-->

</div><!--/main-->
@endsection