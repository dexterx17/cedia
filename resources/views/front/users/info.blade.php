@extends('templates.one.publications')

@section('title',$user_data->nombres.' '.$user_data->apellidos)

@section('content')

<div id="main">

    <div id="biography" class="page home" data-pos="home">
        <div class="pageheader">
            <div class="headercontent">
                <div class="section-container">
                    
                    <div class="row">
                        <div class="col-sm-2 visible-sm"></div>
                        <div class="col-sm-8 col-md-4">
                            <div class="biothumb">
                                <img alt="image" src="{{ asset('img/pics/'.$user_data->imagen) }}" class="img-responsive">

                            </div>
                            
                        </div>
                        <div class="clearfix visible-sm visible-xs"></div>
                        <div class="col-sm-12 col-md-7">
                            <h3 class="title">{{ $user_data->nombres }} {{ $user_data->apellidos }}</h3>
                            <p class="text-justify">
                            	{!! $user_data->about_me !!}
                            </p>

                                <div class="subtitle text-center">
                                    <h3>{{ trans('front.areas_de_interes') }}</h3>
                                </div>
                                <ul class="ul-boxed list-unstyled">
                                    @foreach($user_data->intereses as $int )
                                    <li>{{ $int->interes }}</li>
                                    @endforeach
                                    
                                </ul>
                            
                        </div>  
                        
                    </div>
                </div>        
            </div>
        </div>

        <div class="pagecontents">
            <div class="section color-1">
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title text-center">
                                <h3>Contact & Meet Me</h3>
                            </div>
                           <div class="row">
                                <div class="col-md-8">
                                    <p>I would be happy to talk to you if you need my assistance in your research or whether you need bussiness technical support for your company. Though I have limited time for students but I will be happy to help you if you need any detail about my research.</p>                              
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-unstyled">
                                        <li>
                                            <strong><i class="fa fa-phone"></i>&nbsp;&nbsp;</strong>
                                            <span>Phone: +593 {{ $user_data->telefono }}</span>
                                        </li>
                                       
                                        <li>
                                            <strong><i class="fa fa-envelope"></i>&nbsp;&nbsp;</strong>
                                            <span>{{ $user_data->email }}</span>
                                        </li>
                                        @if($user_data->skype)
                                        <li>
                                            <strong><i class="fa fa-skype"></i>&nbsp;&nbsp;</strong>
                                            <span>{{ $user_data->skype }}</span>
                                        </li>
                                        @endif
                                    </ul>    
                                </div>
                            </div>
                        </div>    
                    </div>    
                </div>
                    
            </div>
        </div>
    </div><!--/biography-->

</div><!--/main-->
@endsection