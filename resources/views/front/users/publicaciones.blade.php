@extends('templates.one.publications')

@section('title',$user_data->nombres.' '.$user_data->apellidos)

@section('content')
<div id="main">
    <div class="page" id="publications">
        <div class="page-container">
            <div class="pageheader">
                <div class="headercontent">
                    <div class="section-container">
                        <h2 class="title">
                            Publications
                        </h2>
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagecontents">
                <div class="section color-1" id="filters">
                    <div class="section-container">
                        <div class="row">
                            <div class="col-md-3">
                                <h3>
                                    Filter by type:
                                </h3>
                            </div>
                            <div class="col-md-6">
                                <select id="cd-dropdown" name="cd-dropdown" class="cd-select">
                                    <option class="filter" value="all" selected>All types</option>
                                    <option class="filter" value="jpaper">Jounal Papers</option>
                                    <option class="filter" value="cpaper">Conference Papers</option>
                                    <option class="filter" value="bookchapter">Book Chapters</option>
                                    <option class="filter" value="book">Books</option>
                                    <!-- <option class="filter" value="report">Reports</option>
                                    <option class="filter" value="tpaper">Technical Papers</option> -->
                                </select>
                            </div>
                            <div class="col-md-3" id="sort">
                                <span>
                                    Sort by year:
                                </span>
                                <div class="btn-group pull-right">
                                    <button class="sort btn btn-default active" data-order="desc" data-sort="data-year" type="button">
                                        <i class="fa fa-sort-numeric-asc">
                                        </i>
                                    </button>
                                    <button class="sort btn btn-default" data-order="asc" data-sort="data-year" type="button">
                                        <i class="fa fa-sort-numeric-desc">
                                        </i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section color-2" id="pub-grid">
                    <div class="section-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pitems" style="   ">
                                    <!-- Paper 0 -->
                                    @foreach($publicaciones as $pub)
                                        @includeIf('publicaciones.'.$pub->tipo,['pub'=>$pub])
                                    @endforeach

                                    <!-- End papers-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection