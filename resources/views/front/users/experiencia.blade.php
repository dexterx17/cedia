@extends('templates.one.publications')

@section('title',$user_data->nombres.' '.$user_data->apellidos)

@section('content')

 <div id="main">

    <div id="teaching" class="page">
        <div class="pageheader">
            <div class="headercontent">
                <div class="section-container">
                    
                    <h2 class="title">{{ trans('front.experiencia_laboral') }}</h2>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <p></p>                                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pagecontents">
            <div class="section color-1"  style="padding-top:10px !important; padding-bottom:10px !important;">
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="title text-center">
                                <h3>Puestos actuales</h3>
                            </div>
                            <ul class="ul-dates">
                                @foreach($trabajos->where('actual',1) as $trabajo)
                                <li>
                                    <div class="dates">
                                        <span>Presente</span>
                                        <span>{{ date('M-Y',strtotime($trabajo->desde)) }}</span>
                                    </div>
                                    <div class="content">
                                        <h4>{{ $trabajo->cargo }} <small>{{ $trabajo->tiempo }}</small>   </h4>
                                        <p>{{ $trabajo->institucion }} 
                                            <br> {{ $trabajo->departamento }}
                                            <small> <b>{{ $trabajo->ubicacion }}</b>  </small>
                                        </p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section color-2" style="padding-top:10px !important;">
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="title text-center">
                                <h3>Historial Laboral</h3>
                            </div>
                            <ul class="ul-dates-gray">
                                @foreach($trabajos->where('actual',0) as $trabajo)
                                <li>
                                    <div class="dates">
                                        <span>{{ date('M-Y',strtotime($trabajo->hasta)) }}</span>
                                        <span>{{ date('M-Y',strtotime($trabajo->desde)) }}</span>
                                    </div>
                                    <div class="content">
                                        <h4>{{ $trabajo->cargo }} <small>{{ $trabajo->tiempo }}</small> @if($trabajo->administrativo==1)<span class="badge">Administrativo</span>@endif</h4>
                                        <p>
                                            @if($trabajo->departamento)
                                            {{ $trabajo->departamento }}
                                            <br> 
                                            @endif
                                            {{ $trabajo->institucion }} 
                                            <small> <b>{{ $trabajo->ubicacion }}</b>  </small>
                                        </p>
                                    </div>
                                </li>
                                @endforeach                               
                            </ul>
                        </div>
                    </div>
                        
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection