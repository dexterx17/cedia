@extends('templates.basic.base')
@section('content')

<div id="info-curso">
    <h2>Curso de Machine Learning Aplicado a Tecnologías Inmersivas para la Academia e Industria</h2>
    <a href="{{ route('evento') }}">Regístrate</a>
</div>
<!-- Header Section Start -->
<header id="hero-area">    
    <div class="overlay"></div>
    @include('templates.basic.partials.menu')
    <div class="container">
      <div class="row justify-content-md-center">
          <div class="col-md-10">
              <div class="item active">
                  <div class="contents text-center">
                      <p class="lead wow fadeIn" data-wow-delay="400ms" data-wow-duration="1000ms">
                          {{ trans('front.grupo_investigacion') }}
                      </p>
                      <h1 class="wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                          {{ trans('front.grupo_nombre') }}
                      </h1>
                      <a class="btn btn-common wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms" href="#portfolio" rel="nofollow">
                          {{ trans('front.nuestros_proyectos') }}
                      </a>
                      <div class="banner_bottom_btn wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1000ms">
                          <a class="js-target-scroll" href="#about">
                              <i class="icon-mouse">
                              </i>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</header>
<!-- Header Section End -->
<!-- About Section Start -->
<section class="section" id="about">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.quienes_somos') }}
            </h3>
        </div>
        <div class="row">
            <div class="col-md-6 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1000ms">
                <h2 class="section-title wow fadeInUp" data-wow-delay="350ms" data-wow-duration="1000ms">
                    Visión
                </h2>
                <p class="text-justify"> El Grupo de Investigación en Automatización, Robótica y Sistemas Inteligentes, GI-ARSI, por sus niveles de excelencia e innovación se constituirá en un centro de investigación tecnológica con liderazgo y proyección nacional e internacional.</p>
                <h2 class="section-title wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1000ms">
                    Misión
                </h2>
                <p class="text-justify"> Contribuir el avance tecnológico del país desarrollando proyectos de investigación y/o prototipos a través de la innovación en el área de automatización, robótica y sistemas inteligentes, para la difusión del conocimiento como proyección social.</p>
            </div>
            <div class="col-md-6 wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1000ms">
                <hr>
                <p class="text-justify">
                En este sentido se proyecta el GI-ARSI empezando por la investigación formativa de
                estudiantes con capacidad de conducción y liderazgo, que estén a la vanguardia del
                desarrollo del país, con fundamentos y conocimientos científico técnicos, sentido
                social y humanístico. Así mismo, el GI-ARSI asume un rol de protagonismo en
                profesores, en cuanto a integrar la investigación generativa como eje fundamental de
                la docencia a fin de ser generadores de conocimiento y producir resultados publicables
                y aplicables mediante la investigación, innovación, y trasferencia tecnológica.
                </p>
                <hr>
            </div>
        </div>
        <div class="counters">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="wow fadeInUp" data-wow-delay=".2s">
                        <div class="facts-item">
                            <div class="fact-count">
                                <h3>
                                    <span class="counter">
                                        {{ $proyectos->count() }}
                                    </span>
                                </h3>
                                <h4>
                                    {{ trans_choice('front.proyectos',$proyectos->count()) }}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="wow fadeInUp" data-wow-delay=".4s">
                        <div class="facts-item">
                            <div class="fact-count">
                                <h3>
                                    <span class="counter">
                                        {{ $publicaciones->count() }}
                                    </span>
                                </h3>
                                <h4>
                                    {{ trans_choice('front.publicaciones',$publicaciones->count()) }}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="wow fadeInUp" data-wow-delay=".6s">
                        <div class="facts-item">
                            <div class="fact-count">
                                <h3>
                                    <span class="counter">
                                        {{ $usuarios->count() }}
                                    </span>
                                </h3>
                                <h4>
                                    {{ trans_choice('front.investigadores',$usuarios->count()) }}
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Section End -->
<!-- Services Section Start -->
@include('front.areas')
<!-- Services Section End -->

@include('front.proyectos')

<!-- Why Section Start -->
@include('front.publicaciones')
<!-- Why Section End -->

<!-- Team Section Start  -->
<section class="team-area section" id="team">
    <div class="container">
        <div class="section-header">
            <h3 class="section-subtitle wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.conoce_nuestro_equipo') }}
            </h3>
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.profesores_investigadores') }}
            </h2>
        </div>
        <div class="row">
            @foreach($usuarios->where('rol','investigador') as $u)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-team wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1000ms">
                    <img alt="{{ $u->nombres }} {{ $u->apellidos }}" src="@if($u->imagen=="") img/team/1.png @else {{ asset('img/pics/'.$u->imagen) }} @endif">
                        <div class="team-content">
                            <h4 class="tem-member">
                                <a href="{{ route('user_info',$u->id) }}" title="{{ $u->nombres }} {{ $u->apellidos }}">
                                    {{ $u->nombres }}
                                    <br>
                                        {{ $u->apellidos }}
                                    </br>
                                </a>
                            </h4>
                            <h6>
                                {{ $u->titulo }}
                            </h6>
                            @if($u->universidades->count()>0)
                            <p>
                                {{ $u->universidad()->nombre }}
                            </p>
                            @endif
                            <!--<ul class="team-social">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-linkedin">
                                        </i>
                                    </a>
                                </li>
                            </ul>-->
                        </div>
                    </img>
                </div>
            </div>
            @endforeach
        </div>
        <div class="section-header">
            <h2 class="section-title wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1000ms">
                {{ trans('front.investigadores_externos') }}
            </h2>
        </div>
        <div class="row">
            @foreach($usuarios->where('rol','asistente') as $u)
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-team wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1000ms">
                    <img alt="{{ $u->nombres }} {{ $u->apellidos }}" src="@if($u->imagen=="") img/team/1.png @else {{ asset('img/pics/'.$u->imagen) }} @endif">
                        <div class="team-content">
                            <h4 class="tem-member">
                                <a href="{{ route('user_info',$u->id) }}" title="{{ $u->nombres }} {{ $u->apellidos }}">
                                    {{ $u->nombres }}
                                    <br>
                                        {{ $u->apellidos }}
                                    </br>
                                </a>
                            </h4>
                            <h6>
                                {{ $u->titulo }}
                            </h6>
                            @if($u->universidades->count()>0)
                            <p>
                                {{ $u->universidad()->nombre }}
                            </p>
                            @endif
                            <!--<ul class="team-social">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-google-plus">
                                        </i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-linkedin">
                                        </i>
                                    </a>
                                </li>
                            </ul>-->
                        </div>
                    </img>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<!-- Team Section End  -->
<!-- Start Get-Strted Section 
<section class="great-started section" id="contact">
    <div class="overlay">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="great-started-text text-center">
                    <h4 class="wow fadeInDown" data-wow-delay="300ms" data-wow-duration="1000ms">
                        We Are Ready to Help You
                    </h4>
                    <h2 class="wow zoomIn" data-wow-delay="100ms" data-wow-duration="1000ms">
                        Get the Best Solution for Your Business
                    </h2>
                    <a class="btn btn-common wow fadeInUp" data-wow-delay="100ms" data-wow-duration="3000ms" href="#">
                        Get Started
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Get-Strted Section -->
<!-- Contact Icon Start -->
<div class="section contact-icon" id="contact">
    <div class="overlay">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="box-icon wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1200ms">
                    <div class="icon icon-primary">
                        <i class="icon-map">
                        </i>
                    </div>
                    <p>
                        Ecuador, Cotopaxi, Latacunga
                        <br>
                            Calle Quijano y Ordoñes y Hermanas Páez
                        </br>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="box-icon wow fadeInUp" data-wow-delay="500ms" data-wow-duration="1200ms">
                    <div class="icon icon-secondary">
                        <i class="icon-envelope">
                        </i>
                    </div>
                    <p>
                        info@giarsi.com
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="box-icon wow fadeInUp" data-wow-delay="700ms" data-wow-duration="1200ms">
                    <div class="icon icon-tertiary">
                        <i class="icon-phone">
                        </i>
                    </div>
                    <p>
                        (+593) (03) 2811 228 ext:4332
                        <br>
                            (+593) 2811-240
                        </br>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Contact Icon End -->
@endsection
