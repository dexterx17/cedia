<?php

return [
	'app_name' => 'Investigation',
    'home' => 'Home',
    'acerca_de' => 'About us',
    'areas_investigacion' => 'Research Areas',
    'proyectos' => 'Projects',
    'publicaciones' => 'Publications',
    'investigadores' => 'Researchers',
    'contactenos' => 'Contact Us',
    'contacto' => 'Contact',
    'investigacion' => 'Investigation',
    'horas_trabajadas' => 'Working hours',
    'nuestros_proyectos' => 'Our Projects',
    'que_hacemos' => 'What we do',
    'robotica' => 'Robotic',
    'mas_informacion' => 'More information',
    'miembros_equipo' => 'Team members',
    'conoce_nuestro_equipo' => 'Meet Our Team'
];
