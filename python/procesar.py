#!/usr/bin/env python
#coding: utf-8
from BeautifulSoup import BeautifulSoup


import re
import time
import sys
import urllib2
import json

#url = ('https://www.google.com.ec/search?q=tendencias+turisticas+en+ecuador&oq=tendencias+tur&q=guayaquil&stick=H4sIAAAAAAAAAONgFuLQz9U3MMo2TFLiBLEMU0oKDbUUs5Ot9HPykxNLMvPz9JPzS_NKiiqtUlKLSzLzwGLFHUzsANBmWAc9AAAA')
#user_id = "StskHA4AAAAJ" //Victor
#user_id = "p47Kw5gAAAAJ" //Jaime
#user_id = "U28c5SYAAAAJ" //Cristian
#user_id = "zPNPhwkAAAAJ" //Washito
#user_id = "CoA7yOsAAAAJ" //Jessica
#user_id = "bejtangAAAAJ" //Fernando
#user_id = "5luiS7cAAAAJ" //Christian
#user_id = "_8mA3O0AAAAJ" //Renato
#user_id = "7vaSiNwAAAAJ" //David
#user_id = "wQPymlsAAAAJ" //Jorge
#user_id = "mNstsLsAAAAJ" //Edwin Pruna
#user_id = "oZCnbUAAAAAJ" //Paola Velasco
#user_id = "J7xpZNEAAAAJ" //Ivon Escobar


if len(sys.argv)>1:
	user_id = sys.argv[1]
else:
	print json.dumps({'error':"Especificar ID de usuario: procesar.py EJ: StskHA4AAAAJ "})
	sys.exit()

url = 'https://scholar.google.es/citations?user='+user_id+'&hl=es&cstart=0&pagesize=100'
#url = 'https://scholar.google.es/citations?user='+user_id+'&hl=es&cstart=90&pagesize=15'
url_article = 'https://scholar.google.es/citations?view_op=view_citation&hl=es&user='+user_id+'&citation_for_view='+user_id

opener = urllib2.build_opener()
opener.addheaders = [('User-agent','Mozilla/5.0')]

print "URL::: "
print url

outUrl=None
try:
	outUrl = opener.open(url).read()
	print outUrl
except:
	print "Esa pagina no existe"

pubs =[]
if outUrl:

	soup = BeautifulSoup(outUrl)

	contenedor_publicaciones = soup.find(id="gsc_a_tw")
	for tr in contenedor_publicaciones.findAll('tr',attrs={'class':'gsc_a_tr'}):
	#for link in contenedor_publicaciones.findAll('a',attrs={'class':'gsc_a_at'}):
		
		link = tr.find('a',attrs={'class':'gsc_a_at'})

		title = link.text
		pub={}
		link_url = link['data-href']
		id_url = link_url[link_url.find(":"):]
		pub['id'] = id_url.encode(sys.stdout.encoding)
		pub['title'] = title.encode(sys.stdout.encoding)

		opener = urllib2.build_opener()
		opener.addheaders = [('User-agent','Mozilla/5.0')]
		url = (url_article+pub['id'])
		print "URL"
		print url
		outUrl = opener.open(url).read()
		soupe = BeautifulSoup(outUrl)
		try:

			#print soupe
			print
			ur = soupe.find('a',attrs={'class':'gsc_vcd_title_link'})
			#print ur
			print
			print ur['href']
			print
			pub['url'] = ur['href'].encode(sys.stdout.encoding)

			detalles = soupe.find(id="gsc_vcd_table")

			for tr in detalles.findAll('div',attrs={'class':'gs_scl'}):
				print 'tr.gs_scl'
				column_name = tr.find('div',attrs={'class':'gsc_vcd_field'})
				column_value = tr.find('div',attrs={'class':'gsc_vcd_value'})

				if(column_name.text=="Autores"):
					pub['autores']=column_value.text.encode(sys.stdout.encoding)

				if 'Fecha' in column_name.text:
					pub['fecha_publicacion']=column_value.text.encode(sys.stdout.encoding)

				if(column_name.text=="Revista"):
					pub['revista']=column_value.text.encode(sys.stdout.encoding)

				if(column_name.text=="Conferencia"):
					pub['conferencia']=column_value.text.encode(sys.stdout.encoding)

				if 'ginas' in column_name.text:
					pub['paginas']=column_value.text.encode(sys.stdout.encoding)

				if(column_name.text=="Editor"):
					pub['editorial']=column_value.text.encode(sys.stdout.encoding)

				if 'Descripci' in column_name.text:
					pub['descripcion']=column_value.text.encode(sys.stdout.encoding)

				if(column_name.text=="Citas totales"):
					link_citas = column_value.find('a')
					pub['citas']=link_citas.text.encode(sys.stdout.encoding)
					pub['citas_url']=link_citas['href'].encode(sys.stdout.encoding)
				



				print
				print column_name.text
				print column_value.text

		except:
			print "Remover espacios y caracteres especiales de link.text"
		time.sleep(3)
		pubs.append(pub)
	#	outfile.write(str(link)+ '\n')
		
	print soup.title
	print
	
	#outfile = open('/var/www/html/ecuador/python/google.txt','w+')
	#for link in soup.findAll('a',attrs={'class':re.compile("^/citations/")}):


	print pubs
	outfile = open('/var/www/html/cedia/python/data2018/'+user_id+'.json','w+')
	outfile.write(json.dumps(pubs))