import scholarly
import json

#print(next(scholarly.search_author('Jaime Santana')))
#print(next(scholarly.search_author('Víctor H. Andaluz')))

search_query = scholarly.search_author('Víctor H. Andaluz')
author = next(search_query).fill()
print(author)

pubs =[]

# Print the titles of the author's publications
#for pub in author.publications:
#	pubs.append(pub.fill())
#	print(pub)

# Take a closer look at the first publication
pub = author.publications[0].fill()
#print(next(scholarly.search_pubs_custom_url('https://scholar.google.com/citations?user=p47Kw5gAAAAJ&hl=en')))

#search_query = scholarly.search_author('Steven A Cholewiak')
#author = next(search_query).fill()
#print(author)

outfile = open('victor.json','w+')
outfile.write(pub)