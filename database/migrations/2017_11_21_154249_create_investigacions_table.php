<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestigacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cargo');
            $table->text('proyecto');
            $table->text('objetivos')->nullable();
            $table->text('descripcion')->nullable();
            $table->double('financiamiento')->nullable()->default(0);
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->text('participantes')->nullable();
            $table->string('ubicacion')->nullable();
            $table->string('imagen')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investigaciones');
    }
}
