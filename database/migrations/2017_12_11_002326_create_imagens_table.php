<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ruta');
            $table->string('nombre')->nullable();
            $table->string('autor')->nullable();
            $table->string('creditos')->nullable();
            $table->boolean('destacada')->default(false);
            $table->string('tabla_referencia');
            $table->integer('id_referencia');
           // $table->unique(['tabla_referencia','id_referencia']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('imagenes');
    }
}
