<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
            $table->text('resumen')->nullable();
            $table->double('presupuesto')->default(0);
            $table->enum('financiamiento',['externo','interno'])->default('interno');
            $table->text('objetivo_general')->nullable();
            $table->text('objetivos_especificos')->nullable();
            $table->date('fecha_inicio');
            $table->date('fecha_final')->nullable();
            $table->timestamps();
        });

        Schema::create('proyecto_user',function(Blueprint $table){
            $table->increments('id');
            $table->boolean('director')->default(false);
            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unique(['proyecto_id','user_id']);
        });

        Schema::create('proyecto_universidad',function(Blueprint $table){
            $table->increments('id');
            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos')->onDelete('cascade');
            $table->integer('universidad_id')->unsigned();
            $table->foreign('universidad_id')->references('id')->on('universidades')->onDelete('cascade');
            $table->unique(['proyecto_id','universidad_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proyecto_user');
        Schema::drop('proyecto_universidad');
        Schema::drop('proyectos');
    }
}
