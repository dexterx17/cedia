<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInteresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intereses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('interes');
            $table->timestamps();
        });
        
        Schema::create('interes_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interes_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('interes_id')->references('id')->on('intereses')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interes_user');
        Schema::drop('intereses');
    }
}
