<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tesis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tema');
            $table->string('titulo')->nullable();
            $table->text('resumen')->nullable();
            $table->string('modalidad')->nullable();
            $table->string('link')->nullable();
            $table->string('instituto')->nullable();
            $table->integer('universidad_id')->unsigned()->nullable();
            $table->foreign('universidad_id')->references('id')->on('universidades')->onDelete('cascade');
            $table->string('carrera')->nullable();
            $table->date('desde')->nullable();
            $table->date('hasta')->nullable();
            $table->enum('tipo',['doctorado','maestria','pregrado'])->default('pregrado');
            $table->enum('estado',['desarrollo','defendida','publicada'])->default('desarrollo');
            $table->integer('director_id')->unsigned()->nullable();
            //$table->foreign('director_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('codirector_id')->unsigned()->nullable();
            $table->timestamps();
        });
        
        Schema::create('tesistas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referencia_id')->unsigned();
            $table->string('referencia_table');

            $table->integer('tesis_id')->unsigned();
            $table->foreign('tesis_id')->references('id')->on('tesis')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tesistas');
        Schema::drop('tesis');
    }
}
