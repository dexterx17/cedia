<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scholar_id');
            $table->string('titulo');
            $table->string('autores')->nullable();
            $table->string('url')->nullable();
            $table->enum('tipo',['bookchapter','cpaper','jpaper','book','magazine'])->default('cpaper');
            $table->text('descripcion')->nullable();
            $table->text('abstract')->nullable();
            $table->date('fecha_publicacion')->nullable();
            $table->string('revista')->nullable();
            $table->string('conferencia')->nullable();
            $table->string('paginas')->nullable();
            $table->string('editorial')->nullable();
            $table->integer('citas')->nullable();
            $table->string('citas_url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::create('publicacion_user',function(Blueprint $table){
            $table->increments('id');
            $table->integer('publicacion_id')->unsigned();
            $table->foreign('publicacion_id')->references('id')->on('publicaciones')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unique(['publicacion_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publicacion_user');
        Schema::drop('users');
        Schema::drop('publicaciones');

    }
}
