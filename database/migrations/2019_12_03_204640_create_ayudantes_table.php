<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAyudantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ayudantes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('imagen')->nullable();
            $table->string('email')->nullable();
            $table->string('carrera')->nullable();
            $table->text('about_me')->nullable();
            $table->string('area_investigacion')->nullable();
            $table->string('telefono')->nullable();
            $table->string('facebook')->nullable();
            $table->string('skype')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('scholar_id')->nullable();
            $table->integer('orden')->default(0);
            $table->boolean('visible')->default(true);
            $table->boolean('tesista')->default(false);
            $table->boolean('pasante')->default(false);

            $table->date('tesista_desde')->nullable();
            $table->date('tesista_hasta')->nullable();

            $table->date('pasante_desde')->nullable();
            $table->date('pasante_hasta')->nullable();

            $table->integer('universidad_id')->unsigned();
            $table->foreign('universidad_id')->references('id')->on('universidades')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ayudantes');
    }
}
