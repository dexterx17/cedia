<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('ruta');
            $table->text('resumen')->nullable();
            $table->string('palabras_clave')->nullable();
            $table->date('fecha')->nullable();
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('documento_tipo')->onDelete('cascade');

            $table->integer('upload_by')->unsigned();
            $table->foreign('upload_by')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('documento_referencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referencia_id')->unsigned();
            $table->string('referencia_table');

            $table->integer('documento_id')->unsigned();
            $table->foreign('documento_id')->references('id')->on('documentos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documento_referencias');
        Schema::drop('documentos');
    }
}
