<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cargo');
            $table->text('funciones')->nullable();
            $table->string('departamento')->nullable();
            $table->string('institucion');
            $table->string('ubicacion')->nullable();
            $table->date('desde');
            $table->date('hasta')->nullable();
            $table->boolean('administrativo')->default(false);
            $table->enum('tiempo',['tiempo completo','medio tiempo',''])->default('');
            $table->boolean('actual')->default(false);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trabajos');
    }
}
