<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePonenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ponencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tema');
            $table->string('evento');
            $table->string('cargo');
            $table->date('fecha');
            $table->text('resumen');
            
            $table->string('lugar');
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            
            $table->timestamps();
        });

        Schema::create('ponentes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referencia_id')->unsigned();
            
            $table->string('referencia_table');
            $table->string('financiamiento');

            $table->integer('ponencia_id')->unsigned();
            $table->foreign('ponencia_id')->references('id')->on('ponencias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ponentes');
        Schema::drop('ponencias');
    }
}
