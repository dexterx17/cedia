<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'nombres' => $faker->name,
        'apellidos' => $faker->name,
        'scholar_id' => 'p47Kw5gAAAAJ',
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Publicacion::class, function (Faker\Generator $faker) {
	return [
		'scholar_id'=>str_random(8),
		'titulo' => $faker->title,
		'url'=> $faker->url,
		'descripcion' => $faker->text,
		//'fecha_publicacion' => $faker->date,
    	'revista'=> $faker->title,
    	'conferencia'=>'',
    	'paginas'=>'7,8',
    	'editorial'=> $faker->title,
    	'citas' => rand(1,10),
    	'citas_url' => $faker->url
	];
});

$factory->define(App\Universidad::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'telefono' => $faker->phoneNumber,
        'logo' => $faker->imageUrl
    ];
});

$factory->define(App\Proyecto::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => '',
        'objetivo_general' => '',
        'objetivos_especificos' => '',
        'fecha_inicio' => $faker->date
    ];
});

$factory->define(App\Investigacion::class, function (Faker\Generator $faker) {
    $users_ids = \DB::table('users')->select('id')->get();
    return [
        'cargo' => $faker->title,
        'proyecto' => $faker->name,
        //'descripcion' => $faker->text,
        //'objetivos' => $faker->text,
        'financiamiento' => rand(1000,10000),
        'desde' => $faker->date,
        'hasta' => $faker->date,
        'participantes' => '',
        'ubicacion' => $faker->name,
        'user_id' => $faker->randomElement($users_ids)->id,
    ];
});

$factory->define(App\Reconocimiento::class, function (Faker\Generator $faker) {
    $users_ids = \DB::table('users')->select('id')->get();
    return [
        'reconocimiento' => $faker->title,
        'fecha' => $faker->date,        
        'user_id' => $faker->randomElement($users_ids)->id
    ];
});

$factory->define(App\Trabajo::class, function (Faker\Generator $faker) {
    $users_ids = \DB::table('users')->select('id')->get();
    return [
        'cargo' => $faker->title,
        'institucion' => $faker->name,
        'desde' => $faker->date,        
        'user_id' => $faker->randomElement($users_ids)->id
    ];
});

$factory->define(App\Interes::class, function (Faker\Generator $faker) {
    return [
        'interes' => $faker->name,
    ];
});