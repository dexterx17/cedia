<?php

use Illuminate\Database\Seeder;

use App\Publicacion;
use App\User;

class PublicacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Publicacion::class,25)->create([]);
    }
}
