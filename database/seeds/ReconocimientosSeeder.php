<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Reconocimiento;

class ReconocimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $victor = User::find(1);

        $r1 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Mejor Investigador Junior 2015-2016',
        	'fecha' => '2016-06-01',
        	'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
        	'user_id' => $victor->id
        ]);

        $r2 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2015-2016',
        	'fecha' => '2016-06-01',
        	'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
        	'user_id' => $victor->id
        ]);

        $r3 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Tele-operación bilateral de un manipulador móvil, Concurso de Innovación Científica y Desarrollo Empresarial Innovate',
        	'fecha' => '2016-06-01',
        	'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
        	'user_id' => $victor->id
        ]);

        $r4 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Reconocimiento de Ambientes Peligrosos con Arácnidos Autónomos',
        	'fecha' => '2016-05-01',
        	'ubicacion' => 'V Feria de Investigación UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi',
        	'user_id' => $victor->id
        ]);

        $r5 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Segundo lugar al proyecto',
        	'descripcion' => 'ASMARTCH: Autonomus Smart Chair, Concurso Internacional Robot Game Zero Latitud 2016',
        	'fecha' => '2016-05-01',
        	'ubicacion' => 'categoría Impacto Tecnológico-realizado en la Universidad Yachay, Ibarra-Ecuador',
        	'user_id' => $victor->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Segundo lugar al proyecto',
        	'descripcion' => 'Silla de Ruedas Robótica',
        	'fecha' => '2016-05-01',
        	'ubicacion' => 'V Feria de Investigación UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi',
        	'user_id' => $victor->id
        ]);

        $r7 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Ponente Magistral',
        	'descripcion' => 'Control Coordinado de Múltiples Robots',
        	'fecha' => '2015-06-01',
        	'ubicacion' => 'XI Congreso de Ciencia y Tecnología ESPE, Universidad de las Fuerzas Armadas ESPE',
        	'user_id' => $victor->id
        ]);

        $r8 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Control de una Silla de Ruedas a Través de Señales Cerebrales',
        	'fecha' => '2014-01-01',
        	'ubicacion' => 'V Feria UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi',
        	'user_id' => $victor->id
        ]);

        $r9 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Control de una Silla de Ruedas a Través de Señales Cerebrales',
        	'fecha' => '2014-01-01',
        	'ubicacion' => 'III Feria Exposición Latinoamericana de Emprendimientos Productivo, Ciencia, Tecnología; Cooperativa de Ahorro y Crédito Cámara de Comercio Ambato Ltda.',
        	'user_id' => $victor->id
        ]);

        $r10 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Control de una Silla de Ruedas a Través de Señales Cerebrales',
        	'fecha' => '2014-04-01',
        	'ubicacion' => 'Jornadas Científicas Estudiantiles – Área de Ingeniería; Universidad Técnica de Ambato',
        	'user_id' => $victor->id
        ]);

        $r11 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Control de Posición Bilateral de una Silla de Ruedas a Través del Kinect',
        	'fecha' => '2013-10-01',
        	'ubicacion' => 'Feria de Proyectos UTA-FISEI 2013, Egresados de la FISEI.',
        	'user_id' => $victor->id
        ]);

        $r12 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al proyecto',
        	'descripcion' => 'Control de una Silla de Ruedas a Través de Señales Cerebrales',
        	'fecha' => '2013-04-01',
        	'ubicacion' => 'Feria de Proyectos UTA-FISEI 2013, Carrera de Ingeniería en Sistemas Computacionales e Informáticos',
        	'user_id' => $victor->id
        ]);

        $r13 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al Proyecto de Innovación',
        	'descripcion' => 'Prótesis Robótica',
        	'fecha' => '2012-07-01',
        	'ubicacion' => 'Escuela Superior Politécnica de Chimborazo',
        	'user_id' => $victor->id
        ]);

        $r14 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Primer lugar al Proyecto',
        	'descripcion' => 'Seguimiento de caminos con Helicópteros usando un controlador de bajo nivel para tareas de vigilancias autónomas',
        	'fecha' => '2012-04-01',
        	'ubicacion' => 'Feria de Proyectos UTA-FISEI 2012, Carrera de Ingeniería Industrial en Procesos de Automatización',
        	'user_id' => $victor->id
        ]);

        $r15 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Segundo lugar al Proyecto',
        	'descripcion' => 'Control de una casa inteligente vía web mediante el  uso de cámaras IP',
        	'fecha' => '2012-04-01',
        	'ubicacion' => 'Feria de Proyectos UTA-FISEI 2012, Carrera de Ingeniería Industrial en Procesos de Automatización',
        	'user_id' => $victor->id
        ]);

        $r16 = factory(Reconocimiento::class)->create([
        	'reconocimiento' => 'Segundo lugar al Proyecto',
        	'descripcion' => 'Control Virtual de un computador con Kinect',
        	'fecha' => '2012-04-01',
        	'ubicacion' => 'Feria de Proyectos UTA-FISEI 2012, Carrera de Ingeniería Industrial en Procesos de Automatización',
        	'user_id' => $victor->id
        ]);

        $chris = User::where('email','=','chriss2592@hotmail.com')->first();

        $r25 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar',
            'descripcion' => 'categoría “MINISUMO”',
            'fecha' => '2015-06-01',
            'ubicacion' => 'IX Concurso de Robótica en la Universidad de las Fuerzas Armadas ESPE-Latacunga',
            'user_id' => $chris->id
        ]);

        $r26 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar',
            'descripcion' => 'categoría “ROBOT SOCCER”',
            'fecha' => '2015-06-01',
            'ubicacion' => 'IX Concurso de Robótica en la Universidad de las Fuerzas Armadas ESPE-Latacunga',
            'user_id' => $chris->id
        ]);

        $r27 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar y Segundo Lugar',
            'descripcion' => 'categoría “Carrera de Humanoides” y “Robot Bailarín”',
            'fecha' => '2014-12-01',
            'ubicacion' => 'concurso “UMEBOT 9” desarrollado en la Escuela Politécnica Nacional',
            'user_id' => $chris->id
        ]);

        $r28 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar',
            'descripcion' => 'categoría “Pelea de Bípedos”',
            'fecha' => '2014-12-01',
            'ubicacion' => 'X Concurso ecuatoriano de Robótica',
            'user_id' => $chris->id
        ]);

        $r29 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar',
            'descripcion' => 'categoría “MINISUMO”',
            'fecha' => '2014-07-01',
            'ubicacion' => 'VIII Concurso de Robótica en la Universidad de las Fuerzas Armadas ESPE-Latacunga',
            'user_id' => $chris->id
        ]);

        $r291 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar',
            'descripcion' => 'categoría “ROBOT LABERINTO”',
            'fecha' => '2014-07-01',
            'ubicacion' => 'VIII Concurso de Robótica en la Universidad de las Fuerzas Armadas ESPE-Latacunga',
            'user_id' => $chris->id
        ]);   
            
        $r292 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar',
            'descripcion' => 'categoría “MINISUMO”',
            'fecha' => '2014-07-01',
            'ubicacion' => 'Primer Encuentro Tecnológico “PELILEO EN LA ERA DIGITAL”',
            'user_id' => $chris->id
        ]);

        $cris = User::where('email','=','cmgallardop@gmail.com')->first();
        
        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'Control Virtual de un computador con Kinect',
            'fecha' => '2012-06-01',
            'ubicacion' => 'Feria de Proyectos UTA-FISEI 2012, Carrera de Ingeniería en Sistemas Computacionales e Informáticos',
            'user_id' => $cris->id
        ]);  
              
        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'Manipulación de artes visuales (mapping) por medio de control bilateral (kinect)',
            'fecha' => '2013-06-01',
            'ubicacion' => 'Feria de Proyectos UTA-FISEI 2013, Carrera de Ingeniería en Sistemas Computacionales e Informáticos',
            'user_id' => $cris->id
        ]); 

        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'ASMARTCH: Autonomus Smart Chair',
            'fecha' => '2016-05-01',
            'ubicacion' => 'Concurso Internacional Robot Game Zero Latitud 2016 - categoría Impacto Tecnológico- realizado en la Universidad Yachay, Ibarra-Ecuador, Mayo 2016',
            'user_id' => $cris->id
        ]);

        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar al proyecto',
            'descripcion' => 'Tele-operación bilateral de un manipulador móvil',
            'fecha' => '2016-05-01',
            'ubicacion' => 'Concurso de Innovación Científica y Desarrollo Empresarial Innovate, Universidad de las Fuerzas Armadas ESPE, Junio 2016',
            'user_id' => $cris->id
        ]);

        $fernando = User::where('email','=','fernandochicaiza137@gmail.com')->first();


        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar al proyecto',
            'descripcion' => 'Tele-operación bilateral de un manipulador móvil',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Concurso de Innovación Científica y Desarrollo Empresarial Innovate, Universidad de las Fuerzas Armadas ESPE, Junio 2016',
            'user_id' => $fernando->id
        ]);

        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar al proyecto',
            'descripcion' => 'Reconocimiento de Ambientes Peligrosos con Arácnidos Autónomos',
            'fecha' => '2016-05-01',
            'ubicacion' => 'V Feria de Investigación UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi, Mayo 2016',
            'user_id' => $fernando->id
        ]);


        $r31 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'ASMARTCH: Autonomus Smart Chair',
            'fecha' => '2016-05-01',
            'ubicacion' => 'Concurso Internacional Robot Game Zero Latitud 2016 - categoría Impacto Tecnológico- realizado en la Universidad Yachay, Ibarra-Ecuador, Mayo 2016',
            'user_id' => $fernando->id
        ]);

        
        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'Silla de Ruedas Robótica',
            'fecha' => '2016-05-01',
            'ubicacion' => 'V Feria de Investigación UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi',
            'user_id' => $fernando->id
        ]); 
               
        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Segundo lugar al proyecto',
            'descripcion' => 'Sistema de Navegación para el control Autónomo y tele-operado de un UAV',
            'fecha' => '2015-06-01',
            'ubicacion' => 'II jornadas estudiantiles científicas, Universidad Técnica de Ambato, 2015',
            'user_id' => $fernando->id
        ]);     

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por la ponencia',
            'descripcion' => 'Tecnologías de Navegación Autónoma con UAVs',
            'fecha' => '2014-06-01',
            'ubicacion' => 'Congreso Internacional de Investigación e Innovación, Universidad del Centro de Estudios Superiores de Cortazar, Guanajuato – México, 2014',
            'user_id' => $fernando->id
        ]);

        $hector = User::where('nombres','=','Héctor C.')->first();

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Libros 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2015-2016',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Libros 2015-2016',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2014-2015',
            'fecha' => '2015-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Libros 2014-2015',
            'fecha' => '2015-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $hector->id
        ]);

        $jess = User::where('email','=','jsortiz4@espe.edu.ec')->first();


        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jess->id
        ]);

        $jorge = User::where('email','=','jssanchez@espe.edu.ec')->first();

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Mejor Investigador Junior 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jorge->id
        ]);
        
        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jorge->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Mejor Investigador Junior 2015-2016',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jorge->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2015-2016',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jorge->id
        ]);

        $r3 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar al proyecto',
            'descripcion' => 'Tele-operación bilateral de un manipulador móvil, Concurso de Innovación Científica y Desarrollo Empresarial Innovate',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $jorge->id
        ]);

        $r4 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Primer lugar al proyecto',
            'descripcion' => 'Reconocimiento de Ambientes Peligrosos con Arácnidos Autónomos',
            'fecha' => '2016-05-01',
            'ubicacion' => 'V Feria de Investigación UTCiencia - Área de Ciencias Exactas, Universidad Técnica de Cotopaxi',
            'user_id' => $jorge->id
        ]);

        $oscar = User::where('email','=','oscararteaga2005@yahoo.es')->first();

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $oscar->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2015-2016',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $oscar->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Libros 2015-2016',
            'fecha' => '2016-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $oscar->id
        ]);


        $edwin = User::where('email','=','eppruna@espe.edu.ec')->first();

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Ponente Magistral',
            'fecha' => '2013-06-01',
            'ubicacion' => 'seminario ESPE Investiga 2013',
            'user_id' => $edwin->id
        ]);
        
        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Ponente Magistral',
            'fecha' => '2014-06-01',
            'ubicacion' => 'seminario ESPE Investiga 2014',
            'user_id' => $edwin->id
        ]);

        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Ponente Magistral',
            'fecha' => '2015-06-01',
            'ubicacion' => 'seminario ESPE Investiga 2015',
            'user_id' => $edwin->id
        ]);

        
        $r6 = factory(Reconocimiento::class)->create([
            'reconocimiento' => 'Reconocimiento por Publicaciones Indexadas 2016-2017',
            'fecha' => '2017-06-01',
            'ubicacion' => 'Universidad de las Fuerzas Armadas ESPE',
            'user_id' => $edwin->id
        ]);

    }
}
