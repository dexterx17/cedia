<?php

use Illuminate\Database\Seeder;

use App\Interes;
use App\User;

class InteresesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $a0 = factory(Interes::class)->create(['interes'=>'Control Autónomo y Tele-Operado Aplicado a la Robótica de Servicio']);
        
        $b0 = factory(Interes::class)->create(['interes'=>'Control Cooperativo']);

        $c0 = factory(Interes::class)->create(['interes'=>'Control Inteligente']);
        
        $d0 = factory(Interes::class)->create(['interes'=>'Aplicaciones de realidad virtual y realidad aumentada enfocadas a salud']);

        $e0 = factory(Interes::class)->create(['interes'=>'Educación']);

        $f0 = factory(Interes::class)->create(['interes'=>'Sector industrial']);

        $a1 = factory(Interes::class)->create(['interes'=>'Materiales']);

        $b1 = factory(Interes::class)->create(['interes'=>'Diseño asistido por Computador']);

        $c1 = factory(Interes::class)->create(['interes'=>'Manufactura asistida por computador']);


        $a2 = factory(Interes::class)->create(['interes'=>'Control y Automatización de Procesos Industriales']);

        $c2 = factory(Interes::class)->create(['interes'=>'Control de plataformas móviles']);

        $d2 = factory(Interes::class)->create(['interes'=>'Realidad virtual con aplicaciones educativas y de salud']);

        $e2 = factory(Interes::class)->create(['interes'=>'Mapeo 2D y 3D para entornos virtuales']);

        $f2 = factory(Interes::class)->create(['interes'=>'Cartografía y Topografía Aérea']);

        $a3 = factory(Interes::class)->create(['interes'=>'Programación (C,C++,Swift4,Python,C#, Java)']);

        $b3 = factory(Interes::class)->create(['interes'=>'Administración de servidores GNU/Linux, BSD']);

        $c3 = factory(Interes::class)->create(['interes'=>'Cloud computing – IaaS con OpenStack']);

        $d3 = factory(Interes::class)->create(['interes'=>'Desarrollo de módulos de interconexión de procesos']);

        $e3 = factory(Interes::class)->create(['interes'=>'Desarrollo de módulos de comunicación sobre internet']);

        $f3 = factory(Interes::class)->create(['interes'=>'Seguridad Informática']);

        $a4 = factory(Interes::class)->create(['interes'=>'Telecomunicaciones']);

        $b4 = factory(Interes::class)->create(['interes'=>'Física cuántica']);

        $c4 = factory(Interes::class)->create(['interes'=>'Diseño electrónico']);
        
        $d4 = factory(Interes::class)->create(['interes'=>'Aplicaciones orientadas a la salud y la rehabilitación']);

        $a5 = factory(Interes::class)->create(['interes'=>'Control en robótica de servicio']);

        $b5 = factory(Interes::class)->create(['interes'=>'Capacitación profesional']);
        
        $a6 = factory(Interes::class)->create(['interes'=>'Sistemas de Control']);

        $b6 = factory(Interes::class)->create(['interes'=>'Automatización Industrial']);

        $a7 = factory(Interes::class)->create(['interes'=>'Modelamiento Matemático']);
        
        $b7 = factory(Interes::class)->create(['interes'=>'Teoría de Control']);
        
        $a8 = factory(Interes::class)->create(['interes'=>'Desarrollo de controladores en robótica']);

        $b8 = factory(Interes::class)->create(['interes'=>'Modelación e identificación de sistemas no lineales']);

        $c8 = factory(Interes::class)->create(['interes'=>'Simulación de sistemas en múltiples entornos y SCADA’s']);
        
        $a9 = factory(Interes::class)->create(['interes'=>'Tratamiento de señales']);

        $b9 = factory(Interes::class)->create(['interes'=>'Realidad virtual y aumentada']);

        $c9 = factory(Interes::class)->create(['interes'=>'Ingeniería en software']);
        
        $d9 = factory(Interes::class)->create(['interes'=>'Ingeniería sistemas e informática']);
        
        $e9 = factory(Interes::class)->create(['interes'=>'Redes de comunicaciones']);

        $a10 = factory(Interes::class)->create(['interes'=>'Diseño mecánico']);

        $b10 = factory(Interes::class)->create(['interes'=>'Robótica']);

        $c10 = factory(Interes::class)->create(['interes'=>'Materiales Inteligentes']);
        
        $d10 = factory(Interes::class)->create(['interes'=>'Sistemas Automotrices']);
        
        $a11 = factory(Interes::class)->create(['interes'=>'sistemas de comunicación aplicados a seguridad y análisis de datos,']);

        $a12 = factory(Interes::class)->create(['interes'=>'Sistemas Informáticos']);

        $b12 = factory(Interes::class)->create(['interes'=>'Informática movil']);
        
        $c12 = factory(Interes::class)->create(['interes'=>'Desarrollo de software']);
        
        $d12 = factory(Interes::class)->create(['interes'=>'Cloud Computing']);

        $e12 = factory(Interes::class)->create(['interes'=>'Internet of things']);
        
        $a13 = factory(Interes::class)->create(['interes'=>'Virtualización de entornos']);
        
        $a14 = factory(Interes::class)->create(['interes'=>'Ingeniería de Software']);

        $b14 = factory(Interes::class)->create(['interes'=>'Ingeniería de Requerimientos']);

        $c14 = factory(Interes::class)->create(['interes'=>'Ingeniería de Pruebas']);

        $d14 = factory(Interes::class)->create(['interes'=>'Línea de Producto Software']);

        $e14 = factory(Interes::class)->create(['interes'=>'Ingeniería de Software Experimental']);
        
        $a15 = factory(Interes::class)->create(['interes'=>'Marketing turístico']);

        $b15 = factory(Interes::class)->create(['interes'=>'Emprendimiento en acción']);
        
        $a16 = factory(Interes::class)->create(['interes'=>'Sistemas de Información Geográfica (GIS)']);

        $b16 = factory(Interes::class)->create(['interes'=>'Desarrollo web']);

        $c16 = factory(Interes::class)->create(['interes'=>'Websockets']);
        
        $d16 = factory(Interes::class)->create(['interes'=>'Aplicaciones orientadas a turismo']);



        $victor = User::find(1);
        $ints = [
            $a0->id,
            $b0->id,
            $c0->id,
            $d0->id,
            $e0->id,
            $f0->id,
        ];
        $victor->intereses()->sync($ints);


        

        $hector = User::where('nombres','=','Héctor C.')->first();
        $ints = [
        	$a1->id,
        	$b1->id,
        	$c1->id,
        	$e0->id,
        	$f0->id
        ];
        $hector->intereses()->sync($ints);


        $chris = User::where('email','=','chriss2592@hotmail.com')->first();
        $ints = [
        	$a2->id,
        	$c0->id,
        	$c2->id,
        	$d2->id,
        	$e2->id,
        	$f2->id,
        ];
        $chris->intereses()->sync($ints);



        $cris = User::where('email','=','cmgallardop@gmail.com')->first();
        $ints = [
            $a3->id,
            $b3->id,
            $c3->id,
            $d3->id,
            $e3->id,
            $f3->id,
        ];
        $cris->intereses()->sync($ints);
        

        $david = User::where('email','=','drrivas@espe.edu.ec')->first();
        $ints = [
            $a4->id,
            $b4->id,
            $c4->id,
            $d4->id
        ];
        $david->intereses()->sync($ints);



        $fernando = User::where('email','=','fernandochicaiza137@gmail.com')->first();
        $ints = [
            $a5->id,
            $c4->id,
            $b5->id,
            $f0->id
        ];
        $fernando->intereses()->sync($ints);

        

        $ivone = User::where('email','=','ipescobar@espe.edu.ec')->first();
        $ints = [
            $d0->id,
            $e0->id,
            $f0->id
        ];
        $ivone->intereses()->sync($ints);      


        $jess = User::where('email','=','jsortiz4@espe.edu.ec')->first();
        $ints = [
            $a6->id,
            $b6->id,
            $d0->id,
            $e0->id,
            $f0->id
        ];
        $jess->intereses()->sync($ints);


        $jorge = User::where('email','=','jssanchez@espe.edu.ec')->first();
        $ints = [
            $a7->id,
            $b7->id,
            $e0->id
        ];
        $jorge->intereses()->sync($ints);



        $jose = User::where('email','=','jazjose@hotmail.es')->first();
        $ints = [
            $a8->id,
            $b8->id,
            $c0->id,
            $c8->id
        ];
        $jose->intereses()->sync($ints);


        $marcelo = User::where('email','=','rmalvarez@espe.edu.ec')->first();
        $ints = [
            $a9->id,
            $b9->id,
            $e0->id,
            $c9->id,
            $d9->id,
            $e9->id
        ];
        $marcelo->intereses()->sync($ints);



        $oscar = User::where('email','=','oscararteaga2005@yahoo.es')->first();
        $ints = [
            $a10->id,
            $b10->id,
            $c10->id,
            $d10->id
        ];
        $oscar->intereses()->sync($ints);


        $paola = User::where('nombres','=','Paola M.')->first();
        $ints = [
            $b9->id,
            $a11->id
        ];
        $paola->intereses()->sync($ints);


        $renato = User::where('email','=','renato_toasa@hotmail.com')->first();
        $ints = [
            $a12->id,
            $b12->id,
            $c12->id,
            $d12->id,
            $e12->id
        ];
        $renato->intereses()->sync($ints);

        $washo = User::where('email','=','wjquevedo@espe.edu.ec')->first();
        $ints = [
            $b9->id,
            $b10->id,
            $a7->id,
            $d12->id,
            $e12->id
        ];
        $washo->intereses()->sync($ints);
        
        $edison = User::where('email','=','egespinosa1@espe.edu.ec')->first();
        $ints = [
            $a14->id,
            $b14->id,
            $c14->id,
            $d14->id,
            $e14->id
        ];
        $edison->intereses()->sync($ints);        

        $aldrin = User::where('email','=','agacosta@espe.edu.ec')->first();
        $ints = [
            $d0->id,
            $a15->id,
            $b15->id,
        ];
        $aldrin->intereses()->sync($ints);
        
        $jaime = User::where('email','=','sistemas@santana.ec')->first();
        $ints = [
            $c12->id,
            $b12->id,
            $a15->id,
            $a16->id,
            $b16->id,
            $c16->id,
            $d16->id
        ];
        $jaime->intereses()->sync($ints);


    }
}
