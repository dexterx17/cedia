<?php

use Illuminate\Database\Seeder;

use App\Ponencia;
use App\User;

class PonenciasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ponencia = new Ponencia();
        $ponencia->tema = "Control Based on Linear Algebra for Mobile Manipulators";
        $ponencia->evento = "IFToMM International Workshop on Computational Kinematics (CK2-017)";
        $ponencia->lugar = "Poitiers-Francia";
        $ponencia->lat = 46.5809846;
        $ponencia->lng = 0.3329725;
        $ponencia->cargo = "Estudiantes de Ingeniería en Electrónica e Instrumentación";
        $ponencia->resumen = "This paper presents a control algorithm based on linear algebra for trajectory tracking of mobile manipulator robots. The proposed control algorithm considers the kinematics of the robot, which is approximated by the Euler method, the control actions for an optimal operation of the system are obtained solving a system of linear equations. In addition, the stability of the system is analyzed by concepts of linear algebra, where it is shown that the control error tends asymptotically to zero. Simulation results show the good performance of the proposed control system.";
        $ponencia->fecha = "2017-05-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Manipulators Path Planning Based on Visual Feedback Between Terrestrial and Aerial Robots Cooperation";
        $ponencia->evento = "IFToMM International Workshop on Computational Kinematics (CK2-017)";
        $ponencia->lugar = "Poitiers-Francia";
        $ponencia->lat = 46.5809846;
        $ponencia->lng = 0.3329725;
        $ponencia->cargo = "Estudiantes de Ingeniería en Electrónica e Instrumentación";
        $ponencia->resumen = "This paper presents an algorithm for path planning in which the evasion of fixed and mobile obstacles is considered in order to be followed by an unmanned land vehicle; path planning is based on visual feedback through an unmanned aerial vehicle. In addition, a path planning algorithm is proposed for the ground vehicle in which a non-constant velocity is considered that is a function of the control error, of the curvature of the road to be followed. The stability of the control algorithm is tested through the Lyapunov method. Finally the experimental results are presented and discussed in which the proposal is validated.";
        $ponencia->fecha = "2017-05-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Virtual Reality Applied to Industrial Processes";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (AVR-2017)";
        $ponencia->lugar = "Ugento-Italia";
        $ponencia->lat = 39.8689625;
        $ponencia->lng = 18.1486988;
        $ponencia->cargo = "Estudiantes de Ingeniería en Electrónica e Instrumentación";
        $ponencia->resumen = "The present paper shows the development of a virtual reality application through the creation of environments that allows supervision, monitoring and control of industrial processes. It has the possibility to simulate or work with live-time information, linked to existing process modules of the most important physics variables such as level, pressure, flow and temperature. The results of the application are presented to validate the project, using the functions of supervision, monitoring and control, the scopes reached in emulation, as well as the contrast between the emulation option and real interplaying with industrial processes.";
        $ponencia->fecha = "2017-06-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Composite Materials for the Construction of Functional Orthoses";
        $ponencia->evento = "Advanced Research in Material Sciences, Manufacturing, Mechanical and Mechatronic Engineering Technology International Conference (AR4MET-2017)";
        $ponencia->lugar = "Melaka, Malaysia";
        $ponencia->lat = 2.2375488;
        $ponencia->lng = 102.1812915;
        $ponencia->cargo = "Estudiantes de Ingeniería Mecatrónica";
        $ponencia->resumen = "One of the plastic fibers used in 3D printing is the thermoplastic elastomer, Ninjaflex, which is a light and moldable material, these characteristics are important in the construction of rehabilitation mechanisms such as orthoses, which are external devices, efficient in correcting motor problems. In this article, the characterization of the flexible material is firstly proposed, the procedure for determining the mechanical properties of the material began with the printing of standardized test pieces, according to ASTM D638, for polymer specimens being printed in 3 different directions; the MT - 50 Universal Testing Machine achieves results of the effort at the creep, force and maximum effort, which show us that the best direction of impression is XZ, with which we proceeded with the design and construction of the orthosis, obtaining positive effects in terms of flexibility and comfort of the model as well as aspects that can be improved such as mobility.";
        $ponencia->fecha = "2017-11-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Characteristics of Magnetorheological Fluids Applied to Prosthesis for Lower Limbs";
        $ponencia->evento = "Advanced Research in Material Sciences, Manufacturing, Mechanical and Mechatronic Engineering Technology International Conference (AR4MET-2017)";
        $ponencia->lat = 2.2375488;
        $ponencia->lng = 102.1812915;
        $ponencia->lugar = "Melaka, Malaysia";
        $ponencia->cargo = "Estudiantes de Ingeniería Mecatrónica";
        $ponencia->resumen = "Magnetorheological Fluids (MRF) are a new type of intelligent materials whose rheological characteristics can change with a magnetic field that can be controlled, it experiments significant reversible changes in their mechanical properties, especially their viscosity. When applying a magnetic field transverse to the flow, the system stops being Newtonian and acquires viscoelastic characteristics, although it is common for dispersions of any type to form structures to facilitate sedimentation, in Magnetorheological (MR) fluids uniform distribution of structures is maintained while the magnetic field is present. It depends of the application that is used for, some of this device that are based on the fluid can be dampers, brakes and clutches, etc., have been successfully introduced in engineering applications and prosthetics devices. Some prosthetics for lower limbs are most rigid and does not provide the suitable damping on the walking generating problems such as scoliosis and hip damage, in order to avoid several human damages; this paper propose a prosthetic for lower limbs using MRF whose mechanical properties allows a complete damping control on the walking. Magnetorheological technology allows an instant response so that users do not have to wait for the prosthesis to respond. MRF damper is characterized according to the requirements of the design, using a system equipped with a hydraulic press to analyze the behavior of the fluid in different states that will change according to the variable magnetic field.";
        $ponencia->fecha = "2017-11-01";
        $ponencia->save();
        

        $ponencia = new Ponencia();
        $ponencia->tema = "Cooperative Control of Sliding Mode for Mobile Manipulators";
        $ponencia->evento = "International Conference on Social Robotics (ICSR 2018)";
        $ponencia->lat = 36.05643;
        $ponencia->lng = 120.34469;
        $ponencia->lugar = "Qingdao, China";
        $ponencia->cargo = "Estudiantes de Ingeniería Mecatrónica";
        $ponencia->resumen = "This article describes the design and implementation of a centralized cooperative control algorithm of mobile manipulators (mobile differential platform manipulator and an omnidirectional platform manipulator) for the execution of diverse tasks in which the participation of two or more robots is necessary, e.g., the handling or transport of objects of a high weight, keeping a platform level at a fixed height, among others. For this, a sliding mode control technique is used that is applied to a fixed operating point located in a virtual line that is generated between the end effectors of the manipulator arms. For the validation of the proposed controller, the stability criterion of Lyapunov will be used and the simulation will be performed to validate the performance and performance of the proposed controller between two heterogeneous manipulators.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
        
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Coordinated and Cooperative Control of Heterogeneus Mobile Manipulators";
        $ponencia->evento = "International Conference on Social Robotics (ICSR 2018)";
        $ponencia->lat = 36.05643;
        $ponencia->lng = 120.34469;
        $ponencia->lugar = "Qingdao, China";
        $ponencia->cargo = "Estudiantes de Ingeniería en Electrónica e Instrumentación";
        $ponencia->resumen = "This paper proposes a multilayer scheme for the cooperative control of n≥2 heterogeneous mobile manipulators that allows to transport an object in common in a coordinated way; for which the kinematic modeling of each mobile manipulator robot is performed. Stability and robustness are demonstrated using the Lyapunov theory in order to obtain asymptotically stable control. Finally, the results are presented to evaluate the performance of the proposed control, which confirms the scope of the controller to solve different movement problems.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
                
        $ponencia = new Ponencia();
        $ponencia->tema = "Autonomous Assistance Control Based on Inattention of the Driver When Driving a Truck Tract";
        $ponencia->evento = "International Conference on Social Robotics (ICSR 2018)";
        $ponencia->lat = 36.05643;
        $ponencia->lng = 120.34469;
        $ponencia->lugar = "Qingdao, China";
        $ponencia->cargo = "Estudiantes de Ingeniería Automotriz";
        $ponencia->resumen = "This article proposes the autonomous assistance of a Truck based on a user’s inattention analysis. The level of user inattention is associated with a standalone controller of driving of assistance and path correction. The assistance algorithm is based on the kinematic model of the Truck and the level of user inattention In addition, a 3D simulator is developed in a virtual environment that allows to emulate the behavior of the vehicle and user in different weather conditions and paths. The experimental results using the virtual simulator, show the correct performance of the algorithm of assistance proposed.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
                       
        $ponencia = new Ponencia();
        $ponencia->tema = "Oil Processes VR Training";
        $ponencia->evento = "International Symposium on Visual Computing (ISVC 2018)";
        $ponencia->lat = 36.0955129;
        $ponencia->lng = -115.1761775;
        $ponencia->lugar = "Las Vegas, Nevada";
        $ponencia->cargo = "Estudiantes de Ingeniería Mecatrónica";
        $ponencia->resumen = "In this paper a virtual reality solution is developed to emulate the environment and the operations of the pitching and receiving traps of pipe scrapers (PIG), with the aim of reinforcing the training of operators in oil camps. To develop this, the information was collected on various pitching and receiving traps of the real pipeline operating companies in the country, thus defining the basic and specific parameters for the virtual recreation of a typical trap model. The 3d models obtains from P&ID’s diagrams to interact with user. The environment, interaction and behavior of pipes was developed in a graphic engine, to make training tasks with real state procedures on the oil industry. The goal is save time, money, resources in the training and learning specific oil industry; and make available a base to simulate another complex process.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
                               
        $ponencia = new Ponencia();
        $ponencia->tema = "Training in Virtual Environment for Hybrid Power Plant";
        $ponencia->evento = "International Symposium on Visual Computing (ISVC 2018)";
        $ponencia->lat = 36.0955129;
        $ponencia->lng = -115.1761775;
        $ponencia->lugar = "Las Vegas, Nevada";
        $ponencia->cargo = "Estudiantes de Ingeniería Electromecánica";
        $ponencia->resumen = "This article describes a Virtual Environments application of a Hybrid Power Plant, for professionals in Electrical Power Systems training. The application is developed in the Game Engine Unity 3D and features three different modes as: immersion, interaction and failure modes, which enhance professional’s skills through visualization of the plant components and different processes operation. Additionally, failure mode is proposed, it simulates wrong maneuvers consequences and effects. The Generation Environment is integrated by wind turbines and photovoltaic panels that interact through a mathematical model and enables manipulation of dependent variables bringing out a more realistic background.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Alternative Treatment of Psychological Disorders Such as Spider Phobia Throught Virtual Reality Environments";
        $ponencia->evento = "International Symposium on Visual Computing (ISVC 2018)";
        $ponencia->lat = 36.0955129;
        $ponencia->lng = -115.1761775;
        $ponencia->lugar = "Las Vegas, Nevada";
        $ponencia->cargo = "Estudiantes de Ingeniería en Software";
        $ponencia->resumen = "This article proposes a tool to support the psychotherapist in the process of treating spider phobia through a system that combines both software and hardware elements to present immersive virtual reality environments to the treated patient. To create the feeling of immersion, environments and models created in Unity are used in conjunction with the patient’s movement tracked through the Kinect motion sensor. For the development of the system, the psychotherapeutic treatment method of systematic desensitization is used, so that the patient can overcome his fear and present non-phobic interactions with spiders. The process of developing the system and redacting this document was supported and supervised by a psychologist specialized in the treatment of phobias. Finally, tests were performed to obtain feedback from specialists and potential patients with a medium degree of phobia, in which the results were very positive and satisfactory.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
                
        $ponencia = new Ponencia();
        $ponencia->tema = "Web System for Visualization of Weather Data of the Hydrometeorological Network of Tungurahua, Ecuador";
        $ponencia->evento = "International Symposium on Visual Computing (ISVC 2018)";
        $ponencia->lat = 36.0955129;
        $ponencia->lng = -115.1761775;
        $ponencia->lugar = "Las Vegas, Nevada";
        $ponencia->cargo = "Investigador ARSI";
        $ponencia->resumen = "The information provided by hydrometeorological stations can propose predictive solutions to adverse changes caused by weather in different regions. In this aspect, the publication of this type of information can help common users, farmers or populations at high risk of flooding or propose solutions to droughts. This work presents a hydrometeorological visualization system published in http://rrnn.tungurahua.gob.ec/red, including wind parameters, wind direction, precipitation, and temperature, in a graphic and documented way. The structure of the system presents the whole information required prior to the programming of the two-dimensional and three-dimensional interfaces, detailing all the tools used for the generation of graphics layers. Additionally, all the required files are presented to show the necessary graphic details, such as elevations, boundaries between cantons, roads, water bodies, and so on. Finally, a quick navigation of the bi-dimensional and three-dimensional interface is presented, where all options contained in the developed system are quickly displayed.";
        $ponencia->fecha = "2018-11-01";
        $ponencia->save();
                        
        $ponencia = new Ponencia();
        $ponencia->tema = "Virtual Reality-Based Memory Assistant for the Elderly";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Investigador ARSI";
        $ponencia->resumen = "Older adults experience several diseases related to the habits of their daily lives, many times resulting in cognitive problems when they reach ages that overcome eighty years. This work presents the development of an application aimed at older adults, integrating scenarios in virtual reality to facilitate the immersion of people with signs of memory loss. Through the application, various tasks related to daily activities, professions, face recognition and weekend games are proposed, all evaluated to show scores at the end of the activities. As a result, the work presents real experiments executed to validate the proposal, showing first and third person views within several built virtual environments.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();
                                
        $ponencia = new Ponencia();
        $ponencia->tema = "Real–Time Virtual Reality Visualizer for Unmanned Aerial Vehicles";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Investigador ARSI";
        $ponencia->resumen = "The visualization of tele-operated and autonomous executions in the field becomes difficult if the real environments are located in remote areas or present potential dangers for visualizing clients. This work proposes an application based on virtual reality to recreate in real time the execution tasks of a UAV, which is operated remotely or autonomously on a real environment. To achieve a third level of immersion, the reconstruction of the real environment where the field tests are executed is considered, offering the possibility of knowing the real scenario where the tests are executed. The consideration of using commercial UAV development kits is taken into account to obtain internal information, as well as to control the drone from client devices. The results presented validate the unification of 3D models and the reconstruction of the environment, as well as the consumption of vehicle information and climate parameters.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();
                                                
        $ponencia = new Ponencia();
        $ponencia->tema = "Autonomous and Tele-Operated Navigation of Aerial Manipulator Robots in Digitalized Virtual Environments";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Investigador ARSI";
        $ponencia->resumen = "This paper presents the implementation of a 3D virtual simulator that allows the analysis of the performance of different autonomous and tele-operated control strategies through the execution of service tasks by an aerial manipulator robot. The simulation environment is development through the digitalization of a real environment by means of 3D mapping with Drones that serves as a scenario to execute the tasks with a robot designed in CAD software. For robot-environment interaction, the Unity 3D graphics engine is used, which exchanges information with MATLAB to close the control loop and allow for feedback to compensate for the error. Finally, the results of the simulation, which validate the proposed control strategies, are presented and discussed.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();
                                                        
        $ponencia = new Ponencia();
        $ponencia->tema = "Virtual reality system for assistance in treating respiratory disorders";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Investigador ARSI";
        $ponencia->resumen = "This paper presents a virtual reality app for pulmonary diseases treatment in children through VR game, which consists of visual and auditory stimulation thus the therapy won’t be annoying to the patient. The procedure consists of a previous stage of respiratory training which the patient is asked for a deep inhalation followed by an exhalation, both within a period of 3 to 6 s and requested 3 times. The app will offer the patient different activities in a virtual environment depending on the inhalation and exhalation exercise required by the stages of their therapy, using HTC VIVE virtual reality glasses. Recognition of inhalation and exhalation exercises is performed using a DTW classifier running in real time in MATLAB and with a direct, two-way connection to Unity. Therefore, the app encourages the patient to perform the exercises therapy in an accurate and fun way and as a result respiratory signal graphs are shown for evaluation by the therapist.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();
        
        $ponencia = new Ponencia();
        $ponencia->tema = "Virtual training for industrial automation processes through pneumatic controls";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Profesor del Departamento de Eléctrica y Electrónica";
        $ponencia->resumen = "This work presents the implementation of virtual environments oriented to managing pneumatic controls applied to industrial processes in order to strengthen training and teaching-learning processes. The implemented application enables the multi-user immersion and interaction with the aim to accomplish predefined tasks to be developed within lab environments and virtualized sceneries for industrial processes. Obtained results show how easy it is to interact with the proposed multi-user environment.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();
        
                
        $ponencia = new Ponencia();
        $ponencia->tema = "e-Tourism: Governmental Planning and Management Mechanism";
        $ponencia->evento = "International Conference on Augmented Reality, Virtual Reality and Computer Graphics (SALENTO AVR 2018)";
        $ponencia->lat = 40.139115;
        $ponencia->lng = 18.482259;
        $ponencia->lugar = "Otranto, Italia";
        $ponencia->cargo = "Profesor del Departamento de Eléctrica y Electrónica";
        $ponencia->resumen = "This article proposes the development of a virtual application as a planning and government management tool, focused on the digitization of objects, monuments-buildings, and real environments using 2D and 3D virtualization techniques. The application considers two modes of use: PLANNER for use by managers and planners of offices and technical units linked to tourism; and TOURIST created for travelers, hikers or tourists in general. The first allows recreating new scenes incorporating objects that make it attractive to the tourist recourse, and thus achieve a projected image of the vision of development proposed by the planner; and the second designed to recreate touristic environments and program circuits, provoking a tourist avatar through HTC VIVE, Oculus Rift and GearVR Headsets to make the application available to the largest market segment of virtual reality viewers. In addition, the app allows the interaction of several users in the same scenario to exchange information and experiences of the place.";
        $ponencia->fecha = "2018-06-01";
        $ponencia->save();

        $ponencia = new Ponencia();
        $ponencia->tema = "Unified Nonlinear Control for Car-like Mobile Robot 4 Wheels Steering";
        $ponencia->evento = "International Conference on Intelligent Robotics and Applications (ICIRA 2018)";
        $ponencia->lat = -32.9351569;
        $ponencia->lng = 151.7545177;
        $ponencia->lugar = "New Castle, Australia";
        $ponencia->cargo = "Estudiante de Ingeniería en Mecatrónica";
        $ponencia->resumen = "This document presents a unified movement control scheme for mobile robots, a mobile four-wheel steering robot, which solves the control of the position of points, tracking problems of trajectories and path following. The control problem is solved according to the CLMR-4WS kinematic model. Then, a control scheme of two angular positions and one heading velocity is considered. The stability of the system is shown through the Lyapunov method.";
        $ponencia->fecha = "2018-08-09";
        $ponencia->save();
        
        
    }

}
