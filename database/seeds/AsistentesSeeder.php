<?php

use Illuminate\Database\Seeder;

use App\Ayudante;
use App\Universidad;

class AsistentesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$espe = Universidad::where('abreviacion','=', 'ESPE')->first();

        $uta = Universidad::where('abreviacion','=', 'UTA')->first();

        $espoch = Universidad::where('abreviacion','=', 'ESPOCH')->first();

        $unach = Universidad::where('abreviacion','=', 'UNACH')->first();

        //Ayudantes 2017
        Ayudante::create([
            'nombres'=> 'Daniel Alejandro',
            'apellidos'=> 'Castillo Carrión',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual, Sector Industrial',
            'pasante' => true,
            'pasante_desde' => '2017-10-01',
            'pasante_hasta' => '2018-02-28',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Edison Ruben',
            'apellidos'=> 'Sasig Simba',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica de servicio',
            'pasante' => true,
            'pasante_desde' => '2016-10-01',
            'pasante_hasta' => '2017-02-01',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Cristian Andrés',
            'apellidos'=> 'Sanaguano Guevara',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica de servicio',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'María Belén',
            'apellidos'=> 'Zapata Sarzosa',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Procesamiento de imagen y redes neuronales',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Johana Fernanda',
            'apellidos'=> 'Guanoluisa Mendoza',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Procesamiento de imagen y redes neuronales',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Manuel Alejandro',
            'apellidos'=> 'León Cárdenas',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Paul Alejandro',
            'apellidos'=> 'Romero Andrade',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Mateo Alejandro',
            'apellidos'=> 'Parreño Alvarez',
            'carrera' => 'Ingeniería Electrónica',
            'area_investigacion' => 'Tecnología de la Información y Comunicación',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'María Inés',
            'apellidos'=> 'Erazo Bravo',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Erick Paul',
            'apellidos'=> 'Mera Otoya',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'pasante' => true,
            'pasante_desde' => '2016-04-01',
            'pasante_hasta' => '2014-08-30',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Diego Omar',
            'apellidos'=> 'Camacho Franco',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'pasante' => true,
            'pasante_desde' => '2016-04-01',
            'pasante_hasta' => '2014-08-30',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Álvaro Patricio',
            'apellidos'=> 'Velasco Vasco',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'pasante' => true,
            'pasante_desde' => '2016-04-01',
            'pasante_hasta' => '2014-08-30',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Orfait Patricio',
            'apellidos'=> 'Ortiz Carvajal',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica de servicio',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Alex Mauricio',
            'apellidos'=> 'Santana Gallo',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica de servicio',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Pablo Sebastián',
            'apellidos'=> 'Aulestia Araujo',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Procesamiento de imagen y redes neuronales',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Jonathan Saúl',
            'apellidos'=> 'Talahua Remache',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Procesamiento de imagen y redes neuronales',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Verónica Elizabeth',
            'apellidos'=> 'Luna Salguero',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Tecnología de la Información y Comunicación',
            'tesista' => true,
            'tesista_desde' => '2016-10-01',
            'tesista_hasta' => '2017-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Vilma Johana',
            'apellidos'=> 'Basantes Basantes',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica y control',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Gabriela Alexandra',
            'apellidos'=> 'Bautista Reinoso',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica y control',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Leandro Gabriel',
            'apellidos'=> 'Corrales Tibán',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Visión Artificial',
            'tesista' => true,
            'tesista_desde' => '2016-01-01',
            'tesista_hasta' => '2017-01-30',
            'universidad_id' => $espe->id
        ]);


        Ayudante::create([
            'nombres'=> 'Catherine Liseth',
            'apellidos'=> 'Gálvez Jácome',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Visión Artificial',
            'tesista' => true,
            'tesista_desde' => '2016-01-01',
            'tesista_hasta' => '2017-01-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Andrés Daniel',
            'apellidos'=> 'Acurio Santamaría',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual',
            'tesista' => true,
            'tesista_desde' => '2016-04-01',
            'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'William Mauricio',
            'apellidos'=> 'López Villavicencio',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual',
            'tesista' => true,
            'tesista_desde' => '2017-02-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Carlos Iván',
            'apellidos'=> 'Bustamante Duque',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Visión Artificial',
            'tesista' => true,
            'tesista_desde' => '2017-05-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Miguel Dario',
            'apellidos'=> 'Escudero Vásconez',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Visión Artificial',
            'tesista' => true,
            'tesista_desde' => '2017-05-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Pablo José',
            'apellidos'=> 'Salazar Villacís',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Instrumentación Industrial',
            'tesista' => true,
            'tesista_desde' => '2017-05-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Marcelo Javier',
            'apellidos'=> 'Silva Salinas',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Instrumentación Industrial',
            'tesista' => true,
            'tesista_desde' => '2017-05-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Germán Israel',
            'apellidos'=> 'Once Sucuzhañay',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Instrumentación Industrial',
            'tesista' => true,
            'tesista_desde' => '2017-03-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Jonathan Javier',
            'apellidos'=> 'Rivera Comi',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Instrumentación Industrial',
            'tesista' => true,
            'tesista_desde' => '2017-03-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Edwin Javier',
            'apellidos'=> 'Mena Cajas',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual',
            'tesista' => true,
            'tesista_desde' => '2017-02-01',
            //'tesista_hasta' => '2017-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Fernando Ramiro',
            'apellidos'=> 'Pusda Cheza',
            'carrera' => 'Ingeniería Automótriz',
            'area_investigacion' => 'Realidad Aumentada',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Francisco Fabián',
            'apellidos'=> 'Valencia Tipán',
            'carrera' => 'Ingeniería Automótriz',
            'area_investigacion' => 'Realidad Aumentada',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Edison Paul',
            'apellidos'=> 'Yugcha Casa',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Eficiencia de Bombas Centrífugas',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Jonathan Ismael',
            'apellidos'=> 'Ubilluz Heredia',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Eficiencia de Bombas Centrífugas',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Fernanda Elizabeth',
            'apellidos'=> 'Yanchapanta Yánez',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Sector Industrial',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Bryan Steeven',
            'apellidos'=> 'Santo Barahona',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Sector Industrial',
            'tesista' => true,
            'tesista_desde' => '2018-11-01',
            'tesista_hasta' => '2019-05-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Jorge Luis',
            'apellidos'=> 'Mora Aguilar',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'tesista' => true,
            'tesista_desde' => '2018-10-01',
            'tesista_hasta' => '2019-03-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Oscar Agustín',
            'apellidos'=> 'Mayorga Mayorga',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual, Inteligencia Artificial',
            'tesista' => true,
            'tesista_desde' => '2018-07-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Elvis Ándres',
            'apellidos'=> 'Bunces Naranjo',
            'carrera' => 'Ingeniería Automotriz',
            'area_investigacion' => 'Realidad Virtual, Robótica Social',
            'tesista' => true,
            'tesista_desde' => '2018-05-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Edison Amilcar',
            'apellidos'=> 'Chicaiza Quispe',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Aumentada',
            'tesista' => true,
            'tesista_desde' => '2018-05-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Edgar Iván',
            'apellidos'=> 'de la Cruz Vaca',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Aumentada',
            'tesista' => true,
            'tesista_desde' => '2018-05-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Rubén Dario',
            'apellidos'=> 'Mullo Aimacaña',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Energías Renovables',
            'tesista' => true,
            'tesista_desde' => '2018-05-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Max Gonzalo',
            'apellidos'=> 'Chiluisa Aimacaña',
            'carrera' => 'Ingeniería Electromecánica',
            'area_investigacion' => 'Realidad Virtual, Energías Renovables',
            'tesista' => true,
            'tesista_desde' => '2018-05-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Cristian Alexis',
            'apellidos'=> 'Yanez Briones',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-05-01',
            'pasante_hasta' => '2018-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Humberto David',
            'apellidos'=> 'Oleas Montesdeoca',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-05-01',
            'pasante_hasta' => '2018-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Mario Fernando',
            'apellidos'=> 'Vargas Brito',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Darwin Santiago',
            'apellidos'=> 'Sarsoza García',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Richar Fernando',
            'apellidos'=> 'Navas Jácome',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);


        Ayudante::create([
            'nombres'=> 'Klever David',
            'apellidos'=> 'Morales Antamba',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Samanta Jennifer',
            'apellidos'=> 'Hurtado Caina',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Milton Eduardo',
            'apellidos'=> 'Cárdenas Arias',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-10-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Héctor Daniel',
            'apellidos'=> 'Tenezaca Bejarano',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-07-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Alexander Marcelo',
            'apellidos'=> 'Ortiz Rendon',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Robótica',
            'pasante' => true,
            'pasante_desde' => '2018-03-01',
            'pasante_hasta' => '2018-07-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Diana Carolina',
            'apellidos'=> 'Torres Castillo',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Realidad Virtual, Robótica',
            'tesista' => true,
            'tesista_desde' => '2018-02-01',
            'tesista_hasta' => '2018-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'María Gabriela',
            'apellidos'=> 'Méndez Hurtado',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Realidad Virtual, Robótica',
            'tesista' => true,
            'tesista_desde' => '2018-02-01',
            'tesista_hasta' => '2018-08-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'María Fernanda',
            'apellidos'=> 'Molina Fernández',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica y Control',
            'tesista' => true,
            'tesista_desde' => '2018-01-01',
            'tesista_hasta' => '2018-12-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Vilma Johana',
            'apellidos'=> 'Basantes Basantes',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica y Control',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Gabriela Alexandra',
            'apellidos'=> 'Bautista Reinoso',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Robótica y Control',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-02-28',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Mateo Alejandro',
            'apellidos'=> 'Parreño Alvarez',
            'carrera' => 'Ingeniería Electrónica',
            'area_investigacion' => 'Tecnología de la Información y Comunicación',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-03-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Manuel Alejandro',
            'apellidos'=> 'León Cárdenas',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-07-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Paul Alejandro',
            'apellidos'=> 'Romero Andrade',
            'carrera' => 'Ingeniería Mecatrónica',
            'area_investigacion' => 'Materiales y técnica de producción',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-07-30',
            'universidad_id' => $espe->id
        ]);

        Ayudante::create([
            'nombres'=> 'Daniel Alejandro',
            'apellidos'=> 'Castillo Carrión',
            'carrera' => 'Ingeniería Electrónica e Instrumentación',
            'area_investigacion' => 'Realidad Virtual, Sector Industrial',
            'tesista' => true,
            'tesista_desde' => '2017-10-01',
            'tesista_hasta' => '2018-07-30',
            'universidad_id' => $espe->id
        ]);


    }
}
