<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UniversidadesSeeder::class);
         $this->call(UsersSeeder::class);
         //$this->call(PublicacionesSeeder::class);
        // $this->call(GoogleScholarSeeder::class);
         $this->call(ProyectosSeeder::class);
         $this->call(InvestigacionesSeeder::class);
         $this->call(TrabajosSeeder::class);
         $this->call(ReconocimientosSeeder::class);
         $this->call(InteresesSeeder::class);
         $this->call(AsistentesSeeder::class);
         $this->call(TitulacionesSeeder::class);
         $this->call(PonenciasSeeder::class);
         $this->call(DocumentosSeeder::class);
    }
}
