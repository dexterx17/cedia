<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Trabajo;


class TrabajosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $victor = User::find(1);

        $t1 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor Titular Principal 1',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2012-11-01',
            'actual' => TRUE,
            'user_id' => $victor->id
        ]);

        $t2 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Maestría',
            'departamento' => 'Facultad de Ingeniería en Sistemas, Electrónica e Industrial',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2016-07-01',
            'hasta' => '2016-08-01',
            'funciones' => 'Maestría en Automatización y Sistemas de Control',
            'user_id' => $victor->id
        ]);

        $t3 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Maestría',
            'departamento' => 'Facultad de Informática y Electrónica',
            'institucion' => 'Escuela Superior Politécnica de Chimborazo',
            'ubicacion' => 'Riobamba, Ecuador',
            'desde' => '2015-01-01',
            'hasta' => '2015-02-01',
            'funciones' => 'Maestría en Sistemas de Control y Automatización Industrial',
            'user_id' => $victor->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Investigador',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Facultad de ingeniería en Sistemas, Electrónica e Industrial',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2012-01-01',
            'hasta' => '2014-02-01',
            'user_id' => $victor->id
        ]);

        $t5 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Investigador',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Facultad de Informática y Electrónica',
            'institucion' => 'Escuela Superior Politécnica de Chimborazo',
            'ubicacion' => 'Riobamba, Ecuador',
            'desde' => '2012-03-01',
            'hasta' => '2013-02-01',
            'user_id' => $victor->id
        ]);

        $t6 = factory(Trabajo::class)->create([
            'cargo' => 'Instructor de Laboratorio',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Automatización y Control, Facultad de Ingeniería Eléctrica y Electrónica,',
            'institucion' => 'Escuela Politécnica Nacional',
            'ubicacion' => 'Quito, Ecuador',
            'desde' => '2006-01-01',
            'hasta' => '2008-12-31',
            'user_id' => $victor->id
        ]);

        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Director del Grupo de Investigación',
            'departamento' => 'Grupo de Investigación de Tecnologías Aplicadas a la Biomedicina – GITbio',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Sangolquí, Ecuador',
            'desde' => '2016-08-01',
            'hasta' => '2017-11-30',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t8 = factory(Trabajo::class)->create([
            'cargo' => 'Director',
            'departamento' => 'Programa de Investigación de Mecatrónica',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2016-02-01',
            'actual' => TRUE,
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Asesor de Investigación del Rectorado',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2014-08-01',
            'hasta' => '2014-11-30',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t10 = factory(Trabajo::class)->create([
            'cargo' => 'Director de Investigación y Desarrollo',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2012-10-01',
            'hasta' => '2014-07-31',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t11 = factory(Trabajo::class)->create([
            'cargo' => 'Presidente del Directorio Centro de Formación Ciudadana de Tungurahua',
            'institucion' => 'Honorable Gobierno Provincial de Tungurahua',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2012-10-01',
            'hasta' => '2015-03-31',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t12 = factory(Trabajo::class)->create([
        	'cargo' => 'Asesor de Proyectos',
        	'institucion' => 'UPDATECOM Cia. Ltda.',
        	'ubicacion' => 'Quito, Ecuador',
        	'desde' => '2012-03-01',
        	'actual' => TRUE,
            'administrativo' => TRUE,
        	'user_id' => $victor->id
        ]);

        $hector = User::where('nombres','=','Héctor C.')->first();


        $t1 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor Titular Agregado 2',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Energía y Mecánica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2008-01-01',
            'actual' => TRUE,
            'user_id' => $hector->id
        ]);
        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Investigador',
            'departamento' => 'Departamento de Ciencias de la Energía y Mecánica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2008-04-01',
            'actual' => TRUE,
            'user_id' => $hector->id
        ]);


        $cris = User::where('email','=','cmgallardop@gmail.com')->first();

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Analista de Investigación',
            'tiempo' => 'tiempo completo',
         //   'departamento' => 'Departamento de Ciencias de la Energía y Mecánica',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2017-10-16',
            'actual' => TRUE,
            'user_id' => $cris->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Analista de Investigación',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Facultad de Ciencias Administrativas',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2017-03-15',
            'hasta' => '2017-10-15',
            'user_id' => $cris->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Auxiliar de Investigación',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Facultad de Ingeniería en Sistemas, Electrónica e Industrial',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2015-09-01',
            'hasta' => '2016-07-31',
            'user_id' => $cris->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Becario de investigación',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Facultad de Ingeniería en Sistemas, Electrónica e Industrial',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2014-01-01',
            'hasta' => '2014-12-31',
            'user_id' => $cris->id
        ]);

        $fernando = User::where('email','=','fernandochicaiza137@gmail.com')->first();

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Investigador auxiliar',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Laboratorio de Investigación, en Automatización, Robótica y Sistemas Inteligentes',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2017-08-01',
            'actual' => TRUE,
            'user_id' => $fernando->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Investigador auxiliar',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Laboratorio de Investigación, en Automatización, Robótica y Sistemas Inteligentes',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2015-08-01',
            'hasta' => '2016-08-01',
            'user_id' => $fernando->id
        ]);


        $jess = User::where('email','=','jsortiz4@espe.edu.ec')->first();

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2016-11-01',
            'actual' => TRUE,
            'user_id' => $jess->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Carrera de Ingeniería en Sistemas',
            'institucion' => 'Universidad Tecnológica Indoamérica',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2015-10-01',
            'hasta' => '2016-03-01',
            'user_id' => $jess->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Carrera de Ingeniería en Sistemas',
            'institucion' => 'Universidad Tecnológica Indoamérica',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2015-04-01',
            'hasta' => '2015-09-01',
            'user_id' => $jess->id
        ]);

        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Carrera de Ingeniería en Sistemas',
            'institucion' => 'Universidad Tecnológica Indoamérica',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2015-03-01',
            'hasta' => '2015-04-01',
            'user_id' => $jess->id
        ]);


        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Facultad de Ingeniería en Sistemas, Electrónica e Industrial',
            'institucion' => 'Universidad Técnica de Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2014-10-01',
            'hasta' => '2015-03-01',
            'user_id' => $jess->id
        ]);


        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesora',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Carrera de Ingeniería Industrial',
            'institucion' => 'Universidad Tecnológica Indoamérica',
            'ubicacion' => 'Ambato, Ecuador',
            'desde' => '2014-09-01',
            'hasta' => '2014-10-01',
            'user_id' => $jess->id
        ]);

        $marcelo = User::where('email','=','rmalvarez@espe.edu.ec')->first();


        $t4 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2010-10-01',
            'actual' => TRUE,
            'user_id' => $marcelo->id
        ]);


        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Integrante del Grupo de Investigación',
            'departamento' => 'Grupo de Investigación de Tecnologías Aplicadas a la Biomedicina – GITbio',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Sangolquí, Ecuador',
            'desde' => '2016-08-01',
            'hasta' => '2017-11-30',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Integrante del Grupo de Investigación',
            'departamento' => 'Grupo de Investigación de Redes Inalámbricas – Wicom Energy',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Sangolquí, Ecuador',
            'desde' => '2014-03-01',
            'hasta' => '2016-07-30',
            'administrativo' => TRUE,
            'user_id' => $victor->id
        ]);

        $oscar = User::where('email','=','oscararteaga2005@yahoo.es')->first();

        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Supervisor de Montaje de Equipos Rotativos',
            'departamento' => 'Proyecto de Ampliación de la Refinería Estatal de Esmeraldas para el Procesamiento de Crudo Pesado',
            'institucion' => 'Refinería Estatal de Esmeraldas',
            'ubicacion' => 'Esmeraldas, Ecuador',
            'desde' => '1997-02-01',
            'hasta' => '1998-08-30',
            'user_id' => $oscar->id
        ]);

        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Investigador',
            'tiempo' => 'medio tiempo',
            'departamento' => 'Departamento de Ciencias de la Energía y Mecánica',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2000-03-01',
            'hasta' => '2011-08-30',
            'user_id' => $oscar->id
        ]);

        $t7 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor-Investigador',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Ciencias de la Energía y Mecánica',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2011-08-01',
            'actual' => TRUE,
            'user_id' => $oscar->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Coordinador de Investigación del DCEM',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2017-01-01',
            'actual' => TRUE,
            'administrativo' => TRUE,
            'user_id' => $oscar->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Jefe del Laboratorio de Soldadura',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2013-05-01',
            'actual' => TRUE,
            'administrativo' => TRUE,
            'user_id' => $oscar->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Jefe del Laboratorio de Refrigeración',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2013-05-01',
            'actual' => TRUE,
            'administrativo' => TRUE,
            'user_id' => $oscar->id
        ]);

        $paola = User::where('nombres','=','Paola M.')->first();

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Asistente de sistemas',
            'institucion' => 'Empresa Eléctrica Ambato',
            'ubicacion' => 'Ambato, Ecuador',
            'departamento' => 'Departamento de Planificación. Sección Estudios Técnicos',
            'funciones' => 'Ingreso de información al Sistema GIS. Digitalización de planos',
            'desde' => '2002-07-01',
            'hasta' => '2003-01-31',
             'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Docente Ocasional',
            'tiempo' => 'tiempo completo',
            'institucion' => 'Universidad Técnica de Cotopaxi',
            'ubicacion' => 'Latacunga, Ecuador',
            'departamento' => 'Unidad Académica de Ciencias de la Ingeniería y Aplicadas - Carreras de Ingeniería Eléctrica',
            'funciones' => 'Asignaturas dictadas: Sistemas de Control, Sistemas Digitales, Teoría Electromagnética, Asignaturas dictadas: Sistemas de Control, Sistemas Digitales, Teoría Electromagnética, - Carrera de Ingeniería Electromecánica',
            'desde' => '2005-10-01',
            'hasta' => '2015-09-31',
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Docente Servicios Profesionales',
            'tiempo' => 'tiempo completo',
            'institucion' => 'Escuela Politécnica del Ejército sede Latacunga',
            'ubicacion' => 'Latacunga, Ecuador',
            'departamento' => 'Departamento de Ciencias Exactas',
            'funciones' => 'Asignatura dictada: Física II',
            'desde' => '2012-04-01',
            'hasta' => '2012-08-30',
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Docente Ocasional',
            'tiempo' => 'tiempo completo',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'funciones' => 'Asignaturas dictadas: Electromagnetismos I, Electromagnetismo II, Señales y Sistemas, Electrónica, Sistemas de Control',
            'desde' => '2015-10-01',
            'actual' => TRUE,
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Coordinadora de la Carrera de Ingeniería Eléctrica',
            'institucion' => 'Universidad Técnica de Cotopaxi',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2006-10-01',
            'hasta' => '2012-10-30',
            'administrativo' => TRUE,
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Coordinadora de Investigación de la Carrera de Ingeniería Eléctrica',
            'institucion' => 'Universidad Técnica de Cotopaxi',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2010-10-01',
            'hasta' => '2011-02-30',
            'administrativo' => TRUE,
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Coordinadora de Trabajo de Grado de la Carrera de Ingeniería Eléctrica',
            'institucion' => 'Universidad Técnica de Cotopaxi',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2011-03-01',
            'hasta' => '2011-08-30',
            'administrativo' => TRUE,
            'user_id' => $paola->id
        ]);

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Coordinadora de Supervisión de Prácticas Preprofesionales de las carreras de Ingeniería Eléctrica e Ingeniería Industrial',
            'institucion' => 'Universidad Técnica de Cotopaxi',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2012-11-01',
            'hasta' => '2015-08-30',
            'administrativo' => TRUE,
            'user_id' => $paola->id
        ]);

        $edwin = User::where('email','=','eppruna@espe.edu.ec')->first();

        $t9 = factory(Trabajo::class)->create([
            'cargo' => 'Jefe del Laboratorio de Redes Industriales y Control de Procesos',
            'institucion' => 'Universidad de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2015-03-01',
            'actual' => TRUE,
            'administrativo' => TRUE,
            'user_id' => $edwin->id
        ]);


        $t1 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor Titular Agregado 3',
            'tiempo' => 'tiempo completo',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2016-12-01',
            'actual' => TRUE,
            'user_id' => $edwin->id
        ]);

        $t1 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor Titular Auxiliar 1',
            'departamento' => 'Departamento de Eléctrica y Electrónica',
            'institucion' => 'Universidad  de las Fuerzas Armadas ESPE',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2008-11-01',
            'hasta' => '2016-11-30',
            'user_id' => $edwin->id
        ]);

        $t1 = factory(Trabajo::class)->create([
            'cargo' => 'Profesor a contrato',
            'institucion' => 'Instituto Tecnológico Superior Aeronáutico',
            'ubicacion' => 'Latacunga, Ecuador',
            'desde' => '2008-03-01',
            'hasta' => '2013-08-30',
            'user_id' => $edwin->id
        ]);

    }
}
