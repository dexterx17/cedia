<?php

use Illuminate\Database\Seeder;

use App\Universidad;

class UniversidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $espe = factory(Universidad::class)->create([
        	'nombre' => 'Universidad de las Fuerzas Armadas',
        	'abreviacion' => 'ESPE',
            'url' => 'http://www.espe.edu.ec/',
            'logo' => 'espe.png'
        ]);

        $uta = factory(Universidad::class)->create([
        	'nombre' => 'Universidad Técnica de Ambato',
        	'abreviacion' => 'UTA',
            'url' => 'http://www.uta.edu.ec/',
            'logo' => 'uta.jpeg'
        ]);

        $espoch = factory(Universidad::class)->create([
        	'nombre' => 'Escuela Superior Politécnica de Chimborazo',
        	'abreviacion' => 'ESPOCH',
            'url' => 'https://www.espoch.edu.ec/',
            'logo' => 'espoch.png'
        ]);

		$unach = factory(Universidad::class)->create([
        	'nombre' => 'Universidad Nacional de Chimborazo',
        	'abreviacion' => 'UNACH',
            'url' => 'http://www.unach.edu.ec/',
            'logo' => 'unach.png'
        ]);

        $epn = factory(Universidad::class)->create([
            'nombre' => 'Escuela Politécnica Nacional',
            'abreviacion' => 'EPN',
            'url' => 'http://www.epn.edu.ec/',
            'logo' => 'epn.png'
        ]);

        $unaut = factory(Universidad::class)->create([
            'nombre' => 'Universidad Nacional de San Juan',
            'abreviacion' => 'INAUT',
            'url' => 'http://www.unsj.edu.ar/',
            'logo' => 'inaut.png'
        ]);

        $urjc = factory(Universidad::class)->create([
            'nombre' => 'Universidad Rey Juan Carlos',
            'abreviacion' => 'URJC',
            'url' => 'https://www.urjc.es/',
            'logo' => 'urjc.png'
        ]);

        $ups = factory(Universidad::class)->create([
            'nombre' => 'Universidad Politécnica Salesiana',
            'abreviacion' => 'UPS',
            'url' => 'http://www.ups.edu.ec/',
            'logo' => 'ups.png'
        ]);

        $puce = factory(Universidad::class)->create([
            'nombre' => 'Pontificia Universidad Católica del Ecuador',
            'abreviacion' => 'PUCE',
            'url' => 'https://www.puce.edu.ec/',
            'logo' => 'puce.png'
        ]);

        $zaragoza = factory(Universidad::class)->create([
            'nombre' => 'Universidad de Zaragoza',
            'abreviacion' => 'Universidad de Zaragoza',
            'url' => 'https://www.unizar.es/',
            'logo' => 'zaragoza.png'
        ]);

        $leira = factory(Universidad::class)->create([
            'nombre' => 'Instituto Politécnico de Leiria',
            'abreviacion' => 'IPL',
            'url' => 'https://www.ipleiria.pt/home/',
            'logo' => 'leira.png'
        ]);
        
        $iessl = factory(Universidad::class)->create([
            'nombre' => 'Hospital Instituto Ecuatoriano de Seguridad Social de Latacunga',
            'abreviacion' => 'IESSL',
            'logo' => 'iess.jpeg'
        ]);

        $uti = factory(Universidad::class)->create([
            'nombre' => 'Universidad Tecnológica Indoamérica',
            'abreviacion' => 'UTI',
            'url' => 'http://www.uti.edu.ec',
            'logo' => 'uti.png'
        ]);

        $uide = factory(Universidad::class)->create([
            'nombre' => 'Universidad Internacional del Ecuador',
            'abreviacion' => 'UIDE',
            'url' => 'https://www.uide.edu.ec',
            'logo' => 'uide.png'
        ]);

        $ucv = factory(Universidad::class)->create([
            'nombre' => 'Universidad Central de Venezuela',
            'abreviacion' => 'UCV',
            'url' => 'http://www.ucv.ve/',
            'logo' => 'ucv.png'
        ]);

        $ucv = factory(Universidad::class)->create([
            'nombre' => 'Escuela Politécnica Superior - Universidad Autónoma de Madrid',
            'abreviacion' => 'UAM',
            'url' => 'https://www.uam.es/ss/Satellite/EscuelaPolitecnica/es/home.htm',
            'logo' => 'UAM-EPS.png',
            'visible' => false
        ]);

    }
}
