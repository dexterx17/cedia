<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Proyecto;
use App\Universidad;

class ProyectosSeeder extends Seeder
{
        /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Universidades 
        $espe = Universidad::where('abreviacion','=', 'ESPE')->first();

        $uta = Universidad::where('abreviacion','=', 'UTA')->first();

        $espoch = Universidad::where('abreviacion','=', 'ESPOCH')->first();

        $unach = Universidad::where('abreviacion','=', 'UNACH')->first();

        $epn = Universidad::where('abreviacion','=', 'EPN')->first();

        $inaut = Universidad::where('abreviacion','=', 'INAUT')->first();

        $urjc = Universidad::where('abreviacion','=', 'URJC')->first();

        $ups = Universidad::where('abreviacion','=', 'UPS')->first();

        $puce = Universidad::where('abreviacion','=', 'PUCE')->first();

        $zaragoza = Universidad::where('abreviacion','=', 'Universidad de Zaragoza')->first();
        
        $leira = Universidad::where('abreviacion','=', 'IPL')->first();

        $iessl = Universidad::where('abreviacion','=', 'IESSL')->first();
       
        $uti = Universidad::where('abreviacion','=', 'UTI')->first();
        
        $uide = Universidad::where('abreviacion','=', 'UIDE')->first();
        
        $ucv = Universidad::where('abreviacion','=', 'UCV')->first();

        //Investigadores
        $victor = User::where('nombres', '=', 'Victor H.')->first();
        $edison = User::where(   'nombres', '=', 'Edison G.')->first();
        $david = User::where(   'nombres', '=', 'David R.')->first();
        $jorge = User::where(   'nombres', '=', 'Jorge S.')->first();
        $jess = User::where(   'nombres', '=', 'Jessica S.')->first();
        $jaime = User::where(   'nombres', '=', 'Jaime Augusto')->first();
        $cris = User::where(   'nombres', '=', 'Cristian Mauricio')->first();
        $washo = User::where(   'nombres', '=', 'Washington')->first();
        $chris = User::where(   'nombres', '=', 'Christian')->first();
        $fer = User::where(   'nombres', '=', 'Fernando')->first();
        $renato = User::where(   'nombres', '=', 'Renato')->first();
        $jose = User::where(   'nombres', '=', 'José L.')->first();
        $hector = User::where('nombres', '=', 'Héctor C.')->first();
        $marcelo = User::where(   'nombres', '=', 'Marcelo R.')->first();
        $oscar = User::where(   'nombres', '=', 'Oscar B.')->first();
        $paola = User::where(   'nombres', '=', 'Paola M.')->first();
        $edwin = User::where(   'nombres', '=', 'Edwin P.')->first();
        $aldrin = User::where(   'nombres', '=', 'Aldrin G.')->first();
        $ivon = User::where(   'nombres', '=', 'Ivón Patricia')->first();
        $victor_zambrano = User::where(   'nombres', '=', 'Victor Danilo')->first();
        $julio = User::where(   'nombres', '=', 'Julio César')->first();
        $lucho = User::where(   'nombres', '=', 'Luis Alfonso')->first();
        $cesar = User::where(   'nombres', '=', 'César A.')->first();
        $julio_acosta = User::where(   'nombres', '=', 'Julio Francisco')->first();
        $franklin = User::where(   'nombres', '=', 'Franklin Manuel')->first();
        $javier = User::where(   'nombres', '=', 'javier Santiago')->first();
        $lore = User::where(   'nombres', '=', 'Mayra Lorena')->first();
        $andres = User::where(   'nombres', '=', 'Ándres')->first();
        $lenin = User::where(   'nombres', '=', 'Lenin Rene')->first();
        $bryan = User::where(   'nombres', '=', 'Bryan Stefano')->first();
        $patricio = User::where(   'nombres', '=', 'Edison Patricio')->first();
        $paul = User::where(   'nombres', '=', 'Paúl Alexi')->first();



        $proyecto = factory(Proyecto::class)->create([
            'nombre' => 'Control Coordinado Multi-Operador Aplicado a un Robot Manipulador Aéreo',
            'resumen' => 'Hasta la presente fecha es prácticamente imposible que un robot sea 100%
autónomo para aplicaciones en ambientes no estructurado, esto se debe a la gran
cantidad de variables que se generan en entornos dinámicos, desconocidos y variantes
en el tiempo, razón por la cual es eminente la utilización de robots tele-operados. La
tele-operación de robots implica maniobrar un robot a distancia para realizar alguna
tarea dada permitiendo de esta manera, que un operador humano pueda
“transportar” su capacidad y destreza hacia ambientes de trabajos remotos y/o
peligrosos, minimizando de esa manera los riesgos asociados, o más aún, alcanzar
lugares inaccesibles por el hombre. Así, se puede extender la inteligencia y experiencia
de expertos hacia aplicaciones a distancia. En este contexto, este trabajo propone la
tele-operación bilateral de un sistema manipulador aéreo; este sistema se caracteriza
por tener un alto grado de redundancia, debido a que combina la capacidad de
manipulación de un brazo robótico de base fija con la navegación de un vehículo aéreo
no tripulado con ala rotativa. Estos sistemas permiten realizar las misiones más
habituales de los sistemas robóticos que requieren tanto la capacidad de navegación
aérea y manipulación.',
            'descripcion' => 'El presente proyecto de investigación tiene como objetivo proponer y analizar analíticamente la estabilidad de un esquema de tele-operación bilateral que permita que uno o varios operadores humanos maniobren de forma coordinada un robot manipulador aéreo, considerando en el desarrollo los efectos negativos que ocasionan los retardos de tiempo, a fin de incrementar la transparencia del sitio remoto a través de entornos de realidad virtual y realidad aumentada, en el sitio local.',
            'objetivo_general' => 'Proponer un esquema de tele-operación bilateral que permita que uno o varios operadores humanos maniobren de forma coordinada un robot manipulador aéreo, considerando en el desarrollo los efectos negativos que ocasionan los retardos de tiempo, a fin de incrementar la transparencia del sitio remoto a través de entornos de realidad virtual y realidad aumentada, en el sitio local.',
            'objetivos_especificos'=>'Implementar la electrónica y mecánica necesaria sobre un brazo robótico y en vehículo aéreo no
tripulado de tipo ala-rotativa e incorporar sensores internos y externos para el monitoreo y control del
sistema manipulador aéreo, además de acondicionar las señales eléctricas de los sensores para su
utilización en algoritmos de control avanzados.
<br>Implementar un simulador 3D en realidad virtual que incorpore las cargas dinámicas del par
plataforma-brazo robótico y que permita la inmersión del operador humano en una tarea de vuelo, en
cual sea posible implementar diferentes algoritmos de control avanzado autónomos y tele-operados.
<br>Desarrollar una interfaz de realidad aumentada para el control de un robot manipulador aéreo que
permita aumentar la trasparencia del sitio remoto a través de diferentes dispositivos hápticos.
<br>Proponer un esquema de tele-operación bilateral que permitan a uno o varios operadores humanos
maniobren de forma coordinada un robot manipulador aéreo fuera de línea de vista de manera
estable ante la presencia de diferentes retardos de tiempo, ya sean constantes o variables, además
analizar de manera formal la estabilidad de los sistemas de tele-operación bilateral propuestos.
<br>Realizar evaluaciones experimentales en entornos parcialmente estructurados del desempeño de los
algoritmos de control y esquemas de tele-operación propuestos utilizando los manipuladores aéreos
en aplicaciones de traslación y manipulación de objetos.',
        'fecha_inicio' => '2017-09-01',
        'fecha_final' => '2018-08-01',
        'presupuesto' => '220600',
        'financiamiento' => 'externo'
        ]);

        $universidades = [
            $espe->id,
            $uta->id,
            $espoch->id,
            $unach->id,
            $inaut->id
        ];
        
        $proyecto->universidades()->sync($universidades);

        $usuarios = [
            $victor->id,
            $paola->id,
            $jess->id,
            $jorge->id,
            $david->id
        ];

        $proyecto->usuarios()->sync($usuarios);

        $proyecto2 = factory(Proyecto::class)->create([
            'nombre' => 'Tele-Operación Bilateral Cooperativo de Múltiples Manipuladores Móviles',
            'resumen' => 'En contraposición de las aplicaciones clásicas de la robótica en ambientes
industriales estructurados, los desarrollos actuales de esta disciplina están orientados
a aplicaciones en escenarios parcialmente estructurados o variantes en el tiempo.
Durante mucho tiempo, varios prototipos de robots han sido desarrollados para
reemplazar a los humanos en tareas peligrosas, rescate, misiones de guerra,
exploración en lugares confinados; sin embargo, un robot 100% autónomo es
prácticamente imposible que realice una tarea específica en un ambiente no
estructurado. Esto debido a la gran cantidad de variables que se generan en entornos
dinámicos, desconocidos y variantes en el tiempo, razón por la cual es eminente la
utilización de robots tele-operados. La tele-operación de robots implica manejar un
robot a distancia para realizar alguna tarea dada. De esta forma se permite que un
operador humano pueda “transportar” su capacidad y destreza hacia ambientes de
trabajos remotos y/o peligrosos, minimizando de esa manera los posibles riesgos
asociados, o más aún, se puede alcanzar lugares inaccesibles para el hombre. Así, se
puede extender la inteligencia y experiencia de expertos hacia aplicaciones a distancia.',
            'descripcion' => 'El presente proyecto tiene como objetivo analizar y diseñar esquemas de tele-operación bilateral que complemente al operador humano de forma sinérgica en el manejo cooperativo de múltiples manipuladores móviles (brazo robótico montado sobre una plataforma móvil). Para este objetivo se considera en el desarrollo tanto los efectos negativos que ocasionan los retardos de tiempo como la transparencia del sistema.',
            'objetivo_general' => 'Analizar y diseñar esquemas de tele-operación bilateral que complemente al operador humano de forma sinérgica en el manejo cooperativo de múltiples manipuladores móviles (brazo robótico montado sobre una plataforma móvil). Para este objetivo se considera en el desarrollo tanto los efectos negativos que ocasionan los retardos de tiempo como la transparencia del sistema.',
            'objetivos_especificos'=>'* Construir tres robots manipuladores móviles e implementar la electrónica necesaria para incorporar sensores internos y externos para su utilización en algoritmos de control avanzados<br>
             * Generar y evaluar diferentes señales hápticas de realimentación que ayuden al operador a entender mejor el estado de los robots y su entorno, aumentando así la transparencia del sistema<br>
             * Diseñar esquemas de tele-operación bilateral que permitan a un operador manejar de forma cooperativa y coordinada a los robots de manera estable, además analizar la estabilidad de los sistemas de tele-operación propuestos<br> 
             * Desarrollar entornos gráficos en 3D y/o interfaces con programas de simulación de vuelo para poder implementar los algoritmos de control y esquemas de tele-operación desarrollados en simulaciones; y finalmente 5) realizar evaluaciones experimentales del desempeño de los algoritmos de control y esquemas de tele-operación propuestos, utilizando la Nube Computacional de CEDIA para el procesamiento del algoritmo de control a ser implementado en cada uno de los robots; y la Red de Internet Avanzada para la interconexión entre Universidades de las cuales una será el sitio remoto, mientras que las otras Universidades serán el sitio local.',
        'fecha_inicio' => '2018-09-01',
        'fecha_final' => '2018-08-01',
        'presupuesto' => '200000',
        'financiamiento' => 'externo'
        ]);

        $universidades = [
            $espe->id,
            $uta->id,
            $espoch->id,
            $epn->id
        ];
        
        $proyecto2->universidades()->sync($universidades);


        $proyecto3 = factory(Proyecto::class)->create([
            'nombre' => 'Sistema de Soporte a la Enseñanza y Comprensión de Lenguaje de Señas Básico, Aplicando Técnicas de Clasificación de Datos para la Interpretación Gestual',
            'resumen' => 'La privación lingüística o de comunicación en personas, presenta
consecuencias en la aceptación social, se propone una solución al problema
incorporando una herramienta a su desarrollo de lenguaje de señas para la
comunicación, sin embargo, el aprendizaje puede resultar tedioso y presentar ciertas
dificultades para niños. Un sistema interactivo y amigable facilitará el aprendizaje del
lenguaje de señas buscando que se involucre la participación activa del usuario en un
ambiente visual; como resultado el proceso de aprendizaje, será más divertido y se
captará de una mejor manera la realización de los signos. El prototipo diseñado
permite obtener un sistema que servirá de soporte al aprendizaje del lenguaje de
señas básico en personas con discapacidad auditiva o personas con familiares que
tengan esta condición, y de alguna forma contribuir a que la comunicación entre ellas
sea más natural. Por otra parte, el proyecto también busca mitigar la falta de docentes
en centros de aprendizaje especial, con un sistema que permite a los estudiantes
practicar el lenguaje sin la presencia de otro ser humano.',
        'fecha_inicio' => '2016-10-01',
        'fecha_final' => '2017-11-01',
        'presupuesto' => '0',
        'financiamiento' => 'interno'
        ]);

        $universidades = [
            $espe->id,
            $ups->id,
            $urjc->id
        ];
        
        $proyecto3->universidades()->sync($universidades);



        $proyecto4 = factory(Proyecto::class)->create([
            'nombre' => 'Desarrollo de un Sistema Prototipo para Interpretación y Comprensión de Lenguaje de Señas Básico, Aplicando Redes Neuronales',
            'resumen' => 'La mayoría de niños con deficiencias auditivas provienen de familia
oyentes, esto aporta que no exista un desarrollo de una forma eficiente de
comunicación entre ellos, según el Instituto Nacional de Estadística y Censos existen
4.926 niños con deficiencias auditivas en la provincia de Cotopaxi.<br>
El proyecto ofrece la construcción de un sistema prototipo para la interactividad por
medio de gestos, se enmarca en el área de interfaces de interacción naturales con los
computadores y las redes neuronales, el sistema enseña el lenguaje de señas básico,
principalmente a niños con discapacidad auditiva, dicho sistema no necesita que el
usuario manipule físicamente la computadora una vez ejecutado el programa, el
manejo del software responderá al movimiento de las manos. La interfaz amigable
muestra opciones para comenzar la presentación de las señas que debe realizar el
usuario, seguidamente el usuario debe realizar la seña presentada y el programa
reconocerá si fue o no realizada correctamente indicando si debe repetirse o continuar
con la siguiente seña, de esta manera se busca que el aprendizaje del lenguaje de
señas sea dinámico y capte de una forma rápida en comparación con métodos
tradicionales.<br>
Para lograr todo esto se utilizará un sensor gestual que permite reconocer el
movimiento de las manos y dedos, donde todas las señales que envía el dispositivo
serán analizadas y procesadas usando redes neuronales para interpretar cada una de
las posiciones, acciones y movimientos que se realizaron, el dispositivo envía
información de coordenadas en el espacio (x, y, z) de cada mano, cada dedo y cada
articulación. Como metodología de desarrollo del producto se usará Kanban,
metodología ágil que permite lograr objetivos parciales e iterativos para cumplir con el
objetivo final de tener un prototipo para interpretación y comprensión de lenguaje de
señas básico que tiene como base un algoritmo de redes neuronales y una interacción
natural con el sistema.',
        'fecha_inicio' => '2016-10-01',
        'fecha_final' => '2018-10-01',
        'presupuesto' => '3098.88',
        'financiamiento' => 'interno'
        ]);

        $universidades = [
            $espe->id,
            $ups->id,
            $urjc->id,
            $puce->id
        ];
        
        $proyecto4->universidades()->sync($universidades);



        $proyecto5 = factory(Proyecto::class)->create([
            'nombre' => 'Desarrollo de un Demostrador Tecnológico en Pacientes con Daño Cerebral Adquirido',
            'resumen' => 'El innovador uso de la Realidad Virtual (RV) no inmersiva como
complemento a las terapias de rehabilitación tradicionales en pacientes con Daño
Cerebral Adquirido (DCA) ha venido ganando terreno sobre todo a nivel internacional,
La Realidad Virtual (RV) es una tecnología que permite a los sujetos interactuar con los
objetos simulados, ofreciéndoles una retroalimentación continua de los progresos alcanzados en el tiempo. Puesto que uno de los apartados cruciales dentro del proceso
rehabilitador en pacientes DCA se basa en la recuperación motora, es sumamente
recomendable ofrecer a este tipo de pacientes técnicas novedosas que generen
cambios en la reorganización cortical.<br>
El enfoque de la Rehabilitación Motora Tradicional (RMT) está basado en la repetición
de movimientos específicos. Este tipo de rehabilitación es tedioso, monótono y
aburrido. Hoy en día, tecnologías emergentes como pueden ser la Rehabilitación
Virtual Motora (RVM) son complementarias a la Rehabilitación Tradicional. La RVM
ofrece un entorno lúdico que involucra a los pacientes y minimiza el grado de
aburrimiento, incrementándose su motivación y adherencia al tratamiento.<br>
Este tipo de técnicas novedosas podrán ser incluidos en los procesos terapéuticos, que
en conjunción con parámetros como intensidad-duración, permitirán obtener
resultados esperanzadores y prometedores en las habilidades motoras.<br>
La RVM es un método empleado cada vez más en rehabilitación de las habilidades
motoras, permitiendo individualizar tratamientos, facilitar la motivación con el
aprendizaje (convirtiendo los ejercicios en tareas agradables y divertidas), y
posibilitando la monitorización e individualización en el tratamiento.<br>
El uso de dispositivos de realidad virtual permite mediante aplicativos de biofeedback
visuales, aplicar programas dirigidos a la mejora de la reorganización cortical, incluso
después de validar este tipo de experimentos.',
        'fecha_inicio' => '2015-01-01',
        'fecha_final' => '2017-01-01',
        'presupuesto' => '64291.50'
        ]);

        $universidades = [
            $espe->id,
            $zaragoza->id,
            $iessl->id
        ];
        
        $proyecto5->universidades()->sync($universidades);



        $proyecto6 = factory(Proyecto::class)->create([
        	'nombre' => 'Creación y Validación de una Herramienta Tecnológica en Niños con Parálisis Cerebral "VRCHILD"',
            'resumen' => 'Una de las líneas de investigación emergente y relevante dentro del campo
de actuación de la Rehabilitación Tradicional en pacientes con alteraciones
neurológicas es el uso de la Rehabilitación Virtual Motora (RVM). La Realidad Virtual,
en conjunción de mecanismos de retroalimentación y estímulos visuales y/o auditivos,
permiten generar aplicativos que refuerzan el proceso rehabilitador en este tipo de
pacientes. Este tipo de herramientas sirven como complemento en las sesiones
terapéuticas correspondientes, generando una rehabilitación más amena, lúdica, de
menor duración, con un grado de mejoría más relevante.',
        'objetivo_general' => 'Desarrollar herramientas tecnológicas basadas en realidad virtual, enfocadas en la rehabilitación física de niños con diferentes patologías.',
        'fecha_inicio' => '2017-01-01',
        'fecha_final' => '2018-01-01',
        'presupuesto' => '90000',
        'financiamiento' =>'interno'
        ]);

        $universidades = [
            $espe->id
        ];
        
        $proyecto6->universidades()->sync($universidades);

        $proyecto7 = factory(Proyecto::class)->create([
        	'nombre' => 'GT-eTURISMO',
            'resumen' => 'El turismo es parte importante de la economía de un país, por lo
            que requiere de estrategias públicas adecuadas que permitan potencializarlo; es así
            como los países tienden a invertir para mantener un crecimiento exponencial y
            fortalecer su competitividad en las atracciones turísticas que poseen. Por otra parte,
            el turismo requiere de formas creativas de difusión, para lo cual son importantes los
            medios tecnológicos de fácil acceso, que permiten acortar distancias entre distintos
            lugares, donde no existan sociedades aisladas y sea posible conocer múltiples
            destinos antes de llegar a ellos, para lo cual se impulsa la formación de grupos
            virtuales, como ejemplo, e-turismo. En este tenor, el presente trabajo propone la
            formación del GT-eTURISMO a fin de desarrollar un sistema de interacción de
            múltiples usuarios en entornos virtuales con el propósito de potenciar el turismo en el
            Ecuador, la primera etapa se enfoca en la Difusión de sitios turísticos para promover e
            incentivar el turismo a través de la virtualización de los lugares turísticos por medio de
            entornos de realidad virtual 3D; mientras que la segunda etapa considera la
            Expedición de sitios turísticos por medio de una aplicación de realidad aumentada en
            la que se muestre información relevante del atractivo turístico por medio de la cámara
            del dispositivo móvil.<br>El presente proyecto de investigación tiene como objetivo desarrollar un sistema que
            permita la interacción del usuario con entornos de realidad virtual y realidad
            aumentada para promover turísticamente las principales Áreas Naturales Protegidas
            (ANP) y Ciudades de Destino (CiD) de la ZONA 3 de la República del Ecuador, bajo la visión de ciudades y destinos inteligentes 3D del mundo a través del desarrollo, uso y
            facilidades tecnológicas para el turista nacional e internacional. Para el cumplimiento
            del objetivo se plantea: 1) estructurar un inventario turístico multimedia con las
            principales ANP y CiD de la ZONA 3 de la República del Ecuador, para generar la
            base de datos que alimentará a las aplicaciones informáticas propuestas; 2)
            desarrollar una aplicación que permita en tiempo real la comunicación y carga de
            datos desde distintas fuentes, ya sea desde dispositivos móviles o equipos
            recolectores de estaciones meteorológicas, para proporcionar información relevante
            de las ANP y CiD de la ZONA 3 de la República del Ecuador; 3) desarrollar una
            aplicación en realidad virtual que permita la interacción e inmersión de múltiples
            usuarios en entornos recreados y/o virtualizados de sitios turísticos, a fin de mostrar
            información en tiempo real del estado climatológico, servicios y actividades que se
            puedan realizar, respondiendo así, las tres preguntas principales de todo turista ¿Qué
            hacer? ¿Qué comer? y ¿Dónde dormir?; 4) desarrollar una aplicación de realidad
            aumentada para dispositivos móviles basada en redes neuronales artificiales, para el
            despliegue autónomo de información complementaria mediante la detección en
            tiempo real de los atractivos turísticos recomendados y atributos relacionados con el
            perfil del turista; 5) implementar una infraestructura Cloud Computing de alta
            disponibilidad, para la gestión de información en tiempo real a ser remitida, según los
            requerimientos, de las aplicaciones de realidad virtual y realidad aumentada; 6)
            realizar evaluaciones experimentales del desempeño de las aplicaciones de realidad
            virtual y realidad aumentada desarrolladas, a fin de evaluar la usabilidad de las
            mismas.',
        'objetivo_general' => 'Desarrollar un sistema que permita la interacción del usuario con entornos de realidad
        virtual y realidad aumentada para promover turísticamente las principales Áreas
        Naturales Protegidas (ANP) y Ciudades de Destino (CiD) de la ZONA 3 de la
        República del Ecuador, bajo la visión de ciudades y destinos inteligentes 3D del
        mundo a través del desarrollo, uso y facilidades tecnológicas para el turista nacional e
        internacional.',
        'fecha_inicio' => '2017-09-01',
        'fecha_final' => '2021-08-01',
        'presupuesto' => '580000'
        ]);

        $universidades = [
            $espe->id,
            $uta->id,
            $espoch->id,
            $unach->id
        ];
        
        $proyecto7->universidades()->sync($universidades);

         $usuarios = [
            $victor->id,
            $marcelo->id,
            $aldrin->id,
        ];
        
        $proyecto7->usuarios()->sync($usuarios);


  $proyecto8 = factory(Proyecto::class)->create([
            'nombre' => 'Realidad Virtual Aplicada a la Rehabilitación de Pacientes con Enfermedades Neurodegenerativas',
            'resumen' => 'La Realidad Virtual (RV) proporciona una experiencia muy motivadora, permitiendo al usuario la práctica de diferentes movimientos a la vez que se manipula un dispositivo de interfaz. Los entornos virtuales pueden ser fácilmente cambiables, permitiendo el diseño de terapias individualizadas y adaptadas a las necesidades de los pacientes. La RV también proporciona estímulos muy ricos y funcionales dentro de contextos motivadores, reforzando y promoviendo una participación más active del sujeto. Son numerosos los estudios que han encontrado incrementos importantes en la motivación y el disfrute, así como mejores rendimientos en los aspectos motor y/o cognitivo tras la finalización de las diferentes intervenciones. Es crucial proporcionar una intervención motivadora para que los pacientes con enfermedades neurodegenerativas se involucren y participen activamente en el proceso de rehabilitación. Este proyecto propone implementar una aplicación de realidad virtual para personas con enfermedades neurodegenerativas, usando dispositivos de interfaz de usuario como las HTC VIVE u otros dispositivos visores de realidad virtual.',
        'objetivo_general' => '*',
        'fecha_inicio' => '2018-09-01',
        'fecha_final' => '2019-08-30',
        'presupuesto' => '9500'
        ]);

        $universidades = [
            $uti->id,
            $espe->id,
            $zaragoza->id
        ];
        
        $proyecto8->universidades()->sync($universidades);

        $proyecto9 = factory(Proyecto::class)->create([
            'nombre' => 'Sistema Experto para el Análisis y Diagnóstico en Tiempo Real de una Misión de Vuelo',
            'resumen' => 'El proyecto busca desarrollar un sistema experto que procesará la información generada por sensores de los componentes de una misión de vuelo, por componentes entendemos al piloto, la aeronave y la misión de vuelo, que buscan determinar las aptitudes físicas de los pilotos que están iniciando su formación para poder ser habilitados como pilotos de la Fuerza Aérea Ecuatoriana. Del piloto se van a monitorear las señales fisiológicas como pulso, presión, ritmo cardiaco, mientras que de la aeronave se va a monitorear la altura, velocidad GPS, posición, orientación. La información generada por los componentes será almacenada en un sistema de adquisición de datos que se desarrollara mediante tarjetas electrónicas. Además, esta se trasmitirá por enlaces inalámbrico a estaciones remotas para el procesamiento y análisis de la información, que será en dos vías, la primera que podrá mostrar la información a un instructor que se encuentre en la misma aeronave mediante un dispositivo móvil, y otra que mediante enlaces de radiofrecuencia o red celular sea receptada en tierra para su análisis experto y toma de decisiones con el uso del sistema, que contará con una base de conocimiento desarrollada en base a técnicas de educción realizadas a un médico aviador y que permitirá valorar y diagnosticar  en base a los datos obtenidos del piloto, aeronave y misión de vuelo y a su vez retroalimentarse',
        'objetivo_general' => '*',
        'fecha_inicio' => '2016-03-01',
        'fecha_final' => '2018-10-30',
        'presupuesto' => '34300'
        ]);

        $universidades = [
            $espe->id
        ];
        
        $proyecto9->universidades()->sync($universidades);


    }
}
