<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Universidad;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $espe = Universidad::where('abreviacion','=', 'ESPE')->first();

        $uta = Universidad::where('abreviacion','=', 'UTA')->first();

        $espoch = Universidad::where('abreviacion','=', 'ESPOCH')->first();

        $unach = Universidad::where('abreviacion','=', 'UNACH')->first();

        $epn = Universidad::where('abreviacion','=', 'EPN')->first();

        $inaut = Universidad::where('abreviacion','=', 'INAUT')->first();

        $urjc = Universidad::where('abreviacion','=', 'URJC')->first();

        $ups = Universidad::where('abreviacion','=', 'UPS')->first();

        $puce = Universidad::where('abreviacion','=', 'PUCE')->first();

        $zaragoza = Universidad::where('abreviacion','=', 'Universidad de Zaragoza')->first();
        
        $leira = Universidad::where('abreviacion','=', 'IPL')->first();

        $iessl = Universidad::where('abreviacion','=', 'IESSL')->first();
       
        $uti = Universidad::where('abreviacion','=', 'UTI')->first();
        
        $uide = Universidad::where('abreviacion','=', 'UIDE')->first();
        
        $ucv = Universidad::where('abreviacion','=', 'UCV')->first();

        $victor = factory(User::class)->create([
            'nombres'=> 'Victor H.',
            'apellidos'=> 'Andaluz',
            'email' => 'victor_hugo0301@hotmail.com',
            'telefono' => '0999703124',
            'titulo' => 'Ph.D. Control Systems',
            'scholar_id' => 'StskHA4AAAAJ',
            'rol' => 'docente',
            'password' => bcrypt('123456'),
            'imagen' => 'victor.jpg',
            'orden' => 1,
            'about_me' => 'El Dr.-Ing. Víctor Hugo Andaluz nació el 3 de enero de 1984 en la ciudad de Ambato, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica Nacional, donde se graduó de Ingeniero en Electrónica y Control. Del 2008 al 2012 fue becario del Instituto Alemán de Intercambio Académico (DAAD) obteniendo su Doctorado, en Ingeniería en Sistemas de Control, luego de cursar sus estudios en el Instituto de Automática de la Universidad Nacional de San Juan, Argentina y en el Instituto de Sistemas en Tiempo Real de la Universidad Leibniz de Hannover, Alemania. Ha trabajado como Instructor de Laboratorio en la Facultad de Eléctrica y Electrónica de la Escuela Politécnica Nacional; como Profesor-Investigador en la Escuela Superior Politécnica de Chimborazo; Además fue Director de Investigación y Desarrollo de la de la Universidad Técnica de Ambato y asesor de Investigación del Rectorado. Desde noviembre del 2014 hasta la presente fecha ocupa el cargo de Profesor Titular Principal 1 de la Universidad de las Fuerzas Armadas ESPE en el Departamento de Eléctrica y Electrónica de Latacunga. Sus áreas de interés, como son: Control Autónomo y Tele-Operado Aplicado a la Robótica de Servicio, Control Cooperativo, Control Inteligente, Aplicaciones de realidad virtual y realidad aumentada enfocadas a salud, educación y al sector industrial.',
            'facebook' => 'https://www.facebook.com/victorhugo.andaluz',
            'cv' => 'CV_VICTOR_ANDALUZ_19-06-2017.pdf'
        ]);
        $victor->universidades()->sync([$espe->id]);


        $edison = factory(User::class)->create([
            'nombres'=> 'Edison G.',
            'apellidos'=> 'Espinosa',
            'email' => 'egespinosa1@espe.edu.ec',
            'telefono' => '',
            'titulo' => 'PH.D. en Sofware',
            'scholar_id' => '',
            'rol' => 'docente',
            'password' => bcrypt('123456'),
            'imagen' => 'gonzalo_espinosa.jpg',
            'orden' => 2,
            'about_me' => 'Espinosa, nació el 24 de mayo de 1967 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Universidad Politécnica de las Fuerzas Armadas ESPE, donde se graduó de Ingeniero en Informática. Del 2010 al 2014 fue becario de la Secretaria de Educación Superior Ciencia Tecnología e Innovación (SENESCYT). Obtuvo su Doctorado en Software, luego de cursar sus estudios en la Universidad Politécnica de Madrid, España. Trabaja en la Universidad de las Fuerzas Armadas ESPE desde marzo de 1989 hasta la presente fecha y ocupa el cargo de Profesor Titular Principal 1 en el Departamento de Eléctrica y Electrónica de Latacunga.'
        ]);
        $edison->universidades()->sync([$espe->id]);

        $david = factory(User::class)->create([
            'nombres'=> 'David R.',
            'apellidos'=> 'Rivas',
            'email' => 'drrivas@espe.edu.ec',
            'telefono' => '0998542162',
            'titulo' => 'Master en Telecomunicaciones',
            'scholar_id' => '7vaSiNwAAAAJ',
            'rol' => 'docente',
            'orden' => 6,
            'password' => bcrypt('123456'),
            'imagen' => 'david_rivas.png',
            'about_me' => 'El ingeniero David Rivas nació el 9 de abril de 1979 en la ciudad de Ambato, Ecuador. Sus estudios superiores fueron en la ingeniería Electrónica e Instrumentación en la Escuela Politécnica del Ejército, obteniendo su título en 2003. En 2013, obtiene su título de Master Universitario en redes de Telecomunicaciones para países de desarrollo en la Universidad Rey Juan Carlos, donde actualmente es candidato a PhD en Tecnologías de la información y comunicación. Desde el 2006, trabaja en la Universidad de las Fuerzas Armadas ESPE extensión Latacunga como profesor de tiempo completo agregado 3 en el departamento de Eléctrica y Electrónica, donde en reiteradas ocasiones ha estado a cargo de proyectos de investigación y laboratorios de la institución.',
            'cv' => 'CV_David_Rivas.pdf',
            'facebook' => 'https://www.facebook.com/david.rivas.75641297'
        ]);
        $david->universidades()->sync([$espe->id]);

        $jorge = factory(User::class)->create([
            'nombres'=> 'Jorge S.',
            'apellidos'=> 'Sanchez',
            'email' => 'jssanchez@espe.edu.ec',
            'telefono' => '0999895298',
            'titulo' => 'Master en Docencia Matemática',
            'scholar_id' => 'wQPymlsAAAAJ',
            'rol' => 'docente',
            'orden' => 4,
            'password' => bcrypt('123456'),
            'imagen' => 'jorge_sanchez.png',
            'about_me' => 'Ingeniero en Electrónica e Instrumentación de la Universidad de las Fuerzas Armadas, Magister en Docencia Matemática de la Universidad Técnica de Ambato, instructor en el SECAP y en FUNDEL, en la actualidad es docente investigador del Departamento de Ciencias Exactas de la Universidad de las Fuerzas Armadas Extensión Latacunga, ha impartido clases de las asignaturas, Álgebra Superior, Geometría y Trigonometría, Estadística, Cálculo Diferencial e Integral, Cálculo Vectorial y Álgebra Lineal. En la parte de vinculación ha dirigido el Propuesta de mejoramiento en la enseñanza de la Matemática mediante la utilización de las TIC´s en los colegios de la zona rural de la Provincia de Cotopaxi, en el año 2015. En el campo de la investigación ha participado en el Proyecto de Investigación “Tele-Operación Bilateral de Múltiplos Manipuladores Móviles”, desarrollado en el periodo 2015-2016, en la actualidad está participando en el proyecto de investigación; “Control Coordinado Multi-Operador aplicado a Robot Manipulador Aéreo”. Cargos que ha desempeñado en el Departamento de Ciencias Exactas: Coordinador del Área de Matemática. (2015) y Coordinador  de Investigación (en la actualidad)',
            'facebook' => 'https://www.facebook.com/profile.php?id=1077296903',
            'cv' => 'CV_Jorge_Sanchez_Mosquera.pdf'
        ]);
        $jorge->universidades()->sync([$espe->id]);

        $jess = factory(User::class)->create([
            'nombres'=> 'Jessica S.',
            'apellidos'=> 'Ortiz',
            'email' => 'jsortiz4@espe.edu.ec',
            'titulo' => 'Magister en Sistemas de Control y Automatización Industrial',
            'telefono' => '0984547194',
            'scholar_id' => 'CoA7yOsAAAAJ',
            'rol' => 'docente',
            'imagen' => 'jes.jpg',
            'password' => bcrypt('123456'),
            'orden' => 9,
            'about_me' => 'La Mg. Jessica Sofía Ortiz Moreano, realizo sus estudios superiores en la Escuela Superior Politécnica de Chimborazo obteniendo el título de Ingeniero Electrónica, Control y Redes Industriales en 2013. Cruzo el Programa de Maestría del Instituto de Posgrados y Educación Continua del 2014 al 2016, graduándose de Magister en Sistemas de Control y Automatización Industrial. Ha trabajado como Investigadora en el Proyecto “Sistema de riego autónomo aplicado a la agricultura hidropónica” de la Universidad Técnica de Ambato; como Profesor (TC) en la Universidad Técnica de Ambato; Docente-Investigador en la Universidad Tecnológica Indoamérica; Actualmente es Docente (TC) en la Universidad de las Fuerzas Armadas ESPE en el Departamento de Eléctrica y Electrónica de Latacunga.',
            'cv' => 'CV_Jessica_Ortiz.pdf'
        ]);
        $jess->universidades()->sync([$espe->id]);

        $jaime = factory(User::class)->create([
            'nombres'=> 'Jaime Augusto',
            'apellidos'=> 'Santana Leon',
            'email' => 'sistemas@santana.ec',
            'telefono' => '0983706086',
            'scholar_id' => 'p47Kw5gAAAAJ',
            'password' => bcrypt('123456'),
            'rol' => 'investigador',
            'imagen' => 'jaime.jpg',
            'orden' => 6,
            'titulo' => 'Ingeniero en Sistemas Computacionales e Informáticos',
            'about_me' => 'Jaime Santana nació el 8 de febrero de 1990 en la ciudad de Quito, Ecuador. Inició sus estudios superiores en la Universidad Ecológica Amazónica en la ciudad de Puyo, donde estudió 2 años de ecoturismo y finalizó sus estudios en la Universidad Técnica de Ambato, donde se graduó de Ingeniero en Sistemas Computacionales e Informáticos en el año 2017. Ha colaborado en el desarrollo de plataformas de posicionamiento turístico para las provincias de Tungurahua, Pastaza y Morona Santiago. Desde el 2012 trabajó para el Honorable Gobierno Provincial de Tungurahua en la implementación del GeoPortal de Recursos Naturales de la provincia.',
            'facebook' => 'https://www.facebook.com/jaime.santana.9041',
            'cv' => 'CV_Jessica_Ortiz.pdf'
        ]);
        $jaime->universidades()->sync([$uta->id]);

        $cris = factory(User::class)->create([
            'nombres'=> 'Cristian Mauricio',
            'apellidos'=> 'Gallardo Paredes',
            'email' => 'cmgallardop@gmail.com',
            'titulo' => 'Ingeniero en Sistemas Computacionales e Informáticos',
            'telefono' => '',
            'rol' => 'investigador',
            'scholar_id' => 'U28c5SYAAAAJ',
            'password' => bcrypt('123456'),
            'imagen' => 'cristian.png',
            'orden' => 4,
            'about_me' => 'El Ingeniero Cristian Mauricio Gallardo Paredes nació el 06 de Septiembre de 1989 en la ciudad de Puerto Ayora privincia de Galápagos – Ecuador. Realizó sus estudios superiores en la Universidad Técnica de Ambato, donde se graduó de Ingeniero en Sistemas computacionales e Informáticos en el año 2016. En el área de investigación tiene sus inicios en el 2012 siendo estudiante de la carrera de ingeniería en Sistemas, en el año 2014-2015 desempeña labores como becario de investigación en la UODIDE-FISEI-UTA, en el año 2015-2016 se desempeña como Auxiliar de investigación en proyecto nacional de convocatoria CEPRA en la cual participan ESPOCH – EPN – ESPEL – UTA. En el año 2017 se desempeña como Analista de Investigación en la Universidad Técnica de Ambato. En la actualidad es parte de un proyecto nacional de convocatoria CEPRA en calidad de Analista de Investigación.',
            'cv' => 'CV_Cristian_Gallardo.pdf'
        ]);
        $cris->universidades()->sync([$uta->id]);

        $washo = factory(User::class)->create([
            'nombres'=> 'Washington',
            'apellidos'=> 'Quevedo',
            'email' => 'wjquevedo@espe.edu.ec',
            'telefono' => '0984963189',
            'titulo' => 'Ingeniero en Electrónica e Instrumentación',
            'scholar_id' => 'zPNPhwkAAAAJ',
            'password' => bcrypt('123456'),
            'imagen' => 'washo.jpg',
            'orden' => 1,
            'rol' => 'investigador',
            'about_me' => 'El Ing. Washington X. Quevedo nació el 21 de diciembre de 1990 en la ciudad de Latacunga, Ecuador. Realizo sus estudios superiores en la Universidad de las Fuerzas Armadas ESPE-Latacunga, donde se graduó de Ingeniero en Electrónica e Instrumentación. Ha colaborado en el Laboratorio de Investigación: Automatización, Robótica y Sistemas inteligentes de la extensión Latacunga en el proyecto: Tele-operación bilateral cooperativo de Múltiples manipuladores móviles cofinanciado por Consorcio Ecuatoriano para el Desarrollo de Internet Avanzado -CEDIA- , en área de investigación de las aplicaciones en ingeniería de la realidad virtual y realidad aumentada por lo que ha generado más de 11 publicaciones indexadas en SCOPUS y en las bases de datos de ISI-WEB y SCIMAGO. Ha obtenido el premio de Galardones Nacionales SCENECYT 2016 en el área de educación, primer lugar en el Hackathon Ciudades Resilientes Quito 2016 y primer lugar el Reto Movistar en Campus Party 2014.',
            'facebook' => 'https://www.facebook.com/waxsho',
            'cv' => 'CV_Washington_Quevedo.pdf'
        ]);
        $washo->universidades()->sync([$espe->id]);

        $chris = factory(User::class)->create([
            'nombres'=> 'Christian',
            'apellidos'=> 'Carvajal',
            'email' => 'chriss2592@hotmail.com',
            'telefono' => '0998071091',
            'titulo' => 'Ingeniero en Electrónica e Instrumentación',
            'scholar_id' => '5luiS7cAAAAJ',
            'password' => bcrypt('123456'),
            'imagen' => 'christian.png',
            'orden' => 3,
            'rol' => 'investigador',
            'about_me' => 'El Ing. Christian Carvajal nació el 25 de febrero de 1992 en la ciudad de Ambato, Ecuador. Realizó sus estudios superiores en la Universidad de las Fuerzas Armadas ESPE, donde se graduó de Ingeniero en Electrónica e Instrumentación en el 2015. Ha trabajado en el proyecto Detección Observación, Comunicación y Reconocimiento en el componente específico Instrumentación y Sistema Electro Óptico, en Centro de Investigación y Desarrollo de las Fuerzas Armadas durante el 2016. Actualmente ese encuentra trabajando en el Proyecto CEDIA, en la Universidad de las Fuerzas Armadas ESPE. Desde el 2016 ha realizado Investigación en diferentes campos obteniendo varias publicaciones.',
            'cv' => 'ChristianCarvajal_CV_ESPE2017.pdf'
        ]);
        $chris->universidades()->sync([$espe->id]);


        $fer = factory(User::class)->create([
            'nombres'=> 'Fernando',
            'apellidos'=> 'Chicaiza',
            'email' => 'fernandochicaiza137@gmail.com',
            'telefono' => '0979036151',
            'titulo' => 'Ingeniero en Electrónica e Instrumentación',
            'scholar_id' => 'bejtangAAAAJ',
            'password' => bcrypt('123456'),
            'imagen' => 'fernando.png',
            'rol' => 'investigador',
            'orden' => 2,
            'about_me' => 'El Ing. Fernando Chicaiza realizó sus estudios superiores en la Universidad de las Fuerzas Armadas ESPE – Latacunga, obteniendo el título en Electrónica e Instrumentación en 2015. Paralelamente, obtiene una certificación CISCO en Routing and Switching luego de capacitación profesional a través de Innovativa de la misma Universidad. Adicionalmente, realizó estudios de inglés académico avanzado en el instituto Browns Language School, obteniendo el CAE de Cambridge. Ha participado en proyectos de investigación desde el 2015: Tele-operación bilateral cooperativo de Múltiples manipuladores móviles cofinanciado por Consorcio Ecuatoriano para el Desarrollo de Internet Avanzado -CEDIA.',
            'cv'=>'FernandoChicaiza_CV_ESPE2017.pdf',
        ]);
        $fer->universidades()->sync([$espe->id]);

        $renato = factory(User::class)->create([
            'nombres'=> 'Renato',
            'apellidos'=> 'Toasa',
            'email' => 'renato_toasa@hotmail.com',
            'telefono' => '',
            'scholar_id' => '_8mA3O0AAAAJ',
            'titulo' => 'Ingeniero en Sistemas Computacionales e Informáticos',
            'imagen' => 'img013-min.png',
            'rol' => 'investigador',
            'orden' => 5,
            'password' => bcrypt('123456'),
            'facebook' => 'https://www.facebook.com/ReNaTo.666',
            'cv' => 'CV_RENATO_TOASA.pdf'
        ]);
        $renato->universidades()->sync([$uta->id]);

        $jose = factory(User::class)->create([
            'nombres'=> 'José L.',
            'apellidos'=> 'Varela',
            'email' => 'jazjose@hotmail.es',
            'telefono' => '0995316811',
            'titulo' => 'Magister en Sistemas de Control',
            'scholar_id' => '',
            'rol' => 'docente',
            'password' => bcrypt('123456'),
            'imagen' => 'jose_varela.png',
            'orden' => 13,
            'about_me' => 'Es ingeniero Industrial en Procesos de Automatización por la Universidad Técnica de Ambato  y tiene una maestría en Sistemas de Control y Automatización Industrial por la Escuela Superior Politécnica de Chimborazo. Ha trabajado en el proyecto de investigación titulado “Robótica de Asistencia para Discapacitados” en la Universidad Técnica de Ambato y en el proyecto “Tele-operación de Múltiples Manipuladores Móviles” como investigador a tiempo completo en la Universidad de las Fuerzas Armadas ESPE. Actualmente se desempeña como docente ocasional a tiempo completo por la Universidad de las Fuerzas Armadas ESPE, donde dicta cátedra en las asignaturas: Electrónica General, Medidas Eléctricas, Control Industrial y Electrónica de Potencia.',
            'cv' => 'CV_Jose_Varela.pdf'
        ]);
        $jose->universidades()->sync([$uti->id]);

        $hector = factory(User::class)->create([
        	'nombres'=> 'Héctor C.',
        	'apellidos'=> 'Terán',
        	'email' => 'hcteran@espe.edu.ec',
        	'telefono' => '0995284544',
            'titulo' => 'Master en Gestión de Energías',
        	'scholar_id' => '',
            'rol' => 'docente',
        	'password' => bcrypt('123456'),
            'imagen' => 'hector_teran.jpg',
            'orden' => 12,
            'about_me' => 'El Msc. Héctor Cochise Terán nació el 11 de mayo de 1982 en la ciudad de New York, Estados Unidos de Norte América. Realizó sus estudios superiores en la Escuela Politécnica del Ejército, donde se graduó de Ingeniero en Electromecánica. Actualmente cuenta con estudios de cuarto nivel en Redes Industriales y Gestión de Energías. Desde 2008 hasta la presente fecha es Docente del Departamento de Ciencias de la Energía y Mecánica de las Universidad de las Fuerzas Armadas ESPE.',
            'cv' => 'CV_HC_TERAN.pdf'
        ]);
        $hector->universidades()->sync([$espe->id]);

  
        $marcelo = factory(User::class)->create([
            'nombres'=> 'Marcelo R.',
            'apellidos'=> 'Alvarez',
            'email' => 'rmalvarez@espe.edu.ec',
            'titulo' => 'Magíster en Redes de Comunicaciones',
            'telefono' => '0992668556',
            'scholar_id' => '',
            'rol' => 'docente',
            'imagen' => 'alvarez.png',
            'password' => bcrypt('123456'),
            'orden' => 11,
            'about_me' => 'Marcelo Alvarez Veintimilla nació el 19 de mayo de 1979 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica del Ejercito hoy Universidad de las Fuerzas Armadas ESPE en la ciudad de Sangolquí, donde se graduó de Ingeniero en Sistemas e Informática. En el año 2015 obtiene grado de Magister en Redes de Comunicaciones en la Pontificia Universidad Católica del Ecuador, de la ciudad de Quito. Ha trabajado como administrador de redes en la empresa CPUSystem, como Jefe Técnico en la empresa Accell Telefonía Celular y como Jefe de TIC´s en Centro de Transferencia y Desarrollo Tecnológico ESPE – CECAI, Actualmente trabaja como docente Tiempo Parcial en el Departamento de Eléctrica y Electrónica de la Universidad de las Fuerzas Armadas ESPE extensión Latacunga.',
            'cv' => 'CV_Marcelo_Alvarez.pdf'
        ]);
        $marcelo->universidades()->sync([$espe->id]);

        $oscar = factory(User::class)->create([
            'nombres'=> 'Oscar B.',
            'apellidos'=> 'Arteaga',
            'email' => 'oscararteaga2005@yahoo.es',
            'titulo' => 'Magíster en Gestión de Energías y Diseño Mecánico',
            'telefono' => '092731693',
            'scholar_id' => '',
            'rol' => 'docente',
            'imagen' => 'oscar2016.jpg',
            'password' => bcrypt('123456'),
            'orden' => 5,
            'about_me' => 'El Ing. Oscar Bladimir Arteaga López nació el 20 de abril de 1970 en la ciudad de Ambato, Ecuador. Realizó sus estudios superiores en la Escuela Superior Politécnica de Chimborazo, donde se graduó de Ingeniero Mecánico. En el 2009 obtuvo el título de Magíster en Tecnología de la Información y Multimedia Educativa, luego de cursar sus estudios en la Universidad Técnica de Ambato, en el 2015 obtuvo el título de Magíster en Magíster en Gestión de Energías, luego de cursar sus estudios en la Universidad Técnica de Cotopaxi y en el 2017 obtuvo el título de Magister en Diseño Mecánico luego de cursar sus estudios en la Universidad Técnica de Ambato. Ha trabajado como Ingeniero Residente en la Refinería Estatal de Esmeraldas; como Consultor en varios proyectos relacionados con el área de mecánica y de energía; como Profesor-Investigador en la Universidad de las Fuerzas Armadas ESPE en el Departamento de Ciencias de la Energía y Mecánica. Desde enero del 2017 hasta la presente fecha ocupa el cargo de Coordinador de Investigación del Departamento de Ciencias de la Energía y Mecánica.',
            'cv' => 'CV_Oscar_Arteaga.pdf'
        ]);
        $oscar->universidades()->sync([$espe->id]);

        $paola = factory(User::class)->create([
            'nombres'=> 'Paola M.',
            'apellidos'=> 'Velasco',
            'email' => 'pmvelasco1@espe.edu.ec',
            'titulo' => 'Magíster en Redes de Comunicaciones',
            'telefono' => '',
            'scholar_id' => 'oZCnbUAAAAAJ',
            'rol' => 'docente',
            'imagen' => 'paola.png',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Nació el 28 de junio de 1979 en la ciudad de Latacunga, Ecuador. Realizó sus estudios de Ingeniería en Electrónica e Instrumentación en la Escuela Politécnica del Ejército, Diplomado en Didáctica de la Educación Superior en la Universidad Técnica de Cotopaxi. En 2015 obtuvo el título de Magister en Redes de Comunicaciones en la Pontificia Universidad Católica del Ecuador. Ha trabajado como docente ocasional tiempo completo en la Universidad Técnica de Cotopaxi, desempeñándose como Coordinadora de la carrera de Ingeniería Eléctrica, Coordinadora de Investigación y Coordinadora de Prácticas Preprofesionales. Actualmente es docente ocasional del Departamento de Eléctrica y Electrónica de la Universidad de las Fuerzas Armadas ESPE extensión Latacunga. Sus áreas de interés son: sistemas de comunicación aplicados a seguridad y análisis de datos, realidad virtual y realidad aumentada.',
            'cv' => 'CV_Paola_Velasco_Actualizado.pdf'
        ]);
        $paola->universidades()->sync([$espe->id]);

        $edwin = factory(User::class)->create([
            'nombres'=> 'Edwin P.',
            'apellidos'=> 'Pruna',
            'email' => 'eppruna@espe.edu.ec',
            'titulo' => 'Magíster en Gestión de Energías',
            'telefono' => '0995475776',
            'scholar_id' => 'mNstsLsAAAAJ',
            'rol' => 'docente',
            'imagen' => 'edwin.jpg',
            'password' => bcrypt('123456'),
            'orden' => 7,
            'about_me' => 'El Ing. Edwin Pruna nació el 26 de mayo de 1983 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica del Ejército, donde se graduó de Ingeniero en Electrónica e Instrumentación. Del 2009 al 2013 realizó sus estudios de postgrado obteniendo los siguientes títulos: Diploma Superior de Gestión para el Aprendizaje Universitario, Diploma Superior en Redes Digitales Industriales y Maestría en Gestión de Energías. Ha trabajado como docente del Instituto Tecnológico Superior Aeronáutico (ITSA). Desde octubre del 2008 hasta la presente fecha ocupa el cargo de Profesor Titular de la Universidad.',
            'cv' => 'CV_Edwin_Pruna.pdf'
        ]);
        $edwin->universidades()->sync([$espe->id]);

        $aldrin = factory(User::class)->create([
            'nombres'=> 'Aldrin G.',
            'apellidos'=> 'Acosta',
            'email' => 'agacosta@espe.edu.ec',
            'titulo' => 'Magíster en Educación y Desarrollo Social',
            'telefono' => '',
            'scholar_id' => '',
            'rol' => 'docente',
            'imagen' => 'aldrin.jpg',
            'password' => bcrypt('123456'),
            'orden' => 8,
            'about_me' => 'Es un profesional en Turismo, con especialización en Educación y Desarrollo Social, de la Universidad Tecnológica Equinoccial – UTE Quito. En el ámbito académico cumplió las funciones de Vicerrector Académico en el ITSA – SENESCYT, como parte del proyecto emblemático de reconversión de la Educación Superior Técnica y Tecnológica del país. También cumplió las funciones de Director de la Carrera de Ing. en Administración Turística y Hotelera en la Universidad de las Fuerzas Armadas ESPE extensión Latacunga, impulsando proyectos turísticos como el i-Tur, Urkuñan, áreas de prácticas (hoteleras y gastronómicas) y recreativas (turismo), etc, y programas de emprendimiento como el Rally de emprendimiento e innovación, Opening Bussines, entre otros, todos estos contribuyendo con la imagen de la institución, carrera y unidad académica. Actualmente es Coordinador de la Unidad de Emprendimiento e innovación. Además, ha realizado consultorías para el Ministerio de Turismo, la Asociación de Municipales del Ecuador, los GADs Municipales, ONGs y Fundaciones como CRISFE Banco del Pichincha. La investigación se ha enfocado al área de emprendimiento y desarrollo turístico, articuladas a ciencias técnicas a través de propuestas como e-turismo: atracciones turísticas a través de realidad virtual y aumentada y artículos como el marketing turístico a través de realidad virtual y emprendimiento en acción, entre otros'
        ]);
        $aldrin->universidades()->sync([$espe->id]);

        $ivon = factory(User::class)->create([
            'nombres'=> 'Ivón Patricia',
            'apellidos'=> 'Escobar',
            'email' => 'ipescobar@espe.edu.ec',
            'titulo' => 'Magíster en Gestión de Energías',
            'telefono' => '0984581805',            
            'scholar_id' => 'J7xpZNEAAAAJ',
            'rol' => 'docente',
            'imagen' => 'ivon.jpg',
            'password' => bcrypt('123456'),
            'orden' => 9,
            'cv' => 'CV_Ivon_Escobar.pdf',
            'about_me' => 'La Ing. Ivón Escobar nació el 27 de julio de 1983 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica del Ejército, donde se graduó de Ingeniera en Electrónica e Instrumentación. Del 2009 al 2013 realizó sus estudios de postgrado obteniendo los siguientes títulos: Diplomado Superior en Docencia Universitaria, Diploma Superior en Redes Digitales Industriales y Maestría en Gestión de Energías. Ha trabajado como docente en la Universidad Técnica de Cotopaxi en la Facultad de Ciencias de la Ingeniería y Aplicadas. Desde abril del 2011 hasta la presente fecha ocupa el cargo de Profesora Titular de la Universidad de las Fuerzas Armadas ESPE en el Departamento de Eléctrica y Electrónica de Latacunga.',
        ]);

        $ivon->universidades()->sync([$espe->id]);

        $victor_zambrano = factory(User::class)->create([
            'nombres'=> 'Victor Danilo',
            'apellidos'=> 'Zambrano León',
            'email' => 'vdzambrano@espe.edu.ec',
            'titulo' => 'Magíster en Gestión de la Producción',
            'telefono' => '',
            'scholar_id' => '4-SpbN8AAAAJ',
            'rol' => 'docente',
            'imagen' => 'danilo.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Es Ingeniero Automotriz de la Universidad de las Fuerzas Armadas ESPE, Magíster en Sistemas Automotrices de la Escuela Politécnica Nacional, Magister en Gestión de la Producción de la Universidad Técnica de Cotopaxi, Diplomado Superior en Autotrónica de la Escuela Politécnica del Ejército; en la actualidad docente investigador del Departamento de Ciencias de la Energía y Mecánica de la Universidad de las Fuerzas Armadas Sede Latacunga, Director de Carrera de Ingeniería Automotriz, ha impartido clases en las áreas de conocimiento de: Sistemas Automotrices; Energía y Termofluidos. En la parte de vinculación ha dirigido el Proyecto: "Asesoramiento y puesta a punto de la maquinaria agrícola de la asociación de mujeres de desarrollo productivo esperanza al futuro perteneciente a la parroquia Chanchagua, para mejorar la actividad productiva de la asociación", en el año 2017. En el campo de la investigación ha participado en los Proyectos de Investigación: "Diseño y construcción de un vehiculo blindado 4x4" año 2013 - 2014; "Diseño y construcción de vehículo fórmula student para concurso Alemania 2012" año 2012.'
        ]);
        $victor_zambrano->universidades()->sync([$espe->id]);

        $julio = factory(User::class)->create([
            'nombres'=> 'Julio César',
            'apellidos'=> 'Tapia León',
            'email' => 'jctapia3@espe.edu.ec',
            'titulo' => 'Magíster Ejecutivo en Dirección de Empresas',
            'telefono' => '',
            'rol' => 'docente',
            'imagen' => 'julio.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Nació el 12 de agosto de 1972 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Universidad Técnica de Ambato, donde se graduó de Administrador de Empresas, es Magister en Administración de Empresas con énfasis en Pequeñas y Medianas Empresas, en la Universidad de las Fuerzas Armadas ESPE; Magister Ejecutivo en Dirección de Empresas con Énfasis en Gerencia Estratégica, en la Universidad Autónoma de los Andes. Desde abril del 2015 es Director de la carrera de Finanzas y Auditoría de la Universidad de las Fuerzas Armadas ESPE; Además fue Docente Invitado del Instituto de Altos Estudios Nacionales IAEN; Docente Tiempo Completo del Departamento Ciencias Económicas, Administrativas y del Comercio CEAC, Coordinador del programa de maestría en Administración de Empresas con énfasis en Pequeñas y Medianas Empresas, en la Universidad de las Fuerzas Armadas ESPE. Sus áreas de interés son en las áreas sociales como marketing, emprendimiento, economía social solidaria, comercio justo.'
        ]);
        $julio->universidades()->sync([$espe->id]);

        $lucho = factory(User::class)->create([
            'nombres'=> 'Luis Alfonso',
            'apellidos'=> 'Lema Cerda',
            'email' => 'lalema@espe.edu.ec',
            'titulo' => 'Magíster en Gestión de Empresas',
            'telefono' => '0998576753',
            'rol' => 'docente',
            'imagen' => 'luis.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Nació el 12 de diciembre de 1975 en la ciudad de Pujilí, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica del Ejército, donde se graduó de Ingeniero en Sistemas e Informática, Ingeniero Comercial, Magister en Gestión de Empresas, Diplomado Universitario de Competencias Docentes, Tecnológico de Monterrey – Cambridge, candidato a Doctorado en Ciencias Económicas y Sociales en la Universidad de Carabobo, Valencia, Venezuela, Presidente de la empresa BYCACE S.A. Desde marzo del 2010 hasta la presente fecha ocupa el cargo de Profesor Titular 1 de la Universidad de las Fuerzas Armadas ESPE en el Departamento de Ciencias Económicas, Administrativas y de Comercio de Latacunga. Sus áreas de interés son: Administración de empresas, Planificación estratégica, Balance scorecard, Auditoría, Seguridad informática, Gestión de tecnologías de información y comunicación, Aplicaciones de realidad virtual orientadas a educación y salud.'
        ]);
        $lucho->universidades()->sync([$espe->id]);

        $cesar = factory(User::class)->create([
            'nombres'=> 'César A.',
            'apellidos'=> 'Naranjo',
            'email' => 'canaranjo@espe.edu.ec',
            'titulo' => 'Master en Redes y Telecomunicaciones',
            'telefono' => '0998025796',
            'rol' => 'docente',
            'imagen' => 'cesar.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Nació el 8 de julio de 1966 en la ciudad de Latacunga, Ecuador. Realizó sus estudios superiores en la Escuela Politécnica del Ejército, donde se graduó de Ingeniero en Electrónica e Instrumentación. Obtuvo el grado de Master en Redes y Telecomunicaciones en la Universidad Técnica de Ambato en el 2010. Del 2012 al 2013 fue becario en la Universidad Autonomía de Madrid España obteniendo su Masterado en Investigación e Innovación en Tecnologías de la Información y las Comunicaciones. Desde octubre de 1990 hasta la presente fecha ocupa el cargo de Profesor Titular Previo de la Universidad de las Fuerzas Armadas ESPE en el Departamento de Eléctrica y Electrónica de Latacunga.'
        ]);
        $cesar->universidades()->sync([$espe->id]);

        $julio_acosta = factory(User::class)->create([
            'nombres'=> 'Julio Francisco',
            'apellidos'=> 'Acosta',
            'email' => 'jfacosta@espe.edu.ec',
            'titulo' => 'Master en Informática Aplicada',
            'telefono' => '0984533158',
            'rol' => 'docente',
            'imagen' => 'julio_acosta.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Nacido en Latacunga el 20 de agosto de 1966, Realizó sus estudios universitarios y tecnológicos en el Instituto tecnológico de las Fuerzas Armadas ITSFFAA, y la Escuela Politécnica del Ejército ESPE, obteniendo los títulos de Programador de computadoras, Tecnólogo en telecomunicaciones, Ingeniero de Ejecución en Electrónica e Instrumentación, e ingeniero Electrónico. Posteriormente realizó estudios de Maestría en Informática Aplicada en la Escuela Superior Politécnica del Chimborazo, y continuó con estudios de Diplomado Superior en Gestión del aprendizaje Universitario en la Escuela Politécnica del Ejército. En el año 2012 se traslada a la Ciudad de Madrid España en donde obtiene su título de Máster Universitario en Investigación e Innovación en Tecnologías de la Información y las Comunicaciones, que lo hacen candidato al grado de PhD por la Universidad Autónoma de Madrid, en donde realiza su trabajo de investigación hasta la actualidad. Ha sido profesor de la Escuela Politécnica del Ejército y la Universidad de las Fuerzas Armadas – ESPE por los últimos 25 años, así como profesor contratado de las Facultades de Ingeniería Mecánica e Ingeniería en Sistemas, Electrónica e Industrial de la Universidad Técnica de Ambato. Sus áreas principales de interés son: Los sistemas de instrumentación virtual, sistemas de supervisión y robótica móvil y colaborativa, en la cual desarrolla su tesis de doctorado.'
        ]);
        $julio_acosta->universidades()->sync([$espe->id]);

        $franklin = factory(User::class)->create([
            'nombres'=> 'Franklin Manuel',
            'apellidos'=> 'Silva',
            'email' => 'fmsilva@espe.edu.ec',
            'titulo' => 'Magister en Gestión de Energías',
            'telefono' => '0999719329',
            'rol' => 'docente',
            'imagen' => 'franklin2.jpg',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Es ingeniero en Electrónica y Control de la Escuela Polítecnica Nacional EPN, magister en Dirección de Empresas de la Universidad Regional Autónoma de los Andes UNIANDES, Diplomado en Redes Digitales Industriales de la Universidad de las Fuerzas Armadas ESPE y Magister en Gestión de Energías de la Universidad Técnica de Cotopaxi UTC. Ha trabajado como docente universitario en la Universidad Técnica de Ambato y la Universidad de la Fuerzas Armadas ESPE, en está última por de 27 años y en la cual además de la docencia ha desempeñado el cargo de Director de la Carrera de Electrónica e Instrumentación. También ha ejercido como profesional de ingeniería eletrónica con su propia empresa, dando servicio en el campo de electrónica, automatización y control a varias empresas e industrias a nivel nacional. Actualmente es docente principal del Departamento de Eléctrica y Electrónica de la Universidad de las Fuerzas Armadas ESPE sede Latacunga.'
        ]);
        $franklin->universidades()->sync([$espe->id]);

        $javier = factory(User::class)->create([
            'nombres'=> 'javier Santiago',
            'apellidos'=> 'Vargas Paredes',
            'email' => 'javiervargas87@gmail.com',
            'titulo' => 'Ingeniero en Sistemas Computacionales e Informáticos',
            'telefono' => '0983157317',
            'rol' => 'investigador',
            'imagen' => 'javier.jpg',
            'password' => bcrypt('123456'),
            'orden' => 9,
            'about_me' => 'El Ing. Javier Santiago Vargas Paredes nació el 04 de enero de 1993 en la ciudad de Ambato, Ecuador. Realizó sus estudios superiores en la Universidad Técnica de Ambato, donde se graduó de Ingeniero en Sistemas Computacionales e Informáticos en el año 2015. Actualmente se encuentra en proceso de graduación de la Maestría en Gerencia de Sistemas Informáticos 2018 de la Universidad Técnica de Ambato, además cuenta con la pre-aceptación para el Doctorado en Computación 2020 de la Facultad de Ciencias Físicas y Matemáticas de la Universidad de Chile. Ha colaborado en varios proyectos de Investigación; CEDIA: analista técnico del proyecto CEPRA-X-2017-08, Universidad Técnica de Ambato - Dirección de Investigación y Desarrollo: asistente de investigación del proyecto SeeKV2, CEDIA: asistente de investigación del proyecto CEPRA-X-2016-01, CEDIA: ayudante técnico del proyecto CEPRA-IX-2015-03. Actualmente se encuentra colaborando en el Laboratorio de Investigación: Automatización, Robótica y Sistemas Inteligentes (ARSI) de la Universidad de las Fuerzas Armadas ESPE extensión Latacunga en el grupo de trabajo GT e-Turismo 2019.'
        ]);
        $javier->universidades()->sync([$uta->id]);

        $lore = factory(User::class)->create([
            'nombres'=> 'Mayra Lorena',
            'apellidos'=> 'Villaroel Herrera',
            'email' => 'lore_m44@hotmail.com',
            'titulo' => 'Ingeniera en Administración Turística y Hotelera',
            'telefono' => '0984114242',
            'rol' => 'investigador',
            'imagen' => 'lore.jpg',
            'password' => bcrypt('123456'),
            'orden' => 8,
            'about_me' => 'Mayra Lorena Villarroel, nació el 4 de diciembre de 1993 en Pujilí, Ecuador. Realizó sus estudios superiores en la Universidad de las Fuerzas Armadas ESPE Exención Latacunga, donde se graduó de Ingeniera en Administración Turística y Hotelera en el 2017, ejerció en el área hotelera como Administradora en el Hotel las cascadas y en el área Turística se desempeño en el diseño de rutas y circuitos turísticos y como counter nacional en la Agencia de Viajes Greivag, actualmente se encuentra colaborando en el Laboratorio de Investigación: Automatización, Robótica y Sistemas Inteligentes (ARSI) de la Universidad de las Fuerzas Armadas ESPE extensión Latacunga en el grupo de trabajo GT e-Turismo. Sus Áreas de interés son: Levantamiento de inventarios turísticos, Diseño de rutas y circuitos turísticos, Metodología para inventarios turísticos, Registro y Categorización de planta turística, Elaboración de paquetes e itinerarios de viajes. Manejo de procesos administrativos y gestión turística.'
        ]);
        $lore->universidades()->sync([$espe->id]);

        $andres = factory(User::class)->create([
            'nombres'=> 'Ándres',
            'apellidos'=> 'Pérez',
            'email' => 'joans11@hotmail.com',
            'titulo' => 'Ingeniero en Electrónica en instrumentación',
            'telefono' => '0998887456',
            'rol' => 'investigador',
            'imagen' => 'andres.jpg',
            'password' => bcrypt('123456'),
            'orden' => 7,
            'about_me' => 'Andrés Pérez nació el 15 de Noviembre de 1990 en la ciudad de Ambato,Ecuador. Realizó sus estudios en la Universidad de las Fuerzas Armadas ESPE, donde se graduó de Ingeniero en Electrónica en instrumentación en el 2014. Ha trabajado en el sector industrial en el diseño de proyectos para Automatización. También a trabajado en el desarrollo de aplicaciones para el envió de información mediante módulos GPRS sincronizados con bases de datos y recolección de datos mediante smartphones. Actualmente se encuentra trabajando en el Proyecto CEDIA realizando la investigación en diferentes campos como IoT e Inteligencia Artificial.'
        ]);
        $andres->universidades()->sync([$espe->id]);

        $lenin = factory(User::class)->create([
            'nombres'=> 'Lenin Rene',
            'apellidos'=> 'Tamayo Hinojosa',
            'email' => 'lenin17ec@hotmail.com',
            'titulo' => 'Magister en ...',
            'telefono' => '0998668024',
            'rol' => 'investigador',
            'imagen' => 'lenin.png',
            'password' => bcrypt('123456'),
            'orden' => 10,
            'about_me' => 'Lenin Tamayo...'
        ]);
        
        $lenin->universidades()->sync([$espe->id]);

        $bryan = factory(User::class)->create([
            'nombres'=> 'Bryan Stefano',
            'apellidos'=> 'Guevara Bermeo',
            'email' => 'bryansgue@hotmail.com',
            'titulo' => 'Ingeniero en',
            'telefono' => '0996319410',
            'rol' => 'investigador',
            'imagen' => 'bryan.jpeg',
            'password' => bcrypt('123456'),
            'orden' => 11,
            'about_me' => 'Bryan Guevara...'
        ]);

        $bryan->universidades()->sync([$espe->id]);

        $patricio = factory(User::class)->create([
            'nombres'=> 'Edison Patricio',
            'apellidos'=> 'Velasco Sánchez',
            'email' => 'epvelasco1912@gmail.com',
            'titulo' => 'Master en',
            'telefono' => '0939228038',
            'rol' => 'investigador',
            'imagen' => 'patrick.jpeg',
            'password' => bcrypt('123456'),
            'orden' => 12,
            'about_me' => 'Patricio Velasco...'
        ]);

        $patricio->universidades()->sync([$espe->id]);

        $paul = factory(User::class)->create([
            'nombres'=> 'Paúl Alexi',
            'apellidos'=> 'Canseco Sánchez',
            'email' => 'paul@test.com',
            'titulo' => 'Master en',
            'telefono' => '0998701477',
            'rol' => 'investigador',
            'imagen' => 'polo.jpg',
            'password' => bcrypt('123456'),
            'orden' => 13,
            'about_me' => 'Paúl Canseco...'
        ]);

        $paul->universidades()->sync([$espe->id]);

    }
}
