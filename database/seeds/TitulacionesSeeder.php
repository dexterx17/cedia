<?php

use Illuminate\Database\Seeder;

use App\Tesis;
use App\Universidad;
use App\User;

class TitulacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Universidades 
        $espe = Universidad::where('abreviacion','=', 'ESPE')->first();

        $uta = Universidad::where('abreviacion','=', 'UTA')->first();

        $espoch = Universidad::where('abreviacion','=', 'ESPOCH')->first();

        $unach = Universidad::where('abreviacion','=', 'UNACH')->first();

        $epn = Universidad::where('abreviacion','=', 'EPN')->first();

        $inaut = Universidad::where('abreviacion','=', 'INAUT')->first();

        $urjc = Universidad::where('abreviacion','=', 'URJC')->first();

        $ups = Universidad::where('abreviacion','=', 'UPS')->first();

        $puce = Universidad::where('abreviacion','=', 'PUCE')->first();

        $zaragoza = Universidad::where('abreviacion','=', 'Universidad de Zaragoza')->first();
        
        $leira = Universidad::where('abreviacion','=', 'IPL')->first();

        $iessl = Universidad::where('abreviacion','=', 'IESSL')->first();
       
        $uti = Universidad::where('abreviacion','=', 'UTI')->first();
        
        $uide = Universidad::where('abreviacion','=', 'UIDE')->first();
        
        $ucv = Universidad::where('abreviacion','=', 'UCV')->first();
        
        $uam = Universidad::where('abreviacion','=', 'UAM')->first();


     //Investigadores
        $victor = User::where('nombres', '=', 'Victor H.')->first();
        $edison = User::where(   'nombres', '=', 'Edison G.')->first();
        $david = User::where(   'nombres', '=', 'David R.')->first();
        $jorge = User::where(   'nombres', '=', 'Jorge S.')->first();
        $jess = User::where(   'nombres', '=', 'Jessica S.')->first();
        $jaime = User::where(   'nombres', '=', 'Jaime Augusto')->first();
        $cris = User::where(   'nombres', '=', 'Cristian Mauricio')->first();
        $washo = User::where(   'nombres', '=', 'Washington')->first();
        $chris = User::where(   'nombres', '=', 'Christian')->first();
        $fer = User::where(   'nombres', '=', 'Fernando')->first();
        $renato = User::where(   'nombres', '=', 'Renato')->first();
        $jose = User::where(   'nombres', '=', 'José L.')->first();
        $hector = User::where('nombres', '=', 'Héctor C.')->first();
        $marcelo = User::where(   'nombres', '=', 'Marcelo R.')->first();
        $oscar = User::where(   'nombres', '=', 'Oscar B.')->first();
        $paola = User::where(   'nombres', '=', 'Paola M.')->first();
        $edwin = User::where(   'nombres', '=', 'Edwin P.')->first();
        $aldrin = User::where(   'nombres', '=', 'Aldrin G.')->first();
        $ivon = User::where(   'nombres', '=', 'Ivón Patricia')->first();
        $victor_zambrano = User::where(   'nombres', '=', 'Victor Danilo')->first();
        $julio = User::where(   'nombres', '=', 'Julio César')->first();
        $lucho = User::where(   'nombres', '=', 'Luis Alfonso')->first();
        $cesar = User::where(   'nombres', '=', 'César A.')->first();
        $julio_acosta = User::where(   'nombres', '=', 'Julio Francisco')->first();
        $franklin = User::where(   'nombres', '=', 'Franklin Manuel')->first();
        $javier = User::where(   'nombres', '=', 'javier Santiago')->first();
        $lore = User::where(   'nombres', '=', 'Mayra Lorena')->first();
        $andres = User::where(   'nombres', '=', 'Ándres')->first();
        $lenin = User::where(   'nombres', '=', 'Lenin Rene')->first();
        $bryan = User::where(   'nombres', '=', 'Bryan Stefano')->first();
        $patricio = User::where(   'nombres', '=', 'Edison Patricio')->first();
        $paul = User::where(   'nombres', '=', 'Paúl Alexi')->first();


        $tesis = new Tesis();
        $tesis->instituto = "Escuela Politécnica Superior de la Universidad Autónoma de Madrid";
        $tesis->tema = "Diseño de un Sistema Robótico Multisensorial para la Optimización de Técnicas de Localización y Búsqueda";
        $tesis->tipo = "doctorado";
        $tesis->resumen = "Hoy día las plataformas robóticas multisensoriales, presentan un amplio espectro de aplicaciones. Estos sistemas facilitan la adquisición de distintas variables físicas, así como su conversión a señal eléctrica. También disponen de cierta capacidad de procesamiento con la que facilitan las tareas de supervisión y control de un determinado sistema. En muchos de estos problemas la determinación de la posición absoluta del robot es difícil de lograr con la precisión necesaria. El éxito de los algoritmos de búsqueda depende en gran medida del grado de exactitud del conocimiento de la posición y de las trayectorias del robot. Para esta tarea es necesaria una integración de la información proveniente de múltiples sensores.
El hablar de una plataforma robótica “Multisensorial” se refiere a la dotación de mayor capacidad de localización y colaboración entre robots y entre tareas, pues se considera que mientras más sensores que determinen el plano del ambiente en donde realizará la búsqueda de la fuente de aplicación, mejores resultados se tendrán en la misma; al mismo tiempo que se mejorará la velocidad de búsqueda y como consecuencia de esto el ahorro de recursos de consumo de las baterías instaladas y mejorar las técnicas de búsqueda. Todos los múltiples sensores con los cuales se implementará la plataforma, entregan información a dos sistemas diferentes, el principal, que es el robot en sí mismo, y a través de él, al sistema de supervisión, que tiene como objetivo procesar de forma más robusta la información entregada por cada uno de los agentes del sistema y de esta forma poder colaborar con cada uno de ellos en su localización y la del conjunto, con respecto a una referencia absoluta y a la o las fuentes de búsqueda. Por supuesto cuanto mayor sea la información, mejor será su procesamiento y los resultados enviados desde el sistema de supervisión serán más efectivos en la tarea de búsqueda. En este sentido la colaboración que presente el robot en ambientes conocidos con sus similares, sean robots móviles, vehículos aéreos o sistemas de manipulación resulta determinante para llevar a cabo las tareas a los robots encomendadas; los algoritmos y sistemas que se desprendan de esta colaboración influirán directamente en la efectividad del sistema global.";
		$tesis->codirector_id = $victor->id;
		$tesis->universidad_id = $uam->id;
		$tesis->save();
		DB::table('tesistas')->insert([
            'tesis_id' => $tesis->id,
            'referencia_id' => $julio_acosta->id,
            'referencia_table' => 'users'
        ]);

        $tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Transporte de Petróleo y Derivados";
        $tesis->tema = "Desarrollo de una Plataforma de Realidad Virtual con Capacidad de Inmersión Aplicada al Entrenamiento dentro de la Industria de Petróleo y Gas";
        $tesis->tipo = "maestria";
        $tesis->resumen = "Este trabajo de titulación propone el desarrollo de una herramienta alternativa para capacitación teórico-práctica basada en realidad virtual 3D que permitirá a las empresas petroleras optimizar los recursos destinados al adiestramiento del personal fijo y de los que ingresan temporalmente. El plan de titulación propone desarrollar un caso de operación de lanzamiento y recepción de raspadores (PIG) que permita replicar el modo secuencial de operación para que la tarea sea exitosa y sin riesgos, minimizando las interferencias en la producción e identificando claramente el cambio de las variables relacionadas a esta operación (presión y flujo). El entorno virtual tendrá señalización apropiada según normativas ANSI y OSHA, en el cual el operador se verá enfrentado a varios peligros y accidentes que deberán ser atendidos por él; se requerirán sus conocimientos teóricos sobre seguridad, su capacidad para identificar condiciones anormales en la planta y su rapidez en la toma de decisiones los cuales serán puestos en práctica durante la operación de lanzamiento y recepción de raspadores (PIG). Además la aplicación desarrollada permitirá una inmersión del usuario en la aplicación ya que tendrá detalles realistas y sonidos que complementan la experiencia, además de la posibilidad de utilizar sus manos dentro del entorno para el cumplimiento de los objetivos requeridos. El usuario puede llegar a experimentar las mismas sensaciones de ansiedad, miedo o preocupación que siente en situaciones de peligro y aprender a lidiar con ellas, con la ventaja de que no se ve expuesta su integridad física y de que puede repetir el proceso cuantas veces quiera. Este entrenamiento brindará al usuario experticia en el manejo de las situaciones antes expuestas y lo capacita de mejor manera para responder a ellas en la realidad. Para la creación del ambiente se considerará una herramienta CAD para desarrollar gráficos en 2D y 3D, mismos que serán exportados a un software de caracterización y animación de objetos virtuales y así conseguir el entorno interactivo deseado.";
		$tesis->director_id = $victor->id;
        $tesis->instituto = "Escuela Politécnica Nacional";
		$tesis->universidad_id = $epn->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-06-01";
		$tesis->save();


        $tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Modelación y Control de un Robot Manipulador Aéreo en Entornos 3D de Realidad Virtual";
        $tesis->tipo = "maestria";
        $tesis->resumen = "El sistema de locomoción es la principal característica de un robot que se encuentra condicionado por su entorno; por lo que se propone generar el control de locomoción de un robot manipulador aéreo en ambientes parcialmente estructurados, para lo cual se desarrollará el control cinemático considerando la posición-orientación del sistema operativo como un solo sistema. En este contexto, este trabajo propone un sistema que se caracteriza por tener un alto grado de redundancia, debido a que combina la capacidad de manipulación de un brazo robótico de base fija con la navegación de un vehículo aéreo no tripulado con ala rotativa. Estos sistemas permiten realizar las misiones más habituales de los sistemas robóticos que requieren tanto la capacidad de navegación aérea y manipulación. 
El presente proyecto de investigación tiene como objetivo proponer y analizar analíticamente la estabilidad al maniobrar de forma coordinada un robot manipulador aéreo, considerando en el desarrollo los efectos negativos que ocasionan los retardos de tiempo, a fin de incrementar la transparencia del sitio remoto a través de herramientas como el software Unity3D-Matlab. La propuesta contempla un esquema de control en cascada formada por dos subsistemas: i) control cinemático para la ejecución de una tarea a través de un coordinado multi-operador; y ii) simulación 3D que permita la inmersión del operador humano para el análisis del desempeño del algoritmo de control propuesto. Es decir, el trabajo propuesto tiene cuatro ítems principales: 1) modelar la cinemática de un robot manipulador aéreo;  2) desarrollar un esquema de control basado en la cinemática del robot para tareas de locomoción autónoma de un robot manipulador aéreo en espacios de trabajos parcialmente estructurado; 3) realizar una simulación 3D en realidad virtual que permita la inmersión del operador humano para el análisis del desempeño del algoritmo de control propuesto y finalmente 4) garantizar por simulación la estabilidad y robustez de los sistemas de control propuestos.";
		$tesis->director_id = $jess->id;
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-06-01";
		$tesis->save();
		
        $tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Emulador de Procesos Industriales en Ambientes de Realidad Virtual";
        $tesis->tipo = "maestria";
        $tesis->resumen = "Esta tesis propone una alternativa potencial y eficaz es la VR que provee técnicas esenciales para el aprendizaje, el cual debe garantizar una simulación de ambientes virtuales inmersivos y multisensoriales para lograr interactuar de manera segura, proporcionando al usuario un alto nivel de percepción  con cada característica del proceso  para satisfacer los requisitos que conlleve las prácticas y se pueda evaluar el pensamiento lógico e hipotético, la toma de decisiones, la acción si es correcta o no, entre otros en la experimentación del ambiente virtual, que genere un conocimiento directo con el mundo real en donde es difícil de visualizar procesos estudiados consiguiendo una mayor asimilación. 
En el plan nacional de Buen Vivir vigente se encuentra detallados doce objetivos nacionales para el buen vivir, de los cuales se hará énfasis en el cuarto objetivo que dice Fortalecer las capacidades y potenciales de la ciudadanía (SENPLADES, 2013), bajo este esquema en este proyecto se propone el desarrollo de un emulador de procesos industriales en ambientes de Realidad Virtual, VR, para estudiantes, docentes, profesionales. 
El trabajo propuesto tiene cuatro ítems principales: 1) investigar las características técnicas del emulador de proceso; 2) desarrollar un entorno virtual de un proceso industrial con motor gráfico UNITY 3D; 3) Implementar un sistema de control en lazo cerrado del proceso industrial virtualizado; 4) Realizar pruebas experimentales para evaluar la inmersión y desempeño del algoritmo de control propuesto.";
		$tesis->director_id = $jess->id;
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-06-01";
		$tesis->save();
		
        $tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Control de un Robot Móvil para el Seguimiento de Objetos Mediante Retroalimentación Visual";
        $tesis->tipo = "maestria";
        $tesis->resumen = "Desarrollo de un sistema de control de un robot móvil para el seguimiento de objetivos mediante retroalimentación visual. Un sistema de visión monocular montado sobre un robot móvil tipo uniciclo provee información limitada para tareas de seguimiento de objetos, muchas de las veces asumiendo ciertos criterios como la profundidad. Para solucionar el problema de la carencia del componente de profundidad, una composición de dos cámaras puede generar un desfase de visión para implementar sistemas de visión estereoscópica y añadir información a una imagen bidimensional. El sistema de control implementado se basa en la modelación cinemática del sistema completo  (robot móvil y estructura articular de las cámaras), así como del modelo cinemático de las cámaras. El robot móvil y la estructura articular utilizada están conformados por motores Dynamixel, descritos como actuadores inteligentes con redes de comunicación serial integrada. El diagrama de control que se presenta utiliza la modelación cinemática para determinar las velocidades adecuadas que todos los actuadores deben adoptar para el seguimiento de objetivos mediante retroalimentación visual en un ambiente estructurado. Esta propuesta se analiza mediante un simulador en Matlab que indica el correcto funcionamiento del sistema de control. Usando un prototipo de investigación, se obtienen los resultados experimentales que demuestran un adecuado funcionamiento del sistema de control para el seguimiento de objetivos mediante retroalimentación visual. Se recomienda que el presente proyecto sea la base para proyectos futuros en los cuales se estudie el desempeño del sistema en ambientes no estructurados.";
		$tesis->director_id = $victor->id;
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-01-01";
		$tesis->estado = "publicada";
		$tesis->save();
		
        $tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Modelación y Control Predictivo de un Robot Móvil con Centro de Masa Desplazado";
        $tesis->tipo = "maestria";
        $tesis->resumen = "En este trabajo se propone la implementación de seguimiento de trayectorias en el plano (X,Y) de un robot móvil tipo uniciclo a través de la aplicación de la estrategia de Control Predictivo basado en Modelos - MPC, para lo cual fue necesario conocer previamente el modelo cinemático y dinámico considerando un centro de masa  desplazado lateralmente del eje que une a las dos ruedas; parámetros que permitieron establecer y formular el esquema de control. Los algoritmos de control fueron implementados en el robot móvil tipo uniciclo AKASHA (no comercial), se consideró velocidades de referencia como señales de control al sistema, como es común en robots comerciales. Además, el modelo tiene una estructura adecuada para el diseño de las leyes de control. Los resultados realizados a través de simulaciones y de forma experimental, muestran que el robot móvil tipo uniciclo converge a la trayectoria deseada conforme a los algoritmos del control predictivo implementados, reduciendo los errores que se presentan debido a las restricciones y demás factores concurrentes en el movimiento del robot móvil hacia la trayectoria deseada. Como se verá en el presente proyecto no existe una única metodología específica en torno a la implementación de los controladores predictivos; sin embargo, se debe considerar que el modelo dinámico de un robot o planta obtenido previamente a la aplicación de un algoritmo MPC debe ser lo suficientemente preciso para que sea eficiente y represente el mismo comportamiento del sistema real; mientras más inexacto el modelo mayor será el error en el control.";
		//$tesis->director_id = $victor->id; //fabricio perez
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-01-01";
		$tesis->estado = "publicada";
		//$tesis->save();
		//estudiante gabriela andaluz
		
		$tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Implementación de un Sistema Automático de Riego por Goteo Basada en Agricultura Hidropónica Mediante Control Fuzzy para la Producción de Tomate Riñón Variedad Daniela";
        $tesis->tipo = "maestria";
        $tesis->resumen = "El presente trabajo tiene como objetivo el cultivo de tomate riñón variedad Daniela basado en agricultura hidropónica con un sistema automático de riego por goteo. El control del sistema  se lo realizara mediante un controlador diseñado con lógica difusa, conociendo las variables de entrada y salida podremos estudiar las características técnicas de los sensores y actuadores usados para estos sistemas, de igual forma su monitoreo permitirá conocer la variación de las variables y su eficiencia frente a los cambios del ambiente y las necesidades fisiológicas del cultivo.  Las variables de entrada que intervienen para el riego por goteo son la temperatura, humedad, pH, y conductividad eléctrica del sustrato; que son modificadas por el riego tanto de agua como de nutriente. Para poder diseñar el controlador nos basaremos en los sistemas de control difuso; y mediante ayudas informáticas evaluaremos la exactitud del controlador de diseño y el controlador implementado. En la implementación con el sistema de riego obtendremos los datos necesarios para conocer si el controlador difuso es óptimo o no para las variaciones del entorno. Mediante la experimentación modificaremos las reglas de control fuzzy, para obtener una mejor respuesta.  En el diseño se determinó que el tipo de controlador será uno con lazo cerrado con control proporcional y se usó como motor de interferencia el algoritmo Mandami. Como conclusión el controlador de diseño con el controlador implementado tiene una alta exactitud. Con la recolección de datos del algoritmo implementado se obtuvo los siguientes valores de error temperatura +0.38°C, de humedad -1.8%, de pH +1 unidad, y de conductividad eléctrica de -588 uS/cm; estos valores se encuentran dentro de los rangos optimos para las necesidades de cultivo de tomate riñon variendad Daniela basado en agricultura hidropónica, donde su desempeño es optimo ya que se encuentra cerca de tener un error en cero.";
		$tesis->director_id = $victor->id; 
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2018-01-01";
		$tesis->estado = "publicada";
		$tesis->save();
		
		$tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Planificación y Seguimiento de Caminos de Manera Autónoma para Robots Móviles Tipo Uniciclo";
        $tesis->tipo = "maestria";
        $tesis->resumen = "En el presente proyecto de investigación como una propuesta tecnológica e innovación, realiza la simulación,  implementación de la planificación y seguimiento de caminos de manera autónoma para robots móviles tipo uniciclo, considerando un ambiente de trabajo estructurado; para la simulación se usó la herramienta de software matemático Matlab, mientras que las pruebas experimentales se realizó con el robot móvil construido en el proyecto de investigación “Tele-operación bilateral cooperativo de múltiples manipuladores móviles”, ejecutado por la Escuela Superior Politécnica de Chimborazo, Universidad Técnica de Ambato, Escuela Politécnica Nacional y la Universidad de las Fuerzas Armadas, como punto de partida se especifica las características cinemáticas y dinámicas de un robot móvil tipo uniciclo, luego se proponer un algoritmo de planificación de caminos basado en algoritmos de exploración rápida de árboles randomicos (RRT) para un ambiente estructurado, luego de realizar la planificación de caminos se implementa un esquema de control para el seguimiento del mismo de manera autónoma, como punto final se realiza una evaluación de simulación y experimental del algoritmo de planificación y esquema de control propuesto en ambientes estructurados.";
		$tesis->director_id = $victor->id; 
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $espoch->id;
		$tesis->desde = "2017-01-01";
		$tesis->hasta = "2017-09-01";
		$tesis->estado = "publicada";
		$tesis->save();
		
		
		$tesis = new Tesis();
        $tesis->titulo = "Maestría en Sistemas de Control y Automatización Industrial";
        $tesis->tema = "Tele-Operación Bilateral de Un Robot Manipulador Móvil";
        $tesis->tipo = "maestria";
        $tesis->resumen = "Desarrollo de un esquema para la tele-operación bilateral de un robot manipulador móvil. Un manipulador robótico montado sobre una base móvil posee tanto la destreza de los manipuladores robóticos y la movilidad de un vehículo, y es una solución versátil que ofrece un gran potencial en una amplia variedad de aplicaciones. Para la implementación del sistema de tele-operación se realizó la modelación cinemática y dinámica del manipulador móvil mediante expresiones matriciales. Los actuadores inteligentes que conforman el robot se controlaron mediante las librerías de MATLAB de comunicación RS-485. El esquema de tele-operación bilateral propuesto consideró la compensación dinámica del manipulador móvil, además se incluyó la realimentación de fuerzas en el sitio local. Por último, usando el control de redundancia se solucionó la configuración del brazo robótico y la evasión de obstáculos. Partiendo de los resultados obtenidos se concluyó que el sistema permite una combinación de manipulabilidad y locomoción en el sitio de trabajo demostrando un adecuado funcionamiento del sistema. Se recomienda analizar el contenido de este trabajo, para determinar el estado de nuestro país en relación a los avances tecnológicos del campo de la robótica.";
		$tesis->director_id = $victor->id; 
        $tesis->instituto = "Escuela Superior Politécnica de Chimborazo";
		$tesis->universidad_id = $edisonspoch->id;
		$tesis->estado = "publicada";
		$tesis->link = "http://dspace.espoch.edu.ec/handle/123456789/6090";
		$tesis->save();
		
		DB::table('tesistas')->insert([
            'tesis_id' => $tesis->id,
            'referencia_id' => $jose->id,
            'referencia_table' => 'users'
        ]);

		

    }
}
