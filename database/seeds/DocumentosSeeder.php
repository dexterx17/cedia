<?php

use Illuminate\Database\Seeder;

use App\Documento;
use App\Tipo_documento;


class DocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$propuestas = new Tipo_documento();
    	$propuestas->tipo = "Propuestas";
    	$propuestas->descripcion = "Propuestas generales de los proyectos";
    	$propuestas->extra = "";
    	$propuestas->save();

    	$informe_trimestral = new Tipo_documento();
    	$informe_trimestral->tipo = "Informes trimestrales o mensuales";
    	$informe_trimestral->descripcion = "Son los informes generales que se presentan periodicamente, suelen contener el resumen de las actividades y entregable realizadas en un periodo de tiempo";
    	$informe_trimestral->extra = "";
    	$informe_trimestral->save();

    	$informe_actividades = new Tipo_documento();
    	$informe_actividades->tipo = "Informes de actividades";
    	$informe_actividades->descripcion = "Son los informes correspondiente a una actividad especifica, y se presentan por lo general trimestral o mensualmente.";
    	$informe_actividades->extra = "";
    	$informe_actividades->save();

    	$informe_entregables = new Tipo_documento();
    	$informe_entregables->tipo = "Informes de entregables";
    	$informe_entregables->descripcion = "Son los informes correspondiente a un entregable especifico, puede ser el resultado de varias actividades.";
    	$informe_entregables->extra = "";
    	$informe_entregables->save();


        $informe_personal = new Tipo_documento();
        $informe_personal->tipo = "Informes personales";
        $informe_personal->descripcion = "Son los informes elaborados por cada investigador, que suelen ser presentados como respaldos para los pagos trimestral o mensualmente";
        $informe_personal->extra = "";
        $informe_personal->save();

    	$manuales = new Tipo_documento();
    	$manuales->tipo = "Manuales";
    	$manuales->descripcion = "Corresponden a manuales de configuracion o usuario que sirven para documentar y guiar a los interesados en revisar el trabajo realizado.";
    	$manuales->extra = "";
    	$manuales->save();

    	$oficios_convenios = new Tipo_documento();
    	$oficios_convenios->tipo = "Oficios/Convenios";
    	$oficios_convenios->descripcion = "Corresponden a documentos oficiales que sirven como respaldo legal de los proyectos, estos pueden ser oficios, convenios, actas o convocatorias.";
    	$oficios_convenios->extra = "";
    	$oficios_convenios->save();

        $presentaciones = new Tipo_documento();
        $presentaciones->tipo = "Presentaciones";
        $presentaciones->descripcion = "Corresponden a presentaciones en powerpoint o pdf que resumen ciertos temas y los presentan en un formato resumido.";
        $presentaciones->extra = "";
        $presentaciones->save();

    	$otros = new Tipo_documento();
    	$otros->tipo = "Otros";
    	$otros->descripcion = "Corresponden al tipo de documentacion general que no entra en ninguna de las categorias anteriores, estos pueden ser diagramas";
    	$otros->extra = "";
    	$otros->save();


        /**
         * 
         **/

        $documento = new Documento();
        $documento->tipo_id = $propuestas->id;
        $documento->nombre = "Propuesta GT-eTurismo,";
        $documento->palabras_clave = "GT, 2018-2019";
        $documento->ruta = "VH_4_GT-eTURISMO.pdf";
        $documento->upload_by = 1;
        $documento->save();
        
        DB::table('documento_referencias')->insert([
            'documento_id' => $documento->id,
            'referencia_id' => 7,
            'referencia_table' => 'proyectos'
        ]);


        $documento = new Documento();
        $documento->tipo_id = $informe_trimestral->id;
        $documento->nombre = "Informe 1 trimestre";
        $documento->palabras_clave = "GT, 2018";
        $documento->ruta = "Informe-1.pdf";
        $documento->upload_by = 1;
        $documento->save();

        DB::table('documento_referencias')->insert([
            'documento_id' => $documento->id,
            'referencia_id' => 7,
            'referencia_table' => 'proyectos'
        ]);

        $documento = new Documento();
        $documento->tipo_id = $informe_trimestral->id;
        $documento->nombre = "Informe 2 trimestre";
        $documento->palabras_clave = "GT, 2018";
        $documento->ruta = "Informe-2 GT e-Turismo 2018-12-20.pdf";
        $documento->upload_by = 1;
        $documento->save();

        DB::table('documento_referencias')->insert([
            'documento_id' => $documento->id,
            'referencia_id' => 7,
            'referencia_table' => 'proyectos'
        ]);


        $documento = new Documento();
        $documento->tipo_id = $informe_trimestral->id;
        $documento->nombre = "Informe 3 trimestre";
        $documento->palabras_clave = "GT, 2018";
        $documento->ruta = "Informe 3 GT e-Turismo 2019-03-31.pdf";
        $documento->upload_by = 1;
        $documento->save();

        DB::table('documento_referencias')->insert([
            'documento_id' => $documento->id,
            'referencia_id' => 7,
            'referencia_table' => 'proyectos'
        ]);

    }
}
