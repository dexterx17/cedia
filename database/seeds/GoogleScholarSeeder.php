<?php

use Illuminate\Database\Seeder;

//use \File;

use App\User;

use App\Publicacion;

class GoogleScholarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$ruta = 'C:\xampp\htdocs\arsi\python\data';
        $files = File::allFiles($ruta);
		foreach ($files as $index=>$file)
		{
		    echo (string)$file, "\n";
		    //echo "FILE $index";

		    $partes = explode('/', $file);
		    $name = $partes[7];		    
		    $name = substr($name, 0, strpos($name,'.'));
		    //echo "  NAME $name";
		   	$user = User::where('scholar_id',$name)->first();
		   	echo $user->nombres.' '.$user->apellidos.'======';

		    $json = json_decode(file_get_contents($file), true); 
		    //print_r($json);
		    foreach ($json as $key => $p) {
		    	echo $p['title'].'||||';
		    	$p['titulo']=trim($p['title']);
		    	if(isset($p['autores']))
		    		$p['autores']=trim($p['autores']);
		    	unset($p['title']);
		    	try{
			    	$pub = Publicacion::where('titulo','=',$p['titulo'])->firstOrFail();
		    	}catch(\Exception $er){
		    		$pub = factory(Publicacion::class)->create($p);
		    	}

		    	$pub->users()->attach($user->id);
		    }
		}
    }
}
