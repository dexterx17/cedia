<?php

use Illuminate\Database\Seeder;

use App\Investigacion;
use App\User;

class InvestigacionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $victor = User::find(1);

        $inv1 = factory(Investigacion::class)->create([
        	'cargo' => 'Investigador',
        	'proyecto' => 'Prototipo de un sistema para interpretación y comprensión del lenguaje de señas básico, aplicando redes neuronales como técnica en la clasificación de datos',
        	'financiamiento' => '3198.88',
        	'participantes' => 'Pontificia Universidad Católica del Ecuador, sede Ambato <br> Universidad de las Fuerzas Armadas ESPE, Sede Latacunga <br> Universidad Rey Juan Carlos, España <br> Universidad Politécnica Salesiana',
        	'desde' => '2017-01-01',
        	'hasta' => '2019-12-31',
        	'user_id'=>$victor->id
        ]);
        
        $inv2 = factory(Investigacion::class)->create([
        	'cargo' => 'Coordinador-Investigador',
        	'proyecto' => 'Tele-Operación Bilateral Cooperativo de Múltiples Manipuladores Móviles ',
        	'financiamiento' => '200000',
        	'participantes' => 'Cooperación Ecuatoriana para el Desarrollo de Internet Avanzado - CEDIA',
        	'desde' => '2015-08-01',
        	'hasta' => '2016-08-31',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'Cuenca, Ecuador'
        ]);

        $inv3 = factory(Investigacion::class)->create([
        	'cargo' => 'Director Subrogante-Investigador',
        	'proyecto' => 'Desarrollo de estrategias avanzadas de control para sistemas móviles (aéreos, acuáticos y terrestres) no tripulados (Unmmaned Mobile Systems - UMS) para exploración en lugares peligrosos o de difícil acceso.',
        	'financiamiento' => '400000',
        	'participantes' => 'Red Ecuatoriana de Universidades para Investigación y Postgrados – REDU',
        	'desde' => '2015-11-01',
        	'hasta' => '2017-11-30',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'Quito, Ecuador'
        ]);

        $inv4 = factory(Investigacion::class)->create([
        	'cargo' => 'Responsable del Proyecto',
        	'proyecto' => 'Fortalecimiento de la Unidad Operativa de Investigación en Tecnología de Alimentos (UOITA) para la investigación, tecnología e innovación en el área de alimentos, con el fin de promover la generación y el desarrollo de empresas agroindustriales en la zona 3 del país; y monitorear el contenido de metales pesados en los cultivos afectados por las cenizas provenientes de las erupciones volcánicas del Tungurahua.',
        	'financiamiento' => '2311083',
        	'participantes' => 'Programa de Canje de Deuda España-Ecuador',
        	'desde' => '2014-07-01',
        	'hasta' => '2017-12-31',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'Quito, Ecuador'
        ]);

        $inv5 = factory(Investigacion::class)->create([
        	'cargo' => 'Coordinador-Investigador',
        	'proyecto' => 'Sistema de riego autónomo aplicado a la agricultura hidropónica',
        	'financiamiento' => '45150',
        	'participantes' => 'Universidad Técnica de Ambato',
        	'desde' => '2014-01-01',
        	'hasta' => '2017-12-31',
        	'user_id'=>$victor->id
        ]);

        $inv6 = factory(Investigacion::class)->create([
        	'cargo' => 'Coordinador-Investigador',
        	'proyecto' => 'Robótica de asistencia a personas con discapacidades',
        	'financiamiento' => '38350',
        	'participantes' => 'Universidad Técnica de Ambato',
        	'desde' => '2012-01-01',
        	'hasta' => '2017-12-31',
        	'user_id'=>$victor->id
        ]);

        $inv7 = factory(Investigacion::class)->create([
        	'cargo' => 'Investigador',
        	'proyecto' => 'Control servo-visual avanzado y teleoperación de robots móviles en el contexto de sistemas productivos',
        	'participantes' => 'Universidad Nacional de San Juan',
        	'desde' => '2011-01-01',
        	'hasta' => '2014-12-31',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'San Juan, Argentina'
        ]);

        $inv8 = factory(Investigacion::class)->create([
        	'cargo' => 'Investigador',
        	'proyecto' => 'Robótica avanzada para sistemas productivos',
        	'participantes' => 'Agencia Nacional de Promoción Científica y Tecnológica',
        	'desde' => '2009-01-01',
        	'hasta' => '2014-12-31',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'San Juan, Argentina'
        ]);

        $inv9 = factory(Investigacion::class)->create([
        	'cargo' => 'Investigador',
        	'proyecto' => 'Sistemas de control avanzado de robots aplicados al sector productivo',
        	'participantes' => 'Consejo Nacional de Investigaciones Científicas y Técnicas',
        	'desde' => '2009-01-01',
        	'hasta' => '2011-12-31',
        	'user_id'=>$victor->id,
        	'ubicacion' => 'San Juan, Argentina'
        ]);

        $hector = User::where('nombres','=','Héctor C.')->first();

        $inv21 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Prototipo de tractor agrícola monoplaza diésel',
            'financiamiento' => '23000',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2013-01-01',
            'hasta' => '2014-12-31',
            'user_id'=>$hector->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $chris = User::where('email','=','chriss2592@hotmail.com')->first();

        $inv21 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Control Coordinado Multi-Operador Aplicado a un Robot Manipulador Aéreo',
            'financiamiento' => '0',
            'participantes' => 'Cooperación Ecuatoriana para el Desarrollo de Internet Avanzado - CEDIA',
            'desde' => '2017-10-01',
            'hasta' => '2017-12-31',
            'user_id'=>$chris->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv22 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Sistema Experto Para el Análisis e Diagnóstico en Tiempo Real de una Misión De Vuelo',
            'financiamiento' => '0',
            'participantes' => 'ESPE-INNOVATIVA EP, Centro de Investigación y Desarrollo de las Fuerzas Armadas',
            'desde' => '2017-07-01',
            'hasta' => '2017-12-31',
            'user_id'=>$chris->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv23 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Detección Observación, Comunicación y Reconocimiento en el componente específico Instrumentación y Sistema Electro Óptico.',
            'financiamiento' => '0',
            'participantes' => 'Escuela Politécnica Nacional <br> Universidad Técnica de Ambato <br> Centro de Investigación y Desarrollo de las Fuerzas Armadas (CIDFAE)',
            'desde' => '2015-10-01',
            'hasta' => '2016-12-31',
            'user_id'=>$chris->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $jess = User::where('email','=','jsortiz4@espe.edu.ec')->first();

        $inv21 = factory(Investigacion::class)->create([
            'cargo' => 'Investigadora',
            'proyecto' => 'Control Coordinado Multi-Operador Aplicado a un Robot Manipulador Aéreo',
            'financiamiento' => '0',
            'participantes' => 'XI Convocatoria CEDIA-CEPRA 2017',
            'desde' => '2016-09-01',
            'hasta' => '2017-12-31',
            'user_id'=>$jess->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv21 = factory(Investigacion::class)->create([
            'cargo' => 'Investigadora',
            'proyecto' => 'Sistema de riego autónomo aplicado a la agricultura hidropónica',
            'financiamiento' => '0',
            'participantes' => 'Universidad Técnica de Ambato',
            'desde' => '2014-05-01',
            'hasta' => '2014-09-31',
            'user_id'=>$jess->id,
            'ubicacion' => 'Ambato, Ecuador'
        ]);

        $marcelo = User::where('email','=','rmalvarez@espe.edu.ec')->first();


        $inv5 = factory(Investigacion::class)->create([
            'cargo' => 'Responsable del proyecto',
            'proyecto' => 'Sistema de soporte a la enseñanza y comprensión de lenguaje de señas básico, aplicando técnicas de clasificación de datos para la interpretación gestual',
            'financiamiento' => '0',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2016-01-01',
            'hasta' => '2017-12-31',
            'user_id'=>$marcelo->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);


        $inv1 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Prototipo de un sistema para interpretación y comprensión del lenguaje de señas básico, aplicando redes neuronales como técnica en la clasificación de datos',
            'financiamiento' => '3198.88',
            'participantes' => 'Pontificia Universidad Católica del Ecuador, sede Ambato <br> Universidad de las Fuerzas Armadas ESPE, Sede Latacunga <br> Universidad Rey Juan Carlos, España <br> Universidad Politécnica Salesiana',
            'desde' => '2016-01-01',
            'hasta' => '2018-12-31',
            'user_id'=>$marcelo->id
        ]);


        $oscar = User::where('email','=','oscararteaga2005@yahoo.es')->first();

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Coordinador-Investigador',
            'proyecto' => 'Tele-Operación Bilateral Cooperativo de Múltiples Manipuladores Móviles',
            'financiamiento' => '200000',
            'participantes' => 'Cooperación Ecuatoriana para el Desarrollo de Internet Avanzado - CEDIA',
            'desde' => '2015-08-01',
            'hasta' => '2016-08-31',
            'user_id'=>$oscar->id,
            'ubicacion' => 'Cuenca, Ecuador'
        ]);

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Director - Investigador',
            'proyecto' => 'Diseño y Construcción de un Prototipo de Vehículo Blindado 4x4',
            'financiamiento' => '25000',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2013-01-01',
            'hasta' => '2014-12-31',
            'user_id'=>$oscar->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Diseño y Construcción de un Prototipo de Tractor Agrícola Monoplaza a Diésel',
            'financiamiento' => '55000',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2012-07-01',
            'hasta' => '2014-07-31',
            'user_id'=>$oscar->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'Diseño y Construcción de un Vehículo Fórmula Student para el Concurso Alemania 2012',
            'financiamiento' => '120000',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2012-01-01',
            'hasta' => '2012-11-30',
            'user_id'=>$oscar->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $paola = User::where('nombres','=','Paola M.')->first();

        $inv21 = factory(Investigacion::class)->create([
            'cargo' => 'Participante – Investigador',
            'proyecto' => 'Control Coordinado Multi-Operador Aplicado a un Robot Manipulador Aéreo',
            'financiamiento' => '0',
            'participantes' => 'XI Convocatoria CEDIA-CEPRA 2017',
            'desde' => '2016-09-01',
            'hasta' => '2017-12-31',
            'user_id'=>$paola->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $edwin = User::where('email','=','eppruna@espe.edu.ec')->first();

        $inv4 = factory(Investigacion::class)->create([
            'cargo' => 'Responsable del Proyecto',
            'proyecto' => 'Implementación Del Sistema De Producción De Estaciones De Control De Procesos Con Fines De Promoción Y Autogestión',
            'financiamiento' => '0',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2012-09-01',
            'hasta' => '2014-07-31',
            'user_id'=>$edwin->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv4 = factory(Investigacion::class)->create([
            'cargo' => 'Responsable del Proyecto',
            'proyecto' => 'Diseño E Implementación De Una Red Industrial Inalámbrica Wirelesshart Para El Monitoreo De Las Variables Temperatura, Presión, Caudal Y El Monitoreo Y Control De La Variable Nivel En El Laboratorio De Redes Industriales Y Control De Procesos',            'financiamiento' => '0',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'financiamiento' => '0',
            'desde' => '2014-03-01',
            'hasta' => '2015-07-31',
            'user_id'=>$edwin->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'VRChild – Creación y validación de una herramienta tecnológica en niños con parálisis cerebral',
            'financiamiento' => '0',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2014-05-01',
            'hasta' => '2015-07-31',
            'user_id'=>$edwin->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

        $inv2 = factory(Investigacion::class)->create([
            'cargo' => 'Investigador',
            'proyecto' => 'VPRehab Desarrollo de un Demostrador Tecnológico en Pacientes con Daño Cerebral Adquirido',
            'financiamiento' => '0',
            'participantes' => 'Universidad de las Fuerzas Armadas ESPE',
            'desde' => '2016-02-01',
            'hasta' => '2018-02-31',
            'user_id'=>$edwin->id,
            'ubicacion' => 'Latacunga, Ecuador'
        ]);

    }
}
