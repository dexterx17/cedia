<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Documento extends Model
{
    protected $table = "documentos";

    protected $fillable = [
    	'nombre', 'resumen', 'ruta', 'palabras_clave', 'fecha', 'tipo_id', 'upload_by'
    ];

    public function tipo(){
    	return $this->belongsTo('App\Tipo_documento');
    }

    public function uploadBy(){
    	return $this->belongsTo('App\User','upload_by','id','users');
    }

    public function scopeReferencias($query){
        $id = $this->id;

    	$items = DB::table('documento_referencias')->where('documento_id','=',$id)
                    ->get();
        //return $items;
        $resultado= collect([]);

        foreach ($items as $key => $item) {
            switch ($item->referencia_table) {
                case 'proyectos':
                    $proyecto = Proyecto::find($item->referencia_id);
                    $resultado->push($proyecto);
                    break;
            }
        }
        return $resultado;
    }
}
