<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    protected $table = "participantes";

    protected $fillable = [
          'nombre', 'apellido', 'ci', 'email', 'institucion','elegido','cargo'
    ];

}
