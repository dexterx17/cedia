<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    protected $table = "imagenes";

    protected $fillable = [
    	'ruta','nombre','autor','creditos',
    	'tabla_referencia','id_referencia','destacada', 'tresd'
    ];

    /**
     * Devuelve la instancia del Modulo a la que pertenece la imagen
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeReferencia($query){
    	
    	switch ($this->tabla_referencia) {
    		case 'proyectos':
    			$proyecto = Proyecto::find($this->id_referencia);
                $proyecto->url = route('proyecto',$proyecto->slug);
                return $proyecto;
    			break;
    		case 'trabajos':
    			$trabajo = Trabajo::find($this->id_referencia);
                //$trabajo->url = route('front.trabajo',$trabajo->slug);
                return $trabajo;
    			break;
    		case 'publicaciones':
    			$publicacion = Publicacion::find($this->id_referencia);
                //$publicacion->url = route('front.publicacion_canton',[$publicacion->canton->slug,$publicacion->slug]);
                return $publicacion;
                break;
            case 'reconocimientos':
                $reconocimiento = Reconocimiento::find($this->id_referencia);
                //$reconocimiento->url = route('front.reconocimiento_canton',[$reconocimiento->canton->slug,$reconocimiento->slug]);
                return $reconocimiento;
    			break;
            case 'investigaciones':
                $investigacion = Investigacion::find($this->id_referencia);
                //$investigacion->url = route('front.investigacion',$investigacion->slug);
                return $investigacion;
                break;
            case 'users':
    			$user = User::find($this->id_referencia);
                $user->url = route('user_info',$user->id);
                return $user;
    			break;
    	}
    }

}
