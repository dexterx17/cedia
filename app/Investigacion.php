<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investigacion extends Model
{
    protected $table = "investigaciones";

    protected $fillable = [
    	'cargo','proyecto','objetivos','descripcion','user_id',
    	'financiamiento','desde','hasta','participantes','ubicacion'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }

}
