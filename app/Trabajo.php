<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajo extends Model
{
    protected $table = 'trabajos';

    protected $fillable = [
    	'cargo','funciones','departamento','institucion','desde','actual',
    	'hasta','administrativo','tiempo','user_id','ubicacion'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }
    
    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }
}
