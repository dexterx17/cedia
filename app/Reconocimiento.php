<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reconocimiento extends Model
{
    protected $table = 'reconocimientos';

    protected $fillable = [
    	'reconocimiento', 'fecha', 'ubicacion','descripcion','imagen','user_id'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }
}
