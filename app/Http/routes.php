<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

Route::get('/', [
	'uses' => 'Front@index',
	'as' => 'home'
]);
Route::get('/i', [
	'uses' => 'Front@i',
	'as' => 'test'
]);
Route::get('/saludo', [
	'uses' => 'Front@saludo',
	'as' => 'saludo'
]);

Route::get('/user/{user_id}/info',[
	'uses' => 'Front@user_info',
	'as' => 'user_info'
]);

Route::get('/proyecto/{poyecto_id}',[
	'uses' => 'Front@proyecto',
	'as' => 'proyecto'
]);

Route::get('/user/{user_id}/experiencia',[
	'uses' => 'Front@user_experiencia',
	'as' => 'user_experiencia'
]);

Route::get('/user/{user_id}/publicaciones',[
	'uses' => 'Front@user_publicaciones',
	'as' => 'user_publicaciones'
]);

Route::get('/user/{user_id}/investigaciones',[
	'uses' => 'Front@user_investigaciones',
	'as' => 'user_investigaciones'
]);

Route::get('/user/{user_id}/reconocimientos',[
	'uses' => 'Front@user_reconocimientos',
	'as' => 'user_reconocimientos'
]);

/* rutas EVENTOs */
Route::get('congreso',[
	'uses' => 'Front@evento',
	'as' => 'evento'
]);

Route::get('curso',[
	'uses' => 'Front@curso',
	'as' => 'curso'
]);

Route::post('congreso',[
	'uses' => 'Participantes@registro',
	'as' => 'evento.registro'
]);
	
Route::get('confirmen',[
	'uses' => 'Front@confirmen',
	'as' =>'confirmen'
]);

/*
|--------------------------------------------------------------------------
|  RUTAS PARA VISTAS ADMINISTRATIVAS
|--------------------------------------------------------------------------
*/

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
//Route::group(['prefix'=>'admin'],function(){

	Route::get('/', [
		'uses'=>'Back@dashboard',
		'as'=>'admin.dashboard']
	);

	Route::get('/q_intereses', [
		'uses'=>'Back@intereses',
		'as'=>'intereses.query']
	);


	// rutas USUARIOS
	Route::resource('/usuarios','Usuarios',['except'=>['update','destroy']]);
	Route::post('usuarios/{user_id}',[
		'uses' =>'Usuarios@update',
		'as'=>'admin.usuarios.update']
	);
	Route::get('usuarios/{id}/destroy',[
			'uses'=>'Usuarios@destroy',
			'as' =>'admin.usuarios.destroy']
	);
	Route::post('usuaros/reordenar',[
		'uses'=>'Usuarios@reordenar',
		'as'=>'admin.usuarios.reordenar']
	);

	// rutas ASISTENTES
	Route::resource('/asistentes','Asistentes',['except'=>['update','destroy']]);
	Route::post('asistentes/{user_id}',[
		'uses' =>'Asistentes@update',
		'as'=>'admin.asistentes.update']
	);
	Route::get('asistentes/{id}/destroy',[
			'uses'=>'Asistentes@destroy',
			'as' =>'admin.asistentes.destroy']
	);
	Route::post('asistents/reordenar',[
		'uses'=>'asistentes@reordenar',
		'as'=>'admin.asistentes.reordenar']
	);


	//rutas PUBLICACIONES
	Route::resource('/publicaciones','Publicaciones',['except'=>['update','destroy']]);
	Route::post('publicaciones/{user_id}',[
		'uses' =>'Publicaciones@update',
		'as'=>'admin.publicaciones.update']
	);
	Route::get('publicaciones/{id}/destroy',[
			'uses'=>'Publicaciones@destroy',
			'as' =>'admin.publicaciones.destroy']
	);


	//rutas PONENCIAS
	Route::resource('/ponencias','Ponencias',['except'=>['update','destroy']]);
	Route::post('ponencias/{user_id}',[
		'uses' =>'Ponencias@update',
		'as'=>'admin.ponencias.update']
	);
	Route::get('ponencias/{id}/destroy',[
			'uses'=>'Ponencias@destroy',
			'as' =>'admin.ponencias.destroy']
	);

	//rutas TITULACIONES
	Route::resource('/titulaciones','Titulaciones',['except'=>['update','destroy']]);
	Route::post('titulaciones/{user_id}',[
		'uses' =>'Titulaciones@update',
		'as'=>'admin.titulaciones.update']
	);
	Route::get('titulaciones/{id}/destroy',[
			'uses'=>'Titulaciones@destroy',
			'as' =>'admin.titulaciones.destroy']
	);

	//rutas UNIVERSIDADES
	Route::resource('/universidades','Universidades',['except'=>['update','destroy']]);
	Route::post('universidades/{user_id}',[
		'uses' =>'Universidades@update',
		'as'=>'admin.universidades.update']
	);
	Route::get('universidades/{id}/destroy',[
			'uses'=>'Universidades@destroy',
			'as' =>'admin.universidades.destroy']
	);

	//rutas INTERESES
	Route::resource('/intereses','Intereses',['except'=>['update','destroy']]);
	Route::post('intereses/{user_id}',[
		'uses' =>'Intereses@update',
		'as'=>'admin.intereses.update']
	);
	Route::get('intereses/{id}/destroy',[
			'uses'=>'Intereses@destroy',
			'as' =>'admin.intereses.destroy']
	);

	//rutas TIPOS DE DOCUMENTOS
	Route::resource('/tipos_documentos','Tipos_documentos',['except'=>['update','destroy']]);
	Route::post('tipos_documentos/{user_id}',[
		'uses' =>'Tipos_documentos@update',
		'as'=>'admin.tipos_documentos.update']
	);
	Route::get('tipos_documentos/{id}/destroy',[
			'uses'=>'Tipos_documentos@destroy',
			'as' =>'admin.tipos_documentos.destroy']
	);

	//rutas DOCUMENTOS
	Route::resource('/documentos','Documentos',['except'=>['destroy']]);
	
	Route::get('documentos/{id}/destroy',[
			'uses'=>'Documentos@destroy',
			'as' =>'admin.documentos.destroy']
	);

	//rutas PROYECTOS
	Route::resource('/proyectos','Proyectos',['except'=>['update','destroy']]);
	Route::post('proyectos/{user_id}',[
		'uses' =>'Proyectos@update',
		'as'=>'admin.proyectos.update']
	);
	Route::get('proyectos/{id}/destroy',[
			'uses'=>'Proyectos@destroy',
			'as' =>'admin.proyectos.destroy']
	);


	Route::resource('/investigaciones','Investigaciones',['except'=>['update','destroy']]);
	Route::post('investigaciones/{investigacion_id}',[
		'uses' =>'Investigaciones@update',
		'as'=>'admin.investigaciones.update']
	);
	Route::get('investigaciones/{id}/destroy',[
			'uses'=>'Investigaciones@destroy',
			'as' =>'admin.investigaciones.destroy']
	);

	Route::resource('/reconocimientos','Reconocimientos',['except'=>['update','destroy']]);
	Route::post('reconocimientos/{reconocimiento_id}',[
		'uses' =>'Reconocimientos@update',
		'as'=>'admin.reconocimientos.update']
	);
	Route::get('reconocimientos/{id}/destroy',[
			'uses'=>'Reconocimientos@destroy',
			'as' =>'admin.reconocimientos.destroy']
	);
	
	Route::resource('/experiencias','Experiencias',['except'=>['update','destroy']]);
	Route::post('experiencias/{experiencia_id}',[
		'uses' =>'Experiencias@update',
		'as'=>'admin.experiencias.update']
	);
	Route::get('experiencias/{id}/destroy',[
			'uses'=>'Experiencias@destroy',
			'as' =>'admin.experiencias.destroy']
	);
	
	/* rutas IMAGENES */
	Route::get('imagenes/{id}/edit',[
		'uses' => 'Imagenes@edit',
		'as' => 'admin.imagenes.edit'
	]);
	Route::post('imagens/{id}',[
		'uses' => 'Imagenes@update',
		'as' => 'admin.imagenes.update'
	]);
	Route::post('imagenes/{canton_id}',[
		'uses' =>'Imagenes@add',
		'as'=>'admin.imagenes.store'
	]);
	Route::get('imagenes/{id}/destroy',[
		'uses'=>'Imagenes@destroy',
		'as' =>'admin.imagenes.destroy'
	]);

	/* rutas PARTICIPANTES */

	Route::resource('/participantes','Participantes',['only'=>['index','edit']]);
	Route::post('participantes/{experiencia_id}',[
		'uses' =>'Participantes@update',
		'as'=>'admin.participantes.update']
	);
	Route::get('/part/{id}','Participantes@elegir')->name('admin.participantes.elegir');
	
	Route::get('/participants/{id}','Participantes@destroy')->name('admin.participantes.destroy');

});

Route::auth();

//Route::get('/home', 'HomeController@index');
