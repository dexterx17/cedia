<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Publicacion;
use App\Proyecto;
use App\Interes;
use App\Ponencia;

class Back extends Controller
{
	/**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='dashboard';
    }


    public function dashboard(){
        $this->datos['usuarios'] = User::all();
        $this->datos['publicaciones'] = Publicacion::all();
    	$this->datos['proyectos'] = Proyecto::all();
        $this->datos['ponencias'] = Ponencia::all();
    	return view('back.dashboard',$this->datos);
    }

    public function intereses(Request $request){
        $keyword = $request->input('keyword');
        $skills = Interes::where('interes','like','%'.$keyword.'%')
                  ->select('id','interes','interes')
                  ->get();
        $skills = Interes::select('interes')->get();
        return json_encode($skills);
    }
}
