<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Ayudante;

use App\Publicacion;
use App\Participante;
use App\Proyecto;
use App\Imagen;

use Mail;

class Front extends Controller
{
	/**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='home';
    }

    public function index(Request $request){
        $this->datos['usuarios'] = User::orderBy('orden','ASC')->get();
        $this->datos['ayudantes'] = Ayudante::orderBy('orden','ASC')->get();
        $publicaciones = Publicacion::orderBy('fecha_publicacion','DESC')->get();
        $publicaciones->each(function($p){
            if (!is_null($p['fecha_publicacion']))
                $p->anio=substr($p['fecha_publicacion'], 0,4);
            else
                $p->anio="0000";
        });
        $publicacionesByDate = $publicaciones->groupBy('anio');
        $this->datos['imagenes'] = Imagen::all();
        $this->datos['publicaciones'] = Publicacion::all();
        $this->datos['proyectos'] = Proyecto::all();
        $this->datos['publicaciones_group'] = $publicacionesByDate;
        return view('welcome',$this->datos);
    }

	public function i(Request $request){
		$this->datos['usuarios'] = User::orderBy('orden','ASC')->get();
		$publicaciones = Publicacion::orderBy('fecha_publicacion','DESC')->get();
		$publicaciones->each(function($p){
			if (!is_null($p['fecha_publicacion']))
				$p->anio=substr($p['fecha_publicacion'], 0,4);
			else
				$p->anio="0000";
		});
		$publicacionesByDate = $publicaciones->groupBy('anio');
    	$this->datos['publicaciones'] = Publicacion::all();
    	$this->datos['proyectos'] = Proyecto::all();
    	$this->datos['publicaciones_group'] = $publicacionesByDate;
		return view('welcome_test',$this->datos);
	}

    public function publicaciones(){
    	echo "List publicaciones";

    	$url = 'http://cse.bth.se/~fer/googlescholar.php?user=U28c5SYAAAAJ';

    	$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', $url, []);
		//echo $res->getStatusCode();
		// "200"
		//echo $res->getHeader('content-type');
		// 'application/json; charset=utf8'
		$response = $res->getBody();

		$response = trim($response);

		$separador = strpos($response,'"publications"');

		$resumen = substr($response,0, ($separador-3) );
		$resumen = str_replace('"total_citations": ,','"total_citations":0,',$resumen);
		$resumen = str_replace('"citations_per_year": ','"citations_per_year":0',$resumen);

		$publicaciones = substr($response,($separador+15), (strlen($response)-71) );

		$publicaciones = str_replace('"citations": ,', '"citations": 0,',$publicaciones);
		$pubs = json_decode($publicaciones);
		//echo $response;
		echo "<hr>";
		echo $resumen;
		echo "<hr>";
		echo $publicaciones;
		echo "<hr>pubs:";
		print_r($pubs);


		//$publicaciones = $response['']
		//foreach ($pubs as $key => $p) {
			
		// /}
		return view('publicaciones',['publicaciones'=>$pubs]);
    }

    public function user_info($user_id){
    	$user = User::find($user_id);
    	$this->datos['page_active'] = 'info';
    	$this->datos['user_data'] = $user;
		return view('front.users.info',$this->datos);
    }

    public function user_experiencia($user_id){
        $user = User::find($user_id);
        $jobs = $user->trabajos()->orderBy('desde','DESC')->get();

        $this->datos['trabajos'] = $jobs;
        $this->datos['user_data'] = $user;
        $this->datos['page_active'] = 'experiencia';

        return view('front.users.experiencia',$this->datos);
    }

    public function user_investigaciones($user_id){
    	$user = User::find($user_id);
    	$pubs = $user->investigaciones()->get();

    	$this->datos['investigaciones'] = $pubs;
    	$this->datos['user_data'] = $user;
    	$this->datos['page_active'] = 'investigaciones';

		return view('front.users.investigaciones',$this->datos);
    }

    public function user_reconocimientos($user_id){
    	$user = User::find($user_id);
    	$pubs = $user->reconocimientos()->get();

    	$this->datos['reconocimientos'] = $pubs;
    	$this->datos['user_data'] = $user;
    	$this->datos['page_active'] = 'reconocimientos';

		return view('front.users.reconocimientos',$this->datos);
    }

    public function user_publicaciones($user_id){
    	$user = User::find($user_id);
    	$pubs = $user->publicaciones()->get();

    	$this->datos['publicaciones'] = $pubs;
    	$this->datos['user_data'] = $user;
    	$this->datos['page_active'] = 'publicaciones';

		return view('front.users.publicaciones',$this->datos);
    }

    public function saludo(Request $request){

        //$user = User::find($request->id);
        if($request->has('id'))
            $users = Participante::where('id','=',$request->id)->get();
        else{
            $users = Participante::where('elegido','=',true)->get();
        }

        foreach ($users as $key => $user) {
                //echo $user->email.' - '. $user->nombre.' '.$user->apellido;
                Mail::send('mails.elegido', ['user' => $user], function ($m) use ($user) {
                    $m->from('info@giarsi.com', 'GIARSI');
                    //dd($user->email);
                $m->to($user->email, $user->nombre.' '.$user->apellido)->subject('Lanzamiento de curso TÉCNICAS DE OPTIMIZACIÓN para ALGORITMOS DE CONTROL AVANZADO');
            });
        }
        
    }

    public function confirmen(Request $request){
        $participantes = [
            ["Augusto Bourgeat ","0501614911 ","0995089 827","labourgeat@espe.edu.ec"],
            ["Blanca Liliana Topon Visarrea","17121114187","0995924164","Blancatopon@uti.edu.ec"],
            ["Bryan Stefano Guevara Bermeo","1600517708","0996319410","Bryansgue@gmail.com"],
            ["Byron Paul Remache Vinueza","1721088837","996669054","Byronremache@uti.edu.ec"],
            ["Carlos Marcelo Silva","1801836360","0998329410","cmsilva@espe.edu.ec"],
            ["Carlos Rafael S�nchez Mosquera","1803232113","0998839912","crsanchez9@espe.edu.ec "],
            ["Cesar Alfredo Naranjo Hidalgo","0501498505","0998025796","Canaranjo@espe.edu.ec"],
            ["Christian Eduardo Iza Llumigusin","1712438264","0990656910","Christianiza@uti.edu.ec"],
            ["Danny Alexander Lozano C","0604053645","0960173995","dalozano.fie@unach.edu.ec"],
            ["Darwin Santiago Sarzosa Garc�a","0504064965","0983229998","Dssarzosa1@espe.edu.ec"],
            ["Edison Luciano Bonilla Borja","0504572546","0983962747","Luciano_bonibo@outlook.es"],
            ["Edison Patricio Velasco S�nchez","0503321101","0939228038","epvelasco1912@gmail.com"],
            ["Edwin Patricio Pruna Panchi","0502651003","0995475776","eppruna@espe.edu.ec"],
            ["Elsa Jacqueline Pozo Jara","0602151060","0999777554","ejpozo3@espe.edu.ec"],
            ["Fabi�n Ramiro Revelo Aguilar","1804628483","0995207617","faby89.science@gmail.com"],
            ["Fabricio Trujillo","1802535797","0988754456","sftrujillo@espe.edu.ec"],
            ["Fausto Ramiro Cabrera Aguayo","0602538584","0982362746","fausto.cabrera@espoch,edu.ec"],
            ["Franklin Manuel Silva Monteros","1801962125","099 971 9329","fmsilva@espe.edu.ec"],
            ["Henry Heriberto Iza Tobar","0502005242","0987241684","Henryiza@hotmail.com"],
            ["Jessica Sofia Ortiz Moreano","0603924226","0984547194","jsortiz4@espe.edu.ec"],
            ["Jorge Luis Hernandez Ambato","0603596867","0999078797","jhernandez@espoch.edu.ec"],
            ["Jos� Luis Varela Ald�s","2200117758","099 531 6811","Josevarela@uti.edu.ec"],
            ["Lenin Rodrigo Villarreal Grijalva","1720778917","0983727988","lrvillarreal1@gmail.com"],
            ["Luis Fernando Recalde","1717893166","0991398168","lfrecalde1@espe.edu.ec"],
            ["Luis Lema Cerda","0502046725","0998576753","lalema@espe.edu.ec"],
            ["Luis Enrique Mena Mena","1204647596","0995461290","lemena@espe.edu.ec"],
            ["Manuel Ignacio Ayala Chauvin","1103320857","0968701477","mayala@uti.edu.ec"],
            ["Marcelo Alvarez Veintimilla ","0502419051","0992668556","rmalvarez@espe.edu.ec"],
            ["Marco Antonio Pilatasig Panchi","0502375876","0984743705","mapilatagsig@espe.edu.ec"],
            ["MIguel Angel Villa Zumba","0602664419","0993495489","mavilla1@espe.edu.ec"],
            ["Mildred Lisseth Cajas Buena�o","0503497604","0999834668","mlcajas@espe.edu.ec"],
            ["Nancy Guerr�n","1707886139","0963085817","neguerron@espe.edu.ec"],
            ["Paola Mariela Proa�o Molina","0503158529","0998602426","pmproano2@espe.edu.ec"],
            ["Paola Velasco","0502519770","0960214845","pmvelasco1@espe.edu.ec"],
            ["Pa�l Alexi Canseco S�nchez","1803253515","098701477","pcpaul_1300@hotmail.com"],
            ["Roberto Salazar","0502847619","0999035220","ersalazar@espe.edu.ec"],
            ["Victor Isaac Herrera Perez","0603944349","isaac.herrera@espoch.edu.ec"],
            ["V�ctor Rub�n Bautista Naranjo","1802732618","0983509255","vrbautista@espe.edu.ec"],
            ["Wilson Patricio Reyes Bedoya","1803135639","0999958098","wpreyes@espe.edu.ec"],
            ["Wilson S�nchez Oca�a","0501529937","0999975572","wesanchez@espe.edu.ec"],
            ["Wilson Tr�vez","0502303662","0984532854","Wotravez@espe.edu.ec"],
            ["David Ricardo Castillo S.","1802634996","998339476","davidcastillo@uti.edu.ec"],
            ["Myriam Emperatriz Cumbajin","1804164422","982678081","myriamcumbajin@uti.edu.ec"],
            ["Jhon Carlos Rodriguez Hidalgo","503993024","Rodriguez_jhon10a@hotmail.com"],
            ["Giovanny Cuzco","1802840718","0991891271","gcuzco@unach.edu.ec"],
            ["V�ctor Hugo Andaluz","1803737442","095 877 9578","Vhandaluz1@espe.edu.ec"],
            ["Jorge S�nchez","1803232121","099 989 5268","Jssanchez@espe.edu.ec"],
            ["Andres Sebastian Ortega Castro","1804388849","969028881","sebasmen2040@gmail.com"],
            ["Hugo Oswaldo Moreno Avil�s","602928780","0996637848","h_moreno@espoch.edu.ec"],
            ["Paulo Leica","1714829585","0984765529","paulo.leica@epn.edu.ec"],
            ["Gabriela Andaluz","1803920386","0999273477","andaluzgaby_4@yahoo.com"]
        ];
        
        $participantes = [
            ["Jaime Santana","1600392359","983706086","sistemas@santana.ec"]
        ];
        foreach($participantes as $p){

            $user = [
                'nombre'=>$p[0],
                'email' => $p[3]
            ];

            Mail::send('mails.confirmen', ['user' => $user], function ($m) use ($user) {
                $m->from('info@giarsi.com', 'GIARSI');
                //dd($user->email);
                $m->to($user['email'],$user['nombre'])->subject('Confirmación nombre para certificado');
            });
        }
        
    }

    /**
     * Muestra la vista detallada de un proyecto
     */
    public function proyecto($id){
        $proyecto = Proyecto::find($id); 
        $proyectos = Proyecto::where('id','!=',$id)->inRandomOrder()->limit(3)->get(); 
        $this->datos['proyecto'] = $proyecto;      
        $this->datos['proyectos'] = $proyectos;
        $this->datos['imagenes'] = Imagen::all();   
        return view('front.proyecto',$this->datos);
    }


    public function evento(){
        return view('templates.event.base', $this->datos);
    }
    
    public function curso(){
        $this->datos['cupos'] = 50 - Participante::count();
        return view('templates.event.curso', $this->datos);
    }
}
