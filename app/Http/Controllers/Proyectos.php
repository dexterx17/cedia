<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Universidad;
use App\Proyecto;
use App\User;
use App\Tipo_documento;

class Proyectos extends Controller
{
     /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='proyectos';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->datos['proyectos_data'] = Proyecto::orderBy('id','ASC')->paginate(15);
        return view('back.proyectos.manager',$this->datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['usuarios']=$this->_dropdownUsuarios();
        return view('back.proyectos.create',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto = new Proyecto($request->all());
        $proyecto->save();

        if(isset($request->universidades)){
            $universidades = [];
            foreach ($request->universidades as $key => $uni) {
                $universidades[]=$uni;
            }
            $proyecto->universidades()->sync($universidades);
        } 

        if(isset($request->usuarios)){
            $usuarios = [];
            foreach ($request->usuarios as $key => $u) {
                $usuarios[]=$u;
            }
            $proyecto->usuarios()->sync($usuarios);
        }

        //flash("$proyecto->titulo actualizado correctamente",'success');
        return redirect()->route('admin.proyectos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->datos['proyecto'] = Proyecto::find($id);
        $this->datos['tipos'] = Tipo_documento::all();
        return view('back.proyectos.show',$this->datos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['usuarios']=$this->_dropdownUsuarios();
        $this->datos['proyecto'] = Proyecto::find($id);
        return view('back.proyectos.edit',$this->datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto = Proyecto::find($id);
        $proyecto = $proyecto->fill($request->all());

        if(isset($request->universidades)){
            $universidades = [];
            foreach ($request->universidades as $key => $u) {
                $universidades[]=$u;
            }
            $proyecto->universidades()->sync($universidades);
        }

        
        if(isset($request->usuarios)){
            $usuarios = [];
            foreach ($request->usuarios as $key => $u) {
                $usuarios[]=$u;
            }
            $proyecto->usuarios()->sync($usuarios);
        }

        $proyecto->save();

        //flash("$proyecto->titulo actualizado correctamente",'success');
        return redirect()->route('admin.proyectos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $proyecto = Proyecto::find($id);
        $proyecto->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("Proyecto $proyecto->titulo eliminada correctamente",'success');
            return redirect()->route('admin.proyectos.index');
        }
    }
    
    /**
     * Devuelve un array con listado de universidades en la forma 
     *  [ id => nombre(Abreviación) ]
     */
    private function _dropdownUniversidades(){
        $us = Universidad::orderBy('nombre','ASC')->get();
        $items = [];

        foreach($us as $index => $universidad){
            $items[$universidad->id] = $universidad->nombre. ' ('.$universidad->abreviacion.')';
        }
        return $items;
    }

    /**
     * Devuelve un array con e listado de usuarios en la forma
     * [ id => nombres apellidos ]
     */
    private function _dropdownUsuarios(){
        $users= [];
        $usuarios = User::orderBy('nombres','ASC')->get();
        foreach ($usuarios as $key => $u) {
            $users[$u->id] = $u->nombres.' '.$u->apellidos;
        }
        return $users;
    }
}
