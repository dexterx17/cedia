<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Participante;

use Mail;

class Participantes extends Controller
{
    /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='usuarios';
    }

    /**
     * Muestra el listado de elementos
     */
    public function index(){
        $this->datos['usuarios_data'] = Participante::orderBy('nombre','ASC')->paginate(200);
        $this->datos['presencial'] = Participante::where('tipo','presencial')->count();
        $this->datos['videoconferencia'] = Participante::where('tipo','videoconferencia')->count();
        return view('back.participantes.manager',$this->datos);
    }

    public function edit($id, Request $request){
        $this->datos['participante'] = Participante::find($id);
        return view('back.participantes.edit',$this->datos);
    }

    public function update($id, Request $request){
        $participante = Participante::find($id);

        $participante->fill($request->all());
        $participante->save();
        return redirect()->route('admin.participantes.index');
    }

    public function elegir($id, Request $request){
        
        $respuesta = (boolean) $request->elegido;
        
        $participante = Participante::find($id);
        $participante->elegido = $respuesta;
        $participante->save();

    }
    public function registro(Request $request){
        $registro = new Participante($request->all());
        $registro->save();

        Mail::send('mails.bienvenida', $registro->toArray(), function ($m) use ($registro) {
            $m->from('giarsi.com@gmail.com', 'TÉCNICAS DE OPTIMIZACIÓN para ALGORITMOS DE CONTROL AVANZADO');
            
            $m->to($registro->email, $registro->nombre.' '.$registro->apellido);
            $m->subject('Registro exitoso: '.date('Y-m-d h:i'));
        });

        return view( 'templates.event.registrado', [ 'registro' => $registro ] );
    }

    
    /**
     * Elimina un elemento de la base de datos
     */
    public function destroy($id, Request $request){
        $usuario = Participante::find($id);
        $usuario->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("User $usuario->titulo eliminada correctamente",'success');
            return redirect()->route('admin.participantes.index');
        }
    }

}
