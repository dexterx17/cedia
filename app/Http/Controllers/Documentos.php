<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Documento;
use App\Tipo_documento;
use App\Proyecto;

use DB;

class Documentos extends Controller
{
         /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='documentos';
    }

    public function index(){
    	$this->datos['documentos_data'] = Documento::orderBy('id','ASC')->paginate(15);
        return view('back.documentos.manager',$this->datos);
    }

    /**
     * Muestra el formulario para la creación de documentos
     */
    public function create(Request $request){
        if($request->has('ref')){
            $this->datos['ref'] = $request->ref;
        }
        if($request->has('tipo')){
            $this->datos['tipo'] = $request->tipo;
        }
        $this->datos['tipos'] = Tipo_documento::all()->pluck('tipo','id');
        $this->datos['proyectos'] = Proyecto::all()->pluck('nombre','id');
        return view('back.documentos.create',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $documento = new Documento($request->all());
        
        if($request->file('documento'))
        {
            $file = $request->file('documento');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/uploads/';
            $file->move($path,$name);
            $documento->ruta=$name;
        }

        $documento->save();

        if($request->has('referencias')){
            foreach ($request->referencias as $key => $ref) {
                DB::table('documento_referencias')->insert([
                    'documento_id' => $documento->id,
                    'referencia_id' => $ref,
                    'referencia_table' => 'proyectos'
                ]);
            }
        }

        //flash("$documento->titulo actualizado correctamente",'success');
        return redirect()->route('admin.documentos.index');
    }

    /**
     * 
     */
    public function edit($id)
    {
        $documento = Documento::find($id);
        $referencias = $documento->referencias()->pluck('id')->toArray();
        $this->datos['referencias'] = $referencias;
        $this->datos['documento'] = $documento;
        $this->datos['proyectos'] = Proyecto::all()->pluck('nombre','id');
        $this->datos['tipos'] = Tipo_documento::all()->pluck('tipo','id');
        return view('back.documentos.edit',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $documento = Documento::find($id);
        $documento->fill($request->all());
        
        if($request->file('documento'))
        {
            $file = $request->file('documento');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/uploads/';
            $file->move($path,$name);
            $documento->ruta=$name;
        }

        $documento->save();

        if($request->has('referencias')){
            DB::table('documento_referencias')->where('documento_id','=',$id)->delete();
            foreach ($request->referencias as $key => $ref) {
                DB::table('documento_referencias')->insert([
                    'documento_id' => $documento->id,
                    'referencia_id' => $ref,
                    'referencia_table' => 'proyectos'
                ]);
            }
        }

        //flash("$documento->titulo actualizado correctamente",'success');
        return redirect()->route('admin.documentos.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $documento = Documento::find($id);
        $documento->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("Documento $documento->titulo eliminada correctamente",'success');
            return redirect()->route('admin.proyectos.index');
        }
    }
    

}
