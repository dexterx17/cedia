<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Tesis;
use App\Universidad;
use App\User;
use App\Ayudante;

use DB;

class Titulaciones extends Controller
{
     /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='tesis';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->datos['tesis_data'] = Tesis::orderBy('id','ASC')->paginate(15);
        return view('back.tesis.manager',$this->datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['investigadores']=$this->_dropdownInvestigadores();
        $this->datos['estudiantes']=$this->_dropdownEstudiantes();
        return view('back.tesis.create',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $tesis = new Tesis($request->all());
        $tesis->save();

        if(isset($request->estudiantes)){

            foreach ($request->estudiantes as $key => $uni) {

                if($uni!=="0"){

                    $tipo = substr($uni,0,1);
                    $code = substr($uni,1,strlen($uni));
                    if($tipo=="e"){//si es de tipo investigador
                        
                        DB::table('tesistas')->insert([
                            'tesis_id' => $tesis->id,
                            'referencia_id' => $code,
                            'referencia_table' => 'users'
                        ]);
                    }else{// si es de tipo estudiante

                        DB::table('tesistas')->insert([
                            'tesis_id' => $tesis->id,
                            'referencia_id' => $code,
                            'referencia_table' => 'ayudantes'
                        ]);
                    }
                }
            }
        }

        //flash("$tesis->titulo actualizado correctamente",'success');
        return redirect()->route('admin.titulaciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['investigadores']=$this->_dropdownInvestigadores();
        $this->datos['estudiantes']=$this->_dropdownEstudiantes();
        $this->datos['tesis'] = Tesis::find($id);
        return view('back.tesis.edit',$this->datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tesis = Tesis::find($id);
        $tesis = $tesis->fill($request->all());

        if(isset($request->estudiantes)){
            DB::table('tesistas')->where('tesis_id','=',$id)->delete();
            foreach ($request->estudiantes as $key => $uni) {

                if($uni!=="0"){

                    $tipo = substr($uni,0,1);
                    $code = substr($uni,1,strlen($uni));
                    if($tipo=="e"){//si es de tipo investigador
                        
                        DB::table('tesistas')->insert([
                            'tesis_id' => $tesis->id,
                            'referencia_id' => $code,
                            'referencia_table' => 'users'
                        ]);
                    }else{// si es de tipo estudiante
                        
                        DB::table('tesistas')->insert([
                            'tesis_id' => $tesis->id,
                            'referencia_id' => $code,
                            'referencia_table' => 'ayudantes'
                        ]);
                    }
                }
            }
        }

        $tesis->save();

        //flash("$tesis->titulo actualizado correctamente",'success');
        return redirect()->route('admin.titulaciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $tesis = Tesis::find($id);
        $tesis->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("Proyecto $tesis->titulo eliminada correctamente",'success');
            return redirect()->route('admin.titulaciones.index');
        }
    }
     
    /**
     * Devuelve un array con listado de universidades en la forma 
     *  [ id => nombre(Abreviación) ]
     */
    private function _dropdownUniversidades(){
        $us = Universidad::orderBy('nombre','ASC')->get();
        $items = [];

        foreach($us as $index => $universidad){
            $items[$universidad->id] = $universidad->nombre. ' ('.$universidad->abreviacion.')';
        }
        return $items;
    }

    /**
     * Devuelve un array con e listado de usuarios en la forma
     * [ id => nombres apellidos ]
     */
    private function _dropdownInvestigadores(){
        $users= [];
        $usuarios = User::orderBy('nombres','ASC')->get();
        $users[]="";
        foreach ($usuarios as $key => $u) {
            $users[$u->id] = $u->nombres.' '.$u->apellidos;
        }
        return $users;
    }

    /**
     * Devuelve un array con e listado de usuarios en la forma
     * [ id => nombres apellidos ]
     */
    private function _dropdownEstudiantes(){
        $users= [];
        $usuarios = User::orderBy('nombres','ASC')->get();
        $users[]="";
        foreach ($usuarios as $key => $u) {
            $users['e'.$u->id] = $u->nombres.' '.$u->apellidos;
        }

        $asistentes = Ayudante::orderBy('nombres','ASC')->get();
        
        foreach ($asistentes as $key => $u) {
            $users['a'.$u->id] = $u->nombres.' '.$u->apellidos;
        }

        return $users;
    }
}
