<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Universidad;

class Universidades extends Controller
{
    
    /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='universidades';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->datos['universidades_data'] = Universidad::orderBy('id','ASC')->paginate(15);
        return view('back.universidades.manager',$this->datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.universidades.create',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $universidad = new Universidad($request->all());
        
        if($request->file('logo'))
        {
            $file = $request->file('logo');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/u/';
            $file->move($path,$name);
            $universidad->logo=$name;
        }

        $universidad->save();

        //flash("$universidad->titulo actualizado correctamente",'success');
        return redirect()->route('admin.universidades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->datos['universidad'] = Universidad::find($id);
        return view('back.universidades.edit',$this->datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $universidad = Universidad::find($id);
        $universidad = $universidad->fill($request->all());
        
        if($request->file('logo'))
        {
            $file = $request->file('logo');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/u/';
            $file->move($path,$name);
            $universidad->logo=$name;
        }

        $universidad->save();

        //flash("$universidad->titulo actualizado correctamente",'success');
        return redirect()->route('admin.universidades.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $universidad = Universidad::find($id);
        $universidad->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("User $universidad->titulo eliminada correctamente",'success');
            return redirect()->route('admin.universidades.index');
        }
    }
}
