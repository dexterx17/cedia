<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Ayudante;
use App\Universidad;

class Asistentes extends Controller
{
     /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='asistentes';
    }

    /**
     * Muestra el listado de elementos
     */
    public function index(){
        $this->datos['asistentes_data'] = Ayudante::orderBy('orden','ASC')->get();
        return view('back.asistentes.manager',$this->datos);
    }

     /**
     * Muestra el formulario para crear un elemento
     */
    public function create(){
        $this->datos['universidades']=$this->_dropdownUniversidades();
        return view('back.asistentes.create',$this->datos);
    }

    /**
     * Toma los datos del formulario de insercion para enviarlos a la base de datos
     */
    public function store(Request $request){
        $this->validate($request,[
            'nombres' => 'required|max:255',
            'apellidos' => 'required|max:255'
        ]);

        $asistente = new Ayudante($request->all());

        if($request->file('imagen'))
        {
            $file = $request->file('imagen');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/pics/';
            $file->move($path,$name);
            $asistente->imagen=$name;
        }

        if($request->has('pasante')){
            $asistente->pasante = TRUE;
        }else{
            $asistente->pasante = FALSE;
        }

        if($request->has('tesista')){
            $asistente->tesista = TRUE;
        }else{
            $asistente->tesista = FALSE;
        }
        $asistente->save();

        //flash("$asistente->titulo actualizado correctamente",'success');
        return redirect()->route('admin.asistentes.index');
    }

    /**
     * Muestra el formulario para editar un elemento
     */
    public function edit($id){
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['usuario'] = Ayudante::find($id);
        return view('back.asistentes.edit',$this->datos);
    }

    
        /**
     * Toma los datos del formulario de actualizacion para enviarlos a la base de datos
     */
    public function update(Request $request, $id){
        $asistente = Ayudante::find($id);

        $asistente->fill($request->all());
        
        if($request->file('imagen'))
        {
            $file = $request->file('imagen');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/pics/';
            $file->move($path,$name);
            $asistente->imagen=$name;
        }

        
        if($request->has('pasante')){
            $asistente->pasante = TRUE;
        }else{
            $asistente->pasante = FALSE;
        }

        if($request->has('tesista')){
            $asistente->tesista = TRUE;
        }else{
            $asistente->tesista = FALSE;
        }
             

        $asistente->save();
        //flash("$asistente->titulo actualizado correctamente",'success');
        return redirect()->route('admin.asistentes.index');
    }
    /**
     * Devuelve un array con listado de universidades en la forma 
     *  [ id => nombre(Abreviación) ]
     */
    private function _dropdownUniversidades(){
        $us = Universidad::orderBy('nombre','ASC')->get();
        $items = [];

        foreach($us as $index => $universidad){
            $items[$universidad->id] = $universidad->nombre. ' ('.$universidad->abreviacion.')';
        }
        return $items;
    }

}
