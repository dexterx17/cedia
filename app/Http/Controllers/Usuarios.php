<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Universidad;
use App\Interes;

class Usuarios extends Controller
{
     /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='usuarios';
    }

    /**
     * Muestra el listado de elementos
     */
    public function index(){
        $this->datos['usuarios_data'] = User::orderBy('orden','ASC')->orderBy('rol','DESC')->get();
        return view('back.usuarios.manager',$this->datos);
    }

    public function show(Request $request, $id){
        $this->datos['user']= User::find($id);
        return view('mails.saludo',$this->datos);
    }

    /**
     * Devuelve un array con listado de universidades en la forma 
     *  [ id => nombre(Abreviación) ]
     */
    private function _dropdownUniversidades(){
        $us = Universidad::orderBy('nombre','ASC')->get();
        $items = [];

        foreach($us as $index => $universidad){
            $items[$universidad->id] = $universidad->nombre. ' ('.$universidad->abreviacion.')';
        }
        return $items;
    }

    /**
     * Muestra el formulario para crear un elemento
     */
    public function create(){
        $intereses = Interes::orderBy('interes','ASC')->lists('interes','id');
        $this->datos['intereses']=$intereses;
        $this->datos['universidades']=$this->_dropdownUniversidades();
        return view('back.usuarios.create',$this->datos);
    }

    /**
     * Toma los datos del formulario de insercion para enviarlos a la base de datos
     */
    public function store(Request $request){
        $this->validate($request,[
            'nombres' => 'required|max:255',
            'apellidos' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'
        ]);

        $usuario = new User($request->all());
        $usuario->password=bcrypt($request->password);

        if($request->file('imagen'))
        {
            $file = $request->file('imagen');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/pics/';
            $file->move($path,$name);
            $usuario->imagen=$name;
        }
            
        if($request->file('cv'))
        {
            $file = $request->file('cv');
            $name = 'cv_'.$usuario->apellidos.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/cv/';
            $file->move($path,$name);
            $usuario->cv=$name;
        }

        $usuario->save();

        if(isset($request->universidades)){
            $cats = [];
            foreach ($request->universidades as $key => $u) {
                $cats[]=$u;
            }
            $usuario->universidades()->sync($cats);
        }
        if(isset($request->intereses)){
            $ints = [];
            foreach ($request->intereses as $key => $u) {
                $interes = Interes::findOrNew($u);
                $ints[]=$interes->id;
            }
            $usuario->intereses()->sync($ints);
        }


        //flash("$usuario->titulo actualizado correctamente",'success');
        return redirect()->route('admin.usuarios.index');
    }

    /**
     * Muestra el formulario para editar un elemento
     */
    public function edit($id){
        $intereses = Interes::orderBy('interes','ASC')->lists('interes','id');
        $this->datos['intereses']=$intereses;
        $this->datos['universidades']=$this->_dropdownUniversidades();
        $this->datos['usuario'] = User::find($id);
        return view('back.usuarios.edit',$this->datos);
    }

    /**
     * Muestra el formulario para editar el perfil de un usuario
     */
    public function edit_perfil($id){
        $universidades = Universidad::orderBy('nombre','ASC')->lists('nombre','id');
        $this->datos['universidades']=$universidades;
        $this->datos['usuario'] = User::find($id);

        return view('back.usuarios.edit_perfil',$this->datos);
    }

    /**
     * Toma los datos del formulario de actualizacion para enviarlos a la base de datos
     */
    public function update(Request $request, $id){
        $usuario = User::find($id);
        if($request->has('nombres'))
            $usuario->nombres =$request->nombres;
        if($request->has('apellidos'))
            $usuario->apellidos =$request->apellidos;
        if($request->has('titulo'))
            $usuario->titulo =$request->titulo;
        if($request->has('email'))
            $usuario->email =$request->email;
        if($request->has('telefono'))
        $usuario->telefono =$request->telefono;
        if($request->has('scholar_id'))
            $usuario->scholar_id =$request->scholar_id;
        if($request->has('type'))
            $usuario->type =$request->type;
        if($request->has('rol'))
            $usuario->rol =$request->rol;
        if($request->has('facebook'))
            $usuario->facebook =$request->facebook;
        if($request->has('skype'))
            $usuario->skype =$request->skype;
        if($request->has('twitter'))
            $usuario->twitter =$request->twitter;
        if($request->has('linkedin'))
            $usuario->linkedin =$request->linkedin;
        if($request->has('orden'))
            $usuario->orden =$request->orden;
        if($request->has('visible'))
            $usuario->visible =$request->visible;
        if($request->has('about_me'))
            $usuario->about_me =$request->about_me;
        
        if($request->has('password')){
            $usuario->password=bcrypt($request->password);
        }

        if(isset($request->universidades)){
            $cats = [];
            foreach ($request->universidades as $key => $u) {
                $cats[]=$u;
            }
            $usuario->universidades()->sync($cats);
        }
        if(isset($request->intereses)){
            $ints = [];
            foreach ($request->intereses as $key => $u) {
                $interes = Interes::findOrNew($u);
                $ints[]=$interes->id;
            }
            $usuario->intereses()->sync($ints);
        }
        
        if($request->file('imagen'))
        {
            $file = $request->file('imagen');
            $name = 'user_'.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/img/pics/';
            $file->move($path,$name);
            $usuario->imagen=$name;
        }   
             
        if($request->file('cv'))
        {
            $file = $request->file('cv');
            $name = 'cv_'.$usuario->apellidos.time().'.'.$file->getClientOriginalExtension();
            
            $path = public_path().'/cv/';
            $file->move($path,$name);
            $usuario->cv=$name;
        }

        $usuario->save();
        //flash("$usuario->titulo actualizado correctamente",'success');
        return redirect()->route('admin.usuarios.index');
    }

     /**
     * Toma la nueva imagen del formulario y la actualiza
     * @param  Request $request [description]
     * @param  integer  $id      Clave primaria del usuario
     * @return [type]           [description]
     */
    public function updatefoto(Request $request, $id){
        $usuario = User::find($id);
        if($request->data)
        {
            $path = public_path().'/imagenes/uploads/';

            //obtengo nombre de la imagen
            $file = $request->name;
            //genero nombre unico para la imagen agregando la fecha
            $name = 'usu_'.time().'.'.$file;
            //creo un archivo temporal vacio con el nuevo nombre en la carpeta que iene permisos de escritura
            $ifp = fopen($path.$name, "wb"); 
            //separo la cadena data por la coma
            $data = explode(',', $request->data);
            //escribo en el nuevo archivo el contenido despues de la coma
            //docodificando el base64
            fwrite($ifp, base64_decode($data[1])); 
            //cierro el archivo
            fclose($ifp); 
            $usuario->imagen=$name;
            $usuario->save();
        }
        if($request->ajax())
        {
            return response()->json(["error"=>FALSE]);
        }else{
            return redirect()->route('admin.usuarios.show',$id);
        }
    }


    /**
     * Elimina un elemento de la base de datos
     */
    public function destroy($id, Request $request){
        $usuario = User::find($id);
        $usuario->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("User $usuario->titulo eliminada correctamente",'success');
            return redirect()->route('admin.usuarios.index');
        }
    }

    /**
     * Guarda el nuevo orden de los cantones
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function reordenar(Request $request){
        foreach ($request->info as $key => $info) {
            $user = User::find($info['user']);
            $user->orden=(int)$info['orden'];
            $user->save();
        }
        return response()->json(['error'=>FALSE,'msg'=>'ok']);
    }
}
