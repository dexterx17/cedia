<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Publicacion;
use App\User;

class Publicaciones extends Controller
{
    /**
     * $datos Guarda las variables que se van a pasar a la vista en un solo array
     * @var array
     */
    var $datos;

    /**
     * Constructor del controlador Front
     * aqui colocamos lo que vayamos a utilizar en todas las vistas que utiliza este controlador
     */
    public function __construct()
    {
        //setea la variable $page para agregar la clase active en el menu principal
        $this->datos['page']='publicaciones';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->datos['publicaciones_data'] = Publicacion::orderBy('id','ASC')->paginate(15);
        return view('back.publicaciones.manager',$this->datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users= [];
        $usuarios = User::orderBy('nombres','ASC')->get();
        foreach ($usuarios as $key => $u) {
            $users[$u->id] = $u->nombres.' '.$u->apellidos;
        }
        $this->datos['usuarios']=$users;
        return view('back.publicaciones.create',$this->datos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $publicacion = new Publicacion($request->all());
        $publicacion->save();

        if(isset($request->usuarios)){
            $users = [];
            foreach ($request->usuarios as $key => $u) {
                $users[]=$u;
            }
            $publicacion->users()->sync($users);
        }
        //flash("$publicacion->titulo actualizado correctamente",'success');
        return redirect()->route('admin.publicaciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users= [];
        $usuarios = User::orderBy('nombres','ASC')->get();
        foreach ($usuarios as $key => $u) {
            $users[$u->id] = $u->nombres.' '.$u->apellidos;
        }
        $this->datos['usuarios']=$users;
        $this->datos['publicacion'] = Publicacion::find($id);
        return view('back.publicaciones.edit',$this->datos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $publicacion = Publicacion::find($id);
        $publicacion = $publicacion->fill($request->all());
        if(isset($request->usuarios)){
            $users = [];
            foreach ($request->usuarios as $key => $u) {
                $users[]=$u;
            }
            $publicacion->users()->sync($users);
        }
        $publicacion->save();

        //flash("$publicacion->titulo actualizado correctamente",'success');
        return redirect()->route('admin.publicaciones.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $usuario = Publicacion::find($id);
        $usuario->delete();
        if($request->ajax())
        {
            return response()->json(['success'=>TRUE,'id'=>$id]);
        }else{
            //flash("User $usuario->titulo eliminada correctamente",'success');
            return redirect()->route('admin.usuarios.index');
        }
    }
}
