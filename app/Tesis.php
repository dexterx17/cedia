<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tesis extends Model
{
    protected $table = "tesis";

    protected $fillable = [
        'titulo', 'resumen', 'modalidad', 'link', 'instituto', 'universidad_id', 'tema',
        'carrera','desde', 'hasta', 'tipo', 'director_id', 'codirector_id', 'estado'
    ];

    public function scopeTesistas($query){
        $tesis_id = $this->id;

        $investigadores =  User::whereIn('id',function($sq) use($tesis_id){
            $sq->select('referencia_id');
            $sq->from('tesistas');
            $sq->where('tesis_id','=',$tesis_id);
            $sq->where('referencia_table','=','users');
        })->get();      

        $ayudantes =  Ayudante::whereIn('id',function($sq) use($tesis_id){
            $sq->select('referencia_id');
            $sq->from('tesistas');
            $sq->where('tesis_id','=',$tesis_id);
            $sq->where('referencia_table','=','ayudantes');
        })->get();

        //$resultado = collect_merge($investigadores,$ayudantes);

        return $investigadores;
    }

    public function director(){
        return $this->belongsTo('App\User');
    }

    public function codirector(){
        return $this->belongsTo('App\User');
    }
}
