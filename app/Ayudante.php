<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ayudante extends Model
{
    protected $table = "ayudantes";
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres','apellidos', 'telefono','scholar_id','email', 'orden','imagen','skype','visible',
        'facebook','twitter','about_me', 'linkedin','universidad_id','tesista','tesista_desde','tesista_hasta',
        'pasante', 'pasante_desde', 'pasante_hasta', 'area_investigacion','carrera'
    ];


    public function universidad(){
        return $this->belongsTo('App\Universidad');
    }
}
