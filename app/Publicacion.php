<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Publicacion extends Model
{
	use SoftDeletes;
	
    protected $table = "publicaciones";

    protected $fillable = [
    	'scholar_id', 'titulo', 'autores','url', 'descripcion', 'fecha_publicacion','abstract',
    	'revista', 'conferencia', 'paginas', 'editorial', 'citas', 'citas_url','tipo'
    ];

	protected $dates = ['deleted_at'];

    public function users(){
    	return $this->belongsToMany('App\User');
    }

    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }
    
}
