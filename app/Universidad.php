<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Universidad extends Model
{
	use SoftDeletes;

    protected $table  = "universidades";

    protected $fillable = [
    	'nombre', 'abreviacion', 'direccion', 'telefono', 'logo', 'url', 'visible'
    ];

    protected $dates = ['deleted_at'];

    public function usuarios(){
    	return $this->belongsToMany('App\User');
    }

    public function proyectos(){
    	return $this->belongsToMany('App\Proyecto');
    }
}
