<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interes extends Model
{
    protected $table = "intereses";

    protected $fillable = ['interes'];

    public function users(){
    	return $this->belongsToMany('App\User')->withTimestamps();
    }
}
