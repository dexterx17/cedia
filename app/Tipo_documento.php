<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_documento extends Model
{
    protected $table = "documento_tipo";

    protected $fillable = [
    	'tipo', 'descripcion','extra'
    ];

    public function documentos(){
    	return $this->hasMany('App\Documento','tipo_id','id');
    }

    public function scopeDocumentosByProyecto($query, $proyecto_id){
    	$tipo_id = $this->id;
    	$docs = Documento::where('tipo_id','=',$tipo_id)
        	->whereIn('id',function($sq) use ($proyecto_id) {
                $sq->select('documento_id');
        		$sq->from('documento_referencias');
                $sq->where('referencia_id','=', $proyecto_id);
        		$sq->where('referencia_table','=',"proyectos");
        	});

        return $docs;
    }

}
