<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres','apellidos', 'titulo', 'telefono','scholar_id','email', 'password', 'type', 'rol','orden','cv','imagen','skype','visible',
        'facebook','twitter','about_me', 'linkedin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at'];

    public function publicaciones(){
        return $this->belongsToMany('App\Publicacion');
    }

    public function universidades(){
        return $this->belongsToMany('App\Universidad');
    }

    public function proyectos(){
        return $this->belongsToMany('App\Proyecto');
    }

    public function intereses(){
        return $this->belongsToMany('App\Interes');
    }

    public function scopeUniversidad($query){
        $user_id = $this->id;
        return $this->universidades()->where('universidades.id',function($sq) use($user_id) {
            $sq->select('universidad_id');
            $sq->from('universidad_user');
            $sq->where('user_id',$user_id);
        })->first();
    }

    public function investigaciones(){
        return $this->hasMany('App\Investigacion');
    }

    public function reconocimientos(){
        return $this->hasMany('App\Reconocimiento');
    }

    public function trabajos(){
        return $this->hasMany('App\Trabajo');
    }

    
    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }

}
