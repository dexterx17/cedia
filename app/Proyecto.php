<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyectos';

    protected $fillable = [
    	'nombre','descripcion','objetivo_general','objetivos_especificos',
    	'fecha_inicio','fecha_final','resumen','financiamiento', 'presupuesto'
    ];

    public function usuarios(){
    	return $this->belongsToMany('App\User');
    }

    public function universidades(){
        return $this->belongsToMany('App\Universidad');
    }
    
    /**
     * Devuelve las imagenes de un canton
     * @param  [type] $query [description]
     * @return App/Imagen       Coleccion de Objetos tipo Imagen
     */
    public function scopeImagenes($query){
        return Imagen::where('tabla_referencia',$this->table)
                ->where('id_referencia',$this->id);
    }
/*
    public function scopeDocumentosByTipo(){
        $tipos = Tipo_documento::whereIn('id', function($sq){
            
        });
    }*/
}
