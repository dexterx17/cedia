<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ponencia extends Model
{
    protected $table = "ponencias";

    protected $fillable = [
        'tema', 'evento', 'lugar', 'participantes', 'cargo', 'fecha',
        'resumen', 'lat', 'lng'
    ];

}
