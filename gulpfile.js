var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function(mix){
//	mix.copy('resources/assets/vendors/fullcalendar/dist/fullcalendar.min.css',
//		'public/css/libs');

	mix.copy('node_modules/chosen-js/chosen.min.css',
		'public/css/libs'); 

	mix.copy('node_modules/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.css',
		'public/css/libs');
	mix.styles([
	  //  '../vendor/bootstrap/css/bootstrap.min.css',
	  //  '../vendor/metisMenu/metisMenu.min.css',
	  //  '../dist/css/sb-admin-2.css',
	  //  '../vendor/font-awesome/css/font-awesome.min.css',
	//	'sb-admin-2.css',
		//'custom.css'
	],'public/css/admin.css');
});

elixir(function(mix){

	mix.styles([
		"basic/bootstrap.min.css",
		"basic/font-awesome.min.css",
		"basic/simple-line-icons.css",
		"basic/owl.carousel.css",
		"basic/owl.theme.css",
		"basic/animate.css",
		"basic/main.css",
		"basic/responsive.css",
		"basic/custom.css"
	],'public/css/front.css');
});

elixir(function(mix){

	mix.copy('node_modules/chosen-js/chosen.jquery.min.js',
		'public/js/libs');

	mix.copy('node_modules/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.js',
		'public/js/libs');

	mix.copy('node_modules/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.min.js',
		'public/js/libs');

	mix.copy('resources/assets/js/lib/typeahead.js/typeahead.bundle.min.js',
		'public/js/libs');
	mix.copy('resources/assets/js/lib/typeahead.js/bloodhound.js',
		'public/js/libs');

	mix.scripts([
		"sb-admin-2.js",
	],'public/js/admin.js');
});

elixir(function(mix){
	mix.scripts([
		"basic/jquery-min.js",
	    "basic/tether.min.js",
	    "basic/bootstrap.min.js",
	    "basic/mixitup.min.js",
	    "basic/owl.carousel.min.js",
	    "basic/jquery.nav.js",
	    "basic/smooth-scroll.js",
	    "basic/smooth-on-scroll.js",
	    "basic/wow.js",
	    "basic/jquery.counterup.min.js",
	    "basic/waypoints.min.js",
	    "basic/form-validator.min.js",
	    "basic/contact-form-script.js",
	    "basic/main.js",
	],'public/js/front.js');
});

